--Single Destruction
function c150000490.initial_effect(c)
    --activate
    local e1=Effect.CreateEffect(c)
    e1:SetCategory(CATEGORY_DESTROY)
    e1:SetType(EFFECT_TYPE_ACTIVATE)
    e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
    e1:SetCode(EVENT_FREE_CHAIN)
    e1:SetCondition(c150000490.condition)
    e1:SetTarget(c150000490.target)
    e1:SetOperation(c150000490.activate)
    c:RegisterEffect(e1)
	--act in hand
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_QP_ACT_IN_NTPHAND)
	e2:SetCondition(c150000490.handcon)
	c:RegisterEffect(e2)
end
function c150000490.handcon(e)
	return tp~=Duel.GetTurnPlayer()
end
function c150000490.condition(e,tp,eg,ep,ev,re,r,rp)
    return Duel.GetFieldGroupCount(tp,0,LOCATION_MZONE)==1
end
function c150000490.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
    if chkc then return Duel.GetFieldGroup(tp,0,LOCATION_MZONE):GetFirst()==chkc
        and Duel.GetFieldGroupCount(tp,0,LOCATION_MZONE)==1 end
    local tc=Duel.GetFieldGroup(tp,0,LOCATION_MZONE):GetFirst()
    if chk==0 then return tc:IsCanBeEffectTarget(e) end
    Duel.SetTargetCard(tc)
    Duel.SetOperationInfo(0,CATEGORY_DESTROY,tc,1,0,0)
end
function c150000490.activate(e,tp,eg,ep,ev,re,r,rp)
    local tc=Duel.GetFirstTarget()
    if tc and tc:IsRelateToEffect(e) then
        Duel.Destroy(tc,REASON_EFFECT)
    end
end
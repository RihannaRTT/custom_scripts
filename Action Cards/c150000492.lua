--Action Crush
function c150000492.initial_effect(c)
    --Activate
    local e1=Effect.CreateEffect(c)
    e1:SetCategory(CATEGORY_DESTROY)
    e1:SetType(EFFECT_TYPE_ACTIVATE)
    e1:SetCode(EVENT_FREE_CHAIN)
    e1:SetTarget(c150000492.target)
    e1:SetOperation(c150000492.activate)
    c:RegisterEffect(e1)
	--act in hand
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_QP_ACT_IN_NTPHAND)
	e2:SetCondition(c150000492.handcon)
	c:RegisterEffect(e2)
end
function c150000492.handcon(e)
	return tp~=Duel.GetTurnPlayer()
end
function c150000492.filter(c)
    return c:IsSetCard(0xac1) and not c:IsType(TYPE_FIELD)
end
function c150000492.target(e,tp,eg,ep,ev,re,r,rp,chk)
    if chk==0 then return Duel.IsExistingMatchingCard(c150000492.filter,tp,0,LOCATION_HAND,1,nil) end
    local g=Duel.GetMatchingGroup(c150000492.filter,tp,0,LOCATION_HAND,nil)
    Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,g:GetCount(),0,0)
end
function c150000492.activate(e,tp,eg,ep,ev,re,r,rp)
    local g=Duel.GetMatchingGroup(c150000492.filter,tp,0,LOCATION_HAND,nil)
    Duel.Destroy(g,REASON_EFFECT)
    local c=e:GetHandler()
    local e3=Effect.CreateEffect(c)
    e3:SetDescription(aux.Stringid(150000492,0))
    e3:SetCategory(CATEGORY_DESTROY)
    e2:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
    e3:SetCode(EVENT_TO_HAND)
    e3:SetCondition(c150000492.descon)
    e3:SetOperation(c150000492.desop)
    e3:SetReset(RESET_PHASE+PHASE_END)
    Duel.RegisterEffect(e1,tp)
end
function c150000492.descon(e,tp,eg,ep,ev,re,r,rp)
    return eg:IsExists(c150000492.filter,1,nil)
end
function c150000492.desop(e,tp,eg,ep,ev,re,r,rp)
    local dg=eg:Filter(c150000492.filter,nil)
    if dg:GetCount()>0 then
        Duel.Destroy(dg,REASON_EFFECT)
    end
end
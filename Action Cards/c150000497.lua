--Action Trap - Break Action
function c150000497.initial_effect(c)
    --Activate
    local e1=Effect.CreateEffect(c)
    e1:SetCategory(CATEGORY_DESTROY)
    e1:SetType(EFFECT_TYPE_ACTIVATE)
    e1:SetCode(EVENT_FREE_CHAIN)
    e1:SetTarget(c150000497.target)
    e1:SetOperation(c150000497.activate)
    c:RegisterEffect(e1)
end
function c150000497.filter(c)
    return c:IsSetCard(0xac1) and not c:IsType(TYPE_FIELD)
end
function c150000497.target(e,tp,eg,ep,ev,re,r,rp,chk)
   local g=Duel.GetMatchingGroup(c150000497.filter,tp,LOCATION_HAND+LOCATION_ONFIELD,LOCATION_HAND+LOCATION_ONFIELD,nil)
	if chk==0 then return g:GetCount()>0 end
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,1,PLAYER_ALL,LOCATION_HAND+LOCATION_ONFIELD)
end
function c150000497.activate(e,tp,eg,ep,ev,re,r,rp)
    Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DESTROY)
	local g=Duel.SelectMatchingCard(tp,c150000497.filter,tp,LOCATION_HAND+LOCATION_ONFIELD,LOCATION_HAND+LOCATION_ONFIELD,1,1,nil)
	if g:GetCount()>0 then
		Duel.HintSelection(g)
		Duel.Destroy(g,REASON_EFFECT)
	end
end
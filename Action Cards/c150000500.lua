--Action Card - Quick Guard
function c150000500.initial_effect(c)
    --Activate
    local e1=Effect.CreateEffect(c)
    e1:SetCategory(CATEGORY_POSITION)
    e1:SetType(EFFECT_TYPE_ACTIVATE)
    e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
    e1:SetCode(EVENT_FREE_CHAIN)
    e1:SetTarget(c150000500.target)
    e1:SetOperation(c150000500.activate)
    c:RegisterEffect(e1)
    --act in hand
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_QP_ACT_IN_NTPHAND)
	e2:SetCondition(c150000500.handcon)
	c:RegisterEffect(e2)
end
function c150000500.handcon(e)
	return tp~=Duel.GetTurnPlayer()
end
function c150000500.filter(c)
    return c:IsPosition(POS_FACEUP_ATTACK) and c:IsCanChangePosition()
end
function c150000500.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
    if chkc then return chkc:IsLocation(LOCATION_MZONE) and c150000500.filter(chkc) end
    if chk==0 then return Duel.IsExistingTarget(c150000500.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,nil) end
    Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FACEUP)
    local g=Duel.SelectTarget(tp,c150000500.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,1,nil)
    Duel.SetOperationInfo(0,CATEGORY_POSITION,g,1,0,0)
end
function c150000500.activate(e,tp,eg,ep,ev,re,r,rp)
    local tc=Duel.GetFirstTarget()
    if tc:IsRelateToEffect(e) and tc:IsPosition(POS_FACEUP_ATTACK) then
        Duel.ChangePosition(tc,POS_FACEUP_DEFENSE)
    end
end
--Action Card - Quiz - 1000 of Riddle
function c150000880.initial_effect(c)
	--Activate/Answer
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(c150000880.target)
	e1:SetOperation(c150000880.activate)
	c:RegisterEffect(e1)
end
function c150000880.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	local op=Duel.SelectOption(tp,aux.Stringid(4018,6),aux.Stringid(4018,7))
	e:SetLabel(op)
end
function c150000880.activate(e,tp,eg,ep,ev,re,r,rp)
	if e:GetLabel()==0 then
	Duel.Damage(tp,1000,REASON_EFFECT)
	elseif e:GetLabel()==1 then
	Duel.Recover(tp,1000,REASON_EFFECT)
	end
end

--Action Card - Quiz - 300 of Riddle
function c150000900.initial_effect(c)
	--Activate/Answer
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(c150000900.target)
	e1:SetOperation(c150000900.activate)
	c:RegisterEffect(e1)
end
function c150000900.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	local op=Duel.SelectOption(tp,aux.Stringid(4018,12),aux.Stringid(4018,13),aux.Stringid(4018,14),aux.Stringid(4018,15))
	e:SetLabel(op)
end
function c150000900.activate(e,tp,eg,ep,ev,re,r,rp)
	if e:GetLabel()==0 then
	Duel.Damage(tp,300,REASON_EFFECT)
	elseif e:GetLabel()==1 then
	Duel.Damage(tp,300,REASON_EFFECT)
	elseif e:GetLabel()==2 then
	Duel.Recover(tp,300,REASON_EFFECT)
	elseif e:GetLabel()==3 then
	Duel.Damage(tp,300,REASON_EFFECT)
	end
end

--Action Card - Nightmare Prey
function c150001250.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DESTROY)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_BATTLE_DESTROYED)
	e1:SetCondition(c150001250.condition)
	e1:SetTarget(c150001250.target)
	e1:SetOperation(c150001250.activate)
	c:RegisterEffect(e1)
	--act in hand
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_QP_ACT_IN_NTPHAND)
	e2:SetCondition(c150001250.handcon)
	c:RegisterEffect(e2)
end
function c150001250.handcon(e)
	return tp~=Duel.GetTurnPlayer()
end
function c150001250.condition(e,tp,eg,ep,ev,re,r,rp,chk)
    local tc=eg:GetFirst()
    return tc:IsPreviousControler(tp) and tc:IsReason(REASON_BATTLE)
end
function c150001250.target(e,tp,eg,ep,ev,re,r,rp,chk)
    local tg=eg:GetFirst():GetReasonCard()
    if chk==0 then return tg:IsOnField() and tg:IsDestructable() end
    Duel.SetTargetCard(tg)
    Duel.SetOperationInfo(0,CATEGORY_DESTROY,tg,1,0,0)
end
function c150001250.activate(e,tp,eg,ep,ev,re,r,rp)
    local tg=eg:GetFirst():GetReasonCard()
    if tg:IsRelateToEffect(e) then
        Duel.Destroy(tg,REASON_EFFECT)
    end
end


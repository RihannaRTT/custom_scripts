--Action Card - Illusion Dance
function c150001300.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_POSITION)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(c150001300.target)
	e1:SetOperation(c150001300.activate)
	c:RegisterEffect(e1)
	--act in hand
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_QP_ACT_IN_NTPHAND)
	e2:SetCondition(c150001300.handcon)
	c:RegisterEffect(e2)
end
function c150001300.handcon(e)
	return tp~=Duel.GetTurnPlayer()
end
function c150001300.filter(c)
    return c:IsPosition(POS_FACEUP_ATTACK) and c:IsCanChangePosition()
end
function c150001300.target(e,tp,eg,ep,ev,re,r,rp,chk)
 if chk==0 then return Duel.IsExistingMatchingCard(c150001300.filter,tp,LOCATION_MZONE,LOCATION_MZONE,1,nil) end
    local g=Duel.GetMatchingGroup(c150001300.filter,tp,LOCATION_MZONE,LOCATION_MZONE,nil)
	Duel.SetOperationInfo(0,CATEGORY_POSITION,g,g:GetCount(),0,0)
end
function c150001300.activate(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local g=Duel.GetMatchingGroup(c150001300.filter,tp,LOCATION_MZONE,LOCATION_MZONE,nil)
	if g:GetCount()>0 then
		local tc=g:GetFirst()
		while tc do
			if Duel.ChangePosition(tc,POS_FACEUP_DEFENSE)~=0 then
			tc:RegisterFlagEffect(150001300,RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END,0,1)
			end
			tc=g:GetNext()
		end
			local e1=Effect.CreateEffect(e:GetHandler())
      		e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
     	 	e1:SetCode(EVENT_PHASE+PHASE_END)
     		e1:SetCountLimit(1)
     	 	e1:SetReset(RESET_PHASE+PHASE_END)
        	e1:SetCondition(c150001300.poscon)
       		e1:SetOperation(c150001300.posop)
       		e1:SetLabelObject(tc)
       		Duel.RegisterEffect(e1,tp)
	end
end
function c150001300.filter2(c)
    return c:IsPosition(POS_FACEUP_DEFENSE) and c:IsCanChangePosition() and c:GetFlagEffect(150001300)~=0
end

function c150001300.poscon(e,tp,eg,ep,ev,re,r,rp)
    return Duel.IsExistingMatchingCard(c150001300.filter2,tp,LOCATION_MZONE,LOCATION_MZONE,1,nil) 
end
function c150001300.posop(e,tp,eg,ep,ev,re,r,rp)
    local g=Duel.GetMatchingGroup(c150001300.filter2,tp,LOCATION_MZONE,LOCATION_MZONE,nil)
	if g:GetCount()>0 then
    			Duel.ChangePosition(g,POS_FACEUP_ATTACK)
	end
end

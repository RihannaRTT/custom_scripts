--Action Card - Guard Cover
function c150001540.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetOperation(c150001540.activate)
	c:RegisterEffect(e1)
	--act in hand
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_QP_ACT_IN_NTPHAND)
	e2:SetCondition(c150001540.handcon)
	c:RegisterEffect(e2)
end
function c150001540.handcon(e)
	return tp~=Duel.GetTurnPlayer()
end
function c150001540.activate(e,tp,eg,ep,ev,re,r,rp)
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_QUICK_O)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_DAMAGE_CAL)
	e1:SetCode(EVENT_CHAINING)
	e1:SetCondition(c150001540.efcon)
	e1:SetTarget(c150001540.eftg)
	e1:SetOperation(c150001540.efop)
	e1:SetReset(RESET_PHASE+PHASE_END)
	Duel.RegisterEffect(e1,tp)
end

function c150001540.tfilter(c,tp)
	return c:IsLocation(LOCATION_MZONE) and c:IsType(TYPE_MONSTER) and c:IsControler(tp)
end
function c150001540.efcon(e,tp,eg,ep,ev,re,r,rp)
    if not re:IsHasProperty(EFFECT_FLAG_CARD_TARGET) then return false end
	local tg=Duel.GetChainInfo(ev,CHAININFO_TARGET_CARDS)
	return tg and tg:GetCount()==1 and tg:IsExists(c150001540.tfilter,1,nil,tp)
end
function c150001540.filter(c,ct)
    return Duel.CheckChainTarget(ct,c)
end
function c150001540.eftg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
    if chkc then return false end
    if chk==0 then return Duel.IsExistingTarget(c150001540.filter,tp,LOCATION_MZONE,0,1,nil,ev) end
    Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET)
    Duel.SelectTarget(tp,c150001540.filter,tp,LOCATION_MZONE,0,1,1,nil,ev)
end
function c150001540.efop(e,tp,eg,ep,ev,re,r,rp)
    local tc=Duel.GetFirstTarget()
    if tc:IsRelateToEffect(e) and Duel.CheckChainTarget(ev,tc) then
        local g=Group.FromCards(tc)
        Duel.ChangeTargetCard(ev,g)
    end
end
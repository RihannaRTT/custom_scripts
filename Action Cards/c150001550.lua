--Action Card - Sound Rebound
function c150001550.initial_effect(c)
	--Activation
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetOperation(c150001550.activate)
	c:RegisterEffect(e1)
	--act in hand
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_QP_ACT_IN_NTPHAND)
	e2:SetCondition(c150001550.handcon)
	c:RegisterEffect(e2)
end
function c150001550.handcon(e)
	return tp~=Duel.GetTurnPlayer()
end
function c150001550.activate(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_CHAIN_SOLVING)
	e1:SetCountLimit(1)
	if Duel.SelectOption(tp,aux.Stringid(150001550,0),aux.Stringid(150001550,1))==0 then
		e1:SetCondition(c150001550.negcon1)
	else
		e1:SetCondition(c150001550.negcon2)
	end
	e1:SetOperation(c150001550.negop)
	e1:SetReset(RESET_PHASE+PHASE_END)
	Duel.RegisterEffect(e1,tp)
end
function c150001550.negcon1(e,tp,eg,ep,ev,re,r,rp)
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_NEGATE)
	return ex and tg~=nil and tc+tg:FilterCount(Card.IsType,nil,TYPE_SPELL)-tg:GetCount()>0 and Duel.IsChainDisablable(ev)
end
function c150001550.negcon2(e,tp,eg,ep,ev,re,r,rp)
	local ex,tg,tc=Duel.GetOperationInfo(ev,CATEGORY_DESTROY)
	return ex and tg~=nil and tc+tg:FilterCount(Card.IsType,nil,TYPE_SPELL)-tg:GetCount()>0 and Duel.IsChainDisablable(ev)
end
function c150001550.negop(e,tp,eg,ep,ev,re,r,rp)
	Duel.NegateEffect(ev)
end
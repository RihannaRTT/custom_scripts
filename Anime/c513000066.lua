--coded by Lyris
--Seal of Orichalcos
--not fully implemented
local s,id,o=GetID()
function s.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetProperty(EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_CANNOT_NEGATE)
	c:RegisterEffect(e1)
	--cannot negate
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_CANNOT_DISABLE)
	e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	c:RegisterEffect(e2)
	--cannot destroy
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e3:SetValue(1)
	c:RegisterEffect(e3)
	--cannot leave field
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_FIELD)
	e5:SetRange(LOCATION_FZONE)
	e5:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e5:SetTargetRange(1,1)
	e5:SetTarget(s.imtg)
	local e5d=e5:Clone()
	e5d:SetCode(EFFECT_CANNOT_TO_DECK)
	c:RegisterEffect(e5d)
	local e5h=e5:Clone()
	e5h:SetCode(EFFECT_CANNOT_TO_HAND)
	c:RegisterEffect(e5h)
	local e5g=e5:Clone()
	e5g:SetCode(EFFECT_CANNOT_TO_GRAVE)
	c:RegisterEffect(e5g)
	local e5r=e5:Clone()
	e5r:SetCode(EFFECT_CANNOT_REMOVE)
	c:RegisterEffect(e5r)
	local e5f=e5:Clone(c)
	e5f:SetCode(EFFECT_CANNOT_ACTIVATE)
	e5f:SetValue(s.ffilter)
	c:RegisterEffect(e5f)
	local e5s=e5:Clone()
	e5s:SetCode(EFFECT_CANNOT_SSET)
	e5s:SetTargetRange(1,0)
	e5s:SetTarget(aux.TargetBoolFunction(Card.IsType,TYPE_FIELD))
	c:RegisterEffect(e5s)
	--cannot overlay (?)
	local f=Card.IsCanOverlay
	Card.IsCanOverlay=function(tc,rp)
		return tc~=c and f(tc,rp)
	end
	--atk up
	local e6=Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_FIELD)
	e6:SetCode(EFFECT_UPDATE_ATTACK)
	e6:SetRange(LOCATION_FZONE)
	e6:SetTargetRange(LOCATION_MZONE,0)
	e6:SetValue(500)
	c:RegisterEffect(e6)
	--szone monsters
	local e7=Effect.CreateEffect(c)
	e7:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e7:SetCode(EVENT_FREE_CHAIN)
	e7:SetRange(LOCATION_FZONE)
	e7:SetCondition(s.mvcon)
	e7:SetOperation(s.mvop)
	c:RegisterEffect(e7)
	--negate
	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_QUICK_O)
	e8:SetCode(EVENT_CHAINING)
	e8:SetRange(LOCATION_HAND)
	e8:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_CANNOT_INACTIVATE+EFFECT_FLAG_CANNOT_NEGATE)
	e8:SetCondition(s.negcon)
	e8:SetCost(s.negcost)
	e8:SetTarget(s.negtg)
	e8:SetOperation(s.negeff)
	c:RegisterEffect(e8)
	--steal soul
	local e9=Effect.CreateEffect(c)
	e9:SetType(EFFECT_TYPE_FIELD)
	e9:SetCode(EFFECT_MATCH_KILL)
	e9:SetRange(LOCATION_FZONE)
	e9:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e9:SetTargetRange(1,1)
	c:RegisterEffect(e9)
end
function s.imtg(e,c)
	return c==e:GetHandler()
end
function s.ffilter(e,re,tp)
	local rc=re:GetHandler()
	return rc:IsType(TYPE_FIELD) and re:IsHasType(EFFECT_TYPE_ACTIVATE) and not rc:IsCode(110000100,110000101)
		and (Duel.GetMasterRule()<3 or rc:IsControler(tp))
end
function s.filter(c,tp)
	local dst=LOCATION_ONFIELD~c:GetLocation()
	return s.mvfilter(c,tp) and Duel.GetLocationCount(tp,dst)
		+Duel.GetMatchingGroupCount(s.mvfilter,tp,dst,0,nil,tp,c)>0
end
function s.mvfilter(c,tp,tc)
	local loc=c:GetLocation()
	if tc and tc:GetSequence()>4 and Duel.GetLocationCount(tp,tc:GetLocation())<=0 then return false end
	return (c:GetOriginalType()&TYPE_MONSTER>0 or c:IsType(TYPE_MONSTER)) and c:GetFlagEffect(id)==0
end
function s.mvcon(e,tp)
	local ph=Duel.GetCurrentPhase()
	return Duel.GetTurnPlayer()==tp and ph>=PHASE_MAIN1 and ph<=PHASE_MAIN2 and Duel.GetCurrentChain()==0
		and Duel.IsExistingMatchingCard(s.filter,tp,LOCATION_ONFIELD,0,1,nil,tp)
		and Duel.GetLocationCount(tp,LOCATION_MZONE)+Duel.GetLocationCount(tp,LOCATION_SZONE)>0
end
function s.reg(e,tc)
	if not tc:IsOnField() then return end
	local c=e:GetHandler()
	if tc:IsLocation(LOCATION_SZONE) then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_CHANGE_TYPE)
		e1:SetReset(RESET_EVENT+RESETS_STANDARD-RESET_TURN_SET)
		e1:SetValue(TYPE_MONSTER+TYPE_SPELL+TYPE_CONTINUOUS)
		tc:RegisterEffect(e1,true)
	end
	tc:RegisterFlagEffect(id,RESET_EVENT+RESETS_STANDARD-0x1820000+RESET_PHASE+PHASE_END,0,1)
end
function s.mvop(e,tp)
	local c=e:GetHandler()
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_OPERATECARD)
	local g=Duel.GetMatchingGroup(s.filter,tp,LOCATION_ONFIELD,0,nil,tp):SelectSubGroup(tp,aux.TRUE,true,1,1)
	if not g then return end
	Duel.Hint(HINT_CARD,0,id)
	Duel.HintSelection(g)
	local tc=g:GetFirst()
	local loc=tc:GetLocation()
	local dest=LOCATION_ONFIELD~loc
	local ftd,zs=Duel.GetLocationCount(tp,dest)
	local za=zs
	local ftl,zb=Duel.GetLocationCount(tp,loc)
	if ftl>0 then for sc in aux.Next(Duel.GetMatchingGroup(s.mvfilter,tp,dest,0,nil,tp,tc)) do
		zs=zs&~2^sc:GetSequence()
	end end
	zs=(zs&0x7f)|0xe0
	local zc=math.log((~zs)&0x1f,2)
	if zc//1~=zc then
		if dest==LOCATION_SZONE then zs=zs<<8 end
		z=Duel.SelectField(tp,1,dest,0,zs,571)
		if dest==LOCATION_SZONE then z=z>>8 end
	else z=(~zs)&0x1f end
	local zc=Duel.GetFieldCard(tp,dest,math.log(z&0x1f,2))
	if zc then
		local tg=g+zc
		if ftl>0 then
			local zi=0x1f
			if ftl<2 then zi=~zb&0x1f end
			Duel.MoveToField(zc,tp,tp,loc,zc:GetPosition(),true,zi)
			if z&0x60>0 then z=0x1f end
			if ftd<1 then z=(~za~2^zc:GetPreviousSequence())&0x1f end
			Duel.MoveToField(tc,tp,tp,dest,tc:GetPosition(),true,z)
		end
		s.reg(e,zc)
	else Duel.MoveToField(tc,tp,tp,dest,tc:GetPosition(),true,z) end
	s.reg(e,tc)
end
function s.negcon(e,tp,eg,ep,ev,re,r,rp)
	return not re:GetHandler():IsCode(id,110000100,110000101) and Duel.IsChainDisablable(ev)
end
function s.negcost(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	if chk==0 then return c:IsAbleToGraveAsCost() end
	Duel.SendtoGrave(c,REASON_COST)
end
function s.negtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_NEGATE,eg,1,0,0)
	if re:GetHandler():IsRelateToEffect(re) then Duel.SetOperationInfo(0,CATEGORY_DESTROY,eg,1,0,0) end
end
function s.negeff(e,tp,eg,ep,ev,re,r,rp)
	if Duel.NegateActivation(ev) and re:GetHandler():IsRelateToEffect(re) then Duel.Destroy(eg,REASON_EFFECT) end
end

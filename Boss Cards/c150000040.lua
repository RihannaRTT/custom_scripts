--Infinity
function c150000040.initial_effect(c)
    --Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(c150000040.condition)
	e1:SetTarget(c150000040.target)
	e1:SetOperation(c150000040.operation)
	c:RegisterEffect(e1)
end
c150000040.mark=0
function c150000040.condition(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():GetFlagEffect(150000050)>0
end
function c150000040.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetChainLimit(c150000040.climit)
end
function c150000040.climit(e,lp,tp)
	return lp==tp or not e:IsHasType(EFFECT_TYPE_ACTIVATE)
end
function c150000040.filter(c,code)
    return c:GetCode()==code and c:IsFacedown() 
end
function c150000040.operation(e,tp,eg,ep,ev,re,r,rp)
	local g1=Duel.GetFirstMatchingCard(c150000040.filter,tp,LOCATION_SZONE,0,nil,150000060)
	local g2=Duel.GetFirstMatchingCard(c150000040.filter,tp,LOCATION_SZONE,0,nil,150000070)
	local g3=Duel.GetFirstMatchingCard(c150000040.filter,tp,LOCATION_SZONE,0,nil,150000080)
	if g1 then 
	Duel.ChangePosition(g1,POS_FACEUP)
	g1:SetStatus(STATUS_EFFECT_ENABLED,true)
	g1:SetStatus(STATUS_CHAINING,true)
	end
	if g2 then 
	Duel.ChangePosition(g2,POS_FACEUP)
	g2:SetStatus(STATUS_EFFECT_ENABLED,true)
	g2:SetStatus(STATUS_CHAINING,true)
	end
	if g3 then 
	Duel.ChangePosition(g3,POS_FACEUP)
	g3:SetStatus(STATUS_EFFECT_ENABLED,true)
	g3:SetStatus(STATUS_CHAINING,true)
	end
end

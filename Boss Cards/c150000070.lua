--Darkness 2
function c150000070.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_ATKCHANGE)
	e1:SetProperty(EFFECT_FLAG_DELAY)
	e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e1:SetCode(EVENT_CHAIN_SOLVED)
	e1:SetRange(LOCATION_SZONE)
	e1:SetCountLimit(1)
	e1:SetCondition(c150000070.condition)
	e1:SetTarget(c150000070.target)
	e1:SetOperation(c150000070.operation)
	c:RegisterEffect(e1)
	--
	local e2=e1:Clone()
	e2:SetProperty(EFFECT_FLAG_DELAY)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_CHAIN_SOLVED)
	e2:SetCondition(c150000070.condition2)
	c:RegisterEffect(e2)
end
c150000070.mark=2
function c150000070.filter(c)
	return c:IsCode(150000040) and c:IsFaceup() and not c:IsStatus(STATUS_DISABLED)
end
function c150000070.condition(e,tp,eg,ep,ev,re,r,rp)
	local re=re:GetHandler()
	return re:IsCode(150000050) and not re:IsStatus(STATUS_DISABLED)
	and Duel.IsExistingMatchingCard(c150000070.filter,tp,LOCATION_SZONE,0,1,nil)
end
function c150000070.condition2(e,tp,eg,ep,ev,re,r,rp)
	local re=re:GetHandler()
	return re:IsSetCard(0x4602) and re:IsType(2004) and e:GetHandler():GetFlagEffect(150000070)>0
end
function c150000070.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(Card.IsFaceup,tp,LOCATION_MZONE,0,1,nil) end 
	local g=Duel.GetMatchingGroup(Card.IsFaceup,tp,LOCATION_MZONE,0,nil)
	Duel.SetOperationInfo(0,CATEGORY_ATKCHANGE,g,1,0,0)
end
function c150000070.operation(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FACEUP)
	local tc=Duel.SelectMatchingCard(tp,Card.IsFaceup,tp,LOCATION_MZONE,0,1,1,nil)
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetValue(4000)
	e1:SetReset(RESET_EVENT+0x1fe0000)
	tc:GetFirst():RegisterEffect(e1)
	e:GetHandler():RegisterFlagEffect(150000070,RESET_EVENT+0x1fe0000,0,1)
end
function c150000070.con1(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():IsLocation(LOCATION_HAND) and Duel.GetTurnCount()==1
end
function c150000070.op1(e,tp,eg,ep,ev,re,r,rp)
	Duel.DisableShuffleCheck()
	Duel.SendtoDeck(e:GetHandler(),nil,2,REASON_RULE)
end

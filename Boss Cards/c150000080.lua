--Darkness 1
function c150000080.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_REMOVE)
	e1:SetProperty(EFFECT_FLAG_DELAY)
	e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e1:SetCode(EVENT_CHAIN_SOLVED)
	e1:SetRange(LOCATION_SZONE)
	e1:SetCountLimit(1)
	e1:SetCondition(c150000080.condition)
	e1:SetTarget(c150000080.target)
	e1:SetOperation(c150000080.operation)
	c:RegisterEffect(e1)
	--
	local e2=e1:Clone()
	e2:SetProperty(EFFECT_FLAG_DELAY)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_CHAIN_SOLVED)
	e2:SetCondition(c150000080.condition2)
	c:RegisterEffect(e2)
end
c150000080.mark=3
function c150000080.filter(c)
	return c:IsCode(150000040) and c:IsFaceup() and not c:IsStatus(STATUS_DISABLED)
end
function c150000080.condition(e,tp,eg,ep,ev,re,r,rp)
	local re=re:GetHandler()
	return re:IsCode(150000050) and not re:IsStatus(STATUS_DISABLED)
	and Duel.IsExistingMatchingCard(c150000080.filter,tp,LOCATION_SZONE,0,1,nil)
end
function c150000080.condition2(e,tp,eg,ep,ev,re,r,rp)
	local re=re:GetHandler()
	return re:IsSetCard(0x4602) and re:IsType(2004) and e:GetHandler():GetFlagEffect(150000080)>0
end
function c150000080.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end 
	Duel.SetTargetParam(2000)
	Duel.SetTargetPlayer(1-tp)
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,2000)
end
function c150000080.operation(e,tp,eg,ep,ev,re,r,rp)
	local d,p=Duel.GetChainInfo(0,CHAININFO_TARGET_PARAM,CHAININFO_TARGET_PLAYER)
	Duel.Damage(p,d,REASON_EFFECT)
	e:GetHandler():RegisterFlagEffect(150000080,RESET_EVENT+0x1fe0000,0,1)
end
function c150000080.con1(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():IsLocation(LOCATION_HAND) and Duel.GetTurnCount()==1
end
function c150000080.op1(e,tp,eg,ep,ev,re,r,rp)
	Duel.DisableShuffleCheck()
	Duel.SendtoDeck(e:GetHandler(),nil,2,REASON_RULE)
end

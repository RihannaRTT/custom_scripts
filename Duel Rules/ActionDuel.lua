--Action Duel Model
--Scripted by: XGlitchy30
--Modified by AntiMetaman, Lyris, Zarc

local tableAction = {
	[0]={
		511005049,
		511005593,
		150000440,
		150000450,
		150000460,
		150000470,
		150000480,
		150000490,
		150000500,
		150000510,
		150000650,
		150000660,
		150000670,
		150000680,
		150000690,
		150000710,
		150000720,
		150000950,
		150000960,
		150000970,
		150000980,
		150000990,
		150001000,
		150001110,
		150001120,
		150001130,
		150001140,
		150001150,
		150001170,
		150001180,
		150001190,
		150001210,
		150001220,
		150001230,
		150001240,
		150001250,
		150001260,
		150001270,
		150001290,
		150001300,
		150001310,
		150001320,
		150001340,
		150001350,
		150001360,
		150001370,
		150001380,
		150001390,
		150001410,
		150001430,
		150001440,
		150001450,
		150001480,
		150001500,
		150001540,
		150001550,
		150001560,
		150001570,
		150001690,
		150001700,
		150001710,
		150001730,
		150001740,
		150001760,
		150001770,
	},
	[150000850]={
		150000860,
		150000870,
		150000880,
		150000890,
		150000900,
		150000910,
		150000920,
		150000930,
	},
}
-- Add Action Card
local function f2(e,tp,eg,ep,ev,re,r,rp)
	local a1,a2=Duel.IsExistingMatchingCard(Card.IsSetCard,tp,LOCATION_HAND,0,1,nil,0xac1),Duel.IsExistingMatchingCard(Card.IsSetCard,1-tp,LOCATION_HAND,0,1,nil,0xac1)
	local fc1,fc2=Duel.GetFieldCard(tp,LOCATION_FZONE,0),Duel.GetFieldCard(1-tp,LOCATION_FZONE,0)
	if not (fc1 or fc2) or a1 and a2 then return end
	local seed=0
	if Duel.GetFieldGroupCount(tp,LOCATION_DECK,0)==0 then
		local g=Duel.GetMatchingGroup(nil,tp,LOCATION_GRAVE+LOCATION_REMOVED+LOCATION_HAND+LOCATION_SZONE,0,nil)
		seed = g:GetFirst():GetCode()
	else
		local g=Duel.GetDecktopGroup(tp,1)
		local tc=g:GetFirst()
		seed=tc:GetCode()
	end
	math.randomseed( seed )
	local t=nil
	local b1,b2=fc1 and tableAction[fc1:GetCode()],fc2 and tableAction[fc2:GetCode()]
	if b1 and b2 then
		local t1,t2=tableAction[fc1:GetCode()],tableAction[fc2:GetCode()]
		if t1 and t2 then t={table.unpack(t1),table.unpack(t2)}
		elseif t1 then t={table.unpack(t1),table.unpack(tableAction[0])}
		else t={table.unpack(tableAction[0]),table.unpack(t2)} end
	elseif b1 then t=tableAction[fc1:GetCode()]
	elseif b2 then t=tableAction[fc2:GetCode()]
	else t={table.unpack(tableAction[0])} end
	e:SetLabel(t[math.random(#t)])
	if not (a1 or a2) then
		local p=Duel.RockPaperScissors()
		if p>1 then return end
		local token=Duel.CreateToken(p,e:GetLabel())
		if token:IsType(TYPE_TRAP) then
			Duel.MoveToField(token,p,p,LOCATION_SZONE,POS_FACEDOWN,true)
			Duel.Activate(token:GetActivateEffect())
		else Duel.SendtoHand(token,nil,REASON_EFFECT) end
	elseif a2 then
		if Duel.TossCoin(tp,1)==0 then return end
		local token=Duel.CreateToken(tp,e:GetLabel())
		if token:IsType(TYPE_TRAP) then
			Duel.MoveToField(token,tp,tp,LOCATION_SZONE,POS_FACEDOWN,true)
			Duel.Activate(token:GetActivateEffect())
		else Duel.SendtoHand(token,nil,REASON_EFFECT) end
	else
		if Duel.TossCoin(1-tp,1)==0 then return end
		local token=Duel.CreateToken(1-tp,e:GetLabel())
		if token:IsType(TYPE_TRAP) then
			Duel.MoveToField(token,1-tp,1-tp,LOCATION_SZONE,POS_FACEDOWN,true)
			Duel.Activate(token:GetActivateEffect())
		else Duel.SendtoHand(token,nil,REASON_EFFECT) end
	end
end
local function f0(e,tp,eg,ep,ev,re,r,rp)
	for p=0,1 do
		Duel.Hint(HINT_SELECTMSG,p,HINTMSG_TOFIELD)
		local card=Duel.SelectMatchingCard(p,Card.IsType,p,LOCATION_DECK+LOCATION_HAND,0,1,1,nil,TYPE_FIELD):GetFirst()
		if card then
			local fc=Duel.GetFieldCard(p,LOCATION_FZONE,0)
			if fc then
				Duel.SendtoGrave(fc,REASON_RULE)
				Duel.BreakEffect()
			end
			Duel.MoveToField(card,p,p,LOCATION_FZONE,POS_FACEUP,true)
			if card:IsPreviousLocation(LOCATION_HAND) then Duel.Draw(p,1,REASON_RULE) end
		end
	end
	f2(e,0,eg,ep,ev,re,r,rp)
	f2(e,1,eg,ep,ev,re,r,rp)
end
local e4=Effect.GlobalEffect()
e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e4:SetCode(EVENT_PHASE_START+PHASE_DRAW)
e4:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
e4:SetCountLimit(1)
e4:SetOperation(f0)
Duel.RegisterEffect(e4,0)
--action card get
local p1=Effect.GlobalEffect()
p1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
p1:SetCode(EVENT_PHASE_START+PHASE_DRAW)
p1:SetCondition(function(e,tp) return Duel.GetTurnPlayer()==tp end)
p1:SetOperation(f2)
Duel.RegisterEffect(p1,0)
local p2=p1:Clone()
p2:SetCode(EVENT_PHASE_START+PHASE_STANDBY)
Duel.RegisterEffect(p2,0)
local p3=p1:Clone()
p3:SetCode(EVENT_PHASE_START+PHASE_MAIN1)
Duel.RegisterEffect(p3,0)
local p4=p1:Clone()
p4:SetCode(EVENT_PHASE_START+PHASE_BATTLE_START)
Duel.RegisterEffect(p4,0)
local p5=p1:Clone()
p5:SetCode(EVENT_PHASE_START+PHASE_MAIN2)
Duel.RegisterEffect(p5,0)
local p6=p1:Clone()
p6:SetCode(EVENT_PHASE_START+PHASE_END)
Duel.RegisterEffect(p6,0)
local o1=p1:Clone()
Duel.RegisterEffect(o1,1)
local o2=p2:Clone()
Duel.RegisterEffect(o2,1)
local o3=p3:Clone()
Duel.RegisterEffect(o3,1)
local o4=p4:Clone()
Duel.RegisterEffect(o4,1)
local o5=p5:Clone()
Duel.RegisterEffect(o5,1)
local o6=p6:Clone()
Duel.RegisterEffect(o6,1)
local p0=Effect.GlobalEffect()
p0:SetType(EFFECT_TYPE_FIELD)
p0:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
p0:SetCode(EFFECT_CANNOT_SSET)
p0:SetTarget(aux.TargetBoolFunction(Card.IsSetCard,0xac1))
p0:SetTargetRange(1,1)
Duel.RegisterEffect(p0,0)

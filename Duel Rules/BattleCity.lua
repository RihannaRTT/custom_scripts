--Battle City
--Scripted by Edo9300
--Updated by Larry126 & Lyris

local function fnegop(e,tp,eg,ep,ev,re,r,rp)
	if re:GetHandler():GetFlagEffect(511000173)>0 and re:IsActiveType(TYPE_TRAP+TYPE_SPELL) then
		Duel.NegateEffect(ev)
	end
end
local function fnegop2(e,tp,eg,ep,ev,re,r,rp)
	for ec in aux.Next(eg) do
		if ec:IsReason(REASON_EFFECT) then ec:RegisterFlagEffect(511000173,RESET_EVENT+RESETS_STANDARD+RESET_CHAIN,0,1) end
	end
end
local function ftgfilter(c,te)
	return c:IsCanBeEffectTarget(te) and c:IsControler(tp)
end
local function fatgfilter(c,ap)
	return Duel.GetAttacker():GetAttackableTarget():IsContains(c) and c:IsControler(ap)
end
local function fblock(e,tp,eg,ep,ev,re,r,rp)
	local te,tg=Duel.GetChainInfo(ev+1,CHAININFO_TRIGGERING_EFFECT,CHAININFO_TARGET_CARDS)
	if te and te~=re and te:IsHasProperty(EFFECT_FLAG_CARD_TARGET) and tg and #tg==1 then
		local g=eg:Filter(ftgfilter,nil,te,tg:GetFirst():GetControler())
		if #g>0 and Duel.SelectYesNo(g:GetFirst():GetSummonPlayer(),aux.Stringid(68823957,1)) then
			local p=g:GetFirst():GetSummonPlayer()
			Duel.Hint(HINT_SELECTMSG,p,HINTMSG_TARGET)
			local rg=g:Select(p,1,1,nil)
			Duel.ChangeTargetCard(ev+1,rg)
		end
	end
	local ac=Duel.GetAttacker()
	if (Duel.CheckEvent(EVENT_BE_BATTLE_TARGET) or Duel.CheckEvent(EVENT_ATTACK_ANNOUNCE))
		and not ac:IsStatus(STATUS_ATTACK_CANCELED) then
		local ap=1-ac:GetControler()
		local at=Duel.GetAttackTarget()
		local ag=eg:Filter(fatgfilter,nil,ap)
		if #ag>0 and Duel.SelectYesNo(ag:GetFirst():GetSummonPlayer(),aux.Stringid(68823957,0)) then
			local p=ag:GetFirst():GetSummonPlayer()
			Duel.Hint(HINT_SELECTMSG,p,HINTMSG_ATTACKTARGET)
			local atg=ag:Select(p,1,1,nil)
			Duel.HintSelection(atg)
			Duel.ChangeAttackTarget(atg:GetFirst())
			atg:GetFirst():RegisterFlagEffect(511004019,RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_DAMAGE,0,1)
		else
			Duel.ChangeAttackTarget(at)
			if at then at:RegisterFlagEffect(511004019,RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_DAMAGE,0,1) end   
		end
	end
end
local function frepfilter(c)
	return c:IsOnField() and c:GetDestination()==LOCATION_GRAVE and c:GetControler()~=c:GetOwner()
end
local function freptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return eg:IsExists(frepfilter,1,nil) end
	local g=eg:Filter(frepfilter,nil)
	for c in aux.Next(g) do
		local rs=c:GetReason()
		if rs&REASON_DESTROY==REASON_DESTROY then
			c:RegisterFlagEffect(511000173,RESET_CHAIN,0,1)
		end
		local p=c:GetControler()
		local tk=Duel.CreateToken(p,c:GetOriginalCode())
		Duel.Exile(c,REASON_RULE)
		Duel.MoveToField(tk,p,p,c:GetPreviousLocation(),c:GetPreviousPosition(),true,2^c:GetPreviousSequence())
		Duel.SendtoGrave(tk,rs)
	end
	return false
end
local function frepval(e,c)
	return c:IsOnField() and c:GetControler()~=c:GetOwner() and c:GetDestination()==LOCATION_GRAVE
end
local function fgrepcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(Card.IsStatus,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,1,nil,STATUS_LEAVE_CONFIRMED)
end
local function fgrepop(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(Card.IsStatus,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,nil,STATUS_LEAVE_CONFIRMED)
	for c in aux.Next(g) do
		if c:GetControler()~=c:GetOwner() then c:RegisterFlagEffect(STATUS_LEAVE_CONFIRMED,RESET_EVENT+RESETS_STANDARD,0,1) end
	end
end
local function fgrepfilter(c)
	return c:GetFlagEffect(STATUS_LEAVE_CONFIRMED)>0
end
local function fgrepcon2(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(fgrepfilter,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,1,nil)
end
local function fgrepop2(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(fgrepfilter,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,nil)
	for c in aux.Next(g) do
		local p=c:GetControler()
		local tk=Duel.CreateToken(p,c:GetOriginalCode())
		Duel.Exile(c,REASON_RULE)
		Duel.MoveToField(tk,p,p,c:GetPreviousLocation(),c:GetPreviousPosition(),true,2^c:GetPreviousSequence())
		Duel.SendtoGrave(tk,REASON_RULE)
	end
end
function mtfilter(c,v,gr)
	return c:GetSummonLocation()==LOCATION_EXTRA and (c:GetMaterialCount()==v or gr and c:GetMaterialCount()>v) and c:IsReleasable() and Duel.GetMZoneCount(c:GetControler(),c)>0
end
function ttcon(e,c,minc)
	if c==nil then return true end
	local tp=c:GetControler()
	return minc<=3 and Duel.IsExistingMatchingCard(mtfilter,tp,LOCATION_MZONE,0,1,nil,3,true)
end
function ttop(e,tp,eg,ep,ev,re,r,rp,c)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_RELEASE)
	local g=Duel.SelectMatchingCard(tp,mtfilter,tp,LOCATION_MZONE,0,1,1,nil,3,true)
	c:SetMaterial(g)
	Duel.Release(g,REASON_SUMMON+REASON_MATERIAL)
end

--To controler's grave
local e1=Effect.GlobalEffect()
e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e1:SetProperty(EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE) 
e1:SetCode(EFFECT_SEND_REPLACE)
e1:SetTarget(freptg)
e1:SetValue(frepval)
Duel.RegisterEffect(e1,0)
local e1a=Effect.GlobalEffect()
e1a:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e1a:SetProperty(EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE)
e1a:SetCode(EVENT_ADJUST)
e1a:SetCondition(fgrepcon)
e1a:SetOperation(fgrepop)
Duel.RegisterEffect(e1a,tp)
local e1b=Effect.GlobalEffect()
e1b:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e1b:SetProperty(EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE)
e1b:SetCode(EVENT_CHAIN_END)
e1b:SetCondition(fgrepcon2)
e1b:SetOperation(fgrepop2)
Duel.RegisterEffect(e1b,0)
--Double Tribute
local e2=Effect.GlobalEffect()
e2:SetProperty(EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE)
e2:SetType(EFFECT_TYPE_FIELD)
e2:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE)
e2:SetCode(EFFECT_DOUBLE_TRIBUTE)
e2:SetTarget(function(e,c) return c:GetSummonLocation()==LOCATION_EXTRA and c:GetMaterialCount()==2 end)
e2:SetValue(aux.TargetBoolFunction(mtfilter,2))
Duel.RegisterEffect(e2,0)
--Triple Tribute
local e2a=Effect.GlobalEffect()
e2a:SetProperty(EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE)
e2a:SetType(EFFECT_TYPE_FIELD)
e2a:SetTargetRange(LOCATION_HAND,LOCATION_HAND)
e2a:SetCode(EFFECT_LIMIT_SUMMON_PROC)
e2a:SetCondition(ttcon)
e2a:SetTarget(aux.TargetBoolFunction(Card.IsCode,513000134,513000135,513000136,513000137,513000138,513000139))
e2a:SetOperation(ttop)
e2a:SetValue(SUMMON_TYPE_ADVANCE)
Duel.RegisterEffect(e2a,0)
local e2b=e2a:Clone()
e2b:SetCode(EFFECT_LIMIT_SET_PROC)
e2b:SetTarget(aux.TargetBoolFunction(Card.IsCode,513000137,513000138,513000139))
Duel.RegisterEffect(e2b,0)
--faceup def
local e3=Effect.GlobalEffect()
e3:SetType(EFFECT_TYPE_FIELD)
e3:SetCode(EFFECT_DEVINE_LIGHT)
e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
e3:SetTargetRange(1,1)
Duel.RegisterEffect(e3,0)
--Cannot Attack
local e6=Effect.GlobalEffect()
e6:SetType(EFFECT_TYPE_FIELD)
e6:SetCode(EFFECT_CANNOT_ATTACK)
e6:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE) 
e6:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE)
e6:SetTarget(function(e,c) return c:IsStatus(STATUS_SPSUMMON_TURN) and c:GetFlagEffect(511004016)==0 end)
Duel.RegisterEffect(e6,0)
--Quick
local e7=Effect.GlobalEffect()
e7:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_SET_AVAILABLE)
e7:SetType(EFFECT_TYPE_FIELD)
e7:SetCode(EFFECT_BECOME_QUICK)
e7:SetTargetRange(0xff,0xff)
e7:SetCondition(function(e) local ph=Duel.GetCurrentPhase() return ph>=PHASE_BATTLE_START and ph<=PHASE_BATTLE end)
Duel.RegisterEffect(e7,0)
local e7a=Effect.GlobalEffect()
e7a:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_SET_AVAILABLE)
e7a:SetType(EFFECT_TYPE_FIELD)
e7a:SetCode(EFFECT_BECOME_QUICK)
e7a:SetTargetRange(LOCATION_ONFIELD,LOCATION_ONFIELD)
e7a:SetTarget(function(e,c) return c:IsType(TYPE_SPELL+TYPE_TRAP) and c:IsFacedown() and Duel.GetTurnPlayer()~=c:GetControler() end)
Duel.RegisterEffect(e7a,0)
--Negate
local e8=Effect.GlobalEffect()
e8:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e8:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE) 
e8:SetCode(EVENT_CHAIN_SOLVING)
e8:SetOperation(fnegop)
Duel.RegisterEffect(e8,0)
local e9=e8:Clone()
e9:SetCode(EVENT_DESTROYED)
e9:SetOperation(fnegop2)
Duel.RegisterEffect(e9,0)
--block
local e10=Effect.GlobalEffect()
e10:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e10:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
e10:SetCode(EVENT_SUMMON_SUCCESS)
e10:SetOperation(fblock)
Duel.RegisterEffect(e10,0)
local e10a=e10:Clone()
e10a:SetCode(EVENT_FLIP_SUMMON_SUCCESS)
Duel.RegisterEffect(e10a,0)
local e10b=e10:Clone()
e10b:SetCode(EVENT_SPSUMMON_SUCCESS)
Duel.RegisterEffect(e10b,0)
--Attack
local e11=Effect.GlobalEffect()
e11:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
e11:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE) 
e11:SetCode(EVENT_LEAVE_FIELD)
e11:SetCondition(function(e,tp) local ph=Duel.GetCurrentPhase() return ph>=PHASE_BATTLE_STEP and ph<PHASE_DAMAGE
	and Duel.GetAttackTarget()~=nil and not Duel.GetAttackTarget():IsReason(REASON_BATTLE) and Duel.GetAttackTarget():GetFlagEffect(511004019)==0 end)
e11:SetOperation(function() Duel.NegateAttack() end)
Duel.RegisterEffect(e11,0)
local e11a=e11:Clone()
e11a:SetCode(EVENT_BE_MATERIAL)
Duel.RegisterEffect(e11a,0)

--Modified by Lyris on 08/26/2019
--Boss Duel
local function afilter0(c)
	return c.mark and c.mark==0
end
local function afilter1(c)
	return c.mark and c.mark==1
end
local function afilter2(c)
	return c.mark and c.mark==2
end
local function afilter3(c)
	return c.mark and c.mark==3
end
local function stop(e,tp,eg,ep,ev,re,r,rp)
	--arrange time
	local g0=Duel.GetMatchingGroup(afilter0,tp,LOCATION_DECK,0,nil)
	local g1=Duel.GetMatchingGroup(afilter1,tp,LOCATION_DECK,0,nil)
	local g2=Duel.GetMatchingGroup(afilter2,tp,LOCATION_DECK,0,nil)
	local g3=Duel.GetMatchingGroup(afilter3,tp,LOCATION_DECK,0,nil)
	for tc3 in aux.Next(g3) do Duel.MoveSequence(tc3,0) end
	for tc2 in aux.Next(g2) do Duel.MoveSequence(tc2,0) end
	for tc1 in aux.Next(g1) do Duel.MoveSequence(tc1,0) end
	for tc0 in aux.Next(g0) do Duel.MoveSequence(tc0,0) end
	Duel.Draw(tp,#g0,REASON_RULE)
end
local function thop(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetFieldGroup(tp,LOCATION_GRAVE,0)
	if Duel.GetTurnPlayer()~=tp or #g==0 then return end
	Duel.SendtoHand(g,nil,REASON_RULE)
end
local function con(e)
	return Duel.GetFieldGroupCount(e:GetHandlerPlayer(),LOCATION_DECK,0)==0
end
local function drepop(e,tp,eg,ep,ev,re,r,rp)
	if not re:IsHasCategory(CATEGORY_DRAW) or not Duel.GetOperationInfo(ev,CATEGORY_DRAW) then return end
	local hc=math.max(Duel.GetFieldGroupCount(tp,LOCATION_HAND,0),5)
	local dc=Duel.GetFieldGroupCount(tp,LOCATION_DECK,0)
	local g=Duel.GetFieldGroup(tp,LOCATION_GRAVE,0)
	if #g>0 then Duel.SendtoDeck(g,nil,0,REASON_RULE) dc=dc+#g end
	if dc>hc then return end
	for i=0,hc-dc do
		local token=Duel.CreateToken(tp,10000)
		if Duel.SendtoDeck(token,nil,1,REASON_RULE)~=0 then
			local e2=Effect.CreateEffect(token)
			e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
			e2:SetCode(EVENT_DRAW)
			e2:SetOperation(function(e) Duel.SendtoDeck(e:GetOwner(),nil,1,REASON_RULE) end)
			token:RegisterEffect(e2)
		end
	end
end
local function ordercon(e,tp,eg,ep,ev,re,r,rp)
	local ct=1
	local g=Duel.GetDecktopGroup(tp,ct)
	local gn=Duel.GetDecktopGroup(tp,ct+1)
	local tc1=g:GetFirst()
	if not tc1 then return false end
	if gn:GetCount()<2 then return false end
	gn:Sub(g)
	local tc2=gn:GetFirst()
	local markchk=tc1.mark
	if not markchk then return false end
	while tc2 do
		if not tc2.mark then return false end
		if markchk>tc2.mark then
			return true
		elseif markchk<tc2.mark then
			markchk=tc2.mark
		end
		ct=ct+1
		g=Duel.GetDecktopGroup(tp,ct)
		gn=Duel.GetDecktopGroup(tp,ct+1)
		gn:Sub(g)
		tc2=gn:GetFirst()
	end
	return false
end
local function orderop(e,tp,eg,ep,ev,re,r,rp)
	local g0=Duel.GetMatchingGroup(afilter0,tp,LOCATION_DECK,0,nil)
	local g1=Duel.GetMatchingGroup(afilter1,tp,LOCATION_DECK,0,nil)
	local g2=Duel.GetMatchingGroup(afilter2,tp,LOCATION_DECK,0,nil)
	local g3=Duel.GetMatchingGroup(afilter3,tp,LOCATION_DECK,0,nil)
	for tc3 in aux.Next(g3) do Duel.MoveSequence(tc3,0) end
	for tc2 in aux.Next(g2) do Duel.MoveSequence(tc2,0) end
	for tc1 in aux.Next(g1) do Duel.MoveSequence(tc1,0) end
	for tc0 in aux.Next(g0) do Duel.MoveSequence(tc0,0) end
end
local function banfilter(c,tp)
	return not c:IsType(TYPE_TOKEN) and c:GetOwner()==tp
end
local function banop(e,tp,eg,ep,ev,re,r,rp)
	local g=eg:Filter(banfilter,nil,tp)
	if g:GetCount()>0 then
		Duel.SendtoGrave(g,r+REASON_DESTROY+REASON_RETURN)
	end
end
local function repfilter(c,tp)
	return c:GetOwner()==tp and c:GetDestination()==LOCATION_REMOVED
end
local function reptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return eg:IsExists(repfilter,1,nil,tp) end
	local g=eg:Filter(repfilter,nil,tp)
	local tc=g:GetFirst()
	while tc do
		tc:SetStatus(STATUS_DESTROY_CONFIRMED,true)
		tc=g:GetNext()
	end
	g:KeepAlive()
	e:SetLabel(r)
	e:SetLabelObject(g)
	return true
end
local function repval(e,c)
	return repfilter(c,e:GetHandlerPlayer())
end
local function repop(e,tp,eg,ep,ev,re,r,rp)
	local g=e:GetLabelObject()
	local tc=g:GetFirst()
	while tc do
		tc:SetStatus(STATUS_DESTROY_CONFIRMED,false)
		tc=g:GetNext()
	end
	local rs=e:GetLabel()
	Duel.Destroy(g,rs+REASON_REPLACE)
end
local function atkcon(e)
	local ph=Duel.GetCurrentPhase()
	return ph>=0x08 and ph<=0x80
end
local function atkval(e,c)
	return c:GetAttack()*2
end
local function ocon(e,tp,eg,ep,ev,re,r,rp)
	return tp==Duel.GetTurnPlayer() and Duel.GetFieldGroupCount(tp,LOCATION_DECK,0)>0
		and Duel.GetDrawCount(tp)>0
end
local function oop(e,tp,eg,ep,ev,re,r,rp)
	local dt=Duel.GetDrawCount(tp)
	if dt~=0 then
		_replace_count=0
		_replace_max=dt
		local e1=Effect.GlobalEffect()
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetCode(EFFECT_DRAW_COUNT)
		e1:SetTargetRange(1,0)
		e1:SetReset(RESET_PHASE+PHASE_DRAW)
		e1:SetValue(0)
		Duel.RegisterEffect(e1,tp)
	end
	_replace_count=_replace_count+1
	if _replace_count>_replace_max then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
	local g=Duel.SelectMatchingCard(tp,Card.IsAbleToHand,tp,LOCATION_DECK,0,1,1,nil)
	if g:GetCount()>0 then
		Duel.SendtoHand(g,nil,REASON_EFFECT)
		Duel.ConfirmCards(1-tp,g)
	end
end
local function effop(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetTurnPlayer()==tp then return end
	local a=Duel.GetFlagEffect(1-tp,951)==0
	local b=Duel.GetFlagEffect(1-tp,952)==0
	local c=Duel.GetFlagEffect(1-tp,953)==0
	if not a and not b and not c or not Duel.SelectYesNo(1-tp,aux.Stringid(4007,0)) then return end
	local op=0
	if a and b and c then
		op=Duel.SelectOption(1-tp,aux.Stringid(4007,1),aux.Stringid(4007,2),aux.Stringid(4007,3))
	elseif a and b then
		op=Duel.SelectOption(1-tp,aux.Stringid(4007,1),aux.Stringid(4007,2))
	elseif b and c then
		op=Duel.SelectOption(1-tp,aux.Stringid(4007,2),aux.Stringid(4007,3))
		op=op+1
	elseif a and c then
		op=Duel.SelectOption(1-tp,aux.Stringid(4007,1),aux.Stringid(4007,3))
		if op==1 then op=op+1 end
	elseif a then
		Duel.SelectOption(1-tp,aux.Stringid(4007,1))
		op=0
	elseif b then
		Duel.SelectOption(1-tp,aux.Stringid(4007,2))
		op=1
	else
		Duel.SelectOption(1-tp,aux.Stringid(4007,3))
		op=2
	end
	if op==0 then
		Duel.RegisterFlagEffect(1-tp,951,nil,0,1)
		local e1=Effect.GlobalEffect()
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_SET_ATTACK_FINAL)
		e1:SetTargetRange(LOCATION_MZONE,0)
		e1:SetCondition(atkcon)
		e1:SetValue(atkval)
		e1:SetReset(RESET_PHASE+PHASE_END)
		Duel.RegisterEffect(e1,1-tp)
	elseif op==1 then
		Duel.RegisterFlagEffect(1-tp,952,nil,0,1)
		--
		local e2=Effect.GlobalEffect()
		e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		e2:SetCode(EVENT_PREDRAW)
		e2:SetCondition(ocon)
		e2:SetOperation(oop)
		e2:SetReset(RESET_PHASE+PHASE_END)
		Duel.RegisterEffect(e2,1-tp)
	else
		Duel.RegisterFlagEffect(1-tp,953,nil,0,1)
		Duel.Recover(1-tp,4000,REASON_RULE)
	end
end
--start
local e10=Effect.GlobalEffect()
e10:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
e10:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e10:SetCode(EVENT_PREDRAW)
e10:SetCountLimit(1)
e10:SetOperation(stop)
Duel.RegisterEffect(e10,0)
--no summon limit
local e3=Effect.GlobalEffect()
e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
e3:SetType(EFFECT_TYPE_FIELD)
e3:SetCode(EFFECT_SET_SUMMON_COUNT_LIMIT)
e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
e3:SetTargetRange(1,0)
e3:SetValue(99)
Duel.RegisterEffect(e3,0)
--grave to hand
local e4=Effect.GlobalEffect()
e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e4:SetCode(EVENT_PHASE_START+PHASE_STANDBY)
e4:SetOperation(thop)
Duel.RegisterEffect(e4,0)
local e5=Effect.GlobalEffect()
e5:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e5:SetCode(EVENT_PHASE_START+PHASE_DRAW)
e5:SetCondition(con)
e5:SetOperation(function(e,tp) if Duel.GetTurnPlayer()==tp and Duel.GetFieldGroupCount(tp,LOCATION_GRAVE,0)>0 then
	Duel.SendtoDeck(Duel.GetFieldGroup(tp,LOCATION_GRAVE,0),nil,1,REASON_RULE)
end end)
Duel.RegisterEffect(e5,0)
--cannot lose for draw
local e1=Effect.GlobalEffect()
e1:SetType(EFFECT_TYPE_FIELD)
e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
e1:SetCode(EFFECT_CANNOT_DRAW)
e1:SetTargetRange(1,0)
e1:SetCondition(con)
Duel.RegisterEffect(e1,0)
local e2=Effect.GlobalEffect()
e2:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
e2:SetCode(EVENT_CHAIN_SOLVING)
e2:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
e2:SetOperation(drepop)
Duel.RegisterEffect(e2,0)
--rearrange
local e6=Effect.GlobalEffect()
e6:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
e6:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e6:SetCode(EVENT_PREDRAW)
e6:SetCountLimit(1)
e6:SetCondition(ordercon)
e6:SetOperation(orderop)
Duel.RegisterEffect(e6,0)
local e7=e6:Clone()
e7:SetCode(EVENT_CHAIN_SOLVED)
Duel.RegisterEffect(e7,0)
local e8=Effect.GlobalEffect()
e8:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
e8:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e8:SetCode(EVENT_ADJUST)
e8:SetCondition(ordercon)
e8:SetOperation(orderop)
Duel.RegisterEffect(e8,0)
--cannot banish
local e9=Effect.GlobalEffect()
e9:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
e9:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e9:SetCode(EVENT_REMOVE)
e9:SetOperation(banop)
Duel.RegisterEffect(e9,0)
--replace
local e14=Effect.GlobalEffect()
e14:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
e14:SetCode(EFFECT_SEND_REPLACE)
e14:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
e14:SetTarget(reptg)
e14:SetValue(repval)
e14:SetOperation(repop)
Duel.RegisterEffect(e14,0)
--opponent effect
local e15=Effect.GlobalEffect()
e15:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e15:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
e15:SetCode(EVENT_ADJUST)
e15:SetCountLimit(1)
e15:SetOperation(effop)
Duel.RegisterEffect(e15,0)

local command = {
	[01]=152000010,
	[02]=152000020,
	[03]=152000030,
	[04]=152000040,
	[05]=152000050,
	[06]=152000060,
	[07]=152000070,
	[08]=152000080,
	[09]=152000090,
	[10]=152000100,
	[11]=152000110,
	[12]=152000120,
	[13]=152000130,
	[14]=152000140,
	[15]=152000150,
	[16]=152000160,
	[17]=152000170,
	[18]=152000180,
	[19]=152000190,
	[20]=152000200,
	[21]=152000210,
	[22]=152000220,
	[23]=152000230,
	[24]=152000240,
	[25]=152000250,
	[26]=152001010,
	[27]=152001020,
}
local function f(e,tp,eg,ep,ev,re,r,rp)
	local p=Duel.GetTurnPlayer()
	local ex={}
	local j
	local tc
	for j=1,#command do
		tc=Duel.CreateToken(p,command[j])
		local ef=tc:GetActivateEffect()
		if ef then table.insert(ex,ef) break end
	end
	local seed=0
	if Duel.GetFieldGroupCount(p,LOCATION_DECK,0)==0 then
		seed = Duel.GetMatchingGroup(nil,p,LOCATION_GRAVE+LOCATION_REMOVED+LOCATION_HAND+LOCATION_SZONE,0,nil):GetFirst():GetCode()
	else seed = Duel.GetDecktopGroup(p,1):GetFirst():GetCode() end
	math.randomseed( seed )
	local te
	while not te do
		tc=Duel.CreateToken(p,command[math.random(#command)])
		te=tc:GetActivateEffect()
	end
	local tpe=tc:GetType()
	local tg=te:GetTarget()
	local co=te:GetCost()
	local op=te:GetOperation()
	e:SetCategory(te:GetCategory())
	e:SetProperty(te:GetProperty())
	Duel.ClearTargetCard()
	Duel.Hint(HINT_CARD,0,tc:GetCode())
	Duel.ConfirmCards(0,tc)
	Duel.ConfirmCards(1,tc)
	tc:CreateEffectRelation(te)
	tc:CancelToGrave(false)
	if co then co(te,p,eg,ep,ev,re,r,rp,1) end
	if tg then tg(te,p,eg,ep,ev,re,r,rp,1) end
	Duel.BreakEffect()
	local g=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
	if g then
		local etc=g:GetFirst()
		while etc do
			etc:CreateEffectRelation(te)
			etc=g:GetNext()
		end
	end
	if op then op(te,p,eg,ep,ev,re,r,rp) end
	tc:ReleaseEffectRelation(te)
	if etc then
		etc=g:GetFirst()
		while etc do
			etc:ReleaseEffectRelation(te)
			etc=g:GetNext()
		end
	end
end
local e2=Effect.GlobalEffect()
e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e2:SetCode(EVENT_PHASE_START+PHASE_STANDBY)
e2:SetOperation(f)
Duel.RegisterEffect(e2,0)

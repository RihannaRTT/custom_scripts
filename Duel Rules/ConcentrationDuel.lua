--concentration duel
--Modified by Lyris on 08/26/2019

local forbidden={[0]={},[1]={}}
local function cfl(e,tp,eg,ep,ev,re,r,rp)
	forbidden[0]={}
	forbidden[1]={}
end
local function damtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return re end
end
local function damop(e,tp,eg,ep,ev,re,r,rp)
	local sg=eg:Clone()
	if sg:GetCount()>0 then
		Duel.SendtoDeck(sg,nil,0,REASON_RULE)
		if sg:IsExists(Card.IsControler,1,nil,0) then
			Duel.ShuffleDeck(0)
			forbidden[0]={}
		end
		if sg:IsExists(Card.IsControler,1,nil,1) then
			Duel.ShuffleDeck(1)
			forbidden[1]={}
		end
	end
end
local function operation(e,tp,eg,ep,ev,re,r,rp)
	local ops={[0]={},[1]={}}
	for p=0,1 do
		ops[p][1]=Duel.GetLocationCount(p,LOCATION_MZONE)>0 and 1050 or 1053
		ops[p][2]=Duel.GetLocationCount(p,LOCATION_SZONE)>0 and 1051 or 1053
		ops[p][3]=Duel.GetLocationCount(p,LOCATION_SZONE)>0 and 1052 or 1053
		local n=Duel.GetFieldGroupCount(p,LOCATION_DECK,0)
		while #ops[p]>0 and n>0 do
			local r={}
			for i=1,n do
				if not forbidden[p][i] then table.insert(r,i) end
			end
			local an=-1
			if #r>0 then an=Duel.AnnounceNumber(p,table.unpack(r))-1 end
			if an<=0 then break end
			local c=Duel.GetFieldCard(p,LOCATION_DECK,an)
			local op=nil
			while not op or ops[p][op]==1053 do op=Duel.SelectOption(p,table.unpack(ops[p])) end
			if ops[p][1]==1053 then
				op=op+1
				if ops[p][2]==1053 then op=op+1 end
			elseif ops[p][2]==1053 and op>1 then op=op+1 end
			Duel.ConfirmCards(p,c)
			Duel.ConfirmCards(1-p,c)
			if op==0 then --normal/set
				if c:IsType(TYPE_MONSTER) then
					local ef=c:IsHasEffect(EFFECT_SPSUMMON_PROC)
					local tmin,tmax=c:GetTributeRequirement()
					if (c:IsSummonable(true,nil) or c:IsMSetable(true,nil)) and ((Duel.IsExistingMatchingCard(Card.IsReleasable,p,LOCATION_MZONE,0,tmin,nil) and Duel.GetLocationCount(p,LOCATION_MZONE)>-tmin) or (Duel.GetLocationCount(p,LOCATION_MZONE)>0 and tmin==0)) then
						local poi=0 --set/summon control variable
						if c:IsSummonable(true,nil) or c:IsMSetable(true,nil) then 
							poi=Duel.SelectOption(p,1,1153)
						elseif c:IsSummonable(true,nil) then
							poi=0
						else
							poi=1
						end
						if poi==0 then
							Duel.DisableShuffleCheck()
							Duel.Summon(p,c,true,nil)
						else
							Duel.DisableShuffleCheck()
							Duel.SpecialSummon(c,0,p,p,false,false,POS_FACEDOWN_DEFENSE)
							Duel.RaiseEvent(c,EVENT_MSET,e,r,rp,tp,0)
						end
						if c:IsLocation(LOCATION_DECK) then
							forbidden[p][an+1]=true
							Duel.Hint(HINT_MESSAGE,p,574)
						end
					elseif c:IsType(TYPE_MONSTER) and ef then
						local st={ef:GetValue()}
						Duel.SendtoHand(c,p,REASON_RULE)
						Duel.SpecialSummonRule(p,c,st[1])
						if c:IsLocation(LOCATION_HAND) then
							Duel.DisableShuffleCheck()
							Duel.SendtoDeck(c,p,an,REASON_RULE)
							forbidden[p][an+1]=true
							Duel.Hint(HINT_MESSAGE,p,574)
						end
					else
						forbidden[p][an+1]=true
						Duel.Hint(HINT_MESSAGE,p,574)
					end
				else
					forbidden[p][an+1]=true
					Duel.Hint(HINT_MESSAGE,p,574)
				end
			elseif op==1 then --spell
				if c:IsType(TYPE_SPELL) then
					local tpe=c:GetType()
					local te=c:GetActivateEffect()
					if te then
						local tg=te:GetTarget()
						local co=te:GetCost()
						local op=te:GetOperation()
						if te:IsActivatable(p) then
							e:SetCategory(te:GetCategory())
							e:SetProperty(te:GetProperty())
							Duel.ClearTargetCard()
							if bit.band(tpe,TYPE_FIELD)~=0 then
								local fc=Duel.GetFieldCard(1-p,LOCATION_SZONE,5)
								if Duel.GetFlagEffect(p,62765383)>0 then
									if fc then Duel.Destroy(fc,REASON_RULE) end
									of=Duel.GetFieldCard(p,LOCATION_SZONE,5)
									if fc and Duel.Destroy(fc,REASON_RULE)==0 then Duel.SendtoGrave(c,REASON_RULE) end
								else
									Duel.GetFieldCard(p,LOCATION_SZONE,5)
									if fc and Duel.SendtoGrave(fc,REASON_RULE)==0 then Duel.SendtoGrave(c,REASON_RULE) end
								end
							end
							Duel.DisableShuffleCheck()
							Duel.MoveToField(c,p,p,LOCATION_SZONE,POS_FACEUP,true)
							Duel.Hint(HINT_CARD,0,c:GetCode())
							c:CreateEffectRelation(te)
							if co then co(te,p,eg,ep,ev,re,r,rp,1) end
							if tg then tg(te,p,eg,ep,ev,re,r,rp,1) end
							Duel.BreakEffect()
							local g=Duel.GetChainInfo(0,CHAININFO_TARGET_CARDS)
							if g then
								local etc=g:GetFirst()
								while etc do
									etc:CreateEffectRelation(te)
									etc=g:GetNext()
								end
							end
							if op then
								if bit.band(tpe,TYPE_EQUIP+TYPE_CONTINUOUS+TYPE_FIELD)==0 and not c:IsHasEffect(EFFECT_REMAIN_FIELD) then
									c:CancelToGrave(false)
								end
								if op then op(e,p,eg,ep,ev,re,r,rp) end
							end
							c:ReleaseEffectRelation(te)
							if etc then 
								etc=g:GetFirst()
								while etc do
									etc:ReleaseEffectRelation(te)
									etc=g:GetNext()
								end
							end
						else
							forbidden[p][an+1]=true
							Duel.Hint(HINT_MESSAGE,p,574)
						end
					else
						forbidden[p][an+1]=true
						Duel.Hint(HINT_MESSAGE,p,574)
					end
				else
					forbidden[p][an+1]=true
					Duel.Hint(HINT_MESSAGE,p,574)
				end
			else --trap
				if c:IsType(TYPE_TRAP) and c:IsSSetable() then
					Duel.DisableShuffleCheck()
					Duel.SendtoHand(c,p,REASON_RULE)
					Duel.SSet(p,c,p,false)
					if c:IsLocation(LOCATION_HAND) then
						Duel.DisableShuffleCheck()
						Duel.SendtoDeck(c,p,an,REASON_RULE)
						forbidden[p][an+1]=true
						Duel.Hint(HINT_MESSAGE,p,574)
					end
				else
					forbidden[p][an+1]=true
					Duel.Hint(HINT_MESSAGE,p,574)
				end
			end
			ops[p][op+1]=nil
			n=Duel.GetFieldGroupCount(p,LOCATION_DECK,0)
		end
	end
end
--clean forbidden love
local e0=Effect.GlobalEffect() --turn end
e0:SetProperty(EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE)
e0:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e0:SetCode(EVENT_TURN_END)
e0:SetCountLimit(1)
e0:SetOperation(cfl)
Duel.RegisterEffect(e0,0)
local e0a=e0:Clone() --when sent to deck
e0a:SetCode(EVENT_TO_DECK)
Duel.RegisterEffect(e0a,0)
--remove
local e0b=Effect.GlobalEffect()
e0b:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e0b:SetCode(EVENT_TO_HAND)
e0b:SetProperty(EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE)
e0b:SetTarget(damtg)
e0b:SetOperation(damop)
Duel.RegisterEffect(e0b,0)
--skip draw phase
local e2=Effect.GlobalEffect()
e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE)
e2:SetType(EFFECT_TYPE_FIELD)
e2:SetCode(EFFECT_SKIP_DP)
e2:SetTargetRange(1,1)
Duel.RegisterEffect(e2,0)
local e3=Effect.GlobalEffect()
e3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e3:SetCode(EVENT_PHASE+PHASE_STANDBY)
e3:SetCountLimit(1)
e3:SetOperation(operation)
Duel.RegisterEffect(e3,0)
local st=Effect.GlobalEffect()
st:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
st:SetCode(EVENT_PHASE_START+PHASE_STANDBY)
st:SetCountLimit(1)
st:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
st:SetOperation(function(e,tp) Duel.SendtoDeck(Duel.GetFieldGroup(tp,LOCATION_HAND,LOCATION_HAND),nil,2,REASON_RULE) end)
Duel.RegisterEffect(st,0)
--[[
	scripter note: not fully working, you can see monster by searching effect's
	also, need event deck shuffle
	the cost may be working wrong if it demand to dischard top card deck, because it may be turned on sending random card, since you don't have top deck card theoricaly
--]]

--次元领域决斗
--Dimension Duel
--scripted by Larry126
--note: Please contact with me if wanting to edit this script

local function f1(c)
	return c:IsHasEffect(EFFECT_LIMIT_SUMMON_PROC) and c:GetFlagEffect(51160002)<=0
end
local function f2(e,tp,eg,ep,ev,re,r,rp)
	return Duel.IsExistingMatchingCard(f1,tp,0xff,0xff,1,nil)
end
local function f3(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local g=Duel.GetMatchingGroup(f1,tp,0xff,0xff,nil)
	if g:GetCount()>0 then
		for tc in aux.Next(g) do
			tc:RegisterFlagEffect(51160002,0,0,0)
		end
	end
end
------------------------------------------------------------------------
--tribute
local function f4(e,c,minc)
	if c==nil then return true end
	if not (minc==0 and Duel.GetLocationCount(c:GetControler(),LOCATION_MZONE)>0 and (Duel.IsPlayerCanSpecialSummon(tp,6,POS_FACEUP,tp,c) or Duel.IsPlayerCanSummon(tp,6,c))) then return false end
	if c:GetFlagEffect(6)==0 then
		e:SetLabel(1)
		c:RegisterFlagEffect(6,RESET_EVENT+RESETS_STANDARD,0,1)
	end
	return e:GetLabel()==1
end
------------------------------------------------------------------------
--spirit charge
local function f5(c)
	return c:IsType(TYPE_MONSTER+TYPE_TRAPMONSTER+TYPE_TOKEN) and c:IsOnField()
end
local function f6(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return eg:IsExists(f5,1,nil) end
end
local function f8(tp,min,max,str)
	if min>max then min,max=max,min end
	local mindc=#tostring(min)
	local maxdc=#tostring(max)
	local dbdmin={}
	local dbdmax={}
	local mi=maxdc-1
	local aux=min
	for i=1,maxdc do
		dbdmin[i]=aux//(10^mi)
		aux=aux%(10^mi)
		mi=mi-1
	end
	aux=max
	mi=maxdc-1
	for i=1,maxdc do
		dbdmax[i]=aux//(10^mi)
		aux=aux%(10^mi)
		mi=mi-1
	end
	local chku=true
	local chkl=true
	local dbd={}
	mi=maxdc-1
	for i=1,maxdc do
		local maxval=9
		local minval=0
		if chku and i>1 and dbd[i-1]<dbdmax[i-1] then
			chku=false
		end
		if chkl and i>1 and dbd[i-1]>dbdmin[i-1] then
			chkl=false
		end
		if chku then
			maxval=dbdmax[i]
		end
		if chkl then
			minval=dbdmin[i]
		end
		local r={}
		local j=1
		for k=minval,maxval do
			r[j]=k
			j=j+1
		end
		if str then Duel.Hint(HINT_SELECTMSG,p,Auxiliary.Stringid(4010,str+i-1)) end
		dbd[i]=Duel.AnnounceNumber(tp,table.unpack(r))
		mi=mi-1
	end
	local number=0
	mi=maxdc-1
	for i=1,maxdc do
		number=number+dbd[i]*10^mi
		mi=mi-1
	end
	return number
end
local function f7(e,tp,eg,ep,ev,re,r,rp)
	local g=eg:Filter(f5,nil)
	for tc in aux.Next(g) do
		local baseAtk=0
		local baseDef=0
		local textAtk=tc:GetTextAttack()
		local textDef=tc:GetTextDefense()
		local p=tc:GetSummonPlayer()
		if textAtk~=-2 and textAtk~=0 then
			local op=Duel.SelectOption(p,aux.Stringid(4010,1),aux.Stringid(4010,2),aux.Stringid(4010,3))
			if op==0 then baseAtk=textAtk
			elseif op==1 then baseAtk=0
			else baseAtk=f8(p,0,textAtk,4) end
			local e1=Effect.CreateEffect(tc)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_SET_BASE_ATTACK)
			e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_UNCOPYABLE)
			e1:SetRange(LOCATION_MZONE)
			e1:SetValue(baseAtk)
			e1:SetReset(RESET_EVENT+0xec0000)
			tc:RegisterEffect(e1)
		end
		if textDef~=-2 and textDef~=0 then
			local op=Duel.SelectOption(p,aux.Stringid(4010,1),aux.Stringid(4010,2),aux.Stringid(4010,3))
			if op==0 then baseDef=textDef
			elseif op==1 then baseDef=0
			else baseDef=f8(p,0,textDef,10) end
			local e2=Effect.CreateEffect(tc)
			e2:SetType(EFFECT_TYPE_SINGLE)
			e2:SetCode(EFFECT_SET_BASE_DEFENSE)
			e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_UNCOPYABLE)
			e2:SetRange(LOCATION_MZONE)
			e2:SetValue(baseDef)
			e2:SetReset(RESET_EVENT+0xec0000)
			tc:RegisterEffect(e2)
		end
	end
end
------------------------------------------------------------------------
--inflict battle damage
local function f9(c)
	return c:IsType(TYPE_MONSTER+TYPE_TRAPMONSTER+TYPE_TOKEN) and c:IsPreviousLocation(LOCATION_ONFIELD)
end
local function fa(e,tp,eg,ep,ev,re,r,rp)
	return eg:IsExists(f9,1,nil)
end
local function fb(e,tp,eg,ep,ev,re,r,rp)
	local g=eg:Filter(f9,nil)
	local dam=0
	if g:GetCount()<1 then return end
	for tc in aux.Next(g) do
		local def=tc:GetPreviousDefenseOnField()
		local atk=tc:GetPreviousAttackOnField()
		local ctl=tc:GetControler()
		if tc:IsPreviousPosition(POS_ATTACK) then
			Duel.Damage(ctl,atk,REASON_BATTLE,true)
		elseif tc:IsPreviousPosition(POS_DEFENSE) then
			Duel.Damage(ctl,def,REASON_BATTLE,true)
		end
	end
	Duel.RDComplete()
end
------------------------------------------------------------------------
--no battle damaged
local function fc(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetAttackTarget()~=nil
end
--limit summon
local e1=Effect.GlobalEffect()
e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e1:SetCode(EVENT_ADJUST)
e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_UNCOPYABLE)
e1:SetCondition(f2)
e1:SetOperation(f3)
Duel.RegisterEffect(e1,tp)
--Dimension Summon
local e4=Effect.GlobalEffect()
e4:SetDescription(aux.Stringid(4010,0))
e4:SetType(EFFECT_TYPE_FIELD)
e4:SetCode(EFFECT_SUMMON_PROC)
e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_IGNORE_RANGE)
e4:SetRange(LOCATION_HAND)
e4:SetCondition(f4)
e4:SetValue(6)
local e9=Effect.GlobalEffect()
e9:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_GRANT)
e9:SetTargetRange(LOCATION_HAND,LOCATION_HAND)
e9:SetTarget(function(e,c) return c:GetFlagEffect(51160002)==0 end)
e9:SetLabelObject(e4)
Duel.RegisterEffect(e9,0)
--spirit
local e6=Effect.GlobalEffect()
e6:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e6:SetCode(EVENT_SUMMON_SUCCESS)
e6:SetTarget(f6)
e6:SetOperation(f7)
e6:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
Duel.RegisterEffect(e6,0)
local e8=e6:Clone()
e8:SetCode(EVENT_SPSUMMON_SUCCESS)
Duel.RegisterEffect(e8,0)
--Damage
local e2=Effect.GlobalEffect()
e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e2:SetCode(EVENT_DESTROYED)
e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET+EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
e2:SetCondition(fa)
e2:SetOperation(fb)
Duel.RegisterEffect(e2,0)
--no battle damage
local e3=Effect.GlobalEffect()
e3:SetType(EFFECT_TYPE_FIELD)
e3:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
e3:SetTargetRange(1,1)
e3:SetCondition(fc)
Duel.RegisterEffect(e3,0)
local e5=Effect.GlobalEffect()
e5:SetType(EFFECT_TYPE_FIELD)
e5:SetCode(EFFECT_SUMMON_COST)
e5:SetTargetRange(0xff,0xff)
e5:SetCost(function(e,tc) e:SetLabelObject(tc) return true end)
e5:SetOperation(function(e,tp)
	local tc=e:GetLabelObject()
	local e11=Effect.GlobalEffect()
	e11:SetType(EFFECT_TYPE_FIELD)
	e11:SetCode(EFFECT_DEVINE_LIGHT)
	e11:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e11:SetTargetRange(1,1)
	Duel.RegisterEffect(e11,0)
	local e7=Effect.GlobalEffect()
	e7:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e7:SetCode(EVENT_SUMMON)
	e7:SetOperation(function() e11:Reset() e1:Reset() end)
	tc:RegisterEffect(e7)
end)
Duel.RegisterEffect(e5,0)

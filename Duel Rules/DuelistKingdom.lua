--おう国こく
--Modified by Lyris on 08/26/2019

local function f3(c,tp)
	return c:IsReason(REASON_EFFECT) and c:IsPreviousLocation(LOCATION_MZONE) and c:GetPreviousControler()==tp
end
local function f2(e,tp,eg,ep,ev,re,r,rp)
	Duel.Damage(0,eg:Filter(f3,nil,0):GetSum(Card.GetAttack)/2,REASON_EFFECT)
	Duel.Damage(1,eg:Filter(f3,nil,1):GetSum(Card.GetAttack)/2,REASON_EFFECT)
end
local function f6(e,c)
	return e:GetHandler():GetFlagEffect(30606547)~=0 and c:GetFieldID()~=e:GetLabel() or c:IsStatus(STATUS_SPSUMMON_TURN)
end
local function f7(e,tp,eg,ep,ev,re,r,rp)
	if e:GetHandler():GetFlagEffect(30606547)~=0 then return end
	local fid=eg:GetFirst():GetFieldID()
	e:GetHandler():RegisterFlagEffect(30606547,RESET_EVENT+0x1fe0000+RESET_PHASE+PHASE_END,0,1)
	e:GetLabelObject():SetLabel(fid)
end
--decrease tribute
local e2=Effect.GlobalEffect()
e2:SetType(EFFECT_TYPE_FIELD)
e2:SetProperty(EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE)
e2:SetCode(EFFECT_DECREASE_TRIBUTE)
e2:SetTargetRange(LOCATION_HAND,LOCATION_HAND)
e2:SetValue(0x2)
Duel.RegisterEffect(e2,0)
--cannot direct attack
local e3=Effect.GlobalEffect()
e3:SetType(EFFECT_TYPE_FIELD)
e3:SetProperty(EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE)
e3:SetCode(EFFECT_CANNOT_DIRECT_ATTACK)
e3:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE)
Duel.RegisterEffect(e3,0)
--destroy
local e9=Effect.GlobalEffect()
e9:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e9:SetProperty(EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DELAY+EFFECT_FLAG_DAMAGE_STEP)
e9:SetCode(EVENT_DESTROYED)
e9:SetOperation(f2)
Duel.RegisterEffect(e9,0)
--cannot attack
local e12=Effect.GlobalEffect()
e12:SetType(EFFECT_TYPE_FIELD)
e12:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
e12:SetCode(EFFECT_CANNOT_ATTACK_ANNOUNCE)
e12:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE)
e12:SetTarget(f6)
Duel.RegisterEffect(e12,0)
--check
local e13=Effect.GlobalEffect()
e13:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e13:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
e13:SetCode(EVENT_ATTACK_ANNOUNCE)
e13:SetOperation(f7)
e13:SetLabelObject(e12)
Duel.RegisterEffect(e13,0)
local e5=Effect.GlobalEffect()
e5:SetType(EFFECT_TYPE_FIELD)
e5:SetCode(EFFECT_SUMMON_COST)
e5:SetTargetRange(0xff,0xff)
e5:SetCost(function(e,tc) e:SetLabelObject(tc) return true end)
e5:SetOperation(function(e,tp)
	local tc=e:GetLabelObject()
	local e11=Effect.GlobalEffect()
	e11:SetType(EFFECT_TYPE_FIELD)
	e11:SetCode(EFFECT_DEVINE_LIGHT)
	e11:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e11:SetTargetRange(1,1)
	Duel.RegisterEffect(e11,0)
	local e7=Effect.GlobalEffect()
	e7:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e7:SetCode(EVENT_SUMMON)
	e7:SetOperation(function() e11:Reset() e7:Reset() end)
	Duel.RegisterEffect(e7,0)
end)
Duel.RegisterEffect(e5,0)
local mCounts={[0]=0,[1]=0}
local e6=Effect.GlobalEffect()
e6:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e6:SetCode(EVENT_TURN_END)
e6:SetCountLimit(1)
e6:SetOperation(function()
	local p=Duel.GetTurnPlayer()
	if Duel.GetActivityCount(p,ACTIVITY_NORMALSUMMON)==0 and Duel.GetActivityCount(p,ACTIVITY_SPSUMMON)==0
		and Duel.GetActivityCount(p,ACTIVITY_FLIPSUMMON)==0 and mCounts[p]==0 then
		Duel.SetLP(p,0)
	end
	mCounts[0]=0
	mCounts[1]=0
end)
Duel.RegisterEffect(e6,0)
local e8=Effect.GlobalEffect()
e8:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e8:SetCode(EVENT_PHASE_START+PHASE_DRAW)
e8:SetOperation(function()
	local p=Duel.GetTurnPlayer()
	mCounts[p]=Duel.GetFieldGroupCount(p,LOCATION_MZONE,0)
end)
Duel.RegisterEffect(e8,0)

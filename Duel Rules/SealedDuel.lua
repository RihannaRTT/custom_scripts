--Sealed Duel
-- - Works with Single Duel

local function op(e,tp,eg,ep,ev,re,r,rp)
	--Shuffle deck and add card
	for p=0,1 do Duel.ConfirmCards(p,Duel.GetFieldGroup(p,LOCATION_DECK+LOCATION_EXTRA,0)) end
	for p=0,1 do Duel.ShuffleDeck(p) end
	for p=0,1 do
		if Duel.SelectYesNo(p,99) then
			local sg=Duel.GetFieldGroup(p,LOCATION_HAND,0)
			local ct=sg:GetCount()
			Duel.SendtoDeck(sg,nil,1,REASON_RULE)
			Duel.Draw(p,ct,REASON_RULE)
		end
	end
end
local e1=Effect.GlobalEffect()
e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e1:SetCode(EVENT_PHASE_START+PHASE_DRAW)
e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
e1:SetCountLimit(1)
e1:SetOperation(op)
Duel.RegisterEffect(e1,0)

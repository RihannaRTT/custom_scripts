local f=function(e,tp,eg,ep,ev,re,r,rp)
	local code=Duel.AnnounceCard(0,511002094,OPCODE_ISCODE,110000000,OPCODE_ISCODE,OPCODE_OR,511001727,OPCODE_ISCODE,OPCODE_OR)
	for p=1,0,-1 do
		local tk=Duel.CreateToken(p,code)
		if not Duel.GetFieldCard(p,LOCATION_FZONE,0) then
			Duel.MoveToField(tk,p,p,LOCATION_FZONE,POS_FACEUP,true)
		end
	end
end
local e4=Effect.GlobalEffect()
e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e4:SetCode(EVENT_STARTUP)
e4:SetOperation(f)
Duel.RegisterEffect(e4,0)

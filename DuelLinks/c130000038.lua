--Maiden in Love
function c130000038.initial_effect(c)
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e1:SetValue(1)
	e1:SetCondition(c130000038.indcon)
	c:RegisterEffect(e1)
	local e5=Effect.CreateEffect(c)
	e5:SetCategory(CATEGORY_COUNTER)
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e5:SetCode(EVENT_BATTLED)
	e5:SetCondition(function() return Duel.GetAttackTarget()==c end)
	e5:SetOperation(c130000038.op)
	c:RegisterEffect(e5)
end
function c130000038.indcon(e)
	return e:GetHandler():IsAttackPos()
end
function c130000038.op(e,tp,eg,ep,ev,re,r,rp)
	local tc=e:GetHandler():GetBattleTarget()
	if tc:IsRelateToBattle() then tc:AddCounter(0x1753,1) end
end

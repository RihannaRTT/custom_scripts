--The Sky Lord
function c130000049.initial_effect(c)
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(function(e,tp) return Duel.IsExistingMatchingCard(c130000049.cfilter1,tp,LOCATION_GRAVE,0,1,nil) end)
	c:RegisterEffect(e1)
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_IGNITION)
	e4:SetRange(LOCATION_SZONE)
	e4:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e4:SetCost(c130000049.cost)
	e4:SetTarget(c130000049.target)
	e4:SetOperation(c130000049.operation)
	c:RegisterEffect(e4)
end
function c130000049.cfilter1(c)
	return c:IsLevelAbove(7) and c:IsSetCard(0x5)
end
function c130000049.cfilter2(c,...)
	return c:IsFaceup() and c:IsCode(table.unpack({...})) and c:IsAbleToGraveAsCost()
end
function c130000049.filter(c,e,tp)
	return c:IsCode(5861892) and c:IsCanBeSpecialSummoned(e,0,tp,true,true)
end
function c130000049.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	if chk==0 then return c:IsAbleToGraveAsCost() and Duel.GetMatchingGroup(c130000049.cfilter2,tp,LOCATION_ONFIELD,0,nil,130000040,130000051):GetClassCount(Card.GetCode)>1 end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
	Duel.SendtoGrave(Duel.GetMatchingGroup(c130000049.cfilter2,tp,LOCATION_ONFIELD,0,nil,130000040,130000051):SelectSubGroup(tp,aux.dncheck,false,2,2),REASON_COST)
end
function c130000049.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and Duel.IsExistingMatchingCard(c130000049.filter,tp,LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE,0,1,nil,e,tp)
	end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE)
end
function c130000049.operation(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetLocationCount(tp,LOCATION_MZONE)<=0 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	Duel.SpecialSummon(Duel.SelectMatchingCard(tp,c130000049.filter,tp,LOCATION_HAND+LOCATION_DECK+LOCATION_GRAVE,0,1,1,nil,e,tp),0,tp,tp,true,true,POS_FACEUP)
end

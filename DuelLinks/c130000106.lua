--coded by Lyris
--Time Angel
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_TRIGGER_F)
	e1:SetCode(EVENT_BATTLE_DESTROYED)
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetCategory(CATEGORY_TOHAND)
	e1:SetCondition(s.condition)
	e1:SetTarget(s.target)
	e1:SetOperation(s.operation)
	c:RegisterEffect(e1)
end
function s.condition(e)
	local c = e:GetHandler()
	return c:IsLocation(LOCATION_GRAVE) and c:IsPreviousPosition(POS_ATTACK)
end
function s.target(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return true
	end
	local g = Duel.GetMatchingGroup(Card.IsAbleToHand, tp, LOCATION_MZONE, LOCATION_MZONE, nil)
	Duel.SetOperationInfo(0, CATEGORY_TOHAND, g, #g, 0, 0)
end
function s.operation(e, tp)
	Duel.SendtoHand(Duel.GetMatchingGroup(Card.IsAbleToHand, tp, LOCATION_MZONE, LOCATION_MZONE, nil), nil, REASON_EFFECT)
end

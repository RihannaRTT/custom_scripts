--coded by Lyris
--Empress's Staff
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_ATTACK_ANNOUNCE)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetCategory(CATEGORY_DAMAGE)
	e1:SetCondition(s.condition)
	e1:SetTarget(s.target)
	e1:SetOperation(s.activate)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_TRAP_ACT_IN_HAND)
	e2:SetCondition(s.handcon)
	c:RegisterEffect(e2)
end
function s.condition(e, tp)
	return Duel.GetTurnPlayer() == 1 - tp
end
function s.target(e, tp, eg, ep, ev, re, r, rp, chk, chkc)
	local a = Duel.GetAttacker()
	if chkc then
		return chkc == a
	end
	if chk == 0 then
		return a:IsOnField() and a:IsCanBeEffectTarget(e)
	end
	Duel.SetTargetCard(a)
	Duel.SetOperationInfo(0, CATEGORY_DAMAGE, nil, 0, 1 - tp, 500)
end
function s.activate(e, tp)
	local tc = Duel.GetFirstTarget()
	if tc:IsRelateToEffect(e) and Duel.NegateAttack() then
		Duel.Damage(1 - tp, 500, REASON_EFFECT)
	end
end
function s.handcon(e)
	return Duel.GetFieldGroupCount(e:GetHandlerPlayer(), LOCATION_MZONE, 0) == 0
end

EFFECT_CANNOT_LOSE_LP               = 401

local e1=Effect.GlobalEffect()
e1:SetType(EFFECT_TYPE_FIELD)
e1:SetCode(EFFECT_CANNOT_LOSE_LP)
e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
e1:SetTargetRange(1,1)
e1:SetValue(1)
Duel.RegisterEffect(e1,0)

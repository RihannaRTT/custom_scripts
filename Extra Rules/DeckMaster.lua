--Deck Master Mode
local function f2(c)
	return c:GetFlagEffect(111004250)>0
end
local gmg,gmgct,gfg,gfgct,iemc,smc,iet,selt=Duel.GetMatchingGroup,Duel.GetMatchingGroupCount,Duel.GetFieldGroup,Duel.GetFieldGroupCount,Duel.IsExistingMatchingCard,Duel.SelectMatchingCard,Duel.IsExistingTarget,Duel.SelectTarget
Duel.GetFieldGroup=function(p,sloc,oloc)
	return gfg(p,sloc,oloc)-gmg(f2,p,sloc,oloc,nil)
end
Duel.GetFieldGroupCount=function(p,sloc,oloc)
	return #(Duel.GetFieldGroup(p,sloc,oloc))
end
Duel.GetMatchingGroup=function(f,p,sloc,oloc,exc,...)
	return gmg(f,p,sloc,oloc,exc,table.unpack{...})-gmg(f2,p,sloc,oloc,exc)
end
Duel.GetMatchingGroupCount=function(f,p,sloc,oloc,exc,...)
	return #(Duel.GetMatchingGroup(f,p,sloc,oloc,exc,table.unpack{...}))
end
Duel.IsExistingMatchingCard=function(f,p,sloc,oloc,ct,exc,...)
	return Duel.GetMatchingGroupCount(f,p,sloc,oloc,exc,table.unpack{...})>=ct
end
Duel.SelectMatchingCard=function(sp,f,p,sloc,oloc,minc,maxc,exc,...)
	return Duel.GetMatchingGroup(f,p,sloc,oloc,exc,table.unpack{...}):Select(sp,minc,maxc,nil)
end
Duel.IsExistingTarget=function(f,p,sloc,oloc,ct,exc,...)
	local t={...}
	return iet(function(c) return (not f or f(c,table.unpack(t))) and not gmg(f2,p,sloc,oloc,exc):IsContains(c) end,p,sloc,oloc,ct,exc)
end
Duel.SelectTarget=function(sp,f,p,sloc,oloc,minc,maxc,exc,...)
	local t={...}
	return selt(sp,function(c) return (not f or f(c,table.unpack(t))) and not gmg(f2,p,sloc,oloc,exc):IsContains(c) end,p,sloc,oloc,minc,maxc,exc)
end
local function f0(e,tp,eg,ep,ev,re,r,rp) for pp=0,1 do
	local g,sc=Group.CreateGroup(),Duel.GetFirstMatchingCard(Card.IsType,pp,LOCATION_DECK+LOCATION_HAND+LOCATION_EXTRA,0,nil,TYPE_MONSTER)
	while sc do
		g:AddCard(sc)
		sc=Duel.GetFirstMatchingCard(Card.IsType,pp,LOCATION_DECK+LOCATION_HAND+LOCATION_EXTRA,0,g,TYPE_MONSTER)
	end
	if #g>0 and Duel.SelectYesNo(pp,2050) then
		Duel.Hint(HINT_SELECTMSG,pp,HINTMSG_TOFIELD)
		local card=g:Select(pp,1,1,nil):GetFirst()
		local code=card:GetOriginalCode()
		Duel.Remove(card,POS_FACEUP,REASON_RULE)
		if card:IsPreviousLocation(LOCATION_HAND) then Duel.Draw(pp,1,REASON_RULE)
		elseif Duel.GetTurnCount()<=1 and card:IsPreviousLocation(LOCATION_DECK) then Duel.ShuffleDeck(pp) end
		local et={card:GetCardEffect()}
		card:ResetEffect(code,RESET_CARD)
		card:ResetEffect(code,RESET_CARD)
		local se=Effect.CreateEffect(card)
		se:SetType(EFFECT_TYPE_FIELD)
		se:SetCode(EFFECT_SUMMON_PROC)
		se:SetCondition(function(e,c,minc) if c==nil then return true end return minc==0 end)
		se:SetValue(SUMMON_TYPE_NORMAL)
		card:RegisterEffect(se)
		local e0=Effect.CreateEffect(card)
		e0:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		e0:SetCode(EVENT_FREE_CHAIN)
		e0:SetRange(LOCATION_REMOVED)
		e0:SetDescription(1151)
		e0:SetCondition(function() return Duel.IsPlayerCanSummon(pp) and Duel.CheckSummonedCount(card) and card:CheckUniqueOnField(pp) and Duel.GetTurnPlayer()==pp and (Duel.GetCurrentPhase()==PHASE_MAIN1 or Duel.GetCurrentPhase()==PHASE_MAIN2) end)
		e0:SetOperation(function()
			if _G["c"..code] and _G["c"..code].initial_effect then
				card:ResetEffect(code,RESET_CARD)
				card:ResetEffect(code,RESET_CARD)
				_G["c"..code].initial_effect(card)
			end
			card:RegisterFlagEffect(111004250,RESET_EVENT+RESETS_STANDARD,0,1)
			local e1=Effect.CreateEffect(card)
			e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
			e1:SetCode(EVENT_DESTROYED)
			e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
			e1:SetOperation(function() Duel.Win(1-pp,65) e1:Reset() end)
			e1:SetReset(RESET_EVENT+RESETS_STANDARD-RESET_TOFIELD)
			card:RegisterEffect(e1)
			Duel.Summon(pp,card,false,se)
			Duel.IncreaseSummonedCount(card)
			card:CompleteProcedure()
			if f2(card) and Duel.SpecialSummon(card,0,pp,pp,true,true,POS_FACEUP_ATTACK)==0 then
				Duel.MoveToField(card,pp,pp,LOCATION_MZONE,POS_FACEUP_ATTACK,true)
				Duel.Hint(HINT_CARD,0,code)
				Duel.RaiseSingleEvent(card,EVENT_SUMMON_SUCCESS,e1,0,pp,pp,0)
				Duel.RaiseEvent(card,EVENT_SUMMON_SUCCESS,e1,0,tp,tp,0)
			end
		end)
		card:RegisterEffect(e0)
		for _,ef in ipairs(et) do
			local e1=ef:Clone()
			e1:SetRange(LOCATION_REMOVED)
			card:RegisterEffect(e1)
		end
		card:RegisterFlagEffect(111004250,RESET_EVENT+RESETS_STANDARD,0,1)
	end
end end
local e4=Effect.GlobalEffect()
e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e4:SetCode(EVENT_STARTUP)
e4:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
e4:SetCountLimit(1)
e4:SetOperation(f0)
Duel.RegisterEffect(e4,0)

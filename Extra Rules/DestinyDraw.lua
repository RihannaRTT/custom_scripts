local function filter2(c,atk)
	return c:GetAttack()<atk
end
local function filter1(c,g)
	return g:IsExists(filter2,1,nil,c:GetAttack())
end
local function drcon(e,tp,eg,ep,ev,re,r,rp)
	local p=Duel.GetTurnPlayer()
	local stlp=e:GetLabel()
	local hflp=math.ceil(Duel.GetLP(p)/2)
	if stlp==0 then e:SetLabel(hflp) stlp=hflp end
	local g1=Duel.GetFieldGroup(p,LOCATION_MZONE,0)
	local g2=Duel.GetFieldGroup(p,0,LOCATION_MZONE)
	return Duel.GetLP(p)<=stlp and g2:GetCount()>0
		and not g1:IsExists(filter1,1,nil,g2)
end
local function drop(e,tp,eg,ep,ev,re,r,rp)
	local p=Duel.GetTurnPlayer()
	local g=Duel.GetFieldGroup(p,LOCATION_DECK,0)
	if g:GetCount()>0 and Duel.GetFlagEffect(p,511004000)==0 and Duel.SelectYesNo(p,15) then
		Duel.RegisterFlagEffect(p,511004000,nil,0,1)
		Duel.Hint(HINT_SELECTMSG,tp,15)
		local tc=g:Select(p,1,1,nil):GetFirst()
		Duel.MoveSequence(tc,0)
	end
end

--Destiny Draw
local e2=Effect.GlobalEffect()
e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e2:SetCode(EVENT_PREDRAW)
e2:SetLabel(0)
e2:SetCondition(drcon)
e2:SetOperation(drop)
Duel.RegisterEffect(e2,0)

local e2=Effect.GlobalEffect()
e2:SetType(EFFECT_TYPE_FIELD)
e2:SetCode(EFFECT_DRAW_COUNT)
e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
e2:SetTargetRange(1,1)
e2:SetValue(function()
	local ct=Duel.GetFieldGroupCount(Duel.GetTurnPlayer(),LOCATION_HAND,0)
	if ct>4 then return 1
	else return 5-ct end
end)
Duel.RegisterEffect(e2,0)

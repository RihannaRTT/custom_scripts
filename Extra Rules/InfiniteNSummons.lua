local e4=Effect.GlobalEffect()
e4:SetType(EFFECT_TYPE_FIELD)
e4:SetCode(EFFECT_SET_SUMMON_COUNT_LIMIT)
e4:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
e4:SetTargetRange(1,1)
e4:SetValue(100)
Duel.RegisterEffect(e4,0)

local function op(e,tp,eg,ep,ev,re,r,rp)
	local mc={1}
	mc[0]=1
	for p=0,1 do
		while Duel.GetFieldGroupCount(p,LOCATION_HAND,0)>mc[p] and Duel.SelectYesNo(p,99) do
			local g=Duel.GetFieldGroup(p,LOCATION_HAND,0)
			Duel.SendtoDeck(g,nil,2,REASON_RULE)
			Duel.ShuffleDeck(p)
			Duel.Draw(p,g:GetCount(),REASON_RULE)
			mc[p]=mc[p]+1
		end
		Duel.SendtoDeck(Duel.GetFieldGroup(p,LOCATION_HAND,0):Select(p,mc[p]-1,mc[p]-1,nil),nil,2,REASON_RULE)
	end
end
local e4=Effect.GlobalEffect()
e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e4:SetCode(EVENT_PHASE_START+PHASE_DRAW)
e4:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
e4:SetCountLimit(1)
e4:SetOperation(op)
Duel.RegisterEffect(e4,0)

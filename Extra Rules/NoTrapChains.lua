local e8=Effect.GlobalEffect()
e8:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e8:SetCode(EVENT_CHAINING)
e8:SetOperation(function(e,tp,eg,ep,ev,re,r,rp)
	local time=re:GetCode()
	if re:IsHasType(EFFECT_TYPE_ACTIVATE) and re:IsActiveType(TYPE_TRAP) then
		Duel.SetChainLimit(function(ef) return not (ef:IsHasType(EFFECT_TYPE_ACTIVATE) and ef:IsActiveType(TYPE_TRAP)) end)
	end
end)
Duel.RegisterEffect(e8,0)

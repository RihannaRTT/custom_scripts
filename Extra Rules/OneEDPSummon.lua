local POpCheck=aux.PendOperationCheck
aux.PendOperationCheck=function(ft1,ft2,ft)
	return POpCheck(ft1,math.min(ft2,1),ft)
end

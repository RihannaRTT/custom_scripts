local function splimit(e,c,tp,sumtp,sumpos)
	return sumtp&SUMMON_TYPE_SYNCHRO==SUMMON_TYPE_SYNCHRO
end
local e1=Effect.GlobalEffect()
e1:SetType(EFFECT_TYPE_FIELD)
e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
e1:SetTargetRange(1,1)
e1:SetTarget(splimit)
Duel.RegisterEffect(e1,0)

--Master Duel Mode w/ Skills
--Script by XGlitchy30
--Modified by Lyris on 09/27/2020

local ft={EFFECT_CANNOT_CHANGE_CONTROL, EFFECT_CANNOT_SUMMON, EFFECT_CANNOT_SPECIAL_SUMMON, EFFECT_CANNOT_MSET, EFFECT_CANNOT_DRAW, EFFECT_UNRELEASABLE_NONSUM, EFFECT_DESTROY_SUBSTITUTE, EFFECT_CANNOT_RELEASE, EFFECT_DESTROY_REPLACE, EFFECT_RELEASE_REPLACE, EFFECT_SEND_REPLACE, EFFECT_CANNOT_DISCARD_HAND, EFFECT_CANNOT_DISCARD_DECK, EFFECT_LEAVE_FIELD_REDIRECT, EFFECT_TO_HAND_REDIRECT, EFFECT_TO_DECK_REDIRECT, EFFECT_TO_GRAVE_REDIRECT, EFFECT_REMOVE_REDIRECT, EFFECT_CANNOT_TO_HAND, EFFECT_CANNOT_TO_DECK, EFFECT_CANNOT_REMOVE, EFFECT_CANNOT_TO_GRAVE, EFFECT_CANNOT_TURN_SET, EFFECT_REVERSE_DAMAGE, EFFECT_REVERSE_RECOVER, EFFECT_CHANGE_DAMAGE, EFFECT_REFLECT_DAMAGE, EFFECT_ACTIVATE_COST, EFFECT_SUMMON_COST, EFFECT_SPSUMMON_COST, EFFECT_MSET_COST, EFFECT_SSET_COST, EFFECT_RCOUNTER_REPLACE, EFFECT_LPCOST_CHANGE, EFFECT_CANNOT_BE_FUSION_MATERIAL, EFFECT_OVERLAY_REMOVE_REPLACE, EFFECT_FORBIDDEN, EFFECT_MUST_BE_FMATERIAL, EFFECT_SPSUMMON_COUNT_LIMIT, EFFECT_LEFT_SPSUMMON_COUNT, EFFECT_MATERIAL_LIMIT, EFFECT_ACTIVATION_COUNT_LIMIT, EFFECT_LIMIT_SPECIAL_SUMMON_POSITION}
local function f1(c)
	if not c:IsStatus(STATUS_EFFECT_ENABLED) then return false end
	for _,e in ipairs(ft) do
		if c:IsHasEffect(e) then return true end
	end
	return false
end
local gmg,gmgct,gfg,gfgct,iemc,smc,iet,selt,it=Duel.GetMatchingGroup,Duel.GetMatchingGroupCount,Duel.GetFieldGroup,Duel.GetFieldGroupCount,Duel.IsExistingMatchingCard,Duel.SelectMatchingCard,Duel.IsExistingTarget,Duel.SelectTarget,Card.IsType
local function f2(c)
	return c:IsOriginalSetCard(0x2889) and not it(c,TYPE_CONTINUOUS+TYPE_FIELD) and not c:GetEquipTarget()
end
local g=Group.CreateGroup()
g:KeepAlive()
Duel.GetFieldGroup=function(p,sloc,oloc)
	return gfg(p,sloc,oloc)-g:Filter(f2,nil)
end
Duel.GetFieldGroupCount=function(p,sloc,oloc)
	return #(Duel.GetFieldGroup(p,sloc,oloc))
end
Duel.GetMatchingGroup=function(f,p,sloc,oloc,exc,...)
	local f=f or aux.TRUE
	return gmg(f,p,sloc,oloc,exc,table.unpack{...})-g:Filter(f2,nil)
end
Duel.GetMatchingGroupCount=function(f,p,sloc,oloc,exc,...)
	return #(Duel.GetMatchingGroup(f,p,sloc,oloc,exc,table.unpack{...}))
end
Duel.IsExistingMatchingCard=function(f,p,sloc,oloc,ct,exc,...)
	return Duel.GetMatchingGroupCount(f,p,sloc,oloc,exc,table.unpack{...})>=ct
end
Duel.SelectMatchingCard=function(sp,f,p,sloc,oloc,minc,maxc,exc,...)
	return Duel.GetMatchingGroup(f,p,sloc,oloc,exc,table.unpack{...}):Select(sp,minc,maxc,nil)
end
Duel.IsExistingTarget=function(f,p,sloc,oloc,ct,exc,...)
	local t={...}
	return iet(function(c) return (not f or f(c,table.unpack(t))) and not g:Filter(f2,nil):IsContains(c) end,p,sloc,oloc,ct,exc)
end
Duel.SelectTarget=function(sp,f,p,sloc,oloc,minc,maxc,exc,...)
	local t={...}
	return selt(sp,function(c) return (not f or f(c,table.unpack(t))) and not g:Filter(f2,nil):IsContains(c) end,p,sloc,oloc,minc,maxc,exc)
end
function Card.IsType(c,ty)
	if f2(c) then return false end
	return it(c,ty)
end
local function f0(e,tp,eg,ep,ev,re,r,rp) for pp=0,1 do
	local sc=Duel.GetFirstMatchingCard(Card.IsOriginalSetCard,pp,LOCATION_DECK+LOCATION_HAND,0,nil,0x2889)
	local sg=Group.CreateGroup()
	while sc do
		sg:AddCard(sc)
		sc=Duel.GetFirstMatchingCard(Card.IsOriginalSetCard,pp,LOCATION_DECK+LOCATION_HAND,0,sg,0x2889)
	end
	if #sg>0 then
		Duel.Hint(HINT_SELECTMSG,pp,HINTMSG_TOFIELD)
		local card=sg:Select(pp,1,1,nil):GetFirst()
		sg:RemoveCard(card)
		g:AddCard(card)
		local code=card:GetOriginalCode()
		local rc=Duel.GetFieldCard(pp,LOCATION_SZONE,0)
		if code//10^4==12119 and not (rc and Duel.GetFieldCard(pp,LOCATION_SZONE,4)) then
			local et={card:GetActivateEffect()}
			for _,ef in ipairs(et) do
				if ef:GetLabel()~=100 then
					local ec,op=ef:GetCode(),ef:GetOperation()
					if ec~=EVENT_FREE_CHAIN then
						local e2=Effect.CreateEffect(card)
						e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
						e2:SetCode(ec)
						e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE|EFFECT_FLAG_IGNORE_IMMUNE|ef:GetProperty())
						e2:SetDescription(ef:GetDescription())
						e2:SetLabel(ef:GetLabel())
						e2:SetCondition(ef:GetCondition() or aux.TRUE)
						e2:SetOperation(function(_,_,efg,_,efv,rse,rs,rsp)
							if (card:IsOriginalCodeRule(111004001) or card:IsFacedown()) and not Duel.SelectYesNo(pp,aux.Stringid(code,15)) then return end
							if not card:IsOnField() then card:SetEntityCode(id)
							else Duel.ChangePosition(card,POS_FACEUP) end
							local dg=Duel.GetMatchingGroup(f1,pp,LOCATION_ONFIELD,LOCATION_ONFIELD,nil)
							local dt={}
							dg:ForEach(function(tc) dt[tc]={}
								for _,f in ipairs(ft) do
									dt[tc][f]={}
									for _,h in ipairs{tc:IsHasEffect(f)} do
										table.insert(dt[tc][f],h)
										h:SetCode(0)
									end
								end
							end)
							local t={[0]={},[1]={}}
							for p=0,1 do for _,f in ipairs(ft) do
								for _,pf in ipairs{Duel.IsPlayerAffectedByEffect(p,f)} do
									t[p][pf]=pf:GetCode()
									pf:SetCode(0)
								end
							end end
							if op then op(e2,pp,efg,pp,efv,rse,rs,rsp) end
							dg:ForEach(function(tc) for _,f in ipairs(ft) do
								for _,h in ipairs(dt[tc][f]) do h:SetCode(f) end
							end end)
							for p,pt in ipairs(t) do for pf,ed in pairs(pt) do
								pf:SetCode(t[p][pf])
							end end
						end)
						Duel.RegisterEffect(e2,pp)
						ef:Reset()
					else
						ef:SetType(ef:GetType()|EFFECT_TYPE_CONTINUOUS)
						ef:SetOperation(function(_,_,efg,_,efv,rse,rs,rsp)
							Duel.ChangePosition(card,POS_FACEUP)
							local dg=Duel.GetMatchingGroup(f1,pp,LOCATION_ONFIELD,LOCATION_ONFIELD,nil)
							local dt={}
							dg:ForEach(function(tc) dt[tc]={}
								for _,f in ipairs(ft) do
									dt[tc][f]={}
									for _,h in ipairs{tc:IsHasEffect(f)} do
										table.insert(dt[tc][f],h)
										h:SetCode(0)
									end
								end
							end)
							local t={[0]={},[1]={}}
							for p=0,1 do for _,f in ipairs(ft) do
								for _,pf in ipairs{Duel.IsPlayerAffectedByEffect(p,f)} do
									t[p][pf]=pf:GetCode()
									pf:SetCode(0)
								end
							end end
							if op then op(ef,pp,efg,pp,efv,rse,rs,rsp) end
							dg:ForEach(function(tc) for _,f in ipairs(ft) do
								for _,h in ipairs(dt[tc][f]) do h:SetCode(f) end
							end end)
							for p,pt in ipairs(t) do for pf,ed in pairs(pt) do
								pf:SetCode(t[p][pf])
							end end
						end)
					end
				end
			end
			local xzt={}
			for tc in aux.Next(Duel.GetFieldGroup(0,0xff,0xff)) do
				xzt[tc]={}
				for _,xz in ipairs{tc:IsHasEffect(EFFECT_DISABLE_FIELD)} do
					table.insert(xzt,xz:Clone())
					xz:Reset()
				end
			end
			while Duel.GetFlagEffect(pp,111004001)>0 do Duel.ResetFlagEffect(pp,111004001) end
			Duel.AdjustInstantly()
			if Duel.SSet(pp,card,pp,false)==0 then
				Duel.MoveToField(card,pp,pp,LOCATION_SZONE,POS_FACEDOWN,true,0x1<<(Duel.GetTurnCount()>1 and 4 or 0))
			elseif card:IsType(TYPE_CONTINUOUS) then
				if Duel.GetDuelOptions()&0x120>0 then
					Duel.MoveSequence(card,1)
					Duel.MoveSequence(card,3)
				end
				Duel.MoveSequence(card,2)
			elseif not card:IsType(TYPE_FIELD) then
				if Duel.GetDuelOptions()&0x120>0 then
					Duel.MoveSequence(card,4)
				end
				Duel.MoveSequence(card,0)
			end
			for tc in aux.Next(Duel.GetFieldGroup(0,0xff,0xff)) do
				local xze=xzt[tc]
				if xze then for _,xz in ipairs(xze) do
					tc:RegisterEffect(xze,true)
				end end
			end
			Duel.RegisterFlagEffect(pp,111004001,0,0,1)
			Duel.AdjustInstantly()
		else
			card:SetEntityCode(111004001)
			Duel.Remove(card,POS_FACEUP,REASON_RULE)
			local et={card:GetActivateEffect()}
			for _,ef in ipairs(et) do
				local op=ef:GetOperation()
				local e1=Effect.CreateEffect(card)
				e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
				e1:SetCode(ef:GetCode())
				e1:SetRange(LOCATION_REMOVED)
				e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE|ef:GetProperty())
				e1:SetDescription(ef:GetDescription())
				e1:SetLabel(ef:GetLabel())
				e1:SetLabelObject(ef:GetLabelObject())
				e1:SetCondition(ef:GetCondition() or aux.TRUE)
				e1:SetOperation(function(_,_,efg,_,efv,rse,rs,rsp)
					if card:IsOriginalCodeRule(111004001) and not Duel.SelectYesNo(pp,aux.Stringid(code,15)) then return end
					card:SetEntityCode(code)
					local dg=Duel.GetMatchingGroup(f1,pp,LOCATION_ONFIELD,LOCATION_ONFIELD,nil)
					local dt={}
					dg:ForEach(function(tc) dt[tc]={}
						for _,f in ipairs(ft) do
							dt[tc][f]={}
							for _,h in ipairs{tc:IsHasEffect(f)} do
								table.insert(dt[tc][f],h)
								h:SetCode(0)
							end
						end
					end)
					local t={[0]={},[1]={}}
					for p=0,1 do for _,f in ipairs(ft) do
						for _,pf in ipairs{Duel.IsPlayerAffectedByEffect(p,f)} do
							t[p][pf]=f
							pf:SetCode(0)
						end
					end end
					if op then op(e1,pp,efg,pp,efv,rse,REASON_RULE|rs,rsp) end
					dg:ForEach(function(tc) for _,f in ipairs(ft) do
						for _,h in ipairs(dt[tc][f]) do h:SetCode(f) end
					end end)
					for p,pt in ipairs(t) do for pf,ed in pairs(pt) do
						pf:SetCode(t[p][pf])
					end end
					Duel.SetChainLimitTillChainEnd(aux.FALSE)
				end)
				card:RegisterEffect(e1)
				ef:Reset()
			end
		end
		if card:IsPreviousLocation(LOCATION_HAND) then Duel.Draw(pp,1,REASON_RULE)
		elseif Duel.GetTurnCount()<=1 then Duel.ShuffleDeck(pp) end
		local ex=card:IsHasEffect(111004001)
		if ex~=nil then ex:GetOperation()(ex,pp,nil,pp,0,nil,0,pp) end
	end
	Duel.Exile(sg,REASON_RULE)
	if sg:IsExists(Card.IsPreviousLocation,1,nil,LOCATION_DECK) then Duel.ShuffleDeck(pp) end
	Duel.Draw(pp,sg:FilterCount(Card.IsPreviousLocation,nil,LOCATION_HAND),REASON_RULE)
end end
--enable skills
local e4=Effect.GlobalEffect()
e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e4:SetCode(EVENT_STARTUP)
e4:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
e4:SetCountLimit(1)
e4:SetOperation(f0)
Duel.RegisterEffect(e4,0)
local e3=e4:Clone()
e3:SetCode(EVENT_PHASE_START+PHASE_DRAW)
e3:SetCountLimit(4)
e3:SetCondition(function() return Duel.GetTurnCount()>1 and (not Duel.GetDuelOptions or Duel.GetDuelOptions()&0x20) end)
Duel.RegisterEffect(e3,0)

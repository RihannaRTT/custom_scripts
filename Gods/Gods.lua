--Divine Hierarchy Token
--scripted by Larry126
if not DivineHierarchy then
	DivineHierarchy = {}
	local function finish_setup()
		--cannot negate
		local e1=Effect.GlobalEffect()
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_CANNOT_DISABLE)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
		local e7=e1:Clone()
		e7:SetCode(EFFECT_CANNOT_DISEFFECT)
		--cannot negate summon
		local e2=Effect.GlobalEffect()
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetCode(EFFECT_CANNOT_DISABLE_SUMMON)
		e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
		local e3=Effect.GlobalEffect()
		e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
		e3:SetCode(EVENT_SUMMON_SUCCESS)
		e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
		e3:SetOperation(function(e) Duel.SetChainLimitTillChainEnd(function(ef) local rank=ef:GetHandler():GetFlagEffectLabel(513000065) return rank and rank>=e:GetHandler():GetFlagEffectLabel(513000065) end) end)
		--destroy equips
		local e4=Effect.GlobalEffect()
		e4:SetDescription(aux.Stringid(4012,1))
		e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_IGNORE_RANGE)
		e4:SetCode(EVENT_PHASE_START+PHASE_END)
		e4:SetCountLimit(1)
		e4:SetOperation(DivineHierarchy.desop)
		Duel.RegisterEffect(e4,0)
		--Once per turn, when a card or effect is activated that targets a Level 10 or higher DIVINE monster you control for an attack by an opponent’s monster (Quick Effect): You can change the target to this card instead.
		local e5=Effect.GlobalEffect()
		e5:SetType(EFFECT_TYPE_QUICK_O)
		e5:SetCode(EVENT_BE_BATTLE_TARGET)
		e5:SetRange(LOCATION_MZONE)
		e5:SetTarget(DivineHierarchy.cbtg)
		e5:SetOperation(DivineHierarchy.cbop)
		local e6=e5:Clone()
		e6:SetCode(EVENT_CHAINING)
		e6:SetCondition(function(e,tp,eg,ep,ev,re,r) return r~=REASON_REPLACE end)
		e6:SetCondition(DivineHierarchy.cecon)
		e6:SetOperation(DivineHierarchy.ceop)
		--rank
		local rank=Effect.GlobalEffect()
		rank:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		rank:SetCode(EVENT_ADJUST)
		rank:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_IGNORE_RANGE)
		rank:SetCondition(DivineHierarchy.hrcon)
		rank:SetOperation(DivineHierarchy.rank)
		Duel.RegisterEffect(rank,0)
		--immunes
		local immunity=Effect.GlobalEffect()
		immunity:SetType(EFFECT_TYPE_FIELD)
		immunity:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE)
		immunity:SetCode(EFFECT_IMMUNE_EFFECT)
		immunity:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
		immunity:SetTarget(DivineHierarchy.hrtg)
		immunity:SetValue(DivineHierarchy.hrfilter)
		Duel.RegisterEffect(immunity,0)
		local control=Effect.GlobalEffect()
		control:SetType(EFFECT_TYPE_FIELD)
		control:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
		control:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE)
		control:SetCode(EFFECT_CANNOT_CHANGE_CONTROL)
		control:SetTarget(DivineHierarchy.control)
		Duel.RegisterEffect(control,0)
		local rel=Effect.GlobalEffect()
		rel:SetType(EFFECT_TYPE_FIELD)
		rel:SetCode(EFFECT_CANNOT_RELEASE)
		rel:SetProperty(EFFECT_FLAG_PLAYER_TARGET+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
		rel:SetTargetRange(1,1)
		rel:SetTarget(DivineHierarchy.rellimit)
		Duel.RegisterEffect(rel,0)
		--last 1 turn
		local ep=Effect.GlobalEffect()
		ep:SetDescription(aux.Stringid(4011,15))
		ep:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		ep:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
		ep:SetRange(LOCATION_MZONE)
		ep:SetCode(EVENT_PHASE_START+PHASE_END)
		ep:SetCountLimit(1)
		ep:SetCondition(function(e) local c=e:GetHandler() local re=c:GetReasonEffect() return c:IsSummonType(SUMMON_TYPE_SPECIAL) and not (re and re:GetHandler():IsCode(10000080)) end)
		ep:SetOperation(DivineHierarchy.stgop)
		 --release limit
		local r1=Effect.GlobalEffect()
		r1:SetType(EFFECT_TYPE_SINGLE)
		r1:SetCode(EFFECT_UNRELEASABLE_SUM)
		r1:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
		r1:SetRange(LOCATION_MZONE)
		r1:SetValue(DivineHierarchy.sumlimit)
		-- immune to leaving
		local im=Effect.GlobalEffect()
		im:SetCode(EFFECT_SEND_REPLACE)
		im:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
		im:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
		im:SetRange(LOCATION_MZONE)
		im:SetTarget(DivineHierarchy.reptg)
		im:SetValue(aux.FALSE)
		local ge1=Effect.GlobalEffect()
		ge1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_GRANT)
		ge1:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE)
		ge1:SetTarget(DivineHierarchy.granttg)
		ge1:SetLabelObject(ep)
		Duel.RegisterEffect(ge1,0)
		local ge2=ge1:Clone()
		ge2:SetLabelObject(r1)
		Duel.RegisterEffect(ge2,0)
		local ge3=ge1:Clone()
		ge3:SetLabelObject(dg)
		Duel.RegisterEffect(ge3,0)
		local ge4=ge1:Clone()
		ge4:SetLabelObject(bt)
		Duel.RegisterEffect(ge4,0)
		local ge5=ge1:Clone()
		ge5:SetLabelObject(im)
		Duel.RegisterEffect(ge5,0)
		local ge6=ge1:Clone()
		ge6:SetLabelObject(e1)
		Duel.RegisterEffect(ge6,0)
		local ge7=ge1:Clone()
		ge7:SetLabelObject(e2)
		Duel.RegisterEffect(ge7,0)
		local ge8=ge1:Clone()
		ge8:SetLabelObject(e3)
		Duel.RegisterEffect(ge8,0)
		local ge9=ge1:Clone()
		ge9:SetLabelObject(e5)
		Duel.RegisterEffect(ge9,0)
		local gea=ge1:Clone()
		gea:SetLabelObject(e6)
		Duel.RegisterEffect(gea,0)
		local geb=ge1:Clone()
		geb:SetLabelObject(e7)
		Duel.RegisterEffect(geb,0)
		local gec=ge1:Clone()
		gec:SetTargetRange(LOCATION_HAND,LOCATION_HAND)
		gec:SetLabelObject(e0)
		Duel.RegisterEffect(gec,0)
	end

	function DivineHierarchy.cbfilter(c,tp)
		return c:IsFaceup() and c:IsLevelAbove(10) and c:IsAttribute(ATTRIBUTE_DIVINE) and c:IsControler(tp)
	end
	function DivineHierarchy.cbtg(e,tp,eg,ep,ev,re,r,rp,chk)
		local dt=Duel.GetAttackTarget()
		if chk==0 then return dt and DivineHierarchy.cbfilter(dt,tp) and Duel.GetAttacker():GetAttackableTarget():IsContains(e:GetHandler()) and not e:GetHandler():IsStatus(STATUS_CHAINING) end
	end
	function DivineHierarchy.cbop(e,tp,eg,ep,ev,re,r,rp)
		local c=e:GetHandler()
		if c:IsRelateToEffect(e) and not Duel.GetAttacker():IsImmuneToEffect(e) then Duel.ChangeAttackTarget(c) end
	end
	function DivineHierarchy.cecon(e,tp,eg,ep,ev,re,r,rp)
		if not re:IsHasProperty(EFFECT_FLAG_CARD_TARGET) then return false end
		local g=Duel.GetChainInfo(ev,CHAININFO_TARGET_CARDS)
		if not g or g:GetCount()~=1 then return false end
		local tc=g:GetFirst()
		local c=e:GetHandler()
		if tc==c or not DivineHierarchy.cbfilter(tc,tp) or not tc:IsLocation(LOCATION_MZONE) then return false end
		return Duel.CheckChainTarget(ev,c)
	end
	function DivineHierarchy.ceop(e,tp,eg,ep,ev,re,r,rp)
		local c=e:GetHandler()
		if c:IsRelateToEffect(e) and Duel.CheckChainTarget(ev,c) then
			Duel.ChangeTargetCard(ev,Group.CreateGroup()+c)
		end
	end
	function DivineHierarchy.granttg(e,c)
		return c:GetFlagEffect(513000065)>0 and c:IsFaceup()
	end
	function DivineHierarchy.rank1(c)
		local code1,code2=c:GetOriginalCodeRule()
		return c:GetFlagEffect(513000065)==0
			and (code1==10000000 or code1==10000020
			or code1==62180201 or code1==57793869
			or code2==10000000 or code2==10000020
			or code2==62180201 or code2==57793869
			or code1==513000135 or code2==513000135
			or code1==513000136 or code2==513000136
			)
	end
	function DivineHierarchy.rank2(c)
		local code1,code2=c:GetOriginalCodeRule()
		return code1==10000010 or code1==21208154
			or code2==10000010 or code2==21208154
			or code1==513000134 or code2==513000134
	end
	function DivineHierarchy.rank3(c)
		local code1,code2=c:GetOriginalCodeRule()
		return code1==170000170 or code2==170000170
	end
	function DivineHierarchy.desop(e,tp,eg,ep,ev,re,r,rp)
		for c in aux.Next(Duel.GetMatchingGroup(function(tc) return tc:IsFaceup() and DivineHierarchy.hrtg(nil,tc) end,tp,LOCATION_MZONE,LOCATION_MZONE,nil)) do
			local dg=Duel.GetMatchingGroup(function(tc) return tc:GetCardTargetCount()>0 and tc:GetCardTarget():IsContains(c) end,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,nil)
			dg:Merge(c:GetEquipGroup())
			Duel.Destroy(dg,REASON_EFFECT)
		end
	end
	function DivineHierarchy.hrcon(e,tp,eg,ev,ep,re,r,rp)
		return Duel.IsExistingMatchingCard(DivineHierarchy.rank1,tp,0xff,0xff,1,nil) or Duel.IsExistingMatchingCard(DivineHierarchy.rank2,tp,0xff,0xff,1,nil)
	end
	function DivineHierarchy.rank(e,tp,eg,ev,ep,re,r,rp)
		for c in aux.Next(Duel.GetMatchingGroup(DivineHierarchy.rank1,tp,0xff,0xff,nil)) do c:RegisterFlagEffect(513000065,0,0,0,1) end
		for c in aux.Next(Duel.GetMatchingGroup(DivineHierarchy.rank2,tp,0xff,0xff,nil)) do c:RegisterFlagEffect(513000065,0,0,0,2) end
		for c in aux.Next(Duel.GetMatchingGroup(DivineHierarchy.rank3,tp,0xff,0xff,nil,170000170)) do c:RegisterFlagEffect(513000065,0,0,0,3) end
	end
	function DivineHierarchy.hrtg(e,c)
		return c:GetFlagEffect(513000065)>0 and c:IsFaceup()
	end
	function DivineHierarchy.hrfilter(e,te,c)
		if not te then return false end
		local tc=te:GetOwner()
		return (te:IsActiveType(TYPE_MONSTER) and c~=tc
			and (not tc:GetFlagEffectLabel(513000065) or c:GetFlagEffectLabel(513000065)>tc:GetFlagEffectLabel(513000065)))
			or (te:IsHasCategory(CATEGORY_TOHAND+CATEGORY_DESTROY+CATEGORY_REMOVE+CATEGORY_TODECK+CATEGORY_RELEASE+CATEGORY_TOGRAVE+CATEGORY_FUSION_SUMMON)
			and te:IsActiveType(TYPE_SPELL+TYPE_TRAP))
	end
	function DivineHierarchy.control(e,c)
		return c:GetFlagEffect(513000065)>0 and c:IsFaceup() and not c:IsHasEffect(513000134)
	end
	function DivineHierarchy.rellimit(e,c,tp,sumtp)
		return c:GetFlagEffect(513000065)>0 and c:IsFaceup() and c:IsControler(1-tp)
	end
	function DivineHierarchy.sumlimit(e,c)
		if not c then return false end
		return e:GetHandler():GetFlagEffect(513000065)>0 and e:GetHandler():IsFaceup() and not c:IsControler(e:GetHandlerPlayer())
	end
	function DivineHierarchy.reptg(e,tp,eg,ep,ev,re,r,rp,chk)
		local c=e:GetHandler()
		if chk==0 then return c:IsReason(REASON_EFFECT) and r&REASON_EFFECT+REASON_RETURN==REASON_EFFECT and not c:IsReason(REASON_RULE) and c:GetFlagEffect(513000065)>0
			and re and (not re:GetHandler():GetFlagEffectLabel(513000065) or re:GetHandler():GetFlagEffectLabel(513000065)<=c:GetFlagEffectLabel(513000065)) end
		return true
	end
	function DivineHierarchy.tglimit(e,c)
		return c:GetFlagEffectLabel(513000065)
			and e:GetHandler():GetFlagEffectLabel(513000065)>c:GetFlagEffectLabel(513000065) or false
	end
	function DivineHierarchy.stgop(e,tp,eg,ep,ev,re,r,rp)
		local c=e:GetHandler()
		if c:IsPreviousLocation(LOCATION_GRAVE) then
			Duel.SendtoGrave(c,REASON_RULE)
		elseif c:IsPreviousLocation(LOCATION_DECK) then
			Duel.SendtoDeck(c,nil,2,REASON_RULE)
		elseif c:IsPreviousLocation(LOCATION_HAND) then
			Duel.SendtoHand(c,nil,REASON_RULE)
		elseif c:IsPreviousLocation(LOCATION_REMOVED) then
			Duel.Remove(c,c:GetPreviousPosition(),REASON_RULE)
		end
	end
	finish_setup()
end

--[[message
Universal single script to add or remove cards freely. Good to debug or test your card scripts.
]]
--created by puzzle edit
io=require("io")
LOCATION_EXMZONE=1024
EXILE_CARD=4096
ADD_COUNTER=8192
SAVE_FIELD=2048
LP_MACHINE=16384
DRAW_COUNT=32768

card_location=LOCATION_GRAVE

local constants={l={
[0]						="0",
[LOCATION_DECK]			="LOCATION_DECK",
[LOCATION_HAND]			="LOCATION_HAND",
[LOCATION_MZONE]		="LOCATION_MZONE",
[LOCATION_SZONE]		="LOCATION_SZONE",
[LOCATION_FZONE]		="LOCATION_FZONE",
[LOCATION_PZONE]		="LOCATION_PZONE",
[LOCATION_GRAVE]		="LOCATION_GRAVE",
[LOCATION_REMOVED]		="LOCATION_REMOVED",
[LOCATION_EXTRA]		="LOCATION_EXTRA",
[LOCATION_OVERLAY]		="LOCATION_OVERLAY",
},p={
[0]						="0",
[POS_FACEDOWN_ATTACK]	="POS_FACEDOWN",
[POS_FACEDOWN_DEFENSE]	="POS_FACEDOWN_DEFENSE",
[POS_FACEUP_ATTACK]		="POS_FACEUP_ATTACK",
[POS_FACEUP_DEFENSE]	="POS_FACEUP_DEFENSE",
[POS_FACEUP]			="POS_FACEUP",
[POS_FACEDOWN]			="POS_FACEDOWN",
}}

custom_list={
[LOCATION_DECK]=67169062,
[LOCATION_HAND]=32807846,
[LOCATION_MZONE]=83764718,
[LOCATION_SZONE]=98494543,
[LOCATION_FZONE]=73628505,
[LOCATION_PZONE]=31531170,
[LOCATION_GRAVE]=81439173,
[LOCATION_REMOVED]=75500286,
[LOCATION_EXTRA]=24094653,
[LOCATION_EXMZONE]=61583217,
[EXILE_CARD]=15256925,
[ADD_COUNTER]=75014062,
[LOCATION_OVERLAY]=27068117,
[SAVE_FIELD]=11961740,
[LP_MACHINE]=58074572,
[DRAW_COUNT]=17052477,
}
opcode_list={
[LOCATION_DECK]={TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ+TYPE_LINK,OPCODE_ISTYPE,OPCODE_NOT},
[LOCATION_HAND]={TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ+TYPE_LINK,OPCODE_ISTYPE,OPCODE_NOT},
[LOCATION_MZONE]={TYPE_MONSTER,OPCODE_ISTYPE},
[LOCATION_SZONE]={TYPE_SPELL+TYPE_TRAP,OPCODE_ISTYPE},
[LOCATION_PZONE]={TYPE_PENDULUM,OPCODE_ISTYPE},
[LOCATION_FZONE]={TYPE_FIELD,OPCODE_ISTYPE},
[LOCATION_GRAVE]=nil,
[LOCATION_REMOVED]=nil,
[LOCATION_EXTRA]={TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ+TYPE_LINK+TYPE_PENDULUM,OPCODE_ISTYPE},
[LOCATION_EXMZONE]={TYPE_FUSION+TYPE_SYNCHRO+TYPE_XYZ+TYPE_LINK+TYPE_PENDULUM,OPCODE_ISTYPE},
}

Debug.ReloadFieldBegin(DUEL_ATTACK_FIRST_TURN,5)

local g=Group.CreateGroup()
g:KeepAlive()
local n=1
function sefilter(c)
	return not g:IsContains(c) and c:GetFlagEffect(1)==0
end
function sefilter_prev(c,g)
	return not g:IsContains(c)
end
function ExileGroup(eg)
	local gg=Group.CreateGroup()
	eg:ForEach(function(tc)
		gg:Merge(tc:GetOverlayGroup())
		local e1=Effect.CreateEffect(tc)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_CANNOT_TO_HAND)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetReset(RESET_EVENT+0x1fe0000)
		tc:RegisterEffect(e1,true)
		local t={EFFECT_CANNOT_TO_DECK,EFFECT_CANNOT_REMOVE,EFFECT_CANNOT_TO_GRAVE}
		for i,code in pairs(t) do
			local ex=e1:Clone()
			ex:SetCode(code)
			tc:RegisterEffect(ex,true)
		end
	end)
	Duel.SendtoGrave(gg,REASON_RULE)
	if not Duel.Exile or Duel.Exile(eg,REASON_RULE)==0 then
		Duel.SendtoDeck(eg,nil,-2,REASON_RULE)
	end
	eg:ForEach(function(tc)
		tc:ResetEffect(0xfff0000,RESET_EVENT)
	end)
end
local counter_list={0x1, 0x3, 0x4, 0x5}
function get_save_location(c)
	if c:IsLocation(LOCATION_PZONE) then return LOCATION_PZONE
	else return c:GetLocation() end
end
function get_save_sequence(c)
	if c:IsOnField() then
		local seq=c:GetSequence()
		if c:IsLocation(LOCATION_PZONE) and seq==4 then seq=1 end
		return seq
	else return 0 end
end
function op(e,tp,eg,ep,ev,re,r,rp,c,sg,og)
	local p=e:GetHandler():GetOwner()
	local lc=e:GetLabel()
	local ctt={}
	for i=1,12 do
		table.insert(ctt,i)
	end
	if lc==4096 then
		local g=g
		local sg=Duel.GetMatchingGroup(sefilter,0,0x7f,0x7f,g)
		if #sg==0 then return end
		--reset all
		if Duel.SelectYesNo(tp,aux.Stringid(10000000,0)) then
			ExileGroup(sg)
			return
		else
			local tg=sg:Select(0,1,99,nil)
			ExileGroup(tg)
			return
		end
	end
	--lp machine
	if lc==16384 then
		local ct=Duel.AnnounceNumber(0,table.unpack(ctt))
		Duel.SetLP(p,ct*1000)
		Duel.AdjustAll()
		return
	end
	if lc==32768 then
		local ct=Duel.AnnounceNumber(0,table.unpack(ctt))
		local e1=Effect.CreateEffect(e:GetOwner())
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetCode(EFFECT_DRAW_COUNT)
		e1:SetTargetRange(1,0)
		e1:SetValue(ct)
		e1:SetReset(RESET_PHASE+PHASE_DRAW+RESET_SELF_TURN)
		Duel.RegisterEffect(e1,p)
		return
	end
	if lc==8192 then
		if #counter_list==0 then return end
		local sg=Duel.GetMatchingGroup(Card.IsFaceup,0,LOCATION_ONFIELD,LOCATION_ONFIELD,nil)
		if #sg==0 then return end
		local tc=sg:Select(0,1,1,nil):GetFirst()
		local counter=Duel.AnnounceNumber(tp,table.unpack(counter_list))
		local ct=Duel.AnnounceNumber(0,table.unpack(ctt))
		tc:AddCounter(counter,ct)
		return
	end
	if lc==128 then
		local sg=Duel.GetMatchingGroup(Card.IsType,0,LOCATION_MZONE,LOCATION_MZONE,nil,TYPE_XYZ)
		if #sg==0 then return end
		local tg=sg:Select(0,1,63,nil)
		local cd,ct=0
		if #tg==1 and Duel.GetMatchingGroupCount(sefilter,p,0x7f-lc,0,g+tg)>0 and Duel.SelectYesNo(tp,1467) then
			cd=Duel.SelectMatchingCard(0,function(tc) return sefilter(tc) and tc:IsCanOverlay() end,p,0x7f-lc,0,1,63,tg)
			ct=#cd
		else
			cd=Duel.AnnounceCard(0)
			ct=Duel.AnnounceNumber(0,table.unpack(ctt))
		end
		for tc in aux.Next(tg) do
			local xg=Group.CreateGroup()
			local d=aux.GetValueType(cd)=="number" and Duel.CreateToken(p,cd) or cd:GetFirst()
			for i=1,ct do
				d:CompleteProcedure()
				xg:AddCard(d)
				d=aux.GetValueType(cd)=="number" and Duel.CreateToken(p,cd) or cd:GetNext()
			end
			if aux.GetValueType(cd)=="number" then Duel.Remove(xg,POS_FACEDOWN,0x20400) end
			Duel.Overlay(tc,xg)
		end
		return
	end
	if lc==2048 then
		local sg=Duel.GetMatchingGroup(sefilter,0,0x7f,0x7f,g)
		local lp0,lp1=Duel.GetLP(0),Duel.GetLP(1)
		local data={
			lp={
				[0]=Duel.GetLP(0),
				[1]=Duel.GetLP(1)
			},
			cards={}
		}
		local cid=0
		local id_list={}
		local xt={
			eq={},
			tg={}
		}
		local cids={}
		for tc in aux.Next(sg) do
			cid=cid+1
			id_list[tc]=cid
			table.insert(cids,"c"..cid)
			local cdata={
				id=cid,
				code=tc:GetOriginalCode(),
				controler=tc:GetControler(),
				owner=tc:GetOwner(),
				location=get_save_location(tc),
				sequence=get_save_sequence(tc),
				position=tc:GetPosition(),
				summon_type=tc:GetSummonType(),
				summon_location=tc:GetSummonLocation(),
				overlay_cards={},
				counter={}
			}
			local og=tc:GetOverlayGroup()
			for oc in aux.Next(og) do
				cid=cid+1
				id_list[oc]=cid
				table.insert(cdata.overlay_cards, {
					id=cid,
					code=oc:GetOriginalCode(),
					owner=oc:GetOwner(),
					summon_type=oc:GetSummonType(),
					summon_location=oc:GetSummonLocation(),
				})
			end
			local qc=tc:GetEquipTarget()
			if qc then
				xt.eq[cid]=qc
				if id_list[qc] then xt.eq[cid]=id_list[qc] end
			end
			local tg=tc:GetCardTarget()
			xt.tg[cid]={}
			for gc in aux.Next(tg) do
				if id_list[gc] then
					table.insert(xt.tg[cid],id_list[gc])
				else table.insert(xt.tg[cid],gc) end
			end
			table.insert(data.cards,cdata)
		end
		for cid,qid in pairs(xt.eq) do
			if type(qid)=="Card" then
				xt.eq[cid]=id_list[qc]
			end
		end
		for cid,tgt in pairs(xt.tg) do
			if type(tgt[j])=="table" then
				for j=1,#tgt do
				if type(tgt[j])=="Card" then
					if tgt[j]~=xt.eq[cid] then
						xt.tg[cid]=id_list[tgt[j]]
					end
				end
			end
		end end
		local str="Debug.ReloadFieldBegin(DUEL_ATTACK_FIRST_TURN,5,prnt)"
		for p,l in pairs(data.lp) do str=str.."\nDebug.SetPlayerInfo("..p..","..l..",0,0)" end
		for _,dt in ipairs(data.cards) do
			str=str.."\nlocal c"..dt.id.."=Debug.AddCard("
				..dt.code..","..dt.owner..","..dt.controler
				..","..constants.l[dt.location]..","
				..dt.sequence..","..constants.p[dt.position]
				..",true)"
			if dt.summon_location>0 or dt.summon_type>0 then
			str=str.."\nDebug.PreSummon(c"..dt.id..","
				..dt.summon_type..","
				..constants.l[dt.summon_location]..")"
			end
			for _,cn in ipairs(dt.counter) do
				str=str.."\nDebug.PreAddCounter("..dt.id..","
					..cn.type..","..cn.count..")"
			end
			for _,ot in ipairs(dt.overlay_cards) do
				str=str.."\nlocal c"..ot.id.."=Debug.AddCard("..ot.code..","
					..ot.owner..","..dt.controler
					..",LOCATION_MZONE,"..dt.sequence
					..",POS_FACEUP_ATTACK,true)"
				if ot.summon_location>0 or ot.summon_type>0 then
					str=str.."\nDebug.PreSummon(c"..ot.id..","
						..ot.summon_type..","
						..constants.l[ot.summon_location or 0]..")"
				end
			end
		end
		for id,qd in pairs(xt.eq) do
			str=str.."\nDebug.PreEquip(c"..id..",c"..qd..")"
		end
		for id,t in pairs(xt.tg) do for _,td in ipairs(t) do
			if not xt.eq[id] then
				str=str.."\nDebug.PreSetTarget(c"..id..",c"
				..td..")"
			end
		end end
		str=str.."\n\nlocal xg=Group.CreateGroup("
			..table.concat(cids,",")..")"
			.."\nlocal e=Effect.GlobalEffect()"
			.."\ne:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)"
			.."\ne:SetCode(EVENT_STARTUP)"
			.."\ne:SetOperation(function() Duel.Exile(Duel.GetFieldGroup(0,LOCATION_DECK+LOCATION_EXTRA,LOCATION_DECK+LOCATION_EXTRA)-xg,REASON_RULE) end)"
			.."\nif prnt then e:GetOperation()()"
			.."\nelse Duel.RegisterEffect(e,0) end\n"
		local f=io.open("./YGO Omega_Data/Files/Scripts/Puzzle_new.lua","w+")
		f:write(str)
		f:close()
		return
	end
	local ftype=opcode_list[lc]
	local cd,ct=0,0
	if Duel.GetMatchingGroupCount(sefilter,p,0x7f-lc,0,g)>0 and Duel.SelectYesNo(0,1467) then
		cd=Duel.SelectMatchingCard(0,sefilter,p,0x7f-lc,0,1,63,g)
		ct=#cd
	elseif ftype then
		cd=Duel.AnnounceCard(0,table.unpack(ftype))
	else
		cd=Duel.AnnounceCard(0)
	end
	if aux.GetValueType(cd)=="number" then ct=Duel.AnnounceNumber(0,table.unpack(ctt)) end
	local d=aux.GetValueType(cd)=="number" and Duel.CreateToken(p,cd) or cd:GetFirst()
	for i=1,ct do
		if lc==1 then
			Duel.SendtoDeck(d,nil,0,0x20400)
		elseif lc==2 then
			Duel.SendtoHand(d,nil,0x20400)
		elseif lc==4 then
			local pos=nil
			if d:IsType(TYPE_LINK) then
				pos=POS_FACEUP_ATTACK
			else
				pos=Duel.SelectPosition(0,d,15)
			end
			Duel.MoveToField(d,0,p,lc,pos,true)
		elseif lc==8 or lc==256 or lc==512 then
			local pos=nil
			if d:IsType(TYPE_PENDULUM) then
				pos=POS_FACEUP_ATTACK
			else
				pos=Duel.SelectPosition(0,d,POS_ATTACK)
			end
			Duel.MoveToField(d,0,p,lc,pos,true)
		elseif lc==16 then
			Duel.SendtoGrave(d,0x20400)
		elseif lc==32 then
			local pos=Duel.SelectPosition(0,d,POS_ATTACK)
			Duel.Remove(d,pos,0x20400)
		elseif lc==64 then
			if d:IsType(TYPE_PENDULUM) then
				local pos=Duel.SelectPosition(0,d,POS_ATTACK)
				if pos==POS_FACEUP_ATTACK then
					Duel.SendtoExtraP(d,nil,0x20400)
				else
					Duel.SendtoDeck(d,nil,0,0x20400)
				end
			else
				Duel.SendtoDeck(d,nil,0,0x20400)
			end
		elseif lc==1024 then
			local pos=nil
			if d:IsType(TYPE_LINK) then
				pos=POS_FACEUP_ATTACK
			else
				pos=Duel.SelectPosition(0,d,15)
			end
			if not d:IsLocation(LOCATION_EXTRA) then
				if d:IsType(TYPE_PENDULUM) then
					Duel.SendtoExtraP(d,nil,0x20400)
				else
					Duel.SendtoDeck(d,nil,0,0x20400)
				end
			end
			Duel.MoveToField(d,p,p,LOCATION_MZONE,pos,true)
		end
		d:CompleteProcedure()
		d=aux.GetValueType(cd)=="number" and Duel.CreateToken(p,cd) or cd:GetNext()
	end
end
prnt=true
function reg(c,n,g)
	c:ResetEffect(c:GetOriginalCode(),RESET_CARD)
	local effect_list={
		EFFECT_CANNOT_TO_DECK,
		EFFECT_CANNOT_TO_HAND,
		EFFECT_CANNOT_REMOVE,
		EFFECT_CANNOT_SPECIAL_SUMMON,
		EFFECT_CANNOT_SUMMON,
		EFFECT_CANNOT_MSET,
		EFFECT_CANNOT_SSET,
		EFFECT_IMMUNE_EFFECT,
		EFFECT_CANNOT_BE_EFFECT_TARGET,
		EFFECT_CANNOT_CHANGE_CONTROL,
	}
	local effect_list_0={
		EFFECT_CHANGE_TYPE,
	}
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_IGNITION+EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetProperty(EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_SET_AVAILABLE+EFFECT_FLAG_BOTH_SIDE)
	e2:SetRange(0xff)
	e2:SetLabel(n)
	e2:SetOperation(op)
	c:RegisterEffect(e2)
	for i,v in pairs(effect_list) do
		local e6=Effect.CreateEffect(c)
		e6:SetType(EFFECT_TYPE_SINGLE)
		e6:SetCode(v)
		e6:SetProperty(0x40500+EFFECT_FLAG_IGNORE_IMMUNE)
		e6:SetValue(aux.TRUE)
		c:RegisterEffect(e6)
	end
	for i,v in pairs(effect_list_0) do
		local e6=Effect.CreateEffect(c)
		e6:SetType(EFFECT_TYPE_SINGLE)
		e6:SetCode(v)
		e6:SetProperty(0x40500+EFFECT_FLAG_IGNORE_IMMUNE)
		e6:SetValue(0)
		c:RegisterEffect(e6)
	end
	g:AddCard(c)
end
do
	local s, var, t = custom_list, 1, {4,0,1,2,3,11,9,8,7,15,14,13,12,6,5,10}
	while true do
		if t[var] == nil then break end
		local card_value,card_code,var_1 = 2^t[var],s[2^t[var]],var+1
		if s[card_value] == nil then break end
		var = var_1
		if card_code and card_code~=0 then
			local a0=Debug.AddCard(card_code,0,0,card_location,0,POS_FACEUP_ATTACK)
			local a1=Debug.AddCard(card_code,1,1,card_location,0,POS_FACEUP_ATTACK)
			reg(a0,card_value,g)
			reg(a1,card_value,g)
		end
	end
end
Debug.SetPlayerInfo(0,8000,0,0)
Debug.SetPlayerInfo(1,8000,0,0)

local load_result=Duel.LoadScript("./single/Puzzle_new.lua")
local ex=Effect.GlobalEffect()
ex:SetType(EFFECT_TYPE_FIELD)
ex:SetCode(EFFECT_TRAP_ACT_IN_SET_TURN)
ex:SetProperty(EFFECT_FLAG_SET_AVAILABLE)
ex:SetTargetRange(LOCATION_SZONE,LOCATION_SZONE)
Duel.RegisterEffect(ex,0)
local ex=Effect.GlobalEffect()
ex:SetType(EFFECT_TYPE_FIELD)
ex:SetCode(EFFECT_TRAP_ACT_IN_HAND)
ex:SetTargetRange(LOCATION_HAND,LOCATION_HAND)
Duel.RegisterEffect(ex,0)
local ex=Effect.GlobalEffect()
ex:SetType(EFFECT_TYPE_FIELD)
ex:SetCode(EFFECT_QP_ACT_IN_SET_TURN)
ex:SetProperty(EFFECT_FLAG_SET_AVAILABLE)
ex:SetTargetRange(LOCATION_SZONE,LOCATION_SZONE)
Duel.RegisterEffect(ex,0)
local ex=Effect.GlobalEffect()
ex:SetType(EFFECT_TYPE_FIELD)
ex:SetCode(EFFECT_QP_ACT_IN_NTPHAND)
ex:SetTargetRange(LOCATION_HAND,LOCATION_HAND)
Duel.RegisterEffect(ex,0)
local ex=Effect.GlobalEffect()
ex:SetType(EFFECT_TYPE_FIELD)
ex:SetCode(EFFECT_HAND_LIMIT)
ex:SetTargetRange(1,1)
ex:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
ex:SetValue(100)
Duel.RegisterEffect(ex,0)

local e=Effect.GlobalEffect()
e:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
e:SetCode(EVENT_STARTUP)
e:SetOperation(function()
	Duel.Exile(Duel.GetFieldGroup(0,LOCATION_DECK+LOCATION_EXTRA,LOCATION_DECK+LOCATION_EXTRA),REASON_RULE)
	Debug.ReloadFieldBegin(DUEL_ATTACK_FIRST_TURN,5,true)
	pcall(dofile,"./YGO Omega_Data/Files/Scripts/Puzzle_new.lua")
end)
Duel.RegisterEffect(e,0)

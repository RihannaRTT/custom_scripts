Contributions to 

Fluorohydride: https://github.com/Fluorohydride/ygopro-scripts
jianmos90: https://code.mycard.moe/mycard/ygopro-rush-duel/ 
ProjectIgnis: https://github.com/ProjectIgnis/CardScripts

Note that many base scripts were taken from older projects and zips that were found on ygopro.co 
These base scripts have since been modified to work with YGO Omega core. Any licenses or works that
were made thereafter, postdate those base scripts. 

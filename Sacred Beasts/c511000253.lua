--混沌幻魔アーミタイル (Anime)
--Armityle the Chaos Phantom (Anime)
--fixed by TOP
local s,id=c511000253,511000253
function s.initial_effect(c)
	c:EnableReviveLimit()
	aux.AddFusionProcMix(c,true,true,6007213,32491822,69890967)
	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_ADD_CODE)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e0:SetValue(43378048)
	c:RegisterEffect(e0)
	--Special Summon with Dimension Fusion Destruction
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e1:SetCode(EFFECT_SPSUMMON_CONDITION)
	e1:SetValue(function(e,se,sp,st) return se:GetHandler():IsCode(100000365) end)
	c:RegisterEffect(e1)
	--Cannot be Destroyed by Battle
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e2:SetValue(1)
	c:RegisterEffect(e2)
	--attack
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(6142213,0))
	e3:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e3:SetType(EFFECT_TYPE_IGNITION)
	e3:SetCountLimit(1)
	e3:SetRange(LOCATION_MZONE)
	e3:SetTarget(s.atktg)
	e3:SetOperation(s.atkop)
	c:RegisterEffect(e3)
	--Control
	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(65305468,0))
	e4:SetCategory(CATEGORY_CONTROL)
	e4:SetType(EFFECT_TYPE_IGNITION)
	e4:SetCountLimit(1)
	e4:SetRange(LOCATION_MZONE)
	e4:SetTarget(s.cttg)
	e4:SetOperation(s.ctop)
	c:RegisterEffect(e4)
	--Dimension Fusion Destruction Special Summon Success
	local e5=Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e5:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_DAMAGE_STEP)
	e5:SetCode(EVENT_SPSUMMON_SUCCESS)
	e5:SetCondition(s.sscon)
	e5:SetOperation(s.ssop)
	c:RegisterEffect(e5)
	--immunities
	local immunity=Effect.CreateEffect(c)
	immunity:SetType(EFFECT_TYPE_SINGLE)
	immunity:SetCode(EFFECT_IMMUNE_EFFECT)
	immunity:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_SINGLE_RANGE)
	immunity:SetRange(LOCATION_MZONE)
	immunity:SetValue(aux.TargetBoolFunction(Effect.IsActiveType,TYPE_TRAP))
	c:RegisterEffect(immunity)
	local ep=Effect.CreateEffect(c)
	ep:SetDescription(aux.Stringid(4011,15))
	ep:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	ep:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
	ep:SetRange(LOCATION_MZONE)
	ep:SetCode(EVENT_PHASE+PHASE_END)
	ep:SetCountLimit(1)
	ep:SetOperation(s.stgop)
	c:RegisterEffect(ep)
	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(4012,1))
	e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE+EFFECT_FLAG_IGNORE_RANGE)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCode(EVENT_PHASE_START+PHASE_END)
	e4:SetCountLimit(1)
	e4:SetOperation(s.desop)
	c:RegisterEffect(e4)
end
function s.stgop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local se=c:GetReasonEffect()
	if c:IsSummonType(SUMMON_TYPE_SPECIAL) and se and se:IsActiveType(TYPE_SPELL) then Duel.SendtoGrave(c,REASON_RULE) end
end
function s.desop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	for _,ef in ipairs({c:GetCardEffect()}) do if ef then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_IMMUNE_EFFECT)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE+EFFECT_FLAG_IGNORE_IMMUNE)
		e1:SetValue(function(e,te) return ef:IsActiveType(TYPE_SPELL) and te==ef end)
		e1:SetReset(RESET_EVENT+RESETS_STANDARD)
		c:RegisterEffect(e1,true)
	end end
	local dg=Duel.GetMatchingGroup(function(tc) return tc:GetCardTargetCount()>0 and tc:GetCardTarget():IsContains(c) end,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,nil)
	dg:Merge(c:GetEquipGroup())
	Duel.Destroy(dg:Filter(Card.IsType,nil,TYPE_SPELL),REASON_RULE)
end
s.listed_names={100000365}
function s.atktg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	local c=e:GetHandler()
	if chkc then return chkc:GetControler()~=tp and chkc:IsLocation(LOCATION_MZONE) end
	if chk==0 then return c:IsAttackable()
		and Duel.IsExistingTarget(nil,tp,0,LOCATION_MZONE,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TARGET)
	Duel.SelectTarget(tp,nil,tp,0,LOCATION_MZONE,1,1,nil)
end
function s.atkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tc=Duel.GetFirstTarget()
	if c:IsRelateToEffect(e) and c:IsFaceup() and tc and tc:IsRelateToEffect(e) 
		and c:IsAttackable() and not c:IsImmuneToEffect(e) and not tc:IsImmuneToEffect(e) then
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_DAMAGE)
		e1:SetCode(EFFECT_SET_ATTACK_FINAL)
		e1:SetValue(10000)
		c:RegisterEffect(e1)
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetCode(EFFECT_MUST_ATTACK)
		c:RegisterEffect(e2)
		local e3=e2:Clone()
		e3:SetCode(EFFECT_MUST_ATTACK_MONSTER)
		e3:SetLabelObject(tc)
		e3:SetValue(s.matg)
		c:RegisterEffect(e3)
	end
end
function s.matg(e,c)
	return c==e:GetLabelObject()
end
function s.cttg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():IsControlerCanBeChanged() end
	Duel.SetOperationInfo(0,CATEGORY_CONTROL,e:GetHandler(),1,0,0)
end
function s.ctop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if not c:IsRelateToEffect(e) or not c:IsControler(tp) then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOZONE)
	if Duel.GetControl(c,1-tp,0) then
		local e1=Effect.CreateEffect(c)
		e1:SetCategory(CATEGORY_REMOVE+CATEGORY_CONTROL)
		e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
		e1:SetCode(EVENT_PHASE+PHASE_END)
		e1:SetRange(LOCATION_MZONE)
		e1:SetCountLimit(1)
		e1:SetTarget(s.furytg)
		e1:SetOperation(s.furyop)
		e1:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
		c:RegisterEffect(e1)
	end
end
function s.furytg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	local g=Duel.GetMatchingGroup(Card.IsAbleToRemove,tp,LOCATION_ONFIELD,0,e:GetHandler())
	Duel.SetOperationInfo(0,CATEGORY_REMOVE,g,#g,0,0)
end
function s.furyop(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(Card.IsAbleToRemove,tp,LOCATION_ONFIELD,0,aux.ExceptThisCard(e))
	if Duel.Remove(g,POS_FACEUP,REASON_EFFECT) then
		local c=e:GetHandler()
		c:RegisterFlagEffect(id,RESET_EVENT+RESETS_STANDARD,0,1)
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_REMOVE_BRAINWASHING)
		e1:SetProperty(EFFECT_FLAG_SET_AVAILABLE)
		e1:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE)
		e1:SetTarget(s.rettg)
		e1:SetLabelObject(c)
		Duel.RegisterEffect(e1,tp)
		--force adjust
		local e2=Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		e2:SetCode(EVENT_CHAIN_SOLVED)
		e2:SetProperty(EFFECT_FLAG_DELAY)
		e2:SetLabelObject(e1)
		Duel.RegisterEffect(e2,tp)
		--reset
		local e3=Effect.CreateEffect(c)
		e3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
		e3:SetCode(EVENT_CHAIN_SOLVED)
		e3:SetLabelObject(e2)
		e3:SetLabel(Duel.GetChainInfo(0,CHAININFO_CHAIN_ID))
		e3:SetOperation(s.reset)
		Duel.RegisterEffect(e3,tp)
	end
end
function s.sscon(e,tp,eg,ep,ev,re,r,rp)
	return re and re:GetHandler():IsCode(100000365)
end
function s.ssop(e,tp,eg,ep,ev,re,r,rp)
	e:GetHandler():CompleteProcedure()
end
function s.rettg(e,c)
	return c==e:GetLabelObject() and c:GetFlagEffect(9720537)==1
end
function s.reset(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetChainInfo(ev,CHAININFO_CHAIN_ID)==e:GetLabel() then
		local e2=e:GetLabelObject()
		local e1=e2:GetLabelObject()
		local tc=e1:GetLabelObject()
		tc:ResetFlagEffect(id)
		e1:Reset()
		e2:Reset()
		e:Reset()
	end
end


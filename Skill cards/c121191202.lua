--coded by Lyris
--Final Draw
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act(c))
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_ACTIVATE)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetCondition(s.acon(c))
	e3:SetOperation(s.aop(c))
	c:RegisterEffect(e3)
	local e1 = e3:Clone()
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetRange(LOCATION_SZONE)
	c:RegisterEffect(e1)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_DRAW)
	e4:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e4:SetOperation(s.reptg)
	c:RegisterEffect(e4)
end
function s.con(c)
	return function(e, tp)
		return (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(c)
	return function()
		Duel.Hint(HINT_CARD, 0, id)
		c:SetTurnCounter(3)
	end
end
function s.acon(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id) and c:IsFaceup() and c:GetFlagEffect(id) == 0 and c:GetTurnCounter() > 0
	end
end
function s.aop(c)
	return function()
		Duel.Hint(HINT_CARD, 0, id)
		c:SetTurnCounter(c:GetTurnCounter() - 1)
		c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	end
end
function s.filter(c, g)
	return c:IsLocation(LOCATION_DECK) or g:IsContains(c)
end
function s.reptg(e, tp, eg, ep, ev, re, r, rp)
	if rp ~= tp or e:GetOwner():GetTurnCounter() > 0 or not eg or #eg == 0 then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	local dg = eg:Clone()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, 1, nil, eg)
	if g:GetFirst():IsLocation(LOCATION_DECK) then
		Duel.SendtoHand(g, nil, 0)
		Duel.ConfirmCards(1 - tp, g)
		Duel.ShuffleDeck(tp)
		Duel.SendtoDeck(eg - g, tp, SEQ_DECKTOP, 0)
	else
		Duel.ConfirmCards(1 - tp, g)
	end
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
	e1:SetCode(EVENT_PHASE_START + PHASE_END)
	e1:SetCountLimit(1)
	e1:SetOperation(s.lop)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
end
function s.lop(e, tp)
	Duel.Win(1 - tp, 65)
end

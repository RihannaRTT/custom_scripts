--coded by Lyris
--TCG "Prescience"
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetDescription(1158)
	e3:SetCondition(s.lookcon)
	e3:SetOperation(s.look)
	c:RegisterEffect(e3)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLP(tp) * 2 < Duel.GetLP(1 - tp) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.lookcon(e, tp)
	local c = e:GetOwner()
	return c:IsOriginalCodeRule(id) and c:IsFaceup() and (Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0 and Duel.GetDecktopGroup(tp, 1):GetFirst():IsFacedown() or Duel.GetFieldGroupCount(tp, 0, LOCATION_DECK) > 0 and Duel.GetDecktopGroup(1 - tp, 1):GetFirst():IsFacedown())
end
function s.look(e, tp, c, sg)
	local g = Duel.GetDecktopGroup(tp, 1) + Duel.GetDecktopGroup(1 - tp, 1)
	Duel.ConfirmCards(tp, g:Filter(Card.IsFacedown, nil))
end

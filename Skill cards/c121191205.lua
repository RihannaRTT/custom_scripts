--coded by Lyris
--TCG "Peak Performance"
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_CANNOT_TO_DECK)
	e1:SetRange(0xff)
	c:RegisterEffect(e1)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_ACTIVATE)
	e4:SetCode(EVENT_FREE_CHAIN)
	e4:SetLabel(100)
	c:RegisterEffect(e4)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_FZONE)
	e2:SetTargetRange(LOCATION_MZONE, LOCATION_MZONE)
	e2:SetTarget(s.target)
	e2:SetValue(200)
	c:RegisterEffect(e2)
	local e3 = e2:Clone()
	e3:SetCode(EFFECT_UPDATE_DEFENSE)
	c:RegisterEffect(e3)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		if not Duel.MoveToField(c, tp, tp, LOCATION_FZONE, POS_FACEUP, true) then
			Duel.MoveSequence(c, 5)
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
end
function s.target(e, c)
	return c:IsRace(RACE_DRAGON + RACE_WINDBEAST + RACE_THUNDER) and (c:IsControler(e:GetHandlerPlayer()) or not c:IsHasEffect(121191258))
end

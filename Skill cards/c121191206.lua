--coded by Lyris
--TCG "Beatdown!"
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.acon(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_UPDATE_ATTACK)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetTargetRange(LOCATION_MZONE, 0)
	e3:SetLabel(0)
	e3:SetCondition(s.con)
	e3:SetValue(s.val)
	c:RegisterEffect(e3)
	e2:SetLabelObject(e3)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_TURN_END)
	e4:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e4:SetCondition(s.con)
	e4:SetOperation(s.flip(c, e3))
	c:RegisterEffect(e4)
end
function s.acon(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	e:GetLabelObject():SetLabel(Duel.GetMatchingGroupCount(s.cfilter, e:GetHandlerPlayer(), LOCATION_MZONE, 0, nil))
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsLevelAbove(5)
end
function s.con(e)
	local c = e:GetOwner()
	return c:IsOriginalCodeRule(id) and c:IsFaceup()
end
function s.val(e, c)
	return e:GetLabel() * 300
end
function s.flip(c, ef)
	return function(e, tp)
		if Duel.GetTurnPlayer() == tp then
			ef:SetLabel(0)
			if c:IsOnField() then
				Duel.ChangePosition(c, POS_FACEDOWN)
			else
				c:SetEntityCode(111004001)
			end
		end
	end
end

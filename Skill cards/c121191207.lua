--coded by Lyris
--Dragon Caller
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.fcon)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_SUMMON_SUCCESS)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCondition(s.con1(c))
	e2:SetOperation(s.act(0))
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetCondition(s.con2(c))
	e3:SetOperation(s.act(1))
	c:RegisterEffect(e3)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con1(c)
	return function(e, tp, eg)
		local tc = eg:GetFirst()
		return tc:GetSummonPlayer() == tp and tc:IsCode(17985575) and c:IsOriginalCodeRule(id) and c:IsFaceup() and c:GetFlagEffect(id) == 0
	end
end
function s.con2(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil) and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0
	end
end
function s.cfilter(c)
	return c:IsCode(43973174) and not c:IsPublic()
end
function s.filter(c, n)
	if n ~= 0 then
		return c:IsCode(17985575)
	else
		return c:IsCode(43973174)
	end
end
function s.act(n)
	return function(e, tp)
		if n == 0 and not Duel.SelectEffectYesNo(tp, e:GetOwner()) then
			return
		end
		Duel.Hint(HINT_CARD, 0, id)
		if n ~= 0 then
			Duel.ConfirmCards(1 - tp, Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_HAND, 0, 1, 1, nil))
		end
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, 1, nil, n)
		if #g > 0 then
			Duel.SendtoHand(g, nil, 0)
			Duel.ConfirmCards(1 - tp, g)
		end
		e:GetOwner():RegisterFlagEffect(id, 0, 0, 1)
	end
end

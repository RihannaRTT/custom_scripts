--coded by Lyris
--TCG "Last Gamble"
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e2:SetCondition(s.fcon(c))
	e2:SetOperation(s.flip(c))
	Duel.RegisterEffect(e2, 0)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_ACTIVATE)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetCountLimit(1, EFFECT_COUNT_CODE_SINGLE)
	e3:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e3:SetCondition(s.con(c))
	e3:SetOperation(s.act)
	c:RegisterEffect(e3)
	local e1 = e3:Clone()
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetRange(LOCATION_SZONE)
	c:RegisterEffect(e1)
end
function s.fcon(c)
	return function()
		return (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnCount() == 5
	end
end
function s.flip(c)
	return function()
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
		Duel.Hint(HINT_CARD, 0, id)
	end
end
function s.con(c)
	return function(e, tp)
		return c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetLP(tp) > 100 and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 1
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.SetLP(tp, 100)
	Duel.DiscardHand(tp, nil, 2, 2, REASON_DISCARD)
	Duel.BreakEffect()
	Duel.Draw(tp, Duel.TossDice(tp, 1), 0)
end

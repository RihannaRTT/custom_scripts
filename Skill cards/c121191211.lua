--coded by Lyris
--TCG "Aroma Strategy"
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
		local e2 = Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e2:SetCode(EVENT_FREE_CHAIN)
		e2:SetDescription(1158)
		e2:SetCondition(s.lcon)
		e2:SetOperation(s.look)
		Duel.RegisterEffect(e2, tp)
	end
end
function s.lcon(e, tp)
	return Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0 and Duel.GetDecktopGroup(tp, 1):GetFirst():IsFacedown()
end
function s.look(e, tp)
	Duel.ConfirmCards(tp, Duel.GetDecktopGroup(tp, 1))
end

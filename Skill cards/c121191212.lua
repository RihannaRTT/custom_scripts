--coded by Lyris
--TCG "Flight of the Harpies"
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
end
function s.count(e, tp)
	local plp, clp = e:GetLabel(), Duel.GetLP(tp)
	e:SetLabel(clp)
	if plp <= clp then
		return
	end
	s[tp] = s[tp] - clp + plp
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and Duel.GetDrawCount(tp) > 0 and s[tp] >= 1800 and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_DECK, 0, 1, nil, 0x64)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_DRAW_COUNT)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_DRAW)
	e1:SetValue(0)
	Duel.RegisterEffect(e1, tp)
	s[tp] = 0
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local tc = Duel.SelectMatchingCard(tp, Card.IsSetCard, tp, LOCATION_DECK, 0, 1, 1, nil, 0x64):GetFirst()
	if tc then
		Duel.SendtoHand(tc, nil, 0)
		Duel.ConfirmCards(1 - tp, tc)
		local c = e:GetOwner()
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEDOWN)
		else
			c:SetEntityCode(111004001)
		end
	end
end

--coded by Lyris
--Call of the Haunted
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.set(c))
	c:RegisterEffect(e0)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e0:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e0:SetCountLimit(1)
	e0:SetOperation(s.rcon(c))
	Duel.RegisterEffect(e0, 0)
	local e5 = Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE)
	e5:SetCode(EFFECT_CANNOT_TO_DECK)
	e5:SetRange(0xff)
	c:RegisterEffect(e5)
	local e1 = Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetHintTiming(0, TIMING_END_PHASE)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e1:SetLabel(100)
	e1:SetCondition(s.condition)
	e1:SetTarget(s.target)
	e1:SetOperation(s.operation)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_SINGLE)
	e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e2:SetCode(EVENT_LEAVE_FIELD_P)
	e2:SetOperation(s.checkop)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_SINGLE)
	e3:SetCode(EVENT_LEAVE_FIELD)
	e3:SetOperation(s.desop)
	e3:SetLabelObject(e2)
	c:RegisterEffect(e3)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
	e4:SetRange(LOCATION_SZONE)
	e4:SetCode(EVENT_LEAVE_FIELD)
	e4:SetCondition(s.descon2)
	e4:SetOperation(s.desop2)
	c:RegisterEffect(e4)
end
function s.set(c)
	return function(e, tp)
		if not Duel.MoveToField(c, tp, tp, LOCATION_SZONE, POS_FACEDOWN, true, 0xe) then
			c:SetEntityCode(id)
			if Duel.GetDuelOptions() & 0x120 > 0 then
				Duel.MoveSequence(c, 1)
				Duel.MoveSequence(c, 3)
			end
			Duel.MoveSequence(c, 2)
		end
	end
end
function s.rcon(c)
	return function(e, tp)
		local tp = c:GetControler()
		if Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 then
			Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0):GetFirst(), nil, SEQ_DECKBOTTOM, REASON_RULE)
		end
	end
end
function s.condition(e, tp)
	return Duel.GetTurnCount() > 2
end
function s.filter(c, e, tp)
	return c:IsCanBeSpecialSummoned(e, 0, tp, false, false)
end
function s.target(e, tp, eg, ep, ev, re, r, rp, chk, chkc)
	if chkc then
		return chkc:GetLocation() == LOCATION_GRAVE and chkc:GetControler() == tp and chkc:IsCanBeSpecialSummoned(e, 0, tp, false, false)
	end
	if chk == 0 then
		return Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.IsExistingTarget(s.filter, tp, LOCATION_GRAVE, 0, 1, nil, e, tp)
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	local g = Duel.SelectTarget(tp, s.filter, tp, LOCATION_GRAVE, 0, 1, 1, nil, e, tp)
	Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, g, 1, 0, 0)
end
function s.operation(e, tp)
	local c = e:GetHandler()
	local tc = Duel.GetFirstTarget()
	if c:IsRelateToEffect(e) and tc:IsRelateToEffect(e) and Duel.SpecialSummonStep(tc, 0, tp, tp, false, false, POS_FACEUP_ATTACK) then
		c:SetCardTarget(tc)
		Duel.SpecialSummonComplete()
	end
end
function s.checkop(e, tp)
	if e:GetOwner():IsDisabled() then
		e:SetLabel(1)
	else
		e:SetLabel(0)
	end
end
function s.desop(e, tp)
	if e:GetLabelObject():GetLabel() ~= 0 then
		return
	end
	local tc = e:GetOwner():GetFirstCardTarget()
	if tc and tc:IsLocation(LOCATION_MZONE) then
		Duel.Destroy(tc, REASON_EFFECT)
	end
end
function s.descon2(e, tp, eg)
	local tc = e:GetOwner():GetFirstCardTarget()
	return tc and eg:IsContains(tc) and tc:IsReason(REASON_DESTROY)
end
function s.desop2(e, tp)
	Duel.Destroy(e:GetOwner(), REASON_EFFECT)
end

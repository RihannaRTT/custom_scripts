--coded by Lyris
--The World's Greatest Fisherman
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.fcon)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con(c)
	return function(e, tp)
		return Duel.CheckLPCost(tp, 500) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil, tp) and c:GetFlagEffect(id) == 0 and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.cfilter(c, tp)
	return c:IsAttribute(ATTRIBUTE_WATER) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_GRAVE, 0, 1, nil, c)
end
function s.filter(c, tc)
	return c:IsAttribute(ATTRIBUTE_WATER) and not c:IsCode(tc:GetCode()) and c:IsLevelBelow(tc:GetLevel() * 2)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	local g = Duel.SelectMatchingCard(tp, Card.IsAttribute, tp, LOCATION_HAND, 0, 1, 1, nil, ATTRIBUTE_WATER)
	Duel.SendtoGrave(g, REASON_DISCARD)
	Duel.PayLPCost(tp, 500)
	local tc = g:GetFirst()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local tg = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_GRAVE, 0, 1, 1, nil, tc)
	if #tg > 0 then
		Duel.BreakEffect()
		Duel.SendtoHand(tg, nil, 0)
		Duel.ConfirmCards(1 - tp, tg)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEDOWN)
		else
			c:SetEntityCode(111004001)
		end
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

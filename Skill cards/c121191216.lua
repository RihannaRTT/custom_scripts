--coded by Lyris
--Mythic Depths
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e5 = Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_SINGLE)
	e5:SetCode(EFFECT_CANNOT_TO_DECK)
	e5:SetRange(0xff)
	c:RegisterEffect(e5)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_ACTIVATE)
	e4:SetCode(EVENT_FREE_CHAIN)
	e4:SetLabel(100)
	c:RegisterEffect(e4)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_FZONE)
	e2:SetTargetRange(LOCATION_MZONE, LOCATION_MZONE)
	e2:SetValue(s.val)
	c:RegisterEffect(e2)
	local e3 = e2:Clone()
	e3:SetCode(EFFECT_UPDATE_DEFENSE)
	c:RegisterEffect(e3)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetRange(LOCATION_FZONE)
	e1:SetCode(EFFECT_CHANGE_CODE)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetValue(22702055)
	c:RegisterEffect(e1)
	local e6 = e1:Clone()
	e6:SetType(EFFECT_TYPE_FIELD)
	e6:SetCode(EFFECT_CHANGE_ENVIRONMENT)
	e6:SetProperty(0)
	c:RegisterEffect(e6)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		if not Duel.MoveToField(c, tp, tp, LOCATION_FZONE, POS_FACEUP, true) then
			Duel.MoveSequence(c, 5)
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
end
function s.val(e, c)
	local r = c:GetRace()
	if not c:IsControler(e:GetHandlerPlayer()) and c:IsHasEffect(121191258) then
		return 0
	elseif bit.band(r, RACE_FISH + RACE_SEASERPENT + RACE_THUNDER + RACE_AQUA) > 0 then
		return 200
	elseif bit.band(r, RACE_MACHINE + RACE_PYRO) > 0 then
		return -200
	else
		return 0
	end
end

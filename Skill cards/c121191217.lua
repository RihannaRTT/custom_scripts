--coded by Lyris
--Catch of the Day
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetCondition(s.dcon(c))
	e1:SetOperation(s.dact)
	c:RegisterEffect(e1)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_BATTLE_DESTROYING)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetCountLimit(1, EFFECT_COUNT_CODE_SINGLE)
	e3:SetCondition(s.tcon1(c))
	e3:SetOperation(s.tact)
	c:RegisterEffect(e3)
	local e4 = e3:Clone()
	e4:SetCode(EVENT_BATTLE_DAMAGE)
	e4:SetCondition(s.tcon2(c))
	c:RegisterEffect(e4)
end
function s.con(c)
	return function(e, tp)
		return (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.dcon(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, nil) and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id) and c:IsFaceup()
	end
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsCode(3643300)
end
function s.dfilter(c)
	return c:IsCode(22702055) and c:GetActivateEffect():IsActivatable(tp, true, true)
end
function s.dact(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local g = Duel.SelectMatchingCard(tp, s.dfilter, tp, LOCATION_GRAVE + LOCATION_DECK, 0, 1, 1, nil)
	local tc = g:GetFirst()
	if tc then
		local fc = Duel.GetFieldCard(tp, LOCATION_FZONE, 0)
		if fc then
			Duel.SendtoGrave(fc, 0)
			Duel.BreakEffect()
		end
		Duel.MoveToField(tc, tp, tp, LOCATION_FZONE, POS_FACEUP, true)
		local te = tc:GetActivateEffect()
		te:UseCountLimit(tp, 1, true)
		local tep = tc:GetControler()
		local cost = te:GetCost()
		if cost then
			cost(te, tep, 1)
		end
		Duel.RaiseEvent(tc, 4179255, te, 0, tp, tp, Duel.GetCurrentChain())
	end
end
function s.tcon1(c)
	return function(e, tp, eg)
		return c:GetFlagEffect(id) == 0 and eg:GetFirst():IsCode(3643300) and c:IsOriginalCodeRule(id) and c:IsFaceup()
	end
end
function s.tcon2(c)
	return function(e, tp, eg, ep)
		return c:GetFlagEffect(id) == 0 and ep ~= tp and eg:GetFirst():IsCode(3643300) and c:IsOriginalCodeRule(id) and c:IsFaceup()
	end
end
function s.tfilter(c, e, tp)
	return c:IsLevelBelow(4) and c:IsAttribute(ATTRIBUTE_WATER) and c:IsCanBeSpecialSummoned(e, 0, tp, false, false, POS_FACEUP_DEFENSE)
end
function s.tact(e, tp)
	local c = e:GetOwner()
	if c:GetFlagEffect(id) > 0 or Duel.GetLocationCount(tp, LOCATION_MZONE) <= 0 or not (Duel.IsExistingMatchingCard(s.tfilter, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, nil, e, tp) and Duel.SelectYesNo(tp, aux.Stringid(id, 15))) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	Duel.SpecialSummon(Duel.SelectMatchingCard(tp, s.tfilter, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, 1, nil, e, tp), 0, tp, tp, false, false, POS_FACEUP_DEFENSE)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

--coded by Lyris
--It's My Lucky Day!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.fcon)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_CHAIN_SOLVING)
	e2:SetRange(LOCATION_SZONE + LOCATION_REMOVED)
	e2:SetCondition(s.dcon(c))
	e2:SetOperation(s.dact)
	c:RegisterEffect(e2)
	local e3 = e2:Clone()
	e3:SetCondition(s.ccon(c))
	e3:SetOperation(s.cact)
	c:RegisterEffect(e3)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.act()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.dcon(c)
	return function(e, tp, eg, ep, ev)
		local ex, ec, ct, p = Duel.GetOperationInfo(ev, CATEGORY_DICE)
		return ex and p == tp and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.CheckLPCost(tp, 1000) and c:GetFlagEffect(id) == 0
	end
end
function s.dact(e, tp)
	local c = e:GetOwner()
	if not Duel.SelectEffectYesNo(tp, c) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	Duel.PayLPCost(tp, 1000)
	if Duel.GetLP(tp) > 0 then
		local e1 = Effect.CreateEffect(e:GetOwner())
		e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e1:SetCode(EVENT_TOSS_DICE_NEGATE)
		Duel.Hint(HINT_NUMBER, ep, HINTMSG_DICE)
		e1:SetLabel(Duel.AnnounceNumber(ep, table.unpack { 1, 2, 3, 4, 5, 6 }))
		e1:SetCondition(s.con)
		e1:SetOperation(s.diceop)
		e1:SetReset(RESET_CHAIN)
		Duel.RegisterEffect(e1, tp)
	else
		Duel.AdjustAll()
	end
	c:RegisterFlagEffect(id, 0, 0, 1)
end
function s.con(e, tp)
	return ep == tp
end
function s.diceop(e, tp)
	local dc = { Duel.GetDiceResult() }
	dc[1] = e:GetLabel()
	Duel.SetDiceResult(table.unpack(dc))
	e:Reset()
end
function s.ccon(c)
	return function(e, tp, eg, ep, ev)
		local ex, ec, ct, p = Duel.GetOperationInfo(ev, CATEGORY_COIN)
		return ex and p == tp and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.CheckLPCost(tp, 1000) and c:GetFlagEffect(id) == 0
	end
end
function s.cact(e, tp)
	local c = e:GetOwner()
	if not Duel.SelectEffectYesNo(tp, c) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	Duel.PayLPCost(tp, 1000)
	if Duel.GetLP(tp) > 0 then
		local e1 = Effect.CreateEffect(e:GetOwner())
		e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e1:SetCode(EVENT_TOSS_COIN_NEGATE)
		e1:SetLabel(Duel.AnnounceCoin(tp))
		e1:SetCondition(s.con)
		e1:SetOperation(s.coinop)
		e1:SetReset(RESET_CHAIN)
		Duel.RegisterEffect(e1, tp)
	else
		Duel.AdjustAll()
	end
	c:RegisterFlagEffect(id, 0, 0, 1)
end
function s.coinop(e, tp)
	local cc = { Duel.GetCoinResult() }
	cc[1] = 1 - e:GetLabel()
	Duel.SetCoinResult(table.unpack(cc))
	e:Reset()
end

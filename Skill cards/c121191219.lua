--coded by Lyris
--Viral Infection
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.fcon)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and c:IsOriginalCodeRule(id) and c:IsFaceup() and c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local ct = Duel.DiscardHand(tp, aux.TRUE, 1, 60, REASON_DISCARD)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CARDTYPE)
	local op = Duel.AnnounceType(tp)
	local g = nil
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	if op == 0 then
		g = Duel.SelectMatchingCard(1 - tp, s.filter, 1 - tp, LOCATION_DECK, 0, ct, ct, nil, TYPE_MONSTER)
	elseif op == 1 then
		g = Duel.SelectMatchingCard(1 - tp, s.filter, 1 - tp, LOCATION_DECK, 0, ct, ct, nil, TYPE_SPELL)
	else
		g = Duel.SelectMatchingCard(1 - tp, s.filter, 1 - tp, LOCATION_DECK, 0, ct, ct, nil, TYPE_TRAP)
	end
	Duel.SendtoGrave(g, 0)
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEDOWN)
	else
		c:SetEntityCode(111004001)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.filter(c, typ)
	return c:IsType(typ)
end

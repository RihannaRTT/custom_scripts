--coded by Lyris
--Royal Flush
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetCondition(s.fcon)
	e0:SetOperation(s.flip)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	if not s.global_check then
		s.global_check = true
		local ge1 = Effect.CreateEffect(c)
		ge1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		ge1:SetCode(EVENT_ATTACK_ANNOUNCE)
		ge1:SetOperation(s.checkop)
		Duel.RegisterEffect(ge1, 0)
	end
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, nil) and Duel.CheckLPCost(tp, 1000) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil) and Duel.GetFlagEffect(tp, id) == 0 and c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.PayLPCost(tp, 1000)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_DIRECT_ATTACK)
	e1:SetProperty(EFFECT_FLAG_OATH + EFFECT_FLAG_IGNORE_IMMUNE)
	e1:SetTargetRange(LOCATION_MZONE, 0)
	e1:SetTarget(aux.TargetBoolFunction(Card.IsCode, 64788463, 90876561))
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SUMMON)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND, 0, 1, 1, nil)
	local tc = g:GetFirst()
	if tc then
		Duel.Summon(tp, tc, true, nil)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.checkop(e, tp, eg)
	local tc = eg:GetFirst()
	if tc:IsCode(64788463, 90876561) and Duel.GetAttackTarget() == nil then
		Duel.RegisterFlagEffect(tc:GetControler(), id, RESET_PHASE + PHASE_END, 0, 1)
	end
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsCode(25652259)
end
function s.filter(c)
	return c:IsCode(64788463) and c:IsSummonable(true, nil)
end

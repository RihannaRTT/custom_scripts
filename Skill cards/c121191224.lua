--coded by Lyris
--Heavy Metal Raiders
local s, id, o = GetID()
function s.initial_effect(c)
	local e6 = Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_SINGLE)
	e6:SetCode(111004001)
	e6:SetOperation(s.act(c))
	c:RegisterEffect(e6)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e0:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e0:SetCountLimit(1)
	e0:SetOperation(s.ret(c))
	Duel.RegisterEffect(e0, 0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_CANNOT_TO_DECK)
	e1:SetRange(0xff)
	c:RegisterEffect(e1)
	local e5 = Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_ACTIVATE)
	e5:SetCode(EVENT_FREE_CHAIN)
	e5:SetLabel(100)
	c:RegisterEffect(e5)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_INDESTRUCTABLE_COUNT)
	e2:SetRange(LOCATION_FZONE)
	e2:SetTargetRange(LOCATION_MZONE, 0)
	e2:SetLabel(0)
	e2:SetTarget(s.indtg)
	e2:SetValue(s.indct)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(3113667, 0))
	e3:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_O)
	e3:SetProperty(EFFECT_FLAG_DELAY + EFFECT_FLAG_DAMAGE_STEP)
	e3:SetCode(EVENT_BATTLE_DESTROYED)
	e3:SetRange(LOCATION_FZONE)
	e3:SetCountLimit(1, EFFECT_COUNT_CODE_SINGLE)
	e3:SetCondition(s.spcon1)
	e3:SetTarget(s.sptg)
	e3:SetOperation(s.spop)
	c:RegisterEffect(e3)
	local e4 = e3:Clone()
	e4:SetCode(EVENT_DESTROYED)
	e4:SetCondition(s.spcon2)
	c:RegisterEffect(e4)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		if not Duel.MoveToField(c, tp, tp, LOCATION_FZONE, POS_FACEUP, true) then
			Duel.MoveSequence(c, 5)
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
end
function s.ret(c)
	return function()
		local tp = c:GetControler()
		Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0):RandomSelect(tp, 1), nil, SEQ_DECKBOTTOM, REASON_RULE)
	end
end
function s.indtg(e, c)
	return c:IsRace(RACE_MACHINE) and c:IsAttribute(ATTRIBUTE_DARK)
end
function s.indct(e, re, r, rp)
	if bit.band(r, REASON_BATTLE) == 0 then
		return 0
	end
	local tp = e:GetHandlerPlayer()
	local a = Duel.GetAttacker()
	local tc = a:GetBattleTarget()
	if tc and tc:IsControler(1 - tp) then
		a, tc = tc, a
	end
	local dam = Duel.GetBattleDamage(tp)
	if not tc or dam <= 0 then
		return 1
	end
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetValue(dam)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD)
	tc:RegisterEffect(e1)
	return 1
end
function s.spcon1(e, tp, eg)
	local tc = eg:GetFirst()
	local bc = tc:GetBattleTarget()
	return tc:IsPreviousControler(1 - tp) and bc:IsControler(tp) and bc:GetOriginalAttribute() == ATTRIBUTE_DARK and bc:GetOriginalRace() == RACE_MACHINE
end
function s.cfilter(c)
	return c:IsReason(REASON_EFFECT) and c:IsPreviousLocation(LOCATION_ONFIELD)
end
function s.spcon2(e, tp, eg, ep, ev, re)
	if not re then
		return false
	end
	local tgp, loc = Duel.GetChainInfo(ev, CHAININFO_TRIGGERING_CONTROLER, CHAININFO_TRIGGERING_LOCATION)
	local rc = re:GetHandler()
	return tgp == tp and loc == LOCATION_MZONE and rc:GetOriginalAttribute() == ATTRIBUTE_DARK and rc:GetOriginalRace() == RACE_MACHINE and eg:IsExists(s.cfilter, 1, nil)
end
function s.spfilter(c, e, tp)
	return c:IsRace(RACE_MACHINE) and c:IsAttribute(ATTRIBUTE_DARK) and c:IsCanBeSpecialSummoned(e, 0, tp, false, false)
end
function s.sptg(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.IsExistingMatchingCard(s.spfilter, tp, LOCATION_HAND, 0, 1, nil, e, tp)
	end
	Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, nil, 1, tp, LOCATION_HAND)
end
function s.spop(e, tp)
	if Duel.GetLocationCount(tp, LOCATION_MZONE) <= 0 then
		return
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	local g = Duel.SelectMatchingCard(tp, s.spfilter, tp, LOCATION_HAND, 0, 1, 1, nil, e, tp)
	if g:GetCount() > 0 then
		Duel.SpecialSummon(g, 0, tp, tp, false, false, POS_FACEUP)
	end
end

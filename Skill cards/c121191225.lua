--coded by Lyris
--TCG "It's a Toon World!"
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_CANNOT_TO_DECK)
	e2:SetRange(0xff)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_ACTIVATE)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetLabel(100)
	c:RegisterEffect(e3)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_CHANGE_CODE)
	e1:SetRange(LOCATION_SZONE)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetValue(15259703)
	c:RegisterEffect(e1)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
		if c:IsOnField() then
			if Duel.GetDuelOptions() & 0x120 > 0 then
				Duel.MoveSequence(c, 1)
				Duel.MoveSequence(c, 3)
			end
			Duel.MoveSequence(c, 2)
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
end

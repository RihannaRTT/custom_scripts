--coded by Lyris
--Double Evolution Pill
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(e, tp)
	local c = e:GetOwner()
	local rg = Duel.GetMatchingGroup(Card.IsType, tp, LOCATION_HAND + LOCATION_GRAVE, 0, exc, TYPE_MONSTER)
	return Duel.GetTurnPlayer() == tp and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0 and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetDrawCount(tp) > 0 and Duel.CheckLPCost(tp, 1500) and rg:CheckSubGroup(s.fgoal, 2, 2, e, tp) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.IsExistingMatchingCard(s.sfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil, e, tp)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_DRAW_COUNT)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_DRAW)
	e1:SetValue(0)
	Duel.RegisterEffect(e1, tp)
	Duel.PayLPCost(tp, 1500)
	local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, e, tp)
	local exc = nil
	if #g == 1 and g:GetFirst():IsLocation(LOCATION_HAND) then
		exc = g:GetFirst()
	end
	local rg = Duel.GetMatchingGroup(Card.IsType, tp, LOCATION_HAND + LOCATION_GRAVE, 0, exc, TYPE_MONSTER)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_REMOVE)
	local sg = rg:SelectSubGroup(tp, s.fgoal, false, 2, 2, e, tp) or Group.CreateGroup()
	Duel.Remove(sg, POS_FACEUP, 0)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	local mg = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, 1, nil, e, tp)
	if #mg > 0 then
		Duel.SpecialSummon(mg, 0, tp, tp, true, false, POS_FACEUP)
	end
end
function s.fgoal(sg, e, tp)
	return sg:FilterCount(Card.IsRace, nil, RACE_DINOSAUR) == 1 and Duel.IsExistingMatchingCard(s.sfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, sg, e, tp)
end
function s.spcost_selector(c, tp, g, sg, i)
	sg:AddCard(c)
	g:RemoveCard(c)
	local flag = false
	if i < 2 then
		flag = g:IsExists(s.spcost_selector, 1, nil, tp, g, sg, i + 1)
	else
		flag = sg:FilterCount(Card.IsRace, nil, RACE_DINOSAUR) == 1
	end
	sg:RemoveCard(c)
	g:AddCard(c)
	return flag
end
function s.filter(c, e, tp)
	return c:IsRace(RACE_DINOSAUR) and c:IsLevelAbove(7) and c:IsType(TYPE_MONSTER) and c:IsCanBeSpecialSummoned(e, 0, tp, true, false)
end
function s.sfilter(c, e, tp)
	return c:IsRace(RACE_DINOSAUR) and c:IsLevelAbove(7) and c:IsType(TYPE_MONSTER) and c:IsCanBeSpecialSummoned(e, 0, tp, true, false)
end

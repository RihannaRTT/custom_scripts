--coded by Lyris
--Ectoplasmic Fortification!
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.fcon(c))
	e2:SetOperation(s.flip(c))
	c:RegisterEffect(e2)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1)
	e1:SetCondition(s.acon(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e0 = e1:Clone()
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetRange(LOCATION_SZONE)
	c:RegisterEffect(e0)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_UPDATE_ATTACK)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetTargetRange(LOCATION_MZONE, 0)
	e3:SetTarget(aux.TargetBoolFunction(Card.IsRace, RACE_ZOMBIE))
	e3:SetCondition(s.con)
	e3:SetValue(s.aval(c))
	c:RegisterEffect(e3)
	local e4 = e3:Clone()
	e4:SetCode(EFFECT_UPDATE_DEFENSE)
	e4:SetValue(s.dval(c))
	c:RegisterEffect(e4)
	local e5 = Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_FIELD)
	e5:SetCode(EFFECT_CHANGE_DAMAGE)
	e5:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e5:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e5:SetTargetRange(1, 0)
	e5:SetCondition(s.con)
	e5:SetValue(s.damval)
	c:RegisterEffect(e5)
end
function s.fcon(c)
	return function(e, tp)
		return (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.flip(c)
	return function()
		Duel.Hint(HINT_CARD, 0, id)
		c:SetTurnCounter(0)
	end
end
function s.acon(c)
	return function(e, tp)
		return s.con(e) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id) and c:IsFaceup()
	end
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsRace(RACE_ZOMBIE)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	c:SetTurnCounter(c:GetTurnCounter() + 1)
end
function s.damval(e, re, dam, r, rp, rc)
	local bc = rc ~= nil and (rc:IsControler(e:GetOwner():GetControler()) and rc or rc:GetBattleTarget())
	if r & REASON_BATTLE > 0 and rc and bc and bc:IsRace(RACE_ZOMBIE) then
		return dam * 2
	else
		return dam
	end
end
function s.con(e)
	local c = e:GetOwner()
	return c:IsOriginalCodeRule(id) and c:IsFaceup()
end
function s.aval(c)
	return function()
		return 100 * c:GetTurnCounter()
	end
end
function s.dval(c)
	return function()
		return -100 * c:GetTurnCounter()
	end
end

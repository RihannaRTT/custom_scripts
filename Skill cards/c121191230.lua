--coded by Lyris
--Millennium Necklace
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.fcon)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() ~= tp and c:IsOriginalCodeRule(id) and c:IsFaceup() and c:GetFlagEffect(id) == 0
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	if not Duel.SelectEffectYesNo(tp, c) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	Duel.SortDecktop(tp, 1 - tp, 3)
	c:RegisterFlagEffect(id, 0, 0, 1)
end

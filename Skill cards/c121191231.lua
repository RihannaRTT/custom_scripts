--coded by Lyris
--TCG "Bandit"
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLP(tp) <= 1500 and Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_SZONE, 1, nil) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONTROL)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, 0, LOCATION_SZONE, 1, 1, nil):GetFirst()
	if tc then
		Duel.MoveToField(tc, tp, tp, LOCATION_SZONE, POS_FACEDOWN, true)
	end
end
function s.filter(c)
	return c:IsFacedown() and c:GetSequence() < 5
end

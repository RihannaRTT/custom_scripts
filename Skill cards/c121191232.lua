--coded by Lyris
--Spell Proof Armor
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.acon(c))
	e1:SetOperation(s.act(c))
	c:RegisterEffect(e1)
	local e5 = Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_FIELD)
	e5:SetCode(EFFECT_IMMUNE_EFFECT)
	e5:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e5:SetTargetRange(LOCATION_MZONE, 0)
	e5:SetCondition(s.con)
	e5:SetTarget(s.filter)
	e5:SetValue(s.efilter)
	c:RegisterEffect(e5)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_DECREASE_TRIBUTE)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetTargetRange(LOCATION_HAND, 0)
	e2:SetCondition(s.con)
	e2:SetTarget(s.filter)
	e2:SetValue(0x1)
	c:RegisterEffect(e2)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_ADJUST)
	e4:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e4:SetOperation(s.flip(c))
	c:RegisterEffect(e4)
	local e3 = e4:Clone()
	e3:SetCode(EVENT_CHAIN_SOLVED)
	c:RegisterEffect(e3)
end
function s.acon(c)
	return function(e, tp)
		return c:GetFlagEffect(id) == 0 and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(c)
	return function()
		Duel.Hint(HINT_CARD, 0, id)
		c:RegisterFlagEffect(id, 0, 0, 1)
	end
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsRace(RACE_MACHINE)
end
function s.filter(e, c)
	return Duel.GetFieldGroupCount(e:GetOwner():GetControler(), LOCATION_MZONE, 0) == 0 and c:IsRace(RACE_MACHINE) and c:IsType(TYPE_NORMAL)
end
function s.efilter(e, te)
	return te:IsActiveType(TYPE_SPELL) and te:GetOwnerPlayer() ~= e:GetHandlerPlayer()
end
function s.con(e)
	local c = e:GetOwner()
	return c:IsOriginalCodeRule(id) and c:IsFaceup()
end
function s.flip(c)
	return function(e, tp)
		if Duel.IsExistingMatchingCard(s.xfilter, tp, LOCATION_GRAVE + LOCATION_ONFIELD, 0, 1, nil) and s.con(e) then
			if c:IsOnField() then
				Duel.ChangePosition(c, POS_FACEDOWN)
			else
				c:SetEntityCode(111004001)
			end
		end
	end
end

--coded by Lyris
--Tribal Synergy
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetCondition(s.fcon)
	e0:SetOperation(s.flip)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e2 = e1:Clone()
	e2:SetDescription(aux.Stringid(id, 1))
	e2:SetCondition(s.dcon(c))
	e2:SetOperation(s.dact)
	c:RegisterEffect(e2)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con(c)
	return function(e, tp)
		local ut = c:GetFlagEffectLabel(id)
		return Duel.IsExistingMatchingCard(s.filter1, tp, LOCATION_HAND, 0, 1, nil, tp) and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not ut or ut & 1 == 0)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local g1 = Duel.SelectMatchingCard(tp, s.filter1, tp, LOCATION_HAND, 0, 1, 1, nil, tp)
	local tc = g1:GetFirst()
	local t = {  }
	if tc:IsSetCard(0x4) then
		table.insert(t, 0x64)
	end
	if tc:IsSetCard(0x64) then
		table.insert(t, 0x4)
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local g2 = Duel.SelectMatchingCard(tp, s.filter2, tp, LOCATION_HAND, 0, 1, 1, g1, t)
	Duel.ConfirmCards(1 - tp, g1 + g2)
	Duel.Draw(tp, 1, 0)
	Duel.ShuffleHand(tp)
	local c = e:GetOwner()
	if c:GetFlagEffect(id) == 0 then
		c:RegisterFlagEffect(id, 0, 0, 1, 1)
	else
		c:SetFlagEffectLabel(id, c:GetFlagEffectLabel(id) | 1)
	end
end
function s.dcon(c)
	return function(e, tp)
		local ut = c:GetFlagEffectLabel(id)
		return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, 0x4) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, 0x64) and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not ut or ut & 2 == 0)
	end
end
function s.filter1(c, tp)
	local b1 = c:IsSetCard(0x4)
	local b2 = c:IsSetCard(0x64)
	if not (b1 or b2) or c:IsPublic() or not c:IsType(TYPE_MONSTER) then
		return false
	end
	local t = {  }
	if b1 then
		table.insert(t, 0x64)
	end
	if b2 then
		table.insert(t, 0x4)
	end
	return Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_HAND, 0, 1, c, t)
end
function s.filter2(c, sets)
	if c:IsPublic() or not c:IsType(TYPE_MONSTER) then
		return false
	end
	for _, set in ipairs(sets) do
		if c:IsSetCard(set) then
			return true
		end
	end
	return false
end
function s.dact(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Draw(tp, 2, 0)
	local c = e:GetOwner()
	if c:GetFlagEffect(id) == 0 then
		c:RegisterFlagEffect(id, 0, 0, 1, 2)
	else
		c:SetFlagEffectLabel(id, c:GetFlagEffectLabel(id) | 2)
	end
end
function s.filter(c, set)
	return c:IsFaceup() and c:IsSetCard(set)
end

--coded by Lyris
--Millennium Eye
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_PREDRAW)
	e1:SetCountLimit(1)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.op)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		local c = e:GetOwner()
		return Duel.GetTurnCount() < 2 and Duel.GetFieldGroupCount(tp, 0, LOCATION_HAND) > 0 and (not c:IsOriginalCodeRule(id) or c:IsFacedown())
	end
end
function s.op(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetFieldGroup(tp, 0, LOCATION_HAND)
	Duel.BreakEffect()
	Duel.ConfirmCards(tp, g)
	Duel.ShuffleHand(1 - tp)
	if Duel.SelectEffectYesNo(tp, e:GetOwner()) then
		Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, 0)
		Duel.ShuffleDeck(1 - tp)
		Duel.Draw(1 - tp, g:Filter(Card.IsLocation, nil, LOCATION_DECK):FilterCount(Card.IsPreviousControler, nil, 1 - tp), 0)
	end
end

--coded by Lyris
--TCG "Mind Scan"
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act(c))
	Duel.RegisterEffect(e2, 0)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetDescription(1158)
	e3:SetCondition(s.lookcon)
	e3:SetOperation(s.look)
	c:RegisterEffect(e3)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_ADJUST)
	e4:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e4:SetOperation(s.flip)
	c:RegisterEffect(e4)
	local e1 = e4:Clone()
	e1:SetCode(EVENT_CHAIN_SOLVED)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function()
		return (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnCount() == 3
	end
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
		Duel.Hint(HINT_CARD, 0, id)
	end
end
function s.lookcon(e, tp)
	local c = e:GetOwner()
	return c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.IsExistingMatchingCard(Card.IsFacedown, tp, 0, LOCATION_SZONE + LOCATION_FZONE, 1, nil)
end
function s.look(e, tp)
	Duel.ConfirmCards(tp, Duel.GetMatchingGroup(Card.IsFacedown, tp, 0, LOCATION_SZONE + LOCATION_FZONE, nil))
end
function s.flip(e, tp)
	local c = e:GetOwner()
	if Duel.GetLP(tp) < 3000 and c:IsOriginalCodeRule(id) and c:IsFaceup() then
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEDOWN)
		else
			c:SetEntityCode(111004001)
		end
	end
end

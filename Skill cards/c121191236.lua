--coded by Lyris
--Nightmare Sonic Blast!
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.flip)
	c:RegisterEffect(e2)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCountLimit(1)
	e1:SetCondition(s.acon(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	for a = 1, 4 do
		Duel.AddCustomActivityCounter(id, a, aux.FilterBoolFunction(Card.IsCode, 66516792))
	end
end
function s.con(c)
	return function(e, tp)
		for a = 1, 4 do
			if Duel.GetCustomActivityCount(id, tp, a) > 0 then
				return false
			end
		end
		local g = Duel.GetFieldGroup(tp, LOCATION_MZONE, 0)
		local tc = g:GetFirst()
		return #g == 1 and tc:IsFaceup() and tc:IsCode(66516792) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.acon(c)
	return function(e, tp)
		for a = 1, 4 do
			if Duel.GetCustomActivityCount(id, tp, a) > 0 then
				return false
			end
		end
		local g = Duel.GetFieldGroup(tp, LOCATION_MZONE, 0)
		local tc = g:GetFirst()
		return #g == 1 and tc:IsFaceup() and tc:IsCode(66516792) and c:IsFaceup() and c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SUMMON)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(aux.TargetBoolFunction(aux.NOT(Card.IsCode), 66516792))
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_CANNOT_MSET)
	Duel.RegisterEffect(e2, tp)
	local e3 = e1:Clone()
	e3:SetCode(EFFECT_CANNOT_FLIP_SUMMON)
	Duel.RegisterEffect(e3, tp)
	local e4 = e1:Clone()
	e4:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	Duel.RegisterEffect(e4, tp)
	local g = Duel.GetDecktopGroup(tp, 4)
	local mg = g:Filter(Card.IsType, nil, TYPE_MONSTER)
	local ct = #mg
	Duel.ConfirmDecktop(tp, 4)
	if ct == 1 or ct > 2 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local sg = mg:Select(tp, 1, 1, nil)
		Duel.SendtoHand(sg, nil, 0)
		g:Sub(sg)
	end
	if ct >= 2 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DESTROY)
		local tg = Duel.SelectMatchingCard(tp, Card.IsFaceup, tp, 0, LOCATION_ONFIELD, 1, 1, nil)
		Duel.HintSelection(tg)
		local tc = tg:GetFirst()
		if tc and not (tc:IsHasEffect(121191250) or tc:IsHasEffect(121191258)) then
			if ct > 2 then
				Duel.BreakEffect()
			end
			Duel.Destroy(tg, 0)
		end
	end
	Duel.SortDecktop(tp, tp, #g)
	for i = 1, #g do
		Duel.MoveSequence(Duel.GetDecktopGroup(tp, 1):GetFirst(), SEQ_DECKBOTTOM)
	end
end

--coded by Lyris
--Hyper Metamorphosis
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.fcon)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil, tp) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 1 and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil, tp) and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil, e, tp) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.DiscardHand(tp, nil, 2, 2, REASON_DISCARD)
	local cg = Duel.SelectReleaseGroup(tp, s.cfilter2, 1, 1, nil, tp)
	Duel.Release(cg, 0)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, 1, nil, e, tp)
	if #g > 0 then
		Duel.SpecialSummon(g, 0, tp, tp, true, true, POS_FACEUP)
	end
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CHANGE_DAMAGE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(0, 1)
	e1:SetValue(s.damval)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
end
function s.damval(e, re, val, r, rp, rc)
	return val / 2
end
function s.cfilter(c, tp)
	return c:IsFaceup() and c:IsLevelBelow(3) and c:IsRace(RACE_INSECT) and c:IsType(TYPE_NORMAL) and Duel.GetMZoneCount(tp, c) > 0
end
function s.cfilter2(c, tp)
	return c:IsLevelBelow(3) and c:IsRace(RACE_INSECT) and c:IsType(TYPE_NORMAL) and Duel.GetMZoneCount(tp, c) > 0
end
function s.filter(c, e, tp)
	return c:IsCode(48579379) and c:IsCanBeSpecialSummoned(e, 0, tp, true, true)
end

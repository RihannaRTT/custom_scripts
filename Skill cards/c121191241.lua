--coded by Lyris
--Digging for Gold
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetFieldGroupCount(tp, LOCATION_GRAVE, 0) > 2
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_REMOVE)
	local g = Duel.GetFieldGroup(tp, LOCATION_GRAVE, 0):Select(tp, 3, 3, nil)
	if #g == 3 then
		Duel.Remove(g, POS_FACEUP, 0)
		Duel.BreakEffect()
		Duel.Draw(tp, 1, 0)
	end
end

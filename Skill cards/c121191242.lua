--coded by Lyris
--Zombie Master
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.flip(c))
	c:RegisterEffect(e2)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetCondition(s.acon(c))
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
	local e1 = e0:Clone()
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetRange(LOCATION_SZONE)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.flip(c)
	return function(e, tp)
		Duel.Hint(HINT_CARD, 0, id)
		if Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
			s.act(e, c:GetControler())
		end
	end
end
function s.acon(c)
	return function(_, tp)
		return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil) and c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	local tc = g:GetFirst()
	if tc then
		Duel.HintSelection(g)
		tc:CopyEffect(17259470, RESET_EVENT + RESETS_STANDARD)
	end
	e:GetOwner():RegisterFlagEffect(id, 0, 0, 1)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.filter(c)
	return c:IsFaceup() and c:IsLevelBelow(4) and c:IsRace(RACE_ZOMBIE)
end

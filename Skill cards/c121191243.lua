--coded by Lyris
--Terror from the Deep!
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_IMMUNE_EFFECT)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetTargetRange(LOCATION_MZONE, 0)
	e3:SetTarget(aux.TargetBoolFunction(Card.IsAttribute, ATTRIBUTE_WATER))
	e3:SetCondition(s.econ)
	e3:SetValue(s.efilter)
	c:RegisterEffect(e3)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_LEAVE_FIELD)
	e4:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e4:SetCondition(s.condition)
	e4:SetOperation(s.operation)
	c:RegisterEffect(e4)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, nil) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_HAND + LOCATION_DECK + LOCATION_GRAVE, 0, nil, tp)
	if #g > 0 and Duel.SelectEffectYesNo(tp, c) then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
		local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND + LOCATION_DECK + LOCATION_GRAVE, 0, 1, 1, nil, tp):GetFirst()
		if tc then
			local fc = Duel.GetFieldCard(tp, LOCATION_FZONE, 0)
			if fc then
				Duel.SendtoGrave(fc, REASON_RULE)
				Duel.BreakEffect()
			end
			Duel.MoveToField(tc, tp, tp, LOCATION_FZONE, POS_FACEUP, true)
			local te = tc:GetActivateEffect()
			te:UseCountLimit(tp, 1, true)
			local tep = tc:GetControler()
			local cost = te:GetCost()
			if cost then
				cost(te, tep, eg, ep, ev, re, r, rp, 1)
			end
			Duel.RaiseEvent(tc, 4179255, te, 0, tp, tp, Duel.GetCurrentChain())
		end
	end
end
function s.filter(c, tp)
	return c:IsCode(22702055) and c:GetActivateEffect() and c:GetActivateEffect():IsActivatable(tp, true, true)
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsCode(76634149)
end
function s.econ(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, nil) and c:IsOriginalCodeRule(id) and c:IsFaceup()
	end
end
function s.efilter(e, te)
	return te:IsActiveType(TYPE_TRAP)
end
function s.dfilter(c, tp)
	return c:IsCode(76634149) and c:IsPreviousPosition(POS_FACEUP) and c:GetPreviousControler() == tp
end
function s.condition(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	return eg:IsExists(s.dfilter, 1, nil, tp) and c:IsOriginalCodeRule(id) and c:IsFaceup()
end
function s.operation(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	if Duel.Draw(tp, 2, 0) == 2 then
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEDOWN)
		else
			c:SetEntityCode(111004001)
		end
	end
end

--coded by Lyris
--TCG "Switcheroo"
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	local e1 = e2:Clone()
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetRange(LOCATION_SZONE)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		local ut = c:GetFlagEffectLabel(id)
		return Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and c:GetFlagEffect(id + o * 10) == 0 and (not ut or ut < 3) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil):GetFirst()
	if tc then
		Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, 0)
		Duel.ShuffleDeck(tp)
		Duel.BreakEffect()
		Duel.Draw(tp, 1, 0)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEDOWN)
		else
			c:SetEntityCode(111004001)
		end
	end
	local ct = c:GetFlagEffectLabel(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 2)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 1)
	end
	c:RegisterFlagEffect(id + o * 10, RESET_PHASE + PHASE_END, 0, 1)
end

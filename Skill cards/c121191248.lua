--coded by Lyris
--Into the Darkness Below
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil) and c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.filter(c)
	return c:IsType(TYPE_NORMAL) and c:IsRace(RACE_FIEND)
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.DiscardHand(tp, s.filter, 1, 1, REASON_DISCARD)
	Duel.BreakEffect()
	if Duel.GetMatchingGroup(s.filter, tp, LOCATION_GRAVE, 0, nil):GetClassCount(Card.GetCode) < 4 or not Duel.SelectYesNo(tp, 1109) then
		Duel.Draw(tp, 1, 0)
	else
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local g = Duel.SelectMatchingCard(tp, Card.IsType, tp, LOCATION_DECK, 0, 1, 1, nil, TYPE_MONSTER)
		Duel.SendtoHand(g, nil, 0)
		Duel.ConfirmCards(1 - tp, g)
		Duel.ShuffleHand(tp)
	end
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEDOWN)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

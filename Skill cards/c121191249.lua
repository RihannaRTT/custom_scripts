--coded by Lyris
--Knight of Legend
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.acon(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_DECREASE_TRIBUTE)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetTargetRange(LOCATION_HAND, LOCATION_HAND)
	e2:SetTarget(aux.TargetBoolFunction(Card.IsCode, 6368038))
	e2:SetCondition(s.condition)
	e2:SetValue(0x2)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_PIERCE)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetTargetRange(LOCATION_MZONE, 0)
	e3:SetTarget(aux.TargetBoolFunction(s.gaiadragon))
	e3:SetCondition(s.condition)
	c:RegisterEffect(e3)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_BATTLE_DAMAGE)
	e4:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e4:SetCondition(s.con)
	e4:SetOperation(s.op)
	c:RegisterEffect(e4)
	local e5 = Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_FIELD)
	e5:SetCode(EFFECT_CANNOT_TRIGGER)
	e5:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e5:SetProperty(EFFECT_FLAG_SET_AVAILABLE)
	e5:SetTargetRange(LOCATION_SZONE, 0)
	e5:SetTarget(s.distg)
	e5:SetCondition(s.con)
	c:RegisterEffect(e5)
end
function s.acon(c)
	return function(e, tp)
		return (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.gaiadragon(c)
	return c:IsSetCard(0xbd) or c:IsCode(66889139, 2519690)
end
function s.filter(c)
	return c:IsFaceup() and s.gaiadragon(c)
end
function s.con(e, tp)
	return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, 0xbd) and s.condition(e)
end
function s.op(e, tp, eg)
	local c = e:GetOwner()
	local tc = eg:GetFirst()
	local bc = tc:GetBattleTarget()
	if tc:IsControler(tp) and bc and bc:IsDefensePos() and (tc:IsSetCard(0xbd) or s.gaiadragon(tc)) and c:GetFlagEffect(id) == 0 and Duel.GetFieldGroupCount(tp, LOCATION_DECK) > 0 then
		Duel.Hint(HINT_CARD, 0, id)
		if Duel.Draw(tp, 2, 0) == 2 then
			Duel.ShuffleHand(tp)
			Duel.BreakEffect()
			Duel.DiscardHand(tp, aux.TRUE, 1, 1, REASON_DISCARD)
		end
		c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	end
end
function s.distg(e, c)
	return c:IsFacedown() and c:GetSequence() < 5
end
function s.condition(e)
	local c = e:GetOwner()
	return c:IsOriginalCodeRule(id) and c:IsFaceup()
end

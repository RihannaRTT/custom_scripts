--coded by Lyris
--Ritual of Black Mastery
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.acon(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(id)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetTargetRange(LOCATION_MZONE, 0)
	e3:SetTarget(aux.TargetBoolFunction(Card.IsCode, 5405694))
	e3:SetCondition(s.con)
	c:RegisterEffect(e3)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_CHAIN_SOLVED)
	e4:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e4:SetCondition(s.drcon(c))
	e4:SetOperation(s.drop)
	c:RegisterEffect(e4)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetTargetRange(LOCATION_MZONE, 0)
	e1:SetTarget(aux.TargetBoolFunction(Card.IsCode, 30208479))
	e1:SetCondition(s.con)
	e1:SetValue(s.val)
	c:RegisterEffect(e1)
	local e5 = Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e5:SetCode(EVENT_BATTLE_DESTROYED)
	e5:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e5:SetCondition(s.thcon)
	e5:SetOperation(s.thop)
	c:RegisterEffect(e5)
end
function s.acon(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil, tp) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(5405694)
end
function s.drcon(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil) and e:GetOwner():GetFlagEffect(id) == 0 and s.con(e)
	end
end
function s.cfilter(c, tp)
	return c:IsFaceup() and c:IsCode(5405694, 30208479) and c:IsSummonType(SUMMON_TYPE_RITUAL) and c:IsStatus(STATUS_SPSUMMON_TURN) and c:IsSummonPlayer(tp)
end
function s.drop(e, tp, eg, ep, ev, re, r, rp)
	if not re:IsHasType(EFFECT_TYPE_ACTIVATE) or not re:IsActiveType(TYPE_TRAP) or rp == tp then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	Duel.BreakEffect()
	Duel.Draw(tp, 1, 0)
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.thcon(e, tp, eg)
	local rc = eg:GetFirst():GetReasonCard()
	return eg:GetCount() == 1 and rc:IsControler(tp) and rc:IsCode(30208479) and s.con(e)
end
function s.thop(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, Card.IsType, tp, LOCATION_GRAVE, 0, 1, 1, nil, TYPE_SPELL)
	Duel.SendtoHand(g, nil, 0)
	Duel.ConfirmCards(1 - tp, g)
end
function s.con(e)
	local c = e:GetOwner()
	return c:IsOriginalCodeRule(id) and c:IsFaceup()
end
function s.val(e, re, rp)
	return rp ~= e:GetHandlerPlayer() and re:IsActiveType(TYPE_SPELL)
end

--coded by Lyris
--Shadow Reborn
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(Card.IsCanBeSpecialSummoned, tp, LOCATION_GRAVE, LOCATION_GRAVE, 1, nil, e, 0, tp, false, false)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.PayLPCost(tp, Duel.GetLP(tp) // 2)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	if Duel.SpecialSummon(Duel.SelectMatchingCard(tp, Card.IsCanBeSpecialSummoned, tp, LOCATION_GRAVE, LOCATION_GRAVE, 1, 1, nil, e, 0, tp, false, false), 0, tp, tp, false, false, POS_FACEUP) > 0 then
		local e4 = Effect.CreateEffect(c)
		e4:SetType(EFFECT_TYPE_FIELD)
		e4:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
		e4:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e4:SetTargetRange(0, 1)
		e4:SetValue(1)
		e4:SetReset(RESET_PHASE + PHASE_END)
		Duel.RegisterEffect(e4, tp)
	end
end

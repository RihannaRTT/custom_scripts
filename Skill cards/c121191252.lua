--coded by Lyris
--Thousand-Eyes Spell
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		if not (c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)) then
			return false
		end
		local chkf = tp
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil, e, tp, chkf)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp, chk)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_BP)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	local chkf = tp
	Duel.DiscardHand(tp, s.cfilter, 1, 1, REASON_DISCARD, nil, e, tp, chkf)
	local bf = Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, Duel.GetFusionMaterial(tp), chkf)
	local br = aux.RitualUltimateTarget(aux.FilterBoolFunction(Card.IsCode, 64631466), Card.GetOriginalLevel, "Greater", LOCATION_HAND)(e, tp, nil, 0, 0, nil, 0, 0, 0)
	local t = { br and 1057, bf and 1056 }
	local v = {  }
	for j = 0, #t - 1 do
		table.insert(v, j)
	end
	for i = #t, 1, -1 do
		if not t[i] then
			table.remove(t, i)
			table.remove(v, i)
		end
	end
	local op = v[Duel.SelectOption(tp, table.unpack(t)) + 1]
	if op ~= 0 then
		local chkf = tp
		local mg1 = Duel.GetFusionMaterial(tp)
		local sg = Duel.GetMatchingGroup(s.filter2, tp, LOCATION_EXTRA, 0, nil, e, tp, mg1, chkf)
		if #sg > 0 then
			::cancel::
			Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
			local tc = sg:Select(tp, 1, 1, nil):GetFirst()
			if tc then
				local mat1 = Duel.SelectFusionMaterial(tp, tc, mg1, nil, chkf)
				if #mat1 < 2 then
					goto cancel
				end
				tc:SetMaterial(mat1)
				Duel.SendtoGrave(mat1, REASON_MATERIAL + REASON_FUSION)
				Duel.BreakEffect()
				Duel.SpecialSummon(tc, SUMMON_TYPE_FUSION, tp, tp, false, false, POS_FACEUP)
				tc:CompleteProcedure()
			end
		end
	else
		aux.RitualUltimateOperation(aux.FilterBoolFunction(Card.IsCode, 64631466), Card.GetOriginalLevel, "Greater", LOCATION_HAND)(e, tp, eg, ep, ev, re, r, rp)
	end
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEDOWN)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.cfilter(c, e, tp, chkf)
	aux.RGCheckAdditional = aux.FilterBoolFunction(aux.NOT(Group.IsContains), c)
	local res = Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, Duel.GetFusionMaterial(tp) - c, chkf) or aux.RitualUltimateTarget(aux.FilterBoolFunction(s.filter1, c), Card.GetOriginalLevel, "Greater", LOCATION_HAND)(e, tp, nil, 0, 0, nil, 0, 0, 0)
	aux.RGCheckAdditional = nil
	return res
end
function s.filter1(c, tc)
	return c:IsCode(64631466) and c ~= tc
end
function s.filter2(c, e, tp, m, chkf)
	return c:IsType(TYPE_FUSION) and c:IsCode(63519819) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_FUSION, tp, false, false) and c:CheckFusionMaterial(m, nil, chkf)
end

--coded by Lyris
--Toon Mayhem!
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_INDESTRUCTABLE_COUNT)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetTargetRange(LOCATION_SZONE, 0)
	e1:SetTarget(s.target)
	e1:SetValue(1)
	c:RegisterEffect(e1)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
end
function s.target(e, c)
	return c:IsCode(15259703) and c:GetSequence() < 5
end

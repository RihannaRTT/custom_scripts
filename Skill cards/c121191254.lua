--coded by Lyris
--Twisted Personality
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	if not s.global_check then
		s.global_check = true
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetLabel(0)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetRange(LOCATION_SZONE + LOCATION_REMOVED)
	e3:SetDescription(1126)
	e3:SetCountLimit(1)
	e3:SetCondition(s.con(c))
	e3:SetOperation(s.act)
	c:RegisterEffect(e3)
end
function s.flip(c)
	return function()
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
		c:SetTurnCounter(0)
	end
end
function s.count(e, tp, eg, ep, ev, re, r, rp)
	local plp, c = e:GetLabel(), e:GetOwner()
	local tp = c:GetControler()
	local clp = Duel.GetLP(tp)
	e:SetLabel(clp)
	if plp <= clp then
		return
	end
	local ct = c:GetTurnCounter()
	if ct >= 3 then
		return
	end
	c:SetTurnCounter(ct + 1)
end
function s.con(c)
	return function(e, tp)
		local ct = c:GetTurnCounter()
		if not (ct > 1 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)) then
			return false
		end
		local res = Duel.GetFieldGroupCount(tp, 0, LOCATION_HAND) > 0
		if ct > 2 then
			res = res or Duel.IsExistingMatchingCard(Card.IsFaceup, tp, 0, LOCATION_ONFIELD, 1, nil)
		end
		return res
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp, c, sg)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	local hg = Duel.GetFieldGroup(tp, 0, LOCATION_HAND)
	local ct = c:GetTurnCounter()
	local b1, b2 = #hg > 0 and ct > 1, Duel.IsExistingMatchingCard(Card.IsFaceup, tp, 0, LOCATION_ONFIELD, 1, nil) and ct > 2
	if not (b1 or b2) then
		return
	end
	if b1 and (not b2 or Duel.SelectOption(tp, 501, 502) == 0) then
		c:SetTurnCounter(ct - 2)
		Duel.BreakEffect()
		local tc = hg:RandomSelect(tp, 1):GetFirst()
		Duel.SendtoGrave(tc, REASON_DISCARD)
	else
		c:SetTurnCounter(ct - 3)
		local g = Duel.SelectMatchingCard(tp, Card.IsFaceup, tp, 0, LOCATION_ONFIELD, 1, 1, nil)
		Duel.HintSelection(g)
		Duel.BreakEffect()
		local tc = g:GetFirst()
		if not tc:IsHasEffect(121191250) and not tc:IsHasEffect(121191258) then
			Duel.Destroy(g, 0)
		end
	end
end

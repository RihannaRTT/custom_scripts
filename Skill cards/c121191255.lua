--coded by Lyris
--Fury of Thunder
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.fcon)
	e2:SetOperation(s.flip)
	c:RegisterEffect(e2)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_TO_GRAVE)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetCondition(s.regcon)
	e3:SetOperation(s.reg)
	c:RegisterEffect(e3)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_CUSTOM + id)
	e0:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e0:SetCountLimit(1)
	e0:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e0:SetOperation(s.dact)
	c:RegisterEffect(e0)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsCode(10000020)
end
function s.con(c)
	return function(e, tp)
		local ct = 4 - Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0)
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, nil) and ct > 0 and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsPlayerCanDraw(tp, ct)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp, chk)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Draw(tp, 4 - Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0), 0)
end
function s.filter(c)
	return (c:IsPreviousPosition(POS_FACEUP) or not c:IsPreviousLocation(LOCATION_ONFIELD)) and c:IsCode(10000020) and not c:IsReason(REASON_RETURN)
end
function s.regcon(e, tp, eg)
	return eg:IsExists(s.filter, 1, nil)
end
function s.reg(e, tp, eg)
	Duel.RaiseEvent(eg, EVENT_CUSTOM + id, e, REASON_RULE, tp, tp, 0)
end
function s.dact(e, tp, eg, ep, ev, re, r, rp)
	if not Duel.SelectEffectYesNo(tp, e:GetOwner()) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	local g = eg:Filter(s.filter, nil)
	Duel.SendtoDeck(g, tp, SEQ_DECKSHUFFLE, 0)
end

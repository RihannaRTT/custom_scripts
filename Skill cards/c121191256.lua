--coded by Lyris
--It's No Monster, It's a God!
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not c:IsOriginalCodeRule(id) or c:IsFacedown())
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	if Duel.DiscardHand(tp, Card.IsDiscardable, 1, 99, REASON_DISCARD) == 0 then
		return
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, 1, nil, 10000000)
	Duel.SendtoHand(g, nil, 0)
	Duel.ConfirmCards(1 - tp, g)
end

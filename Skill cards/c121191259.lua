--coded by Lyris
--The Psychic Duelist
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.fcon(c))
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetDescription(aux.Stringid(id, 0))
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetCountLimit(1)
	e3:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e3:SetDescription(aux.Stringid(id, 1))
	e3:SetCondition(s.dcon)
	e3:SetOperation(s.dact)
	c:RegisterEffect(e3)
end
function s.fcon(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
	end
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.filter(c)
	return c:IsFacedown() and c:GetSequence() < 5
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_SZONE, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_OPSELECTED, 1 - tp, e:GetDescription())
	Duel.PayLPCost(tp, Duel.GetLP(tp) // 2)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, 0, LOCATION_SZONE, 1, 1, nil)
	Duel.HintSelection(g)
	if #g == 0 then
		return
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CARDTYPE)
	local op = Duel.AnnounceType(tp, 71, 72)
	Duel.BreakEffect()
	Duel.ConfirmCards(1 - tp, g)
	local tc = g:GetFirst()
	if op == 1 and tc:IsType(TYPE_SPELL) or op == 2 and tc:IsType(TYPE_TRAP) then
		local e1 = Effect.CreateEffect(e:GetOwner())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_CANNOT_TRIGGER)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		tc:RegisterEffect(e1, true)
	end
end
function s.dcon(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetFieldGroupCount(tp, 0, LOCATION_HAND) > 0
end
function s.dact(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_OPSELECTED, 1 - tp, e:GetDescription())
	Duel.PayLPCost(tp, Duel.GetLP(tp) // 2)
	local tc = Duel.GetFieldGroup(tp, 0, LOCATION_HAND):RandomSelect(1 - tp, 1, nil):GetFirst()
	if not tc then
		return
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CARDTYPE)
	local op = Duel.AnnounceType(tp)
	Duel.BreakEffect()
	Duel.ConfirmCards(1 - tp, tc)
	Duel.ShuffleHand(tp)
	if op == 0 and tc:IsType(TYPE_MONSTER) or op == 1 and tc:IsType(TYPE_SPELL) or op == 2 and tc:IsType(TYPE_TRAP) then
		Duel.Damage(1 - tp, 500, REASON_RULE)
	end
end

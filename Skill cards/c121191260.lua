--coded by Lyris
--Guardians of the Tomb
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_SPSUMMON_SUCCESS)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetCondition(s.con(c))
	e3:SetOperation(s.act)
	c:RegisterEffect(e3)
end
function s.flip(c)
	return function()
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
end
function s.cfilter(c, tp)
	return c:IsPreviousLocation(LOCATION_SZONE) and c:GetPreviousSequence() < 5 and c:GetPreviousControler() == tp and c:IsControler(tp)
end
function s.con(c)
	return function(e, tp, eg)
		local ph = Duel.GetCurrentPhase()
		return Duel.GetTurnPlayer() ~= tp and (ph == PHASE_MAIN1 or ph == PHASE_MAIN2) and eg:IsExists(s.cfilter, 1, nil, tp) and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) == 0 and c:GetFlagEffect(id) == 0
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp, chk)
	local c = e:GetOwner()
	if not Duel.SelectEffectYesNo(tp, c) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	if Duel.Draw(tp, 1, 0) > 0 and Duel.IsExistingMatchingCard(Card.IsSSetable, tp, LOCATION_HAND, 0, 1, nil) and Duel.SelectYesNo(tp, 1159) then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SET)
		Duel.SSet(tp, Duel.SelectMatchingCard(tp, Card.IsSSetable, tp, LOCATION_HAND, 0, 1, 1, nil), tp, false)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

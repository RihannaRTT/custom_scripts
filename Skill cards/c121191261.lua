--coded by Lyris
--Union Combination
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.fcon)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con(c)
	return function(e, tp)
		if not (c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)) or c:GetFlagEffect(id) > 0 then
			return false
		end
		local chkf = tp
		return Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, Duel.GetFusionMaterial(tp):Filter(Card.IsOnField, nil), chkf)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local dg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil)
	Duel.SendtoDeck(dg, nil, SEQ_DECKBOTTOM, 0)
	local chkf = tp
	local mg = Duel.GetFusionMaterial(tp):Filter(Card.IsOnField, nil) + Duel.GetMatchingGroup(s.filter3, tp, LOCATION_GRAVE, 0, nil) + Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_SZONE, 0, nil)
	local sg = Duel.GetMatchingGroup(s.filter2, tp, LOCATION_EXTRA, 0, nil, e, tp, mg, chkf)
	::cancel::
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	local tc = sg:Select(tp, 1, 1, nil):GetFirst()
	if tc then
		local mat = Duel.SelectFusionMaterial(tp, tc, mg, nil, chkf)
		if #mat < 2 then
			goto cancel
		end
		tc:SetMaterial(mat)
		Duel.Remove(mat, POS_FACEUP, REASON_MATERIAL + REASON_FUSION)
		Duel.BreakEffect()
		Duel.SpecialSummon(tc, 0, tp, tp, true, false, POS_FACEUP)
		tc:CompleteProcedure()
	end
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CHANGE_DAMAGE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(0, 1)
	e1:SetValue(0)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_NO_EFFECT_DAMAGE)
	e2:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e2, tp)
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEDOWN)
	end
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.isalpha(c)
	return c:IsCode(1561110, 2111707, 25119460, 58859575, 65172015, 75906310, 84243274, 91998119, 99724761)
end
function s.filter2(c, e, tp, m, chkf)
	return c:IsType(TYPE_FUSION) and s.isalpha(c) and c:IsCanBeSpecialSummoned(e, 0, tp, true, false) and c:CheckFusionMaterial(m + Duel.GetMatchingGroup(s.filter3, tp, LOCATION_GRAVE, 0, nil) + Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_SZONE, 0, nil), nil, chkf)
end
function s.filter3(c)
	return c:IsType(TYPE_MONSTER) and c:IsCanBeFusionMaterial()
end

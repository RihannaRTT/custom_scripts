--coded by Lyris
--Spell of Mask
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.fcon)
	e2:SetOperation(s.flip)
	c:RegisterEffect(e2)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_DESTROYED)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCondition(s.regcon)
	e1:SetOperation(s.reg(c))
	Duel.RegisterEffect(e1, 0)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_CUSTOM + id)
	e0:SetOperation(s.act)
	Duel.RegisterEffect(e0, 0)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetCondition(s.scon)
	e3:SetOperation(s.sact)
	c:RegisterEffect(e3)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.cfilter(c, tp)
	return c:IsCode(49064413) and c:IsSummonType(SUMMON_TYPE_RITUAL) and c:IsReason(REASON_EFFECT) and c:GetReasonPlayer() ~= tp
end
function s.regcon(e, tp, eg)
	return eg:IsExists(s.cfilter, 1, nil, e:GetOwner():GetControler())
end
function s.reg(c)
	return function(e)
		Duel.RaiseEvent(c, EVENT_CUSTOM + id, e, 0, 0, c:GetControler(), 0)
	end
end
function s.filter(c, e, tp)
	return c:IsCode(48948935) and c:IsCanBeSpecialSummoned(e, 0, tp, true, true)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	local ct = c:GetFlagEffectLabel(id)
	local tp = c:GetControler()
	if ct and ct & 1 > 0 or not (Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK + LOCATION_GRAVE + LOCATION_HAND, 0, 1, nil, e, tp) and Duel.SelectEffectYesNo(tp, e:GetOwner())) then
		return
	end
	if not c:IsOriginalCodeRule(id) or c:IsFacedown() then
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	Duel.SpecialSummon(Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_GRAVE + LOCATION_HAND, 0, 1, 1, nil, e, tp), 0, tp, tp, true, true, POS_FACEUP)
	if ct then
		c:SetFlagEffectLabel(id, ct | 1)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 1)
	end
end
function s.scon(e, tp)
	local c = e:GetOwner()
	local ct = c:GetFlagEffectLabel(id)
	return c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.CheckReleaseGroup(tp, Card.IsCode, 1, nil, 48948935) and Duel.GetMatchingGroup(Card.IsCode, tp, LOCATION_DECK + LOCATION_GRAVE, 0, nil, 49064413, 94377247):GetClassCount(Card.GetCode) > 1 and (not ct or ct & 2 == 0)
end
function s.sact(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Release(Duel.SelectReleaseGroup(tp, Card.IsCode, 1, 1, nil, 48948935), 0)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.GetMatchingGroup(Card.IsCode, tp, LOCATION_DECK + LOCATION_GRAVE, 0, nil, 49064413, 94377247):SelectSubGroup(tp, aux.dncheck, false, 2, 2) or Group.CreateGroup()
	if g and #g > 0 then
		Duel.BreakEffect()
		Duel.SendtoHand(g, nil, 0)
		Duel.ConfirmCards(1 - tp, g)
	end
	local c = e:GetOwner()
	if ct then
		c:SetFlagEffectLabel(id, ct | 2)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 2)
	end
end

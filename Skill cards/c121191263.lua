--coded by Lyris
--Magician's Act
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e0:SetCondition(s.con)
	e0:SetOperation(s.act)
	Duel.RegisterEffect(e0, 0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCondition(s.dcon)
	e2:SetOperation(s.dact)
	c:RegisterEffect(e2)
end
function s.chk(loc, tp)
	return Duel.IsExistingMatchingCard(Card.IsCode, tp, loc, 0, 1, nil, 46986414, 120130000)
end
function s.con(e)
	return e:GetLabel() ~= 1
end
function s.act(e, tp)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEUP)
	end
	Duel.Hint(HINT_CARD, 0, id)
	e:SetLabel(1)
	local tp = c:GetControler()
	local b1, b2 = s.chk(LOCATION_DECK, tp), s.chk(LOCATION_HAND, tp)
	local op = aux.SelectFromOptions(tp, { b1, 1190 }, { b2, aux.Stringid(id, 0) })
	if op == 1 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local g = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK, 0, 1, 1, nil, 46986414, 120130000)
		Duel.SendtoHand(g, nil, 0)
		Duel.ConfirmCards(1 - tp, g)
		Duel.ShuffleHand(tp)
		Duel.BreakEffect()
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
		local dg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil)
		Duel.SendtoDeck(dg, nil, SEQ_DECKSHUFFLE, 0)
	else
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
		local g = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_HAND, 0, 1, 1, nil, 46986414, 120130000)
		Duel.ConfirmCards(1 - tp, g)
		Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, 0)
		Duel.ShuffleDeck(tp)
		Duel.BreakEffect()
		Duel.Draw(tp, 1, 0)
	end
end
function s.dcon(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and e:GetLabel() ~= 1 and Duel.CheckReleaseGroup(tp, Card.IsOriginalCodeRule, 1, nil, 46986414, 120130000)
end
function s.dact(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	e:SetLabel(1)
	Duel.Release(Duel.SelectReleaseGroup(tp, Card.IsOriginalCodeRule, 1, 1, nil, 46986414, 120130000), 0)
	Duel.BreakEffect()
	Duel.Draw(tp, 2, 0)
end

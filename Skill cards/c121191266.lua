--coded by Lyris
--I'm Just Gonna Attack!
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1, EFFECT_COUNT_CODE_SINGLE)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e3 = e1:Clone()
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetRange(LOCATION_SZONE)
	c:RegisterEffect(e3)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_CHAINING)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCondition(s.negcon)
	e2:SetOperation(s.negop)
	c:RegisterEffect(e2)
end
function s.flip(c)
	return function()
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(Card.IsFaceup, tp, LOCATION_MZONE, 0, 1, nil)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, Card.IsFaceup, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	if tc then
		local e1 = Effect.CreateEffect(e:GetOwner())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		e1:SetValue(100)
		tc:RegisterEffect(e1, true)
	end
end
function s.negcon(e, tp, eg, ep, ev, re, r, rp)
	local ph = Duel.GetCurrentPhase()
	return Duel.GetTurnPlayer() == tp and ph >= PHASE_BATTLE_START and ph <= PHASE_BATTLE and rp ~= tp and re:IsActiveType(TYPE_TRAP) and re:IsHasType(EFFECT_TYPE_ACTIVATE) and e:GetOwner():GetFlagEffect(id) == 0
end
function s.negop(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	if not Duel.SelectEffectYesNo(tp, c) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_CHAIN_SOLVING)
	e1:SetCondition(s.discon(re))
	e1:SetOperation(s.disop)
	e1:SetReset(RESET_CHAIN)
	Duel.RegisterEffect(e1, tp)
	Duel.SetChainLimit(s.chlim(tp))
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.discon(re)
	return function(_, _, _, _, _, rf)
		return re == rf
	end
end
function s.chlim(tp)
	return function(fe)
		return fe:GetHandler():IsControler(1 - tp)
	end
end
function s.disop(e, tp, eg, ep, ev, re, r, rp)
	local rc = re:GetHandler()
	if Duel.NegateEffect(ev) and rc:IsRelateToEffect(re) and rc:IsCanTurnSet() then
		rc:CancelToGrave()
		Duel.ChangePosition(rc, POS_FACEDOWN)
		Duel.RaiseEvent(rc, EVENT_SSET, e, REASON_RULE, tp, tp, 0)
	end
end

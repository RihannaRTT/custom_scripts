--coded by Lyris
--Beasts of Phantom
local s, id, o = GetID()
function s.initial_effect(c)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_ACTIVATE)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetCondition(s.fcon)
	e3:SetOperation(s.flip)
	c:RegisterEffect(e3)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetDescription(1056)
	e1:SetCondition(s.dcon(c))
	e1:SetOperation(s.dact)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_PHASE_START + PHASE_END)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetDescription(1190)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		local e0 = Effect.CreateEffect(c)
		e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e0:SetCode(EVENT_SPSUMMON_SUCCESS)
		e0:SetCondition(s.regcon)
		e0:SetOperation(s.reg)
		Duel.RegisterEffect(e0, 0)
	end
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.regcon(e, tp, eg)
	return eg:IsExists(s.cfilter, 1, nil)
end
function s.reg(e)
	Duel.RegisterFlagEffect(e:GetOwner():GetControler(), id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.dcon(c)
	return function(e, tp)
		local chkf = tp
		return c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) & 1 == 0 and Duel.IsExistingMatchingCard(s.filter1, tp, LOCATION_HAND, 0, 1, nil, e, tp, Duel.GetFusionMaterial(tp), chkf)
	end
end
function s.filter1(c, e, tp, m, chkf)
	return Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, (m or Group.CreateGroup()) - c, chkf)
end
function s.filter2(c, e, tp, m, chkf)
	return c:IsType(TYPE_FUSION) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_FUSION, tp, false, false) and c:CheckFusionMaterial(m, nil, chkf)
end
function s.dact(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local chkf = tp
	local mg1 = Duel.GetFusionMaterial(tp)
	local sg = Duel.GetMatchingGroup(s.filter2, tp, LOCATION_EXTRA, 0, nil, e, tp, mg1, chkf)
	if Duel.DiscardHand(tp, s.filter1, 1, 1, REASON_DISCARD, nil, e, tp, mg1, chkf) > 0 then
		mg1:Sub(Duel.GetOperatedGroup())
		Duel.BreakEffect()
		::cancel::
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
		local tc = sg:Select(tp, 1, 1, nil):GetFirst()
		if tc then
			local mat1 = Duel.SelectFusionMaterial(tp, tc, mg1, nil, chkf)
			if #mat1 < 2 then
				goto cancel
			end
			tc:SetMaterial(mat1)
			Duel.SendtoGrave(mat1, REASON_MATERIAL + REASON_FUSION)
			Duel.BreakEffect()
			Duel.SpecialSummon(tc, SUMMON_TYPE_FUSION, tp, tp, false, false, POS_FACEUP)
			tc:CompleteProcedure()
		end
	end
	local ct = c:GetFlagEffectLabel(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 1)
	else
		c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1, 1)
	end
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsCode(4796100) and c:IsSummonType(SUMMON_TYPE_FUSION)
end
function s.con(c)
	return function(e, tp)
		return c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and Duel.GetFlagEffect(tp, id) > 0 and c:GetFlagEffect(id) & 2 == 0
	end
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x1b)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	if not Duel.SelectEffectYesNo(tp, c, aux.Stringid(id, 15)) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_GRAVE, 0, 1, 1, nil)
	Duel.SendtoHand(g, nil, 0)
	Duel.ConfirmCards(1 - tp, g)
	local ct = c:GetFlagEffectLabel(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 2)
	else
		c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1, 2)
	end
end

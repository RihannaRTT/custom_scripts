--coded by Lyris
--Magnetic Attraction
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.fcon)
	e2:SetOperation(s.flip)
	c:RegisterEffect(e2)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_SUMMON_SUCCESS)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCondition(s.dcon(c))
	e1:SetOperation(s.dact)
	c:RegisterEffect(e1)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetCondition(s.con(c))
	e3:SetOperation(s.act)
	c:RegisterEffect(e3)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.filter(c, code)
	return c:IsLevelBelow(4) and c:IsSetCard(0x2066) and not c:IsCode(code)
end
function s.dcon(c)
	return function(e, tp, eg)
		local tc = eg:GetFirst()
		return tc:IsFaceup() and tc:IsLevelBelow(4) and tc:IsSetCard(0x2066) and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil, tc:GetCode()) and (c:GetFlagEffect(id) == 0 or c:GetFlagEffectLabel(id) & 0x1 == 0)
	end
end
function s.dact(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	if not Duel.SelectEffectYesNo(tp, c) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil, eg:GetFirst():GetCode())
	Duel.SendtoHand(g, nil, 0)
	Duel.ConfirmCards(1 - tp, g)
	local ct = c:GetFlagEffectLabel(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 0x1)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 0x1)
	end
end
function s.cfilter(c)
	return c:IsLevelBelow(4) and c:IsSetCard(0x2066) and not c:IsPublic()
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetMatchingGroup(s.cfilter, tp, LOCATION_HAND, 0, nil):GetClassCount(Card.GetCode) > 1 and Duel.IsExistingMatchingCard(s.sfilter, tp, LOCATION_DECK, 0, 1, nil) and (c:GetFlagEffect(id) == 0 or c:GetFlagEffectLabel(id) & 0x2 == 0)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetMatchingGroup(s.cfilter, tp, LOCATION_HAND, 0, nil):SelectSubGroup(tp, aux.dncheck, false, 2, 2) or Group.CreateGroup()
	Duel.ConfirmCards(1 - tp, g)
	if #g > 1 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local sg = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK, 0, 1, 1, nil, 75347539)
		Duel.SendtoHand(sg, nil, 0)
		Duel.ConfirmCards(1 - tp, sg)
	end
	local ct = c:GetFlagEffectLabel(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 0x2)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 0x2)
	end
end

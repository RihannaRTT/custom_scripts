--coded by Lyris
--Powerful Group of Guys
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.acon)
	e2:SetOperation(s.fact)
	c:RegisterEffect(e2)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	Duel.AddCustomActivityCounter(id, ACTIVITY_SUMMON, s.counterfilter)
	Duel.AddCustomActivityCounter(id, ACTIVITY_SPSUMMON, s.counterfilter)
	Duel.AddCustomActivityCounter(id, ACTIVITY_FLIPSUMMON, s.counterfilter)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_TURN_END)
	e4:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e4:SetCondition(s.fcon(c))
	e4:SetOperation(s.flip(c))
	c:RegisterEffect(e4)
end
function s.acon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.fact()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.counterfilter(c)
	return c:IsSetCard(0xc008)
end
function s.con(c)
	return function(e, tp)
		local g = Duel.GetFieldGroup(tp, LOCATION_MZONE, 0)
		local tc = g:GetFirst()
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id) and c:IsFaceup() and #g == 1 and tc:IsFaceup() and tc:IsSetCard(0xc008) and Duel.GetFieldGroupCount(tp, 0, LOCATION_MZONE) > 1 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil, e, tp, tc:GetLevel()) and Duel.GetCustomActivityCount(id, tp, ACTIVITY_SUMMON) + Duel.GetCustomActivityCount(id, tp, ACTIVITY_SPSUMMON) + Duel.GetCustomActivityCount(id, tp, ACTIVITY_FLIPSUMMON) == 0
	end
end
function s.filter(c, e, tp, lv)
	return c:IsSetCard(0xc008) and c:GetLevel() < lv and c:IsCanBeSpecialSummoned(e, 0, tp, false, false)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e1:SetReset(RESET_PHASE + PHASE_END)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(aux.TargetBoolFunction(aux.NOT(s.counterfilter)))
	Duel.RegisterEffect(e1, tp)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_CANNOT_SUMMON)
	Duel.RegisterEffect(e2, tp)
	local e3 = e1:Clone()
	e3:SetCode(EFFECT_CANNOT_FLIP_SUMMON)
	Duel.RegisterEffect(e3, tp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	Duel.SpecialSummon(Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, 1, nil, e, tp, Duel.GetFieldGroup(tp, LOCATION_MZONE, 0):GetFirst():GetLevel()), 0, tp, tp, false, false, POS_FACEUP)
end
function s.fcon(c)
	return function()
		return c:IsOriginalCodeRule(id) and c:IsFaceup()
	end
end
function s.flip(c)
	return function()
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEDOWN)
		else
			c:SetEntityCode(111004001)
		end
	end
end

--coded by Lyris
--Ancient Fusion
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.fcon)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con(c)
	return function(e, tp)
		return c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil, e, tp)
	end
end
function s.ffilter(c)
	return s.agg(c) and c:IsOnField()
end
function s.fchk(tp, sg, fc)
	if sg:IsExists(aux.NOT(Card.IsOnField), 1, nil) then
		return sg:IsExists(s.ffilter, 1, nil)
	end
	return true
end
function s.mfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsCanBeFusionMaterial()
end
function s.sfilter(c, e, tp, m, chkf)
	return c:IsType(TYPE_FUSION) and c:IsSetCard(0x7) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_FUSION, tp, true, true) and c:CheckFusionMaterial(m, nil, chkf)
end
function s.cfilter(c, e, tp)
	local chkf = tp
	local mg = Duel.GetFusionMaterial(tp):Filter(Card.IsOnField, nil)
	if mg:IsExists(s.agg, 1, nil) then
		mg:Merge(Duel.GetMatchingGroup(s.mfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, c))
		aux.FCheckAdditional = s.fchk
	end
	local res = Duel.IsExistingMatchingCard(s.sfilter, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, mg, chkf)
	aux.FCheckAdditional = nil
	return res
end
function s.agg(c)
	return c:IsCode(83104731, 95735217, 7171149, 12652643)
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	if Duel.DiscardHand(tp, s.cfilter, 1, 1, REASON_DISCARD, nil, e, tp) > 0 then
		local chkf = tp
		local mg1 = Duel.GetFusionMaterial(tp):Filter(Card.IsOnField, nil)
		local mg2 = Duel.GetMatchingGroup(s.mfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, c)
		if mg1:IsExists(s.agg, 1, nil) then
			mg1:Merge(mg2)
			aux.FCheckAdditional = s.fchk
		end
		local sg = Duel.GetMatchingGroup(s.sfilter, tp, LOCATION_EXTRA, 0, nil, e, tp, mg1, chkf)
		aux.FCheckAdditional = nil
		::cancel::
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
		local tc = sg:Select(tp, 1, 1, nil):GetFirst()
		if tc then
			aux.FCheckAdditional = s.fchk
			local mat1 = Duel.SelectFusionMaterial(tp, tc, mg1, nil, chkf)
			aux.FCheckAdditional = nil
			if #mat1 < 2 then
				goto cancel
			end
			tc:SetMaterial(mat1)
			Duel.SendtoGrave(mat1, REASON_MATERIAL + REASON_FUSION)
			Duel.BreakEffect()
			Duel.SpecialSummon(tc, SUMMON_TYPE_FUSION, tp, tp, true, true, POS_FACEUP)
			tc:CompleteProcedure()
			local e3 = Effect.CreateEffect(c)
			e3:SetType(EFFECT_TYPE_SINGLE)
			e3:SetCode(EFFECT_CHANGE_INVOLVING_BATTLE_DAMAGE)
			e3:SetValue(aux.ChangeBattleDamage(1, HALF_DAMAGE))
			e3:SetReset(RESET_EVENT + RESETS_STANDARD)
			tc:RegisterEffect(e3, true)
		end
	end
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CHANGE_DAMAGE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(0, 1)
	e1:SetValue(0)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_NO_EFFECT_DAMAGE)
	e2:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e2, tp)
end

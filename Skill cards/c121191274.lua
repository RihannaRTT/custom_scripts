--coded by Lyris
--Cyber Blade Fusion
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.fcon)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCountLimit(2)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
s.fchecks = aux.CreateChecks(Card.IsFusionCode, { 97023549, 11460577 })
function s.con(c)
	return function(e, tp)
		return c:GetFlagEffect(id) == 0 and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and aux.MustMaterialCheck(nil, tp, EFFECT_MUST_BE_FMATERIAL) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil, e, tp)
	end
end
function s.cfilter(c, e, tp)
	local mg = Duel.GetMatchingGroup(s.mfilter, tp, LOCATION_ONFIELD + LOCATION_DECK + LOCATION_HAND, 0, 1, c)
	return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_EXTRA, 0, 1, nil, e, tp) and mg:CheckSubGroupEach(s.fchecks, s.goal, e, tp)
end
function s.mfilter(c)
	return c:IsFusionCode(97023549, 11460577) and c:IsCanBeFusionMaterial()
end
function s.goal(g, e, tp)
	return Duel.IsExistingMatchingCard(s.sfilter, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, g) and g:IsExists(Card.IsOnField, 1, nil)
end
function s.filter(c, e, tp, sg)
	return c:IsCode(10248389) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_FUSION, tp, true, true) and (not sg or Duel.GetLocationCountFromEx(tp, tp, sg, c) > 0)
end
function s.rfilter(c)
	return c:IsLocation(LOCATION_HAND) or (c:IsOnField() and c:IsFacedown())
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	if Duel.DiscardHand(tp, s.cfilter, 1, 1, REASON_DISCARD, nil, e, tp) > 0 then
		local mg = Duel.GetMatchingGroup(s.mfilter, tp, LOCATION_ONFIELD + LOCATION_DECK + LOCATION_HAND, 0, 1, nil)
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
		local sg = mg:SelectSubGroupEach(tp, s.fchecks, false, s.goal, e, tp) or Group.CreateGroup()
		if not sg then
			return
		end
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
		local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_EXTRA, 0, 1, 1, nil, e, tp):GetFirst()
		local cg = sg:Filter(s.rfilter, nil)
		if #cg > 0 then
			Duel.ConfirmCards(1 - tp, cg)
			Duel.ShuffleHand(tp)
		end
		tc:SetMaterial(sg)
		Duel.SendtoGrave(sg, REASON_MATERIAL + REASON_FUSION)
		Duel.SpecialSummon(tc, SUMMON_TYPE_FUSION, tp, tp, true, true, POS_FACEUP)
		tc:CompleteProcedure()
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(400)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD)
		tc:RegisterEffect(e1, true)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

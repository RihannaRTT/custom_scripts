--coded by Lyris
--Crystal Transcendance
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetCondition(s.fcon)
	e0:SetOperation(s.flip)
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e2:SetTargetRange(1, 0)
	e2:SetCondition(s.lcon)
	e2:SetTarget(s.ltg)
	c:RegisterEffect(e2)
	local e3 = e2:Clone()
	e3:SetCode(EFFECT_CANNOT_SUMMON)
	c:RegisterEffect(e3)
	local e4 = e2:Clone()
	e4:SetCode(EFFECT_CANNOT_FLIP_SUMMON)
	c:RegisterEffect(e4)
	local e5 = e2:Clone()
	e5:SetCode(EFFECT_CANNOT_MSET)
	c:RegisterEffect(e5)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_SZONE + LOCATION_REMOVED)
	e1:SetCountLimit(2)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.lcon(e)
	local c = e:GetOwner()
	return c:IsOriginalCodeRule(id) and c:IsFaceup()
end
function s.ltg(e, c)
	return not (c:IsSetCard(0x34) or c:IsRace(RACE_DRAGON))
end
function s.plfilter(c)
	return c:IsSetCard(0x1034) and c:IsType(TYPE_MONSTER)
end
function s.goal(g, sg)
	return sg:GetClassCount(Card.GetCode) >= #g
end
function s.con(c)
	return function(e, tp)
		return Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and Duel.IsExistingMatchingCard(s.plfilter, tp, LOCATION_DECK, 0, 1, nil) and c:GetFlagEffect(id) == 0 and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local ft = Duel.GetLocationCount(tp, LOCATION_SZONE)
	local g = Duel.GetMatchingGroup(s.plfilter, tp, LOCATION_DECK, 0, nil)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local cg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):SelectSubGroup(tp, s.goal, false, 1, math.min(ft, #g, 3), g) or Group.CreateGroup()
	local ct = #cg
	Duel.SendtoGrave(cg, 0)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local sg = g:SelectSubGroup(tp, aux.dncheck, false, ct, ct) or Group.CreateGroup()
	local c = e:GetOwner()
	Duel.BreakEffect()
	for tc in aux.Next(sg) do
		Duel.MoveToField(tc, tp, tp, LOCATION_SZONE, POS_FACEUP, true)
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_CHANGE_TYPE)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetValue(TYPE_SPELL + TYPE_CONTINUOUS)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD - RESET_TURN_SET)
		tc:RegisterEffect(e1, true)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

--coded by Lyris
--Armed and Ready!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.fcon)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con(c)
	return function(e, tp)
		return c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil, e, tp)
	end
end
function s.cfilter(c, e, tp)
	if not c:IsSetCard(0x41) or not c:IsFaceup() or Duel.GetMZoneCount(tp, c) <= 0 then
		return false
	end
	local mt = _G["c" .. c:GetOriginalCode()]
	if not mt or not mt.lvup then
		return false
	end
	return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND + LOCATION_DECK + LOCATION_GRAVE, 0, 1, nil, mt.lvup, e, tp)
end
function s.filter(c, t, e, tp)
	return c:IsCode(table.unpack(t)) and c:IsCanBeSpecialSummoned(e, SUMMON_VALUE_LV, tp, true, true)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local g = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_MZONE, 0, 1, 1, nil, e, tp)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	Duel.SendtoGrave(tc, 0)
	local mt = _G["c" .. tc:GetOriginalCode()]
	if not mt or not mt.lvup then
		return
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	local sg = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND + LOCATION_DECK + LOCATION_GRAVE, 0, 1, 1, nil, mt.lvup, e, tp)
	if #sg > 0 then
		Duel.BreakEffect()
		Duel.SpecialSummon(sg, SUMMON_VALUE_LV, tp, tp, true, true, POS_FACEUP)
	end
end

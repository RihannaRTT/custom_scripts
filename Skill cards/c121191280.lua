--coded by Lyris
--Machine Angel Ascension
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetCondition(s.fcon)
	e0:SetOperation(s.flip)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetDescription(1190)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e2 = e1:Clone()
	e2:SetDescription(aux.Stringid(id, 0))
	e2:SetCondition(s.dcon(c))
	e2:SetOperation(s.dact)
	c:RegisterEffect(e2)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con(c)
	return function(e, tp)
		return c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil)
	end
end
function s.cfilter(c)
	return c:IsType(TYPE_SPELL)
end
function s.filter(c)
	if c:IsSetCard(0x2093) then
		return c:GetType() & 0x81 == 0x81
	elseif c:IsSetCard(0x124) then
		return c:GetType() & 0x82 == 0x82
	else
		return false
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_OPSELECTED, 0, e:GetDescription())
	if Duel.DiscardHand(tp, s.cfilter, 1, 1, REASON_DISCARD) > 0 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil):GetFirst()
		Duel.BreakEffect()
		Duel.SendtoHand(tc, nil, 0)
		Duel.ConfirmCards(1 - tp, tc)
	end
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.dcon(c)
	return function(e, tp)
		return c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_GRAVE, 0, 1, nil)
	end
end
function s.dact(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_OPSELECTED, 0, e:GetDescription())
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_GRAVE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, 0)
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

--coded by Lyris
--Rainbow Crystal Collection
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EFFECT_DESTROY_REPLACE)
	e1:SetTarget(s.tg)
	e1:SetValue(s.val(c))
	Duel.RegisterEffect(e1, 0)
	if not s.global_check then
		s.global_check = true
		local f1, f2, f3, f4, f5, f6, f7, f8, f9, fa = Card.IsCanBeFusionMaterial, Duel.GetMatchingGroup, Duel.GetFusionMaterial, Duel.IsExistingTarget, Duel.SelectTarget, Card.IsLocation, Duel.SpecialSummon, Duel.SpecialSummonStep, Duel.GetLocationCount, Card.IsCanBeSpecialSummoned
		Card.IsCanBeFusionMaterial = function(tc, fc)
			if not fc or not fc:IsSetCard(0x2034) then
				return f1(tc, fc)
			end
			return f1(tc, fc) or not tc:IsHasEffect(EFFECT_CANNOT_BE_FUSION_MATERIAL) and s.con(tc)
		end
		Duel.GetMatchingGroup = function(f, p, sloc, oloc, exc, ...)
			local t = { ... }
			local g = f2(f, p, sloc, oloc, exc, table.unpack(t))
			for tc in aux.Next(f2(s.mgfilter1, p, sloc | LOCATION_MZONE, oloc | LOCATION_MZONE, exc, f, t)) do
				if s.con(tc) then
					if s.mchk(tc, p, sloc) or s.mchk(tc, 1 - p, oloc) then
						g:RemoveCard(tc)
					end
					if s.schk(tc, p, sloc) or s.schk(tc, 1 - p, oloc) then
						g:AddCard(tc)
					end
				end
			end
			return g:Filter(s.mgfilter2, nil, p, f6, sloc, oloc)
		end
		Duel.GetMatchingGroupCount = function(f, p, sloc, oloc, exc, ...)
			return #(Duel.GetMatchingGroup(f, p, sloc, oloc, exc, table.unpack { ... }))
		end
		Duel.GetFusionMaterial = function(p)
			local mg = f3(p)
			for tc in aux.Next(f2(s.fmfilter, p, LOCATION_MZONE, 0, exc)) do
				if not tc:IsHasEffect(EFFECT_CANNOT_BE_FUSION_MATERIAL) and s.con(tc) then
					mg:AddCard(tc)
				end
			end
			return mg
		end
		Duel.IsExistingMatchingCard = function(f, p, sloc, oloc, ct, exc, ...)
			return #(Duel.GetMatchingGroup(f, p, sloc, oloc, exc, table.unpack { ... })) >= ct
		end
		Duel.SelectMatchingCard = function(sp, f, p, sloc, oloc, minc, maxc, exc, ...)
			return Duel.GetMatchingGroup(f, p, sloc, oloc, exc, table.unpack { ... }):Select(sp, minc, maxc, nil)
		end
		Duel.IsExistingTarget = function(f, p, sloc, oloc, ct, exc, ...)
			local t, sl, ol = { ... }, sloc > 0 and 1 or 0, oloc > 0 and 1 or 0
			return f4(s.xtfilter, p, sloc | (sl * LOCATION_MZONE), oloc | (ol * LOCATION_MZONE), ct, exc, p, sloc, oloc, f)
		end
		Duel.SelectTarget = function(sp, f, p, sloc, oloc, minc, maxc, exc, ...)
			local t, sl, ol = { ... }, sloc > 0 and 1 or 0, oloc > 0 and 1 or 0
			return f5(sp, function(tc)
				if s.con(tc) and (s.mchk(tc, p, sloc) or s.mchk(tc, 1 - p, oloc)) and not (s.schk(tc, p, sloc) or s.schk(tc, 1 - p, oloc)) then
					return false
				end
				return not f or f(tc, table.unpack(t))
			end, p, sloc | (sl * LOCATION_MZONE), oloc | (ol * LOCATION_MZONE), minc, maxc, exc)
		end
		Card.IsLocation = function(tc, loc)
			if f6(tc, LOCATION_MZONE) and loc & LOCATION_SZONE > 0 then
				return s.con(tc) or f6(tc, loc)
			end
			return f6(tc, loc)
		end
		Duel.SpecialSummon = function(tg, st, p, tp, nc, nl, sp, z)
			if z == nil then
				z = 0xff
			end
			local g = Group.CreateGroup() + tg
			local ct = 0
			local sg = Group.CreateGroup()
			local re = nil
			for tc in aux.Next(g) do
				if tc:GetCounter(0x105c) > 0 then
					tc:RemoveCounter(p, 0x105c, 1, 0)
					tc:SetStatus(STATUS_NO_LEVEL, false)
					ct = ct + 1
					sg:AddCard(tc)
					re = tc:GetReasonEffect()
				end
			end
			Duel.RaiseEvent(sg, EVENT_SPSUMMON_SUCCESS, re, re and REASON_EFFECT or 0, p, tp, 0)
			return f7(tg, st, p, tp, nc, nl, sp, z) + ct
		end
		Duel.SpecialSummonStep = function(tc, st, p, tp, nc, nl, sp, z)
			if tc:GetCounter(0x105c) > 0 then
				tc:RemoveCounter(p, 0x105c, 1, 0)
				tc:SetStatus(STATUS_NO_LEVEL, false)
				return true
			end
			if z == nil then
				z = 0xff
			end
			return f8(tc, st, p, tp, nc, nl, sp, z)
		end
		Duel.GetLocationCount = function(p, loc, tp, r, z)
			if tp == nil then
				tp = p
			end
			if r == nil then
				r = LOCATION_REASON_TOFIELD
			end
			if z == nil then
				z = 0xff
			end
			local res = f9(p, loc, tp, r, z)
			if loc == LOCATION_MZONE then
				res = res + #f2(s.con, 0, LOCATION_MZONE, LOCATION_MZONE, nil)
			end
			return res
		end
		Card.IsCanBeSpecialSummoned = function(tc, e, st, p, nc, nl, sp, tp, z)
			if sp == nil then
				sp = POS_FACEUP
			end
			if tp == nil then
				tp = p
			end
			if z == nil then
				z = 0xff
			end
			if tc:GetCounter(0x105c) > 0 then
				return Duel.IsPlayerCanSpecialSummonMonster(p, tc:GetCode(), nil, nil, nil, nil, nil, nil, nil, sp, tp, st)
			end
			return fa(tc, e, st, p, nc, nl, sp, tp, z)
		end
	end
end
function s.mgfilter1(sc, f, t)
	return not f or f(sc, table.unpack(t))
end
function s.mgfilter2(tc, p, f6, sloc, oloc)
	return (tc:IsControler(p) and (f6(tc, sloc) or s.con(tc) and f6(tc, LOCATION_MZONE))) or (tc:IsControler(1 - p) and (f6(tc, oloc) or s.con(tc) and f6(tc, LOCATION_MZONE)))
end
function s.fmfilter(sc)
	return s.con(sc)
end
function s.xtfilter(tc, p, sloc, oloc, f)
	if s.con(tc) and (s.mchk(tc, p, sloc) or s.mchk(tc, 1 - p, oloc)) and not (s.schk(tc, p, sloc) or s.schk(tc, 1 - p, oloc)) then
		return false
	end
	return not f or f(tc, table.unpack(t))
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
end
function s.mchk(c, p, loc)
	return c:IsControler(p) and loc & LOCATION_ONFIELD == LOCATION_MZONE
end
function s.schk(c, p, loc)
	return c:IsControler(p) and loc & LOCATION_SZONE > 0
end
function s.filter(c, tp)
	return c:IsFaceup() and (c:GetCounter(0x105c) == 0 and c:IsLocation(LOCATION_MZONE) or c:GetFlagEffect(id) > 0) and c:IsSetCard(0x1034) and c:IsControler(tp)
end
function s.tg(e, tp, eg, ep, ev, re, r, rp, chk)
	local c = e:GetOwner()
	local tp = c:GetControler()
	local g = eg:Filter(s.filter, nil, tp)
	if chk == 0 then
		return #g > 0
	end
	if not Duel.SelectEffectYesNo(tp, c) then
		return false
	end
	Duel.Hint(HINT_CARD, 0, id)
	local ct = #g
	for tc in aux.Next(g) do
		if tc:GetCounter(0x105c) == 0 and (ct <= 1 or Duel.SelectEffectYesNo(tp, tc, aux.Stringid(id, 1))) and tc:AddCounter(0x105c, 1) then
			ct = ct - 1
			tc:RegisterFlagEffect(id, RESET_EVENT + RESETS_STANDARD - RESET_TURN_SET + RESET_CHAIN, 0, 1)
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_CHANGE_TYPE)
			e1:SetValue(TYPE_SPELL + TYPE_CONTINUOUS)
			e1:SetCondition(s.con())
			e1:SetReset(RESET_EVENT + RESETS_STANDARD - RESET_TURN_SET)
			tc:RegisterEffect(e1, true)
			local e8 = e1:Clone()
			e8:SetCode(EFFECT_REMOVE_RACE)
			e8:SetValue(RACE_ALL)
			tc:RegisterEffect(e8, true)
			local e9 = e1:Clone()
			e9:SetCode(EFFECT_REMOVE_ATTRIBUTE)
			e9:SetValue(0xff)
			tc:RegisterEffect(e9, true)
			local ea = e1:Clone()
			ea:SetCode(EFFECT_SET_BASE_ATTACK)
			ea:SetValue(0)
			tc:RegisterEffect(ea, true)
			local eb = e1:Clone()
			eb:SetCode(EFFECT_SET_BASE_DEFENSE)
			eb:SetValue(0)
			tc:RegisterEffect(eb, true)
			tc:SetStatus(STATUS_NO_LEVEL, true)
			local e0 = Effect.CreateEffect(c)
			e0:SetType(EFFECT_TYPE_SINGLE)
			e0:SetCode(EFFECT_CANNOT_ATTACK)
			e0:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
			e0:SetRange(LOCATION_MZONE)
			e0:SetCondition(s.con())
			e0:SetReset(RESET_EVENT + RESETS_STANDARD - RESET_TURN_SET)
			tc:RegisterEffect(e0, true)
			local e3 = e0:Clone()
			e3:SetCode(EFFECT_CANNOT_CHANGE_POSITION)
			tc:RegisterEffect(e3, true)
			local e7 = e0:Clone()
			e7:SetCode(EFFECT_CANNOT_CHANGE_POS_E)
			tc:RegisterEffect(e7, true)
			local e4 = e0:Clone()
			e4:SetCode(EFFECT_IGNORE_BATTLE_TARGET)
			e4:SetValue(1)
			tc:RegisterEffect(e4, true)
			local e5 = e0:Clone()
			e5:SetCode(EFFECT_DISABLE)
			tc:RegisterEffect(e5, true)
			local e6 = e0:Clone()
			e6:SetCode(EFFECT_DISABLE_EFFECT)
			e6:SetValue(RESET_TURN_SET)
			tc:RegisterEffect(e6, true)
			Duel.ChangePosition(tc, POS_FACEUP_ATTACK)
			local e2 = Effect.CreateEffect(c)
			e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
			e2:SetCode(EVENT_ADJUST)
			e2:SetOperation(s.reset(tc))
			e2:SetReset(RESET_PHASE + PHASE_END)
			Duel.RegisterEffect(e2, tp)
		end
	end
	return true
end
function s.reset(tc)
	return function(e)
		tc:ResetFlagEffect(id)
		e:Reset()
	end
end
function s.val(c)
	return function(_, tc)
		return s.filter(tc, c:GetControler())
	end
end
function s.con(c)
	if c then
		return c:GetCounter(0x105c) > 0
	end
	return function(e, tp, eg, ep, ev, re, r, rp)
		return e:GetHandler():GetCounter(0x105c) > 0
	end
end

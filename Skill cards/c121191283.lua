--coded by Lyris, edited by RTT
--Energizing Elements
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.fcon)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con(c)
	return function(e, tp)
		return c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, LOCATION_MZONE, 1, nil, ATTRIBUTE_EARTH + ATTRIBUTE_FIRE + ATTRIBUTE_WATER + ATTRIBUTE_WIND)
	end
end
function s.filter(c, at)
	return c:IsFaceup() and c:IsAttribute(at)
end
function s.chk(e, tp, a)
	if not Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, a) then
		local c = e:GetOwner()
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEDOWN)
		else
			c:SetEntityCode(111004001)
		end
		return false
	end
	return true
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATTRIBUTE)
	local a = Duel.AnnounceAttribute(tp, 1, ATTRIBUTE_EARTH + ATTRIBUTE_FIRE + ATTRIBUTE_WATER + ATTRIBUTE_WIND)
	if not s.chk(e, tp, a) then
		return
	end
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetTargetRange(LOCATION_MZONE, LOCATION_MZONE)
	e1:SetTarget(aux.TargetBoolFunction(Card.IsAttribute, a))
	e1:SetReset(RESET_PHASE + PHASE_END)
	e1:SetValue(500)
	Duel.RegisterEffect(e1, tp)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_UPDATE_DEFENSE)
	e2:SetValue(-400)
	Duel.RegisterEffect(e2, tp)
	local e3 = e1:Clone()
	e3:SetCode(EFFECT_CHANGE_INVOLVING_BATTLE_DAMAGE)
	e3:SetValue(aux.ChangeBattleDamage(1, HALF_DAMAGE))
	Duel.RegisterEffect(e3, tp)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_ADJUST)
	e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e4:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e4:SetLabel(a)
	e4:SetCondition(s.rcon)
	e4:SetOperation(s.rflip)
	Duel.RegisterEffect(e4, tp)
	local e5 = e4:Clone()
	e5:SetCode(EVENT_CHAIN_SOLVED)
	Duel.RegisterEffect(e5, tp)
end
function s.rfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_MONSTER)
end
function s.rcon(e)
	local tp = e:GetHandlerPlayer()
	local a = e:GetLabel()
	local g = Duel.GetMatchingGroup(s.rfilter, tp, LOCATION_MZONE, 0, nil):Filter(Card.IsAttribute, nil, a)
	return #g == 0
end
function s.rflip(e, tp, eg, ep, ev, re, r, rp)
	local a = e:GetLabel()
	local g = Duel.GetMatchingGroup(s.rfilter, tp, LOCATION_MZONE, 0, nil):Filter(Card.IsAttribute, nil, a)
	if #g == 0 then
		local c = e:GetOwner()
		Duel.ChangePosition(c, POS_FACEDOWN)
	end
end

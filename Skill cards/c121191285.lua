--coded by Lyris
--I've Got Dino DNA!
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE + PHASE_DRAW)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCountLimit(1)
	e1:SetCondition(s.rcon)
	e1:SetOperation(s.ract)
	c:RegisterEffect(e1)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_DRAW)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetCondition(s.dcon)
	e3:SetOperation(s.dact)
	c:RegisterEffect(e3)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCountLimit(1)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.flip(c)
	return function()
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
end
function s.rcon(e, tp)
	return Duel.GetTurnPlayer() == tp
end
function s.ract(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Recover(tp, 200, 0)
end
function s.cfilter(c)
	return c:IsRace(RACE_DINOSAUR) and c:IsReason(REASON_RULE) and not c:IsPublic()
end
function s.dcon(e, tp, eg, ep, ev)
	return Duel.GetTurnPlayer() == tp and Duel.GetCurrentPhase() == PHASE_DRAW and ev > 0 and eg:IsExists(s.cfilter, 1, nil)
end
function s.dact(e, tp, eg, ep, ev, re, r, rp)
	if not Duel.SelectEffectYesNo(tp, e:GetOwner()) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	local cc = eg:Filter(s.cfilter, nil):GetFirst()
	if not cc then
		return
	elseif eg:FilterCount(s.cfilter, nil) > 1 then
		cc = eg:FilterSelect(tp, s.cfilter, 1, 1, nil):GetFirst()
	end
	Duel.ConfirmCards(1 - tp, cc)
	Duel.ShuffleHand(tp)
	Duel.SendtoDeck(cc, nil, SEQ_DECKBOTTOM, 0)
	Duel.BreakEffect()
	Duel.Draw(tp, 1, 0)
end
function s.filter(c)
	return c:IsFaceup() and c:IsRace(RACE_DINOSAUR)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	if tc then
		local e1 = Effect.CreateEffect(e:GetOwner())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		e1:SetValue(200)
		tc:RegisterEffect(e1, true)
	end
end

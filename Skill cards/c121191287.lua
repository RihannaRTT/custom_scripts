--coded by Lyris
--HEROES UNITE - FUSION!!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		local chkf = tp
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil, e, tp, chkf)
	end
end
function s.cfilter(c, e, tp, chkf)
	return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, Duel.GetFusionMaterial(tp) - c, chkf)
end
function s.filter(c, e, tp, m, chkf)
	local mg = m:Clone()
	if aux.GetMaterialListCount(c) > 2 and Duel.IsExistingMatchingCard(s.efilter, tp, LOCATION_MZONE, 0, 2, nil, c) then
		mg:Merge(Duel.GetMatchingGroup(s.dfilter, tp, LOCATION_DECK, 0, 1, nil))
	end
	return c:IsType(TYPE_FUSION) and c:IsSetCard(0x3008) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_FUSION, tp, false, false) and c:CheckFusionMaterial(mg, nil, chkf)
end
function s.dfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsCanBeFusionMaterial()
end
function s.efilter(c, tc)
	return aux.IsMaterialListCode(tc, c:GetCode())
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local chkf = tp
	Duel.DiscardHand(tp, s.cfilter, 1, 1, REASON_DISCARD, nil, e, tp, chkf)
	local mg = Duel.GetFusionMaterial(tp)
	::cancel::
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	local tc = Duel.GetMatchingGroup(s.filter, tp, LOCATION_EXTRA, 0, nil, e, tp, mg, chkf):Select(tp, 1, 1, nil):GetFirst()
	if tc then
		local mat = Duel.SelectFusionMaterial(tp, tc, mg + (aux.GetMaterialListCount(tc) > 2 and Duel.IsExistingMatchingCard(s.efilter, tp, LOCATION_MZONE, 0, 2, nil, tc) and Duel.GetMatchingGroup(s.dfilter, tp, LOCATION_DECK, 0, 1, nil) or Group.CreateGroup()), nil, chkf)
		if #mat < 2 then
			goto cancel
		end
		tc:SetMaterial(mat)
		Duel.SendtoGrave(mat, REASON_MATERIAL + REASON_FUSION)
		Duel.BreakEffect()
		Duel.SpecialSummon(tc, SUMMON_TYPE_FUSION, tp, tp, false, false, POS_FACEUP)
		tc:CompleteProcedure()
	end
end

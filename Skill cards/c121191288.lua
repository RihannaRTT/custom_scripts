--coded by Lyris
--Chemistry in Motion
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1, EFFECT_COUNT_CODE_SINGLE)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e2 = e1:Clone()
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetRange(LOCATION_SZONE)
	c:RegisterEffect(e2)
end
function s.flip(c)
	return function()
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
end
function s.con(e, tp)
	if not (Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)) then
		return false
	end
	local ct = e:GetOwner():GetTurnCounter() + 1
	if ct == 1 then
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil, tp)
	elseif ct == 2 then
		return Duel.IsExistingMatchingCard(s.sfilter, tp, LOCATION_HAND, 0, 1, nil)
	elseif ct == 3 then
		return Duel.IsExistingMatchingCard(s.nfilter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, nil, 45898858)
	else
		return Duel.IsExistingMatchingCard(Card.IsFaceup, tp, 0, LOCATION_MZONE, 1, nil)
	end
end
function s.cfilter(c, tp)
	return c:IsRace(RACE_DINOSAUR) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil, c:GetAttribute())
end
function s.filter(c, at)
	return c:IsRace(RACE_DINOSAUR) and not c:IsAttribute(at)
end
function s.sfilter(c)
	return c:IsAttribute(ATTRIBUTE_WATER + ATTRIBUTE_WIND) and c:IsRace(RACE_DINOSAUR) and c:IsSummonable(true, nil)
end
function s.nfilter(c)
	return c:IsAttribute(ATTRIBUTE_WATER) and c:IsRace(RACE_DINOSAUR)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	local ct = c:GetTurnCounter() + 1
	c:SetTurnCounter(ct)
	if ct == 1 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DISCARD)
		local tc = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_HAND, 0, 1, 1, nil, tp):GetFirst()
		local at = tc:GetAttribute()
		Duel.SendtoGrave(tc, REASON_DISCARD)
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil, at)
		Duel.SendtoHand(g, nil, 0)
		Duel.ConfirmCards(1 - tp, g)
	elseif ct == 2 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SUMMON)
		local tc = Duel.SelectMatchingCard(tp, s.sfilter, tp, LOCATION_HAND, 0, 1, 1, nil):GetFirst()
		if tc then
			Duel.Summon(tp, tc, true, nil)
		end
	elseif ct == 3 then
		for tc in aux.Next(Duel.GetMatchingGroup(s.nfilter, tp, LOCATION_MZONE, 0, nil)) do
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_CHANGE_CODE)
			e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
			e1:SetValue(22587018)
			tc:RegisterEffect(e1, true)
		end
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local g = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, 1, nil, 45898858)
		Duel.SendtoHand(g, nil, 0)
		Duel.ConfirmCards(1 - tp, g)
	else
		c:SetTurnCounter(0)
		for tc in aux.Next(Duel.GetMatchingGroup(Card.IsFaceup, tp, 0, LOCATION_MZONE, nil)) do
			local e2 = Effect.CreateEffect(c)
			e2:SetType(EFFECT_TYPE_SINGLE)
			e2:SetCode(EFFECT_CHANGE_ATTRIBUTE)
			e2:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
			e2:SetValue(ATTRIBUTE_FIRE)
			tc:RegisterEffect(e2, true)
			local e3 = e2:Clone()
			e3:SetCode(EFFECT_CHANGE_RACE)
			e3:SetValue(RACE_PYRO)
			tc:RegisterEffect(e3, true)
		end
	end
end

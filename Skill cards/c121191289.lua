--coded by Lyris
--The Roids are Alright
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(id)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetCondition(s.con)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_SUMMON_PROC)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetTargetRange(LOCATION_HAND, LOCATION_HAND)
	e2:SetDescription(90)
	e2:SetCondition(s.scon)
	e2:SetTarget(aux.TargetBoolFunction(s.roid))
	c:RegisterEffect(e2)
	local e5 = e2:Clone()
	e5:SetCode(EFFECT_SET_PROC)
	c:RegisterEffect(e5)
	local e6 = Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_FIELD)
	e6:SetCode(EFFECT_UPDATE_ATTACK)
	e6:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e6:SetTargetRange(LOCATION_MZONE, 0)
	e6:SetCondition(s.acon)
	e6:SetTarget(s.atg)
	e6:SetValue(s.aval)
	c:RegisterEffect(e6)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_ADJUST)
	e4:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e4:SetCondition(s.con)
	e4:SetOperation(s.flip(c))
	c:RegisterEffect(e4)
	local e3 = e4:Clone()
	e3:SetCode(EVENT_CHAIN_SOLVED)
	c:RegisterEffect(e3)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
		if Duel.GetFirstMatchingCard(Card.IsOriginalCodeRule, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, 23299957) then
			_G["c23299957"].filter2 = function(tc, e, tp, m, f, chkf)
				return tc:IsType(TYPE_FUSION) and (Duel.IsPlayerAffectedByEffect(tp, id) and tc:IsSetCard(0x16) or tc:IsSetCard(0x1016)) and (not f or f(tc)) and tc:IsCanBeSpecialSummoned(e, SUMMON_TYPE_FUSION, tp, false, false) and tc:CheckFusionMaterial(m, nil, chkf)
			end
		end
	end
end
function s.con(e)
	local c = e:GetOwner()
	return c:IsOriginalCodeRule(id) and c:IsFaceup()
end
function s.scon(e, tc, minc)
	if tc == nil then
		return true
	end
	return s.con(e) and minc == 0 and Duel.GetLocationCount(tc:GetControler(), LOCATION_MZONE) > 0
end
function s.roid(c)
	return c:IsSetCard(0x16) and c:GetOriginalLevel() < 7
end
function s.acon(e)
	local ph = Duel.GetCurrentPhase()
	return s.con(e) and (ph == PHASE_DAMAGE or ph == PHASE_DAMAGE_CAL) and Duel.GetAttackTarget() ~= nil
end
function s.atg(_, tc)
	return s.roid(tc) and Duel.GetAttacker() == tc and not tc:IsType(TYPE_FUSION)
end
function s.aval(_, tc)
	return tc:GetLevel() * 100
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and (not c:IsRace(RACE_MACHINE) or c:IsFacedown())
end
function s.flip(c)
	return function(e, tp)
		if Duel.IsExistingMatchingCard(s.xfilter, tp, LOCATION_ONFIELD, 0, 1, nil) then
			if c:IsOnField() then
				Duel.ChangePosition(c, POS_FACEDOWN)
			else
				c:SetEntityCode(111004001)
			end
		end
	end
end

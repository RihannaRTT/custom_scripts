--coded by Lyris
--Behold, Gate Guardian!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetDescription(1071)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetDescription(94)
	e2:SetCondition(s.acon(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_GRANT)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetTargetRange(LOCATION_MZONE, 0)
	e3:SetCondition(s.gcon(c))
	e3:SetTarget(aux.TargetBoolFunction(Card.IsCode, 25833572))
	c:RegisterEffect(e3)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_TRIGGER_O)
	e4:SetCode(EVENT_BE_BATTLE_TARGET)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCountLimit(1)
	e4:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e4:SetCategory(CATEGORY_ATKCHANGE)
	e4:SetOperation(s.op)
	e3:SetLabelObject(e4)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, nil)
	end
end
function s.acon(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not (not c:IsOriginalCodeRule(id) or c:IsFacedown()) or Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, nil)) and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and Duel.IsExistingMatchingCard(Card.IsLevel, tp, LOCATION_DECK, 0, 1, nil, 11)
	end
end
function s.act()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsCode(25955164, 62340868, 98434877)
end
function s.filter(c, e, tp)
	return c:IsLevel(7) and c:IsCanBeSpecialSummoned(e, 0, tp, false, false)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0), nil, SEQ_DECKSHUFFLE, 0)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, Card.IsLevel, tp, LOCATION_DECK, 0, 1, 1, nil, 11)
	Duel.BreakEffect()
	Duel.SendtoHand(g, nil, 0)
	Duel.ConfirmCards(1 - tp, g)
	local ft = Duel.GetLocationCount(tp, LOCATION_MZONE)
	local tg = Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK + LOCATION_GRAVE, 0, nil, e, tp)
	if ft > 0 and #tg > 0 and Duel.SelectEffectYesNo(tp, c) then
		Duel.ShuffleHand(tp)
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
		local sg = tg:SelectSubGroup(tp, aux.dabcheck, false, 1, math.min(2, ft)) or Group.CreateGroup()
		for tc in aux.Next(sg) do
			if Duel.SpecialSummonStep(tc, 0, tp, tp, false, false, POS_FACEUP) then
				local e1 = Effect.CreateEffect(c)
				e1:SetType(EFFECT_TYPE_SINGLE)
				e1:SetCode(EFFECT_DISABLE)
				e1:SetReset(RESET_EVENT + RESETS_STANDARD)
				tc:RegisterEffect(e1, true)
				local e2 = e1:Clone()
				e2:SetCode(EFFECT_DISABLE_EFFECT)
				e2:SetValue(RESET_TURN_SET)
				tc:RegisterEffect(e2, true)
				local e3 = e1:Clone()
				e3:SetCode(EFFECT_SET_ATTACK_FINAL)
				e3:SetValue(0)
				tc:RegisterEffect(e3, true)
				local e4 = e3:Clone()
				e4:SetCode(EFFECT_SET_DEFENSE_FINAL)
				tc:RegisterEffect(e4, true)
			end
		end
		Duel.SpecialSummonComplete()
	end
end
function s.gcon(c)
	return function()
		return c:IsOriginalCodeRule(id) and c:IsFaceup()
	end
end
function s.op(e, tp)
	local tc = Duel.GetAttacker()
	if not tc:IsRelateToBattle() then
		return
	end
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SET_ATTACK)
	e1:SetReset(RESET_PHASE + PHASE_DAMAGE)
	e1:SetValue(0)
	tc:RegisterEffect(e1, true)
end

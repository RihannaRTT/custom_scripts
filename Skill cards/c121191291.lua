--coded by Lyris
--Under Pressure
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1, EFFECT_COUNT_CODE_SINGLE)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e2 = e1:Clone()
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetRange(LOCATION_SZONE)
	c:RegisterEffect(e2)
end
function s.flip(c)
	return function()
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
end
function s.con(c)
	return function(e, tp)
		return c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_ONFIELD, 0, 1, nil) and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 3
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(62397231)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	local g = Duel.GetDecktopGroup(tp, 4)
	Duel.ConfirmDecktop(tp, 4)
	if g:IsExists(Card.IsCode, 1, nil, 15981690) then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
		local tg = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_ONFIELD, 0, 1, 1, nil)
		Duel.HintSelection(tg)
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		e1:SetValue(1000)
		tg:GetFirst():RegisterEffect(e1, true)
	end
	if g:IsExists(Card.IsCode, 1, nil, 22587018) then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
		local tg = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_ONFIELD, 0, 1, 1, nil)
		Duel.HintSelection(tg)
		local e2 = Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetCode(EFFECT_IMMUNE_EFFECT)
		e2:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		e2:SetValue(s.efilter)
		tg:GetFirst():RegisterEffect(e2, true)
	end
	if g:IsExists(Card.IsCode, 1, nil, 58071123) then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DESTROY)
		local tg = Duel.SelectMatchingCard(tp, Card.IsFaceup, tp, 0, LOCATION_MZONE, 1, 1, nil)
		Duel.HintSelection(tg)
		local tc = tg:GetFirst()
		if tc and not (tc:IsHasEffect(121191258) or tc:IsHasEffect(121191250)) and Duel.Destroy(tc, 0) > 0 then
			Duel.Damage(1 - tp, 800, 0)
		end
	end
	Duel.BreakEffect()
	aux.PlaceCardsOnDeckBottom(tp, g, 0)
end
function s.efilter(e, te)
	return te:GetOwnerPlayer() ~= e:GetHandlerPlayer() and te:IsActiveType(TYPE_MONSTER)
end

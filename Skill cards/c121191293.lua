--coded by Lyris
--HERO World
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.flip(e)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEUP)
	end
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, tp)
	end
end
function s.pfilter(c)
	return c:IsFaceup() and c:IsCanChangePosition()
end
function s.ffilter(tc)
	return tc:IsFaceup() and tc:IsSummonType(SUMMON_TYPE_SPECIAL) and not tc:IsAttribute(ATTRIBUTE_FIRE)
end
function s.filter(c, tp)
	if not (c:IsSetCard(0x8) and c:IsType(TYPE_NORMAL + TYPE_FUSION)) then
		return false
	end
	local b = false
	if c:IsAttribute(ATTRIBUTE_EARTH) then
		b = b or Duel.IsExistingMatchingCard(Card.IsFaceup, tp, 0, LOCATION_MZONE, 1, nil)
	end
	if c:IsAttribute(ATTRIBUTE_WIND) then
		b = b or Duel.IsExistingMatchingCard(Card.IsType, tp, LOCATION_ONFIELD, LOCATION_ONFIELD, 1, nil, TYPE_SPELL + TYPE_TRAP)
	end
	if c:IsAttribute(ATTRIBUTE_FIRE) then
		b = b or Duel.IsExistingMatchingCard(s.ffilter, tp, LOCATION_MZONE, LOCATION_MZONE, 1, nil)
	end
	if c:IsAttribute(ATTRIBUTE_WATER) then
		b = b or Duel.IsPlayerCanDraw(tp, 1)
	end
	if c:IsAttribute(ATTRIBUTE_LIGHT) then
		b = b or Duel.IsExistingMatchingCard(s.pfilter, tp, LOCATION_MZONE, LOCATION_MZONE, 1, nil)
	end
	return b
end
function s.getattr(g)
	local aat = 0
	for tc in aux.Next(g) do
		aat = aat | tc:GetAttribute()
	end
	return aat
end
function s.afilter(c, at)
	return c:IsFaceup() and c:IsAttribute(ATTRIBUTE_EARTH)
end
function s.cfilter(c)
	return c:IsSetCard(0x8) and c:IsType(TYPE_NORMAL + TYPE_FUSION)
end
function s.rfilter(c)
	return c:IsFaceup() and c:IsSummonType(SUMMON_TYPE_SPECIAL) and not c:IsAttribute(ATTRIBUTE_FIRE) and (c:IsControler(tp) or not c:IsHasEffect(121191258))
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.DiscardHand(tp, aux.TRUE, 1, 1, REASON_DISCARD)
	local at = Duel.AnnounceAttribute(tp, 1, s.getattr(Duel.GetMatchingGroup(s.cfilter, tp, LOCATION_MZONE, 0, nil)) & ~ATTRIBUTE_DARK)
	if at == ATTRIBUTE_EARTH then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DESTROY)
		local g1 = Duel.SelectMatchingCard(tp, s.afilter, tp, LOCATION_MZONE, 0, 1, 1, nil)
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DESTROY)
		local g2 = Duel.SelectMatchingCard(tp, Card.IsFaceup, tp, 0, LOCATION_MZONE, 1, 1, nil)
		Duel.HintSelection(g1 + g2)
		local tc = g2:GetFirst()
		if not (tc:IsHasEffect(121191258) or tc:IsHasEffect(121191250)) then
			g1:Merge(g2)
		end
		Duel.Destroy(g1, 0)
	elseif at == ATTRIBUTE_FIRE then
		local g = Duel.GetMatchingGroup(s.rfilter, tp, LOCATION_ONFIELD, LOCATION_ONFIELD, nil)
		Duel.SendtoHand(g, nil, 0)
	elseif at == ATTRIBUTE_LIGHT then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_POSCHANGE)
		local g = Duel.SelectMatchingCard(tp, s.pfilter, tp, LOCATION_MZONE, LOCATION_MZONE, 1, 1, nil)
		Duel.HintSelection(g)
		local tc = g:GetFirst()
		if tc:IsControler(tp) or not tc:IsHasEffect(121191258) then
			Duel.ChangePosition(tc, POS_FACEUP_DEFENSE, 0, POS_FACEUP_ATTACK)
		end
	elseif at == ATTRIBUTE_WATER then
		Duel.Draw(tp, 1, 0)
		local e1 = Effect.CreateEffect(e:GetOwner())
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_TRAP_ACT_IN_HAND)
		e1:SetTargetRange(LOCATION_HAND, 0)
		e1:SetCountLimit(1, id)
		e1:SetTarget(aux.TargetEqualFunction(Card.GetType, TYPE_TRAP))
		e1:SetReset(RESET_PHASE + PHASE_END, 2)
		Duel.RegisterEffect(e1, tp)
		local e2 = Effect.CreateEffect(e:GetOwner())
		e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e2:SetCode(EVENT_CHAINING)
		e2:SetLabelObject(e1)
		e2:SetOperation(s.reset)
		e2:SetReset(RESET_PHASE + PHASE_END, 2)
		Duel.RegisterEffect(e2, tp)
	else
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DESTROY)
		local g = Duel.SelectMatchingCard(tp, Card.IsType, tp, LOCATION_ONFIELD, LOCATION_ONFIELD, 1, 1, nil, TYPE_SPELL + TYPE_TRAP)
		Duel.HintSelection(g)
		Duel.Destroy(g, 0)
	end
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.xfilter(c)
	return c:GetType() == TYPE_TRAP
end
function s.reset(e, tp, eg)
	local ef = e:GetLabelObject()
	if eg:IsExists(s.xfilter, 1, nil) then
		ef:Reset()
		e:Reset()
	end
end

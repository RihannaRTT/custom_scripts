--coded by Lyris
--Power Bond
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		if Duel.GetTurnPlayer() ~= tp or not (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) or not not c:IsOriginalCodeRule(id) and c:IsFaceup() then
			return false
		end
		local chkf = tp
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil, e, tp, chkf)
	end
end
function s.cfilter(c, e, tp, chkf)
	return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, Duel.GetFusionMaterial(tp) - c, chkf)
end
function s.filter(c, e, tp, m, chkf)
	return c:IsType(TYPE_FUSION) and c:IsRace(RACE_MACHINE) and c:CheckFusionMaterial(m, nil, chkf) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_FUSION, tp, false, false)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local chkf = tp
	Duel.DiscardHand(tp, s.cfilter, 1, 1, REASON_DISCARD, nil, e, tp, chkf)
	Duel.BreakEffect()
	local mg = Duel.GetFusionMaterial(tp)
	::cancel::
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	local tc = Duel.GetMatchingGroup(s.filter, tp, LOCATION_EXTRA, 0, nil, e, tp, mg, chkf):Select(tp, 1, 1, nil):GetFirst()
	if tc then
		local mat = Duel.SelectFusionMaterial(tp, tc, mg, nil, chkf)
		if #mat < 2 then
			goto cancel
		end
		tc:SetMaterial(mat)
		Duel.SendtoGrave(mat, REASON_MATERIAL + REASON_FUSION)
		Duel.BreakEffect()
		Duel.SpecialSummon(tc, SUMMON_TYPE_FUSION, tp, tp, false, false, POS_FACEUP)
		tc:CompleteProcedure()
		local c = e:GetOwner()
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(tc:GetBaseAttack())
		e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		tc:RegisterEffect(e1, true)
		local e3 = Effect.CreateEffect(c)
		e3:SetType(EFFECT_TYPE_SINGLE)
		e3:SetCode(EFFECT_CANNOT_DIRECT_ATTACK)
		e3:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		tc:RegisterEffect(e3, true)
		local e2 = Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e2:SetCode(EVENT_PHASE + PHASE_END)
		e2:SetCountLimit(1)
		e2:SetLabel(tc:GetBaseAttack())
		e2:SetReset(RESET_PHASE + PHASE_END)
		e2:SetOperation(s.dop)
		Duel.RegisterEffect(e2, tp)
	end
end
function s.dop(e, tp)
	Duel.Damage(tp, e:GetLabel(), 0)
end

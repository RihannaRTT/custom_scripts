--coded by Lyris, edited by RTT
--Beware the Brothers Paradox!
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetCondition(s.acon(c))
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_DECREASE_TRIBUTE)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetTargetRange(LOCATION_HAND, 0)
	e1:SetCondition(s.con)
	e1:SetTarget(aux.TargetBoolFunction(Card.IsLevel, 7))
	e1:SetValue(0x1, id)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_EXTRA_SUMMON_COUNT)
	e2:SetDescription(91)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetTargetRange(LOCATION_HAND, 0)
	e2:SetTarget(aux.TargetBoolFunction(Card.IsLevel, 7))
	e2:SetValue(0x1)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CHANGE_DAMAGE)
	e3:SetRange(LOCATION_MZONE)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetTargetRange(0, 1)
	e3:SetValue(s.rdcon)
	c:RegisterEffect(e3)
	local e4 = e3:Clone()
	e4:SetCode(EFFECT_NO_EFFECT_DAMAGE)
	c:RegisterEffect(e4)
	local e5 = Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_FIELD)
	e5:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e5:SetCode(EFFECT_CANNOT_ATTACK_ANNOUNCE)
	e5:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e5:SetTargetRange(LOCATION_MZONE, 0)
	e5:SetCondition(s.atkcon(c))
	e5:SetTarget(s.atktg)
	c:RegisterEffect(e5)
	local e6 = Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e6:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e6:SetCode(EVENT_ATTACK_ANNOUNCE)
	e6:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e6:SetCondition(s.con)
	e6:SetOperation(s.checkop)
	e6:SetLabelObject(e5)
	c:RegisterEffect(e6)
	local e7 = Effect.CreateEffect(c)
	e7:SetType(EFFECT_TYPE_FIELD)
	e7:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e7:SetCode(EFFECT_EXTRA_ATTACK_MONSTER)
	e7:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e7:SetTargetRange(LOCATION_MZONE, 0)
	e7:SetTarget(s.extg)
	e7:SetValue(1)
	c:RegisterEffect(e7)
end
function s.acon(c)
	return function(_, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetFieldGroupCount(tp, LOCATION_EXTRA, 0) == 0 and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil)
	end
end
function s.cfilter(c)
	return c:IsCode(25955164, 62340868, 98434877, 25833572) and not c:IsPublic()
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	Duel.ConfirmCards(1 - tp, Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_HAND, 0, 1, 1, nil))
end
function s.rdcon(e, re, val, r, rp, rc)
	if bit.band(r, REASON_EFFECT) ~= 0 then
		return 0
	else
		return val
	end
end
function s.con(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	return c:IsFaceup() and c:IsOriginalCodeRule(id)
end
function s.atkcon(c)
	return function(e)
		return s.con(e) and c:GetFlagEffect(id) > 0
	end
end
function s.atktg(e, c)
	return c:GetFieldID() ~= e:GetLabel()
end
function s.extg(e, c)
	return c:GetAttackAnnouncedCount() > 0
end
function s.checkop(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	if c:GetFlagEffect(id) > 0 then
		return
	end
	local fid = eg:GetFirst():GetFieldID()
	c:RegisterFlagEffect(id, RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END, 0, 1)
	e:GetLabelObject():SetLabel(fid)
end

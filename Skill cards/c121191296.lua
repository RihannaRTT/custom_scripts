--coded by Lyris
--Elements of Thunder, Water, and Wind
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetLP(tp) <= 1000
	end
end
function s.filter(c, code)
	return c:IsFaceup() and c:IsCode(code)
end
function s.sfilter(c, e, tp)
	return c:IsCode(25833572) and c:IsCanBeSpecialSummoned(e, 0, tp, true, true)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetFieldGroup(tp, LOCATION_ONFIELD, LOCATION_ONFIELD)
	local sg = g:Filter(Card.IsControler, nil, tp)
	if #g > 0 and sg:IsExists(s.filter, 1, nil, 25955164) and sg:IsExists(s.filter, 1, nil, 62340868) and sg:IsExists(s.filter, 1, nil, 98434877) and Duel.IsExistingMatchingCard(s.sfilter, tp, LOCATION_DECK, 0, 1, nil, e, tp) and Duel.SelectOption(tp, 1151, 1152) > 0 then
		Duel.SendtoGrave(g, 0)
		Duel.BreakEffect()
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
		Duel.SpecialSummon(Duel.SelectMatchingCard(tp, s.sfilter, tp, LOCATION_DECK, 0, 1, 1, nil, e, tp), 0, tp, tp, true, false, POS_FACEUP)
	else
		local e1 = Effect.CreateEffect(e:GetOwner())
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_SUMMON_PROC)
		e1:SetTargetRange(LOCATION_HAND, 0)
		e1:SetDescription(90)
		e1:SetCondition(s.scon)
		e1:SetTarget(aux.TargetBoolFunction(Card.IsCode, 25955164, 62340868, 98434877))
		e1:SetReset(RESET_PHASE + PHASE_END)
		Duel.RegisterEffect(e1, tp)
	end
end
function s.scon(e, c, minc)
	if c == nil then
		return true
	end
	return minc == 0 and Duel.GetLocationCount(c:GetControler(), LOCATION_MZONE) > 0
end

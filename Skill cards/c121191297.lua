--coded by Lyris
--Believe in your Bro
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_SZONE + LOCATION_REMOVED)
	e1:SetLabel(0)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.flip(c)
	return function()
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
end
function s.con(e, tp)
	if not (Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and e:GetLabel() < 7 and e:GetOwner():GetFlagEffect(id) == 0) then
		return false
	end
	local b = false
	local g = Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_MZONE, 0, 1, nil)
	local hero = g:IsExists(Card.IsSetCard, 1, nil, 0x3008)
	local roid = g:IsExists(s.mroid, 1, nil)
	if e:GetLabel() & 1 == 0 and hero then
		b = b or Duel.IsExistingMatchingCard(Card.IsType, tp, LOCATION_HAND, 0, 1, nil, TYPE_TRAP) and Duel.IsExistingMatchingCard(s.afilter, tp, LOCATION_GRAVE, 0, 1, nil)
	end
	if e:GetLabel() & 2 == 0 and roid then
		b = b or Duel.IsExistingMatchingCard(Card.IsType, tp, LOCATION_HAND, 0, 1, nil, TYPE_SPELL) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.IsExistingMatchingCard(s.sfilter, tp, LOCATION_DECK, 0, 1, nil, e, tp)
	end
	if e:GetLabel() & 4 == 0 and hero and roid then
		local chkf = tp
		b = b or Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and Duel.IsExistingMatchingCard(s.ffilter, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, Duel.GetFusionMaterial(tp):Filter(s.mfilter, nil), chkf)
	end
	return b
end
function s.mroid(c)
	return c:IsRace(RACE_MACHINE) and c:IsSetCard(0x16)
end
function s.afilter(c)
	return c:IsLevelBelow(6) and s.mroid(c)
end
function s.sfilter(c, e, tp)
	return c:IsLevelBelow(6) and (s.mroid(c) or c:IsSetCard(0x3008)) and c:IsCanBeSpecialSummoned(e, 0, tp, false, false, POS_FACEUP_DEFENSE)
end
function s.mfilter(c)
	return c:IsOnField() and c:IsSetCard(0x3008, 0x16)
end
function s.ffilter(c, e, tp, m, chkf)
	return c:IsType(TYPE_FUSION) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_FUSION, tp, false, false) and c:CheckFusionMaterial(m, nil, chkf)
end
function s.dfilter(c, atk)
	return c:IsFaceup() and c:IsAttackBelow(atk)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	local g = Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_MZONE, 0, 1, nil)
	local hero = g:IsExists(Card.IsSetCard, 1, nil, 0x3008)
	local roid = g:IsExists(s.mroid, 1, nil)
	local b1 = e:GetLabel() & 1 == 0 and hero and Duel.IsExistingMatchingCard(Card.IsType, tp, LOCATION_HAND, 0, 1, nil, TYPE_TRAP) and Duel.IsExistingMatchingCard(s.afilter, tp, LOCATION_GRAVE, 0, 1, nil)
	local b2 = e:GetLabel() & 2 == 0 and roid and Duel.IsExistingMatchingCard(Card.IsType, tp, LOCATION_HAND, 0, 1, nil, TYPE_SPELL) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.IsExistingMatchingCard(s.sfilter, tp, LOCATION_DECK, 0, 1, nil, e, tp)
	local chkf = tp
	local b3 = e:GetLabel() & 4 == 0 and hero and roid and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and Duel.IsExistingMatchingCard(s.ffilter, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, Duel.GetFusionMaterial(tp):Filter(s.mfilter, nil), chkf)
	local op
	if not aux.SelectFromOptions then
		local options = { { b1, 1104 }, { b2, 1152 }, { b3, aux.Stringid(id, 0) } }
		local ops = {  }
		local opvals = {  }
		for i = 1, #options do
			if options[i][1] then
				table.insert(ops, options[i][2])
				table.insert(opvals, options[i][3] or i)
			end
		end
		if #ops == 0 then
			return nil
		end
		op = opvals[Duel.SelectOption(tp, table.unpack(ops)) + 1]
	else
		op = aux.SelectFromOptions(tp, { b1, 1104 }, { b2, 1152 }, { b3, aux.Stringid(id, 0) })
	end
	if op == 1 then
		Duel.DiscardHand(tp, Card.IsType, 1, 1, REASON_DISCARD, nil, TYPE_TRAP)
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local tc = Duel.SelectMatchingCard(tp, s.afilter, tp, LOCATION_GRAVE, 0, 1, 1, nil):GetFirst()
		if tc then
			Duel.SendtoHand(tc, nil, 0)
			local dg = Duel.GetMatchingGroup(s.dfilter, tp, 0, LOCATION_MZONE, nil, tc:GetAttack())
			if #dg > 0 and Duel.SelectEffectYesNo(tp, c) then
				Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DESTROY)
				local dc = dg:Select(tp, 1, 1, nil):GetFirst()
				if not (dc:IsHasEffect(121191258) or dc:IsHasEffect(121191250)) then
					Duel.Destroy(dc, 0)
				end
			end
		end
		e:SetLabel(e:GetLabel() | 1)
	elseif op == 2 then
		Duel.DiscardHand(tp, Card.IsType, 1, 1, REASON_DISCARD, nil, TYPE_SPELL)
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
		local tc = Duel.SelectMatchingCard(tp, s.sfilter, tp, LOCATION_DECK, 0, 1, 1, nil, e, tp):GetFirst()
		if Duel.SpecialSummonStep(tc, 0, tp, tp, false, false, POS_FACEUP_DEFENSE) then
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_DISABLE)
			e1:SetReset(RESET_EVENT + RESETS_STANDARD)
			tc:RegisterEffect(e1, true)
			local e2 = e1:Clone()
			e2:SetCode(EFFECT_DISABLE_EFFECT)
			e2:SetValue(RESET_TURN_SET)
			tc:RegisterEffect(e2, true)
		end
		Duel.SpecialSummonComplete()
		e:SetLabel(e:GetLabel() | 2)
	else
		Duel.DiscardHand(tp, aux.TRUE, 1, 1, REASON_DISCARD)
		local mg = Duel.GetFusionMaterial(tp):Filter(s.mfilter, nil)
		::cancel::
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
		local tc = Duel.SelectMatchingCard(tp, s.ffilter, tp, LOCATION_EXTRA, 0, 1, 1, nil, e, tp, mg, chkf):GetFirst()
		if tc then
			local mat = Duel.SelectFusionMaterial(tp, tc, mg, nil, chkf)
			if #mat < 2 then
				goto cancel
			end
			tc:SetMaterial(mat)
			Duel.SendtoGrave(mat, REASON_MATERIAL + REASON_FUSION)
			Duel.BreakEffect()
			Duel.SpecialSummon(tc, SUMMON_TYPE_FUSION, tp, tp, false, false, POS_FACEUP)
			tc:CompleteProcedure()
		end
		e:SetLabel(e:GetLabel() | 4)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

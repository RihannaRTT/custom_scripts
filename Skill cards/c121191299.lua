--coded by The Razgriz, edited by BBeretta
--Ruthless Means
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.acon)
	e1:SetOperation(s.fact)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.flipcon)
	e2:SetOperation(s.flipop)
	c:RegisterEffect(e2)
end
function s.acon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.fact()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.cfilter(c)
	return c:IsSetCard(0x6008) and c:IsType(TYPE_MONSTER) and c:IsFaceup()
end
function s.opfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsFaceup() and (c:GetAttack() > 0 or c:GetDefense() > 0)
end
function s.flipcon(e, tp)
	return Duel.GetFlagEffect(tp, id) == 0 and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(Card.IsDiscardable, tp, LOCATION_HAND, 0, 1, nil) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.IsExistingMatchingCard(s.opfilter, tp, 0, LOCATION_MZONE, 1, nil)
end
function s.flipop(e, tp)
	Duel.RegisterFlagEffect(tp, id, 0, 0, 0)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DISCARD)
	if Duel.DiscardHand(tp, Card.IsDiscardable, 1, 1, REASON_COST + REASON_DISCARD) ~= 0 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SELECT)
		local tc = Duel.SelectMatchingCard(tp, s.opfilter, tp, 0, LOCATION_MZONE, 1, 1, nil):GetFirst()
		local atk = tc:GetAttack()
		local def = tc:GetDefense()
		local b1 = e:GetLabel()
		local b2 = e:GetLabel()
		if tc then
			local op = aux.SelectFromOptions(tp, { b1, aux.Stringid(id, 0) }, { b2, aux.Stringid(id, 1) })
			if op == 1 then
				local e1 = Effect.CreateEffect(e:GetHandler())
				e1:SetType(EFFECT_TYPE_SINGLE)
				e1:SetCode(EFFECT_UPDATE_ATTACK)
				e1:SetRange(LOCATION_MZONE)
				e1:SetValue(-def)
				e1:SetReset(RESET_EVENT | RESETS_STANDARD)
				tc:RegisterEffect(e1)
			elseif op == 2 then
				local e1 = Effect.CreateEffect(e:GetHandler())
				e1:SetType(EFFECT_TYPE_SINGLE)
				e1:SetCode(EFFECT_UPDATE_DEFENSE)
				e1:SetRange(LOCATION_MZONE)
				e1:SetValue(-atk)
				e1:SetReset(RESET_EVENT | RESETS_STANDARD)
				tc:RegisterEffect(e1)
			end
		end
	end
end

--coded by RTT
--Welcome to the Jungle
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_ATKCHANGE + CATEGORY_DEFCHANGE)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_TRIGGER_F)
	e1:SetCode(EVENT_BATTLE_DAMAGE)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCondition(s.bacon)
	e1:SetOperation(s.baop)
	c:RegisterEffect(e1)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEUP)
	end
end
function s.bacon(e, tp, eg, ep, ev, re, r, rp)
	local ac = Duel.GetAttacker()
	local bc = Duel.GetAttackTarget()
	if not bc then
		return false
	end
	if not ac:IsControler(tp) then
		ac, bc = bc, ac
	end
	e:SetLabelObject(bc)
	return ep == tp and ac:IsFaceup() and ac:IsControler(tp) and ac:IsSetCard(0x4) and ac:IsPosition(POS_FACEUP_ATTACK) and bc:IsControler(1 - tp)
end
function s.baop(e, tp, eg, ep, ev, re, r, rp)
	local ac = Duel.GetAttacker()
	local bc = Duel.GetAttackTarget()
	if not ac:IsControler(tp) then
		ac, bc = bc, ac
	end
	if not ac:IsRelateToBattle() or not bc:IsRelateToBattle() or not bc:IsPosition(POS_FACEUP) then
		return
	end
	local val = math.max(ac:GetAttack(), 0)
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
	e1:SetValue(-val)
	bc:RegisterEffect(e1)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_UPDATE_DEFENSE)
	bc:RegisterEffect(e2)
end

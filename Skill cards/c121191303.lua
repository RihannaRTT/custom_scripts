--coded by Lyris
--Archfiend's Conscription
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetCondition(s.fcon)
	e0:SetOperation(s.flip)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SUMMON)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetCondition(s.rcon)
	e1:SetTarget(aux.NOT(aux.TargetEqualFunction(Card.GetRace, RACE_FIEND)))
	c:RegisterEffect(e1)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_CANNOT_MSET)
	c:RegisterEffect(e2)
	local e5 = e1:Clone()
	e5:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	c:RegisterEffect(e5)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetCondition(s.con)
	e3:SetOperation(s.act)
	c:RegisterEffect(e3)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.rcon(e, tp)
	return e:GetOwner():IsOriginalCodeRule(id)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and e:GetOwner():GetFlagEffect(id) == 0
end
function s.filter(c, e, tp, lv)
	return c:IsSetCard(0x45) and c:IsLevel(lv) and c:IsCanBeSpecialSummoned(e, 0, tp, false, false)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local d = Duel.TossDice(tp, 1)
	Duel.SetLP(tp, Duel.GetLP(tp) - d * 200)
	if Duel.GetLP(tp) > 0 and d > 1 and d < 5 and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 then
		local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK + LOCATION_GRAVE + LOCATION_HAND, 0, nil, e, tp, d)
		if #g > 0 and Duel.SelectYesNo(tp, 1152) then
			Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
			local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_GRAVE + LOCATION_HAND, 0, 1, 1, nil, e, tp, d)
			Duel.SpecialSummon(g, 0, tp, tp, false, false, POS_FACEUP)
		end
	end
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

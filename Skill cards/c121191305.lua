--coded by The Razgriz, edited by BBeretta
--Unlocking the Power
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
	local ea = Effect.CreateEffect(c)
	ea:SetType(EFFECT_TYPE_FIELD)
	ea:SetCode(EFFECT_SPSUMMON_PROC)
	ea:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	ea:SetRange(LOCATION_HAND)
	ea:SetCondition(s.spcon)
	ea:SetTarget(s.sptg)
	ea:SetOperation(s.spop)
	local eb = Effect.CreateEffect(c)
	eb:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_GRANT)
	eb:SetRange(0x5f)
	eb:SetTargetRange(LOCATION_HAND, 0)
	eb:SetTarget(function(e, c)
		return c:IsCode(6007213, 32491822, 69890967)
	end)
	eb:SetLabelObject(ea)
	Duel.RegisterEffect(eb, tp)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEUP)
	end
end
function s.spfilter(c)
	return c:IsFaceup() and ((c:IsType(TYPE_CONTINUOUS) and c:IsType(TYPE_TRAP)) or (c:IsType(TYPE_CONTINUOUS) and c:IsType(TYPE_SPELL)) or (c:IsType(TYPE_MONSTER) and c:IsRace(RACE_FIEND) and c:IsPosition(POS_FACEUP))) and c:IsAbleToGraveAsCost()
end
function s.spcon(e, c)
	if c == nil then
		return true
	end
	local tp = c:GetControler()
	local g = Duel.GetMatchingGroup(s.spfilter, tp, LOCATION_ONFIELD, 0, nil, nil)
	return g:CheckSubGroup(aux.mzctcheck, 3, 3, tp)
end
function s.sptg(e, tp, eg, ep, ev, re, r, rp, c)
	local g = Duel.GetMatchingGroup(s.spfilter, tp, LOCATION_ONFIELD, 0, nil, nil)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local sg = g:SelectSubGroup(tp, aux.mzctcheck, true, 3, 3, tp)
	if sg then
		sg:KeepAlive()
		e:SetLabelObject(sg)
		return true
	else
		return false
	end
end
function s.spop(e, tp, eg, ep, ev, re, r, rp, c)
	local g = e:GetLabelObject()
	if not g then
		return
	end
	Duel.SendtoGrave(g, REASON_COST)
	g:DeleteGroup()
end

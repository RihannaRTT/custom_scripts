--coded by Lyris
--Fog Warning
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_ADD_COUNTER + 0x1019)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.op)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_GRANT)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetTargetRange(LOCATION_MZONE, LOCATION_MZONE)
	e2:SetLabelObject(e1)
	e2:SetCondition(s.gcon)
	c:RegisterEffect(e2)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEUP)
	end
end
function s.gcon(e)
	local c = e:GetOwner()
	return c:IsOriginalCodeRule(id) and c:IsFaceup()
end
function s.con(c)
	return function(e, tp, eg, ep, ev, re, r, rp)
		return re:GetHandler() ~= c and re:GetHandler():IsControler(c:GetControler()) and e:GetHandler():GetFlagEffect(id) == 0
	end
end
function s.op(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetHandler()
	c:RegisterFlagEffect(id, RESET_CHAIN, 0, 0)
	c:AddCounter(0x1019, 1)
end

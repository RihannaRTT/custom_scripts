--coded by Lyris
--Dark Unity
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetCondition(s.fcon)
	e0:SetOperation(s.flip)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.apply)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(id, 1))
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCondition(s.rcon)
	e2:SetOperation(s.rop)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetDescription(1190)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetCondition(s.scon)
	e3:SetOperation(s.sop)
	c:RegisterEffect(e3)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.condition(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	return c:IsFaceup() and c:IsOriginalCodeRule(id)
end
function s.cost(e, tp, eg, ep, ev, re, r, rp, chk)
	local cf = c48130397.cost
	local c = e:GetOwner()
	if chk == 0 then
		return cf(e, tp, eg, ep, ev, re, r, rp, 0) and c:IsOriginalCodeRule(id) and c:IsFaceup()
	end
	Duel.Hint(HINT_CARD, tp, id)
	cf(e, tp, eg, ep, ev, re, r, rp, 1)
end
function s.filter2(c, e, tp, m, f, chkf)
	return c:IsType(TYPE_FUSION) and c.dark_calling and (not f or f(c)) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_FUSION, tp, true, false) and c:CheckFusionMaterial(m, nil, chkf)
end
function s.target(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		local chkf = tp
		local mg1 = Duel.GetFusionMaterial(tp):Filter(c48130397.filter3, nil, e)
		local mg2 = Duel.GetMatchingGroup(c48130397.filter1, tp, 0, LOCATION_MZONE, nil, e)
		mg1:Merge(mg2)
		local res = Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, mg1, nil, chkf)
		if not res then
			local ce = Duel.GetChainMaterial(tp)
			if ce ~= nil then
				local fgroup = ce:GetTarget()
				local mg3 = fgroup(ce, e, tp)
				local mf = ce:GetValue()
				res = Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, mg3, mf, chkf)
			end
		end
		return res and mg1:IsExists(aux.NOT(Card.IsImmuneToEffect), 2, nil, e)
	end
	Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, nil, 1, tp, LOCATION_EXTRA)
	if e:IsHasType(EFFECT_TYPE_ACTIVATE) then
		Duel.SetChainLimit(aux.FALSE)
	end
end
function s.activate(e, tp, eg, ep, ev, re, r, rp)
	local chkf = tp
	local mg1 = Duel.GetFusionMaterial(tp):Filter(c48130397.filter3, nil, e)
	local mg2 = Duel.GetMatchingGroup(c48130397.filter1, tp, 0, LOCATION_MZONE, nil, e)
	mg1:Merge(mg2)
	local sg1 = Duel.GetMatchingGroup(s.filter2, tp, LOCATION_EXTRA, 0, nil, e, tp, mg1, nil, chkf)
	local mg3 = nil
	local sg2 = nil
	local ce = Duel.GetChainMaterial(tp)
	if ce ~= nil then
		local fgroup = ce:GetTarget()
		mg3 = fgroup(ce, e, tp)
		local mf = ce:GetValue()
		sg2 = Duel.GetMatchingGroup(s.filter2, tp, LOCATION_EXTRA, 0, nil, e, tp, mg3, mf, chkf)
	end
	if #sg1 > 0 or (sg2 ~= nil and #sg2 > 0) then
		local sg = sg1:Clone()
		if sg2 then
			sg:Merge(sg2)
		end
		::cancel::
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
		local tg = sg:Select(tp, 1, 1, nil)
		local tc = tg:GetFirst()
		if sg1:IsContains(tc) and (sg2 == nil or not sg2:IsContains(tc) or not Duel.SelectYesNo(tp, ce:GetDescription())) then
			local mat1 = Duel.SelectFusionMaterial(tp, tc, mg1, nil, chkf)
			if #mat1 < 2 then
				goto cancel
			end
			tc:SetMaterial(mat1)
			Duel.SendtoGrave(mat1, REASON_EFFECT + REASON_MATERIAL + REASON_FUSION)
			Duel.BreakEffect()
			Duel.SpecialSummon(tc, SUMMON_TYPE_FUSION, tp, tp, true, false, POS_FACEUP)
		else
			local mat2 = Duel.SelectFusionMaterial(tp, tc, mg3, nil, chkf)
			if #mat2 < 2 then
				goto cancel
			end
			local fop = ce:GetOperation()
			fop(ce, e, tp, tc, mat2)
		end
		tc:CompleteProcedure()
	end
end
function s.apply(e, tp, eg, ep, ev, re, r, rp)
	for tc in aux.Next(Duel.GetMatchingGroup(Card.IsOriginalCodeRule, tp, 0xff, 0xff, nil, 48130397)) do
		local e1 = Effect.CreateEffect(e:GetOwner())
		e1:SetCategory(CATEGORY_SPECIAL_SUMMON + CATEGORY_FUSION_SUMMON)
		e1:SetDescription(aux.Stringid(id, 0))
		e1:SetType(EFFECT_TYPE_ACTIVATE)
		e1:SetCode(EVENT_FREE_CHAIN)
		e1:SetHintTiming(0, TIMINGS_CHECK_MONSTER + TIMING_END_PHASE)
		e1:SetCondition(s.condition)
		e1:SetCost(s.cost)
		e1:SetTarget(s.target)
		e1:SetOperation(s.activate)
		tc:RegisterEffect(e1, true)
	end
end
function s.rfilter(c)
	return c:IsFaceup() and c:IsSummonType(SUMMON_TYPE_SPECIAL) and not c:IsRace(RACE_ROCK)
end
function s.rcon(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	local u = c:GetFlagEffectLabel(id)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not u or u & 1 == 0) and Duel.IsExistingMatchingCard(s.rfilter, tp, 0, LOCATION_MZONE, 1, nil)
end
function s.rop(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	Duel.SetLP(tp, Duel.GetLP(tp) - 2000)
	if Duel.GetLP(tp) > 0 then
		Duel.BreakEffect()
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
		local g = Duel.SelectMatchingCard(tp, s.rfilter, tp, 0, LOCATION_MZONE, 1, 1, nil)
		Duel.HintSelection(g)
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_CHANGE_RACE)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD)
		e1:SetValue(RACE_ROCK)
		g:GetFirst():RegisterEffect(e1, true)
	else
		Duel.AdjustAll()
	end
	local u = c:GetFlagEffectLabel(id)
	if u then
		c:SetFlagEffectLabel(id, u | 1)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 1)
	end
end
function s.scon(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	local u = c:GetFlagEffectLabel(id)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not u or u & 2 == 0) and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK, 0, 1, nil, 94820406)
end
function s.sop(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	Duel.DiscardHand(tp, nil, 1, 1, REASON_DISCARD)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK, 0, 1, 1, nil, 94820406)
	Duel.SendtoHand(g, nil, 0)
	Duel.ConfirmCards(1 - tp, g)
	local u = c:GetFlagEffectLabel(id)
	if u then
		c:SetFlagEffectLabel(id, u | 2)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 2)
	end
end

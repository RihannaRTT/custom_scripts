--coded by The Razgriz, edited by BBeretta
--Delicious Morsel
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCode(EVENT_BATTLE_DAMAGE)
	e1:SetCondition(s.damcon)
	e1:SetOperation(s.damop)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_PHASE + PHASE_END)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCountLimit(1)
	e2:SetCondition(s.reccon)
	e2:SetOperation(s.recop)
	c:RegisterEffect(e2)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEUP)
	end
end
function s.damcon(e, tp, eg, ep, ev, re, r, rp)
	local a = Duel.GetBattleMonster(tp)
	if a and a:IsRace(RACE_ZOMBIE) and a:IsSetCard(0x8e) then
		Duel.RegisterFlagEffect(tp, id, RESET_PHASE + PHASE_END, 0, 1)
	end
	return ep == 1 - tp
end
function s.damop(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	local dam = 0
	if c:GetFlagEffect(id) > 0 then
		dam = c:GetFlagEffectLabel(id)
	end
	c:ResetFlagEffect(id)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1, dam + ev)
end
function s.reccon(e, tp, eg, ep, ev, re, r, rp)
	return Duel.GetFlagEffect(tp, id) > 0
end
function s.recop(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	local heal = 0
	if c:GetFlagEffect(id) > 0 then
		heal = c:GetFlagEffectLabel(id) // 2
	end
	Duel.Recover(tp, heal, 0)
end

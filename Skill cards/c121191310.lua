--coded by RTT
--Order of the Queen
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_BATTLED)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCondition(s.con)
	e1:SetOperation(s.operation)
	c:RegisterEffect(e1)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
	end
end
function s.operation(e, tp)
	local a = Duel.GetAttacker()
	local d = Duel.GetAttackTarget()
	local p = e:GetHandler():GetControler()
	if d == nil then
		return
	end
	local tc = nil
	if a:GetControler() == p and a:IsSetCard(0x4) and d:IsStatus(STATUS_BATTLE_DESTROYED) then
		tc = d
	end
	if not tc then
		return
	end
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_DISABLE)
	e1:SetReset(RESET_EVENT + 0x17a0000)
	tc:RegisterEffect(e1, true)
	local e2 = Effect.CreateEffect(e:GetHandler())
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_DISABLE_EFFECT)
	e2:SetReset(RESET_EVENT + 0x17a0000)
	tc:RegisterEffect(e2, true)
end
function s.cfilter(c, code)
	return c:IsFaceup() and c:IsCode(code)
end
function s.con(e)
	local tp = e:GetHandlerPlayer()
	return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, nil, 15951532)
end

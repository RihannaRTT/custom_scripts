--coded by RTT, edited by BBeretta
--Archfiend's Promotion
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetCondition(s.fcon)
	e0:SetOperation(s.flip)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetCountLimit(1)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil, tp) and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil, e, tp) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local cg = Duel.SelectReleaseGroup(tp, s.cfilter, 1, 1, nil, tp)
	Duel.Release(cg, 0)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, 1, nil, e, tp)
	if #g > 0 then
		Duel.SpecialSummon(g, 0, tp, tp, false, false, POS_FACEUP)
	end
end
function s.cfilter(c, tp)
	local g = c:GetColumnGroup():Filter(Card.IsControler, nil, 1 - tp)
	return c:IsFaceup() and #g == 0 and c:IsReleasable() and c:IsCode(73219648) and Duel.GetMZoneCount(tp, c) > 0
end
function s.filter(c, e, tp)
	return c:IsLevelBelow(8) and c:IsCode(52248570, 8581705, 9603356, 35798491, 72192100) and c:IsCanBeSpecialSummoned(e, 0, tp, false, false)
end

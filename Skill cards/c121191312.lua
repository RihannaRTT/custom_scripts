--coded by RTT
--Setting Sun
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_REMOVE)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetTargetRange(LOCATION_MZONE, 0)
	e1:SetTarget(s.batg)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetCategory(CATEGORY_REMOVE + CATEGORY_TOHAND + CATEGORY_SEARCH)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCountLimit(1)
	e2:SetCondition(s.condition)
	e2:SetTarget(s.target)
	e2:SetOperation(s.operation)
	c:RegisterEffect(e2)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEUP)
	end
end
function s.batg(e, c)
	return c:IsFaceup() and c:IsCode(54493213, 80887952, 17286057, 51043053)
end
function s.cfilter(c)
	return c:IsFaceup() and c:GetTextAttack() == -2 and c:IsType(TYPE_MONSTER)
end
function s.condition(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil) and c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
end
function s.xfilter(c, code)
	return c:IsFaceup() and c:IsCode(code)
end
function s.filter(c)
	return c:GetTextAttack() == -2 and c:IsType(TYPE_MONSTER) and c:IsAbleToHand() and not Duel.IsExistingMatchingCard(s.xfilter, tp, LOCATION_ONFIELD, 0, 1, nil, c:GetCode())
end
function s.target(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return Duel.IsExistingMatchingCard(Card.IsAbleToRemove, tp, LOCATION_HAND, 0, 1, nil)
	end
	Duel.SetOperationInfo(0, CATEGORY_REMOVE, nil, 1, tp, LOCATION_HAND)
end
function s.operation(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetHandler()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_REMOVE)
	local tc = Duel.SelectMatchingCard(tp, Card.IsAbleToRemove, tp, LOCATION_HAND, 0, 1, 1, nil):GetFirst()
	if not tc then
		return
	end
	if Duel.Remove(tc, POS_FACEUP, 0) > 0 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local sc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil):GetFirst()
		Duel.BreakEffect()
		Duel.SendtoHand(sc, nil, 0)
		Duel.ConfirmCards(1 - tp, sc)
	end
end

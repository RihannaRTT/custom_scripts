--coded by The Razgriz, edited by BBeretta and RTT
--Victory of the Shadow Riders
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.set(c))
	c:RegisterEffect(e0)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e0:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e0:SetCountLimit(1)
	e0:SetOperation(s.rcon(c))
	Duel.RegisterEffect(e0, 0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_CANNOT_TO_DECK)
	e2:SetRange(0xff)
	c:RegisterEffect(e2)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PREDRAW)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e1:SetRange(LOCATION_SZONE)
	e1:SetCondition(s.flipcon)
	e1:SetOperation(s.flipop)
	Duel.RegisterEffect(e1, tp)
	local e3 = Effect.CreateEffect(c)
	e3:SetCategory(CATEGORY_TOHAND + CATEGORY_SEARCH)
	e3:SetType(EFFECT_TYPE_ACTIVATE)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetLabel(100)
	e3:SetOperation(s.activate)
	c:RegisterEffect(e3)
end
function s.set(c)
	return function(e, tp)
		if not Duel.MoveToField(c, tp, tp, LOCATION_SZONE, POS_FACEDOWN, true, LOCATION_HAND) then
			c:SetEntityCode(id)
			if Duel.GetDuelOptions() & LOCATION_DECK > 0 then
				Duel.MoveSequence(c, 1)
				Duel.MoveSequence(c, 3)
			end
			Duel.MoveSequence(c, 2)
		end
	end
end
function s.rcon(c)
	return function(e, tp)
		local tp = c:GetControler()
		if Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 then
			Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0):GetFirst(), nil, SEQ_DECKBOTTOM, REASON_RULE)
		end
	end
end
function s.filter(c)
	return c:IsCode(69890967, 6007213, 32491822) and c:IsAbleToHand()
end
function s.flipcon(e)
	local tp = e:GetHandlerPlayer()
	return Duel.GetTurnCount(tp) == 1 and Duel.GetTurnPlayer() == tp
end
function s.flipop(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetHandler()
	Duel.ChangePosition(c, POS_FACEUP)
	local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK, 0, nil)
	local gs = Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_SZONE, 0, nil)
	if #g > 0 and #gs > 0 then
		Duel.Hint(HINT_CARD, tp, id)
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local sg = g:Select(tp, 1, 1, nil)
		Duel.SendtoHand(sg, nil, 0)
		Duel.ConfirmCards(1 - tp, sg)
	end
	local e2 = Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(id, 1))
	e2:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e2:SetType(EFFECT_TYPE_IGNITION)
	e2:SetRange(LOCATION_SZONE)
	e2:SetCountLimit(1)
	e2:SetCondition(s.spdcon)
	e2:SetCost(s.spcost)
	e2:SetTarget(s.sptg)
	e2:SetOperation(s.spop)
	Duel.RegisterEffect(e2, tp)
end
function s.spcon(e)
	local tp = e:GetHandlerPlayer()
	return Duel.GetFlagEffect(tp, id) == 0
end
function s.spcost(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return Duel.IsExistingMatchingCard(Card.IsDiscardable, tp, LOCATION_HAND, 0, 1, nil)
	end
	Duel.DiscardHand(tp, Card.IsDiscardable, 1, 1, REASON_COST + REASON_DISCARD)
end
function s.spfilter(c, e, tp)
	return c:IsRace(RACE_FIEND) and c:IsAttack(0) and c:IsDefense(0) and c:IsCanBeSpecialSummoned(e, 0, tp, false, false)
end
function s.sptg(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.IsExistingMatchingCard(s.spfilter, tp, LOCATION_GRAVE, 0, 1, nil, e, tp)
	end
	Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, nil, 1, tp, LOCATION_GRAVE)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	local g = Duel.SelectMatchingCard(tp, s.spfilter, tp, LOCATION_GRAVE, 0, 1, 1, nil, e, tp)
	if #g > 0 then
		Duel.SpecialSummon(g, 0, tp, tp, false, false, POS_FACEUP)
	end
end
function s.spdcon(e)
	local c = e:GetHandler()
	return c:IsLocation(LOCATION_SZONE)
end
function s.activate(e, tp, eg, ep, ev, re, r, rp)
	local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK, 0, nil)
	local gs = Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_SZONE, 0, nil)
	if #g > 0 and #gs > 0 then
		Duel.Hint(HINT_CARD, tp, id)
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local sg = g:Select(tp, 1, 1, nil)
		Duel.SendtoHand(sg, nil, 0)
		Duel.ConfirmCards(1 - tp, sg)
	end
end

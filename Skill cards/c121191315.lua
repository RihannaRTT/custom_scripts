--coded by The Razgriz, edited by BBeretta and Lyris
--Cloudy Skies of Grey
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_LEAVE_FIELD_P)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetOperation(s.ctop)
	c:RegisterEffect(e1)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEUP)
	end
end
function s.ctfilter(c)
	local count = c:GetCounter(0x1019)
	return c:IsFaceup() and c:IsType(TYPE_MONSTER) and c:IsCanAddCounter(0x1019, count)
end
function s.ctop(e, tp, eg, ep, ev, re, r, rp)
	local count = 0
	for tc in aux.Next(eg) do
		if tc and tc:IsLocation(LOCATION_MZONE) and tc:IsSetCard(0x18) and tc:IsReason(REASON_DESTROY) and tc:GetCounter(0x1019) > 0 then
			count = count + tc:GetCounter(0x1019)
			local g = Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_MZONE, LOCATION_MZONE, eg)
			if g:GetCount() > 0 then
				if count > 0 and (Duel.GetFieldGroupCount(tp, LOCATION_MZONE, 0) > 0 or Duel.GetFieldGroupCount(tp, 0, LOCATION_MZONE) > 0) then
					Duel.Hint(HINT_CARD, 0, id)
					Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_COUNTER)
					local g = Duel.SelectMatchingCard(tp, s.ctfilter, tp, LOCATION_MZONE, LOCATION_MZONE, 1, 1, eg)
					Duel.HintSelection(g)
					local tc = g:GetFirst()
					tc:AddCounter(0x1019, count)
					local count = 0
				end
			end
		end
	end
end

--coded by Lyris
--Ancient Ruler Rises
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e2:SetCountLimit(1)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_UPDATE_ATTACK)
	e3:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e3:SetTargetRange(LOCATION_MZONE, 0)
	e3:SetTarget(aux.TargetBoolFunction(Card.IsCode, 25343280))
	e3:SetValue(s.val)
	c:RegisterEffect(e3)
end
function s.flip(e, tp)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEUP)
	end
end
function s.cfilter(c)
	return c:IsLevel(2) and c:IsRace(RACE_ZOMBIE)
end
function s.filter(c)
	return c:IsFaceupEx() and c:IsCode(31076103, 4081094, 78697395)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_DECK, 0, 1, nil) and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK + LOCATION_GRAVE + LOCATION_REMOVED, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	Duel.SendtoGrave(Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_DECK, 0, 1, 1, nil), 0)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	Duel.MoveToField(Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_GRAVE + LOCATION_REMOVED, 0, 1, 1, nil):GetFirst(), tp, tp, LOCATION_SZONE, POS_FACEUP, true)
end
function s.pfilter(c)
	return c:IsFaceupEx() and c:IsCode(52550973, 89959682)
end
function s.val(e, c)
	return Duel.GetMatchingGroupCount(s.pfilter, e:GetOwner():GetControler(), LOCATION_GRAVE + LOCATION_ONFIELD, 0, 1, nil) * 500
end

--coded by Lyris
--TCG "Middle Age Mechs"
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act(c))
	Duel.RegisterEffect(e1, 0)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_CANNOT_TO_DECK)
	e0:SetRange(0xff)
	c:RegisterEffect(e0)
	c:EnableCounterPermit(0xb)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_SZONE)
	e2:SetTargetRange(LOCATION_MZONE, LOCATION_MZONE)
	e2:SetTarget(aux.TargetBoolFunction(Card.IsSetCard, 0x7))
	e2:SetValue(300)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_SUMMON_SUCCESS)
	e3:SetRange(LOCATION_SZONE)
	e3:SetOperation(s.addc)
	c:RegisterEffect(e3)
	local e4 = e3:Clone()
	e4:SetCode(EVENT_MSET)
	c:RegisterEffect(e4)
	local e4 = Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(92001300, 0))
	e4:SetType(EFFECT_TYPE_FIELD)
	e4:SetCode(EFFECT_SUMMON_PROC)
	e4:SetRange(LOCATION_SZONE)
	e4:SetTargetRange(LOCATION_HAND, 0)
	e4:SetTarget(aux.TargetBoolFunction(Card.IsSetCard, 0x7))
	e4:SetCondition(s.sumcon)
	e4:SetOperation(s.sumop)
	e4:SetValue(SUMMON_TYPE_ADVANCE)
	c:RegisterEffect(e4)
	local e5 = Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_ACTIVATE)
	e5:SetCode(EVENT_FREE_CHAIN)
	e5:SetLabel(100)
	c:RegisterEffect(e5)
end
function s.con(c)
	return function()
		return Duel.GetTurnPlayer() == c:GetControler() and (not c:IsOriginalCodeRule(id) or c:IsFacedown())
	end
end
function s.act(c)
	return function(e, tp)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		else
			c:SetEntityCode(id)
			local tp = c:GetControler()
			Duel.MoveToField(c, tp, tp, LOCATION_SZONE, POS_FACEUP, true)
		end
		if Duel.GetDuelOptions() & 0x120 > 0 then
			Duel.MoveSequence(c, 1)
			Duel.MoveSequence(c, 3)
		end
		Duel.MoveSequence(c, 2)
		c:SetCardData(CARDDATA_TYPE, TYPE_SPELL + TYPE_CONTINUOUS)
		Duel.Hint(HINT_CARD, 0, id)
	end
end
function s.addc(e, tp, eg, ep, ev, re, r, rp)
	e:GetOwner():AddCounter(0xb, 1)
end
function s.sumcon(e, c, minc)
	if c == nil then
		return e:GetOwner():IsReleasable()
	end
	local mi, ma = c:GetTributeRequirement()
	if mi < minc then
		mi = minc
	end
	if ma < mi then
		return false
	end
	return ma > 0 and e:GetOwner():GetCounter(0xb) >= mi and Duel.GetLocationCount(c:GetControler(), LOCATION_MZONE) > 0
end
function s.sumop(e, tp, eg, ep, ev, re, r, rp, c)
	Duel.Release(e:GetOwner(), REASON_COST)
	c:SetMaterial(Group.FromCards(e:GetOwner()))
end



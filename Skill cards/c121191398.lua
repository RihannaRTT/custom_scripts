--coded by Lyris
--TCG "Cyberdark Style"
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetCondition(s.fcon)
	e0:SetOperation(s.flip)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e1:SetLabel(3)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x4093)
end
function s.con(c)
	return function(e, tp)
		local lpc = e:GetLabel() * 1000
		return c:IsOriginalCodeRule(id) and c:IsFaceup() and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and lpc > 0 and Duel.GetLP(tp) <= lpc and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 3, nil) and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 3, 3, nil)
	Duel.ConfirmCards(1 - tp, g)
	local sc = g:RandomSelect(1 - tp, 1):GetFirst()
	g:RemoveCard(sc)
	Duel.SendtoHand(sc, nil, 0)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, sc):GetFirst()
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, 0)
	Duel.BreakEffect()
	local c = e:GetOwner()
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEDOWN)
	else
		c:SetEntityCode(111004001)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() - 1)
end

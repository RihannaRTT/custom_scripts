--coded by Lyris
--Pre-Errata "Final Draw"
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.fcon(c))
	e2:SetOperation(s.flip(c))
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_ACTIVATE)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetCountLimit(1)
	e3:SetCondition(s.con(c))
	e3:SetOperation(s.act(c))
	c:RegisterEffect(e3)
	local e1 = e3:Clone()
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetRange(LOCATION_SZONE)
	c:RegisterEffect(e1)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_DRAW)
	e4:SetRange(LOCATION_SZONE + LOCATION_REMOVED)
	e4:SetOperation(s.reptg)
	c:RegisterEffect(e4)
end
function s.fcon(c)
	return function(e, tp)
		return (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.flip(c)
	return function()
		Duel.Hint(HINT_CARD, 0, id)
		c:SetTurnCounter(0)
	end
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id) and c:IsFaceup()
	end
end
function s.act(c)
	return function()
		Duel.Hint(HINT_CARD, 0, id)
		c:SetTurnCounter(c:GetTurnCounter() + 1)
	end
end
function s.reptg(e, tp, eg, ep, ev, re, r, rp)
	if rp ~= tp or e:GetOwner():GetTurnCounter() < 3 or #eg ~= 1 or not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, nil, tp, LOCATION_DECK, 0, 1, 1, nil)
	Duel.SendtoHand(g, nil, 0)
	Duel.ConfirmCards(1 - tp, g)
	Duel.ShuffleDeck(tp)
	Duel.SendtoDeck(eg, tp, SEQ_DECKTOP, 0)
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
	e1:SetCode(EVENT_PHASE + PHASE_END)
	e1:SetCountLimit(1)
	e1:SetOperation(s.op)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
end
function s.op(e, tp)
	Duel.Win(1 - tp, 65)
end

--coded by Lyris
--Pre-Errata "Nightmare Sonic Blast!"
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.fcon(c))
	e2:SetOperation(s.flip)
	c:RegisterEffect(e2)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_SZONE)
	e1:SetCountLimit(1)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(66516792)
end
function s.fcon(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_ONFIELD, 0, 1, nil) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_ONFIELD, 0, 1, nil) and c:IsFaceup() and c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetDecktopGroup(tp, 4)
	local ct = g:FilterCount(Card.IsType, nil, TYPE_MONSTER)
	Duel.ConfirmDecktop(tp, 4)
	if ct == 1 or ct > 2 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local sg = g:Select(tp, 1, 1, nil)
		Duel.SendtoHand(sg, nil, 0)
		g:Sub(sg)
	end
	if ct >= 2 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DESTROY)
		local tg = Duel.SelectMatchingCard(tp, nil, tp, 0, LOCATION_ONFIELD, 1, 1, nil)
		Duel.HintSelection(tg)
		local tc = tg:GetFirst()
		if tc and not (tc:IsHasEffect(121191250) or tc:IsHasEffect(121191258)) then
			if ct > 2 then
				Duel.BreakEffect()
			end
			Duel.Destroy(tg, 0)
		end
	end
	Duel.SortDecktop(tp, tp, #g)
end

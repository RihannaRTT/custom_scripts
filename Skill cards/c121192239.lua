--coded by Lyris
--Pre-Errata "Ritual Ceremony"
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil, tp) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local tc = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_HAND, 0, 1, 1, nil, tp):GetFirst()
	Duel.ConfirmCards(1 - tp, tc)
	Duel.ShuffleHand(tp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, s.filter2, tp, LOCATION_DECK, 0, 1, 1, nil, tc, tp)
	if #g > 0 then
		Duel.SendtoHand(g, nil, 0)
		Duel.ConfirmCards(1 - tp, g)
	end
end
function s.cfilter(c, tp)
	return c:GetType() & 0x81 == 0x81 and not c:IsPublic() and Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_DECK, 0, 1, nil, c, tp, true)
end
function s.filter2(c, mc, tp, chk)
	if not c:GetType() & 0x82 == 0x82 then
		return false
	end
	if aux.IsCodeListed(c, mc:GetCode()) then
		return true
	end
	local mt = getmetatable(c)
	return mt and mt.ritualFilter and mt.ritualFilter(mc, c:GetActivateEffect(), tp, chk)
end

--coded by Lyris, edited by RTT
--Pre-Errata "Cocoon of Ultra Evolution"
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.fcon)
	e2:SetOperation(s.flip)
	c:RegisterEffect(e2)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetRange(LOCATION_REMOVED + LOCATION_SZONE)
	e0:SetLabel(0)
	e0:SetDescription(1152)
	e0:SetCondition(s.scon(c))
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
	local e1 = e0:Clone()
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetCondition(s.dcon(c))
	e1:SetOperation(s.dact)
	c:RegisterEffect(e1)
end
function s.fcon(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsOriginalCodeRule(111004001) or c:IsFacedown())
end
function s.flip()
	Duel.Hint(HINT_CARD, 0, id)
end
function s.scon(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and LOCATION_MZONE or 0, 1, nil, tp) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil, e, tp) and c:IsOriginalCodeRule(id) and c:IsFaceup() and s.isOpenState(tp) and e:GetLabel() < 1
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_OPSELECTED, 0, e:GetDescription())
	local loc = LOCATION_MZONE
	if Duel.GetLocationCount(tp, LOCATION_MZONE) < 1 then
		loc = 0
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_RELEASE)
	local g = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_MZONE, loc, 1, 1, nil, tp)
	if #g > 0 and Duel.Release(g, 0) > 0 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
		local sg = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil, e, tp)
		if #sg > 0 then
			Duel.SpecialSummon(sg, 0, tp, tp, true, false, POS_FACEUP)
		end
	end
	if c:IsOnField() then
		Duel.ChangePosition(c, POS_FACEDOWN)
	else
		c:SetEntityCode(111004001)
	end
	e:SetLabel(e:GetLabel() + 1)
end
function s.dcon(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(Card.IsRace, tp, LOCATION_GRAVE, 0, 1, nil, RACE_INSECT) and Duel.IsPlayerCanDraw(tp, 1) and s.isOpenState(tp) and c:IsOriginalCodeRule(id) and c:IsFaceup() and e:GetLabel() < 1
	end
end
function s.dact(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_OPSELECTED, 0, e:GetDescription())
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = Duel.SelectMatchingCard(tp, Card.IsRace, tp, LOCATION_GRAVE, 0, 1, 1, nil, RACE_INSECT)
	local tc = g:GetFirst()
	if tc then
		Duel.HintSelection(g)
		Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, 0)
		if tc:IsLocation(LOCATION_DECK) then
			Duel.ShuffleDeck(tp)
		end
		if tc:IsLocation(LOCATION_DECK + LOCATION_EXTRA) then
			Duel.BreakEffect()
			Duel.Draw(tp, 1, 0)
			if c:IsOnField() then
				Duel.ChangePosition(c, POS_FACEDOWN)
			else
				c:SetEntityCode(111004001)
			end
		end
	end
	e:SetLabel(e:GetLabel() + 1)
end
function s.cfilter(c, tp)
	return c:IsFaceup() and c:IsRace(RACE_INSECT) and c:GetEquipCount() > 0 and (c:IsControler(tp) or not c:IsHasEffect(121191258))
end
function s.filter(c, e, tp)
	return c:IsRace(RACE_INSECT) and c:IsType(TYPE_MONSTER) and c:IsCanBeSpecialSummoned(e, 0, tp, true, false)
end
function s.isOpenState(tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
end
function s.tg(e)
	return function(ef, tc, sump, st, spos, tp, se)
		return se ~= e
	end
end

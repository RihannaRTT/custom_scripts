--coded by Lyris
--Pre-Errata "Inner Conflict"
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(Card.IsAbleToChangeControler, tp, 0, LOCATION_MZONE, 1, nil) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.PayLPCost(tp, Duel.GetLP(tp) // 2)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONTROL)
	local g = Duel.SelectMatchingCard(tp, Card.IsAbleToChangeControler, tp, 0, LOCATION_MZONE, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	if tc and not tc:IsHasEffect(121191258) and Duel.GetControl(g, tp, PHASE_END, 1) then
		local e1 = Effect.CreateEffect(e:GetOwner())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_CANNOT_DIRECT_ATTACK)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		tc:RegisterEffect(e1, true)
	end
end

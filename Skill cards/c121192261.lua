--coded by Lyris
--Pre-Errata "Union Combination"
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	local e1 = e2:Clone()
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetRange(LOCATION_SZONE)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		if not (Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)) or c:GetFlagEffect(id) > 0 then
			return false
		end
		local chkf = tp
		return Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, Duel.GetFusionMaterial(tp):Filter(Card.IsOnField, nil), chkf)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local chkf = tp
	::cancel::
	local mg = Duel.GetFusionMaterial(tp):Filter(Card.IsOnField, nil)
	local sg = Duel.GetMatchingGroup(s.filter2, tp, LOCATION_EXTRA, 0, nil, e, tp, mg, chkf)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	local tc = sg:Select(tp, 1, 1, nil):GetFirst()
	if tc then
		if s.isalpha(tc) then
			mg:Merge(Duel.GetMatchingGroup(s.filter1, tp, LOCATION_GRAVE, 0, nil) + Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_SZONE, 0, nil))
		end
		local mat = Duel.SelectFusionMaterial(tp, tc, mg, nil, chkf)
		if #mat < 2 then
			goto cancel
		end
		tc:SetMaterial(mat)
		if not s.isalpha(tc) then
			local gmat = mat:Filter(Card.IsLocation, nil, LOCATION_GRAVE)
			Duel.Remove(gmat, POS_FACEUP, REASON_MATERIAL + REASON_FUSION)
			mat:Sub(gmat)
			Duel.SendtoGrave(mat, REASON_MATERIAL + REASON_FUSION)
		else
			Duel.Remove(mat, POS_FACEUP, REASON_MATERIAL + REASON_FUSION)
		end
		Duel.BreakEffect()
		Duel.SpecialSummon(tc, SUMMON_TYPE_FUSION, tp, tp, true, false, POS_FACEUP)
		tc:CompleteProcedure()
	end
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CHANGE_DAMAGE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(0, 1)
	e1:SetValue(0)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_NO_EFFECT_DAMAGE)
	e2:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e2, tp)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.isalpha(c)
	return c:IsCode(1561110, 2111707, 25119460, 58859575, 65172015, 75906310, 84243274, 91998119, 99724761)
end
function s.filter2(c, e, tp, m, chkf)
	local mg = m:Clone()
	if s.isalpha(c) then
		mg:Merge(Duel.GetMatchingGroup(s.filter1, tp, LOCATION_GRAVE, 0, nil) + Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_SZONE, 0, nil))
	end
	return c:IsType(TYPE_FUSION) and c:IsAttribute(ATTRIBUTE_LIGHT) and c:IsRace(RACE_MACHINE) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_FUSION, tp, true, false) and c:CheckFusionMaterial(mg, nil, chkf)
end
function s.filter1(c)
	return c:IsType(TYPE_MONSTER) and c:IsCanBeFusionMaterial()
end

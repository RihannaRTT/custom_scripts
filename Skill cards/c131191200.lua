--coded by Lyris
--Power of Dark
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		Duel.MoveToField(Duel.CreateToken(tp, 59197169), tp, tp, LOCATION_FZONE, POS_FACEUP, true)
	end
end

--coded by Lyris
--Prescience
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_TURN_END)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCondition(s.con)
	e2:SetOperation(s.flip(c))
	c:RegisterEffect(e2)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		local e3 = Effect.CreateEffect(c)
		e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e3:SetCode(EVENT_FREE_CHAIN)
		e3:SetDescription(1158)
		e3:SetCondition(s.lookcon)
		e3:SetOperation(s.look)
		e3:SetReset(RESET_PHASE + PHASE_END, 5)
		Duel.RegisterEffect(e3, tp)
	end
end
function s.lookcon(e, tp, eg, ep, ev, re, r, rp)
	return e:GetOwner():IsOriginalCodeRule(id) and (Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0 and Duel.GetDecktopGroup(tp, 1):GetFirst():IsFacedown() or Duel.GetFieldGroupCount(tp, 0, LOCATION_DECK) > 0 and Duel.GetDecktopGroup(1 - tp, 1):GetFirst():IsFacedown())
end
function s.look(e, tp, eg, ep, ev, re, r, rp, c, sg)
	local g = Duel.GetDecktopGroup(tp, 1) + Duel.GetDecktopGroup(1 - tp, 1)
	Duel.ConfirmCards(tp, g:Filter(Card.IsFacedown, nil))
end
function s.con()
	return Duel.GetTurnCount() == 5
end
function s.flip(c)
	return function()
		c:SetEntityCode(111004001)
		Duel.SetChainLimitTillChainEnd(aux.FALSE)
	end
end

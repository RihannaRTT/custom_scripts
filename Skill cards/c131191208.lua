--coded by Lyris
--Grit
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
	e2:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCondition(s.fcon(c))
	e2:SetOperation(s.op(c))
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CHANGE_DAMAGE)
	e3:SetRange(LOCATION_REMOVED)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetTargetRange(1, 0)
	e3:SetCondition(s.con(c))
	e3:SetValue(s.val)
	c:RegisterEffect(e3)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_PHASE + PHASE_END)
	e4:SetRange(LOCATION_REMOVED)
	e4:SetCountLimit(1)
	e4:SetOperation(s.flip(c))
	c:RegisterEffect(e4)
end
function s.act(c)
	return function(e, tp)
		if Duel.GetLP(tp) >= 4000 then
			c:SetEntityCode(id)
		end
	end
end
function s.fcon(c)
	return function(e, tp)
		return Duel.GetLP(tp) >= 4000 and not c:IsOriginalCodeRule(id)
	end
end
function s.op(c)
	return function()
		c:SetEntityCode(id)
		Duel.Hint(HINT_CARD, 0, id)
	end
end
function s.con(c)
	return function()
		return c:IsOriginalCodeRule(id)
	end
end
function s.val(e, re, val)
	local c = e:GetOwner()
	local tp = e:GetOwnerPlayer()
	local lp = Duel.GetLP(tp)
	if val >= lp and c:GetFlagEffect(id) == 0 then
		Duel.Hint(HINT_NUMBER, 0, val)
		local ct = Duel.GetTurnPlayer() == tp and 2 or 1
		local e6 = Effect.CreateEffect(c)
		e6:SetType(EFFECT_TYPE_FIELD)
		e6:SetCode(59822133)
		e6:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e6:SetTargetRange(1, 0)
		e6:SetReset(RESET_PHASE + PHASE_END + RESET_SELF_TURN, ct)
		Duel.RegisterEffect(e6, tp)
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD)
		e5:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
		e5:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e5:SetTargetRange(1, 0)
		e5:SetLabel(0)
		e5:SetTarget(s.ltg(tp))
		e5:SetReset(RESET_PHASE + PHASE_END + RESET_SELF_TURN, ct)
		Duel.RegisterEffect(e5, tp)
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_LEFT_SPSUMMON_COUNT)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetTargetRange(1, 0)
		e1:SetValue(s.lval(tp, ef))
		e1:SetReset(RESET_PHASE + PHASE_END + RESET_SELF_TURN, ct)
		Duel.RegisterEffect(e1, tp)
		c:RegisterFlagEffect(id, 0, 0, 1)
		return lp - 1
	else
		return val
	end
end
function s.ltg(tp)
	return function(e)
		if Duel.GetActivityCount(tp, ACTIVITY_SPSUMMON) > 0 then
			e:SetLabel(1)
			return true
		end
		return Duel.GetTurnCount() ~= Duel.GetFlagEffectLabel(tp, id) and e:GetLabel() > 0
	end
end
function s.lval(tp, ef)
	return function(e, re, tp)
		return Duel.GetTurnCount() ~= Duel.GetFlagEffectLabel(tp, id) and 1 - ef:GetLabel() or 1
	end
end
function s.flip(c)
	return function()
		if c:IsOriginalCodeRule(id) then
			c:SetEntityCode(111004001)
		end
	end
end

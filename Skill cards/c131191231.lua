--coded by Lyris
--Bandit
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLP(tp) <= 1500 and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_SZONE, 1, nil) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil)
	Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONTROL)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, 0, LOCATION_SZONE, 1, 1, nil):GetFirst()
	if tc then
		Duel.MoveToField(tc, tp, tp, LOCATION_SZONE, POS_FACEDOWN, true)
	end
end
function s.filter(c)
	return c:IsFacedown() and c:GetSequence() < 5
end

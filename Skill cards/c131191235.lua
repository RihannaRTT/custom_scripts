--coded by Lyris
--Mind Scan
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act(c))
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetRange(LOCATION_REMOVED)
	e3:SetDescription(1158)
	e3:SetCondition(s.lookcon)
	e3:SetOperation(s.look)
	c:RegisterEffect(e3)
end
function s.con(c)
	return function()
		return not c:IsOriginalCodeRule(id) and Duel.GetTurnCount() == 3
	end
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
		Duel.Hint(HINT_CARD, 0, id)
	end
end
function s.lookcon(e, tp)
	local c = e:GetOwner()
	return c:IsOriginalCodeRule(id) and Duel.GetLP(tp) >= 3000 and Duel.IsExistingMatchingCard(Card.IsFacedown, tp, 0, LOCATION_ONFIELD, 1, nil)
end
function s.look(e, tp)
	Duel.ConfirmCards(tp, Duel.GetMatchingGroup(Card.IsFacedown, tp, 0, LOCATION_ONFIELD, nil))
end

--coded by Lyris
--A Trick up the Sleeve
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.filter(c)
	return c:IsLevelAbove(7) and c:IsAttribute(ATTRIBUTE_DARK) and c:IsRace(RACE_SPELLCASTER)
end
function s.act(c)
	return function(e, tp)
		local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil)
		if #g == 0 then
			return
		end
		c:SetEntityCode(id)
		local tc = g:RandomSelect(tp, 1):GetFirst()
		if Duel.GetTurnCount() > 1 then
			Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0), nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.SendtoHand(tc, nil, REASON_RULE + REASON_DRAW)
			Duel.Draw(tp, #hg - 1, REASON_RULE)
		else
			Duel.MoveSequence(tc, SEQ_DECKTOP)
		end
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetTargetRange(1, 0)
		e1:SetTarget(aux.TargetBoolFunction(Card.IsType, TYPE_EFFECT))
		e1:SetReset(RESET_PHASE + PHASE_END + RESET_SELF_TURN)
		Duel.RegisterEffect(e1, tp)
	end
end

--coded by Lyris
--Arcana Swap
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.op)
	c:RegisterEffect(e1)
end
function s.act(c)
	return function(e, tp)
		if Duel.GetTurnCount() > 1 and Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_HAND, 0, 1, nil, 0x5) and Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
			s.op(e, tp)
		end
	end
end
function s.con(c)
	return function(e, tp)
		return not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_HAND, 0, 1, nil, 0x5)
	end
end
function s.op(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = hg:Select(tp, 1, 63, nil)
	Duel.ConfirmCards(1 - tp, g)
	Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.ShuffleDeck(tp)
	Duel.BreakEffect()
	Duel.Draw(tp, #g, REASON_RULE)
end

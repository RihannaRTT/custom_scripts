--coded by Lyris
--Balance
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		local n1 = Duel.GetMatchingGroupCount(Card.IsType, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, TYPE_MONSTER)
		local n2 = Duel.GetMatchingGroupCount(Card.IsType, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, TYPE_SPELL)
		local n3 = Duel.GetMatchingGroupCount(Card.IsType, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, TYPE_TRAP)
		if n1 < 6 or n2 < 6 or n3 < 6 then
			return
		end
		c:SetEntityCode(id)
		local sum = n1 + n2 + n3
		local g = Duel.GetFieldGroup(tp, LOCATION_DECK, 0)
		local mg = g:Filter(Card.IsType, nil, TYPE_MONSTER):RandomSelect(tp, 1) + g:Filter(Card.IsType, nil, TYPE_SPELL):RandomSelect(tp, 1) + g:Filter(Card.IsType, nil, TYPE_TRAP):RandomSelect(tp, 1)
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND)
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.SendtoHand(mg, nil, REASON_RULE + REASON_DRAW)
			Duel.Draw(tp, #hg - 3, REASON_RULE)
		else
			for mc in aux.Next(mg) do
				Duel.MoveSequence(mc, SEQ_DECKTOP)
			end
		end
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetTargetRange(1, 0)
		e1:SetReset(RESET_PHASE + PHASE_MAIN1 + RESET_SELF_TURN)
		Duel.RegisterEffect(e1, tp)
		local e2 = e1:Clone()
		e2:SetCode(EFFECT_CANNOT_ACTIVATE)
		e2:SetValue(aux.TargetBoolFunction(Effect.IsActiveType, TYPE_MONSTER))
		Duel.RegisterEffect(e2, tp)
	end
end

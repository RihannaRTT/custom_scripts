--coded by Lyris
--Blessing of the Cyber Angel
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_PREDRAW)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0 and Duel.GetDrawCount(tp) > 0 and Duel.GetLP(tp) <= 2000
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_DRAW_COUNT)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_DRAW)
	e1:SetValue(0)
	Duel.RegisterEffect(e1, tp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	Duel.SendtoDeck(Duel.CreateToken(tp, Duel.AnnounceCard(tp, TYPE_MONSTER, OPCODE_ISTYPE, 0x2093, OPCODE_ISSETCARD, OPCODE_AND)), nil, SEQ_DECKTOP, REASON_RULE)
	Duel.Draw(tp, 1, REASON_RULE)
end

--coded by Lyris
--Card of Sanctity
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_SUMMON_SUCCESS)
	e1:SetCountLimit(1)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCondition(s.con)
	e1:SetOperation(s.op)
	c:RegisterEffect(e1)
end
function s.con(e, tp, eg)
	return eg:IsExists(Card.IsCode, 1, nil, 10000020)
end
function s.op(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_TURN_END)
	e1:SetCountLimit(1)
	e1:SetOperation(s.act(c))
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
end
function s.act(c)
	return function()
		if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
			return
		end
		c:SetEntityCode(id)
		Duel.Hint(HINT_CARD, 0, id)
		Duel.Draw(tp, 6 - Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0), REASON_RULE)
		Duel.Draw(1 - tp, 6 - Duel.GetFieldGroupCount(tp, 0, LOCATION_HAND), REASON_RULE)
		Duel.SetChainLimitTillChainEnd(aux.FALSE)
	end
end

--coded by Lyris
--Creator
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_PREDRAW)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0 and Duel.GetDrawCount(tp) > 0 and Duel.GetLP(tp) <= 2000 and not c:IsOriginalCodeRule(id)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_DRAW_COUNT)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_DRAW)
	e1:SetValue(0)
	Duel.RegisterEffect(e1, tp)
	local g = Group.CreateGroup()
	local toCreate = { 12580477, 18144506, 83764718, 44095762, 71413901 }
	for i, code in ipairs(toCreate) do
		g:AddCard(Duel.CreateToken(tp, code))
	end
	Duel.Remove(g, POS_FACEDOWN, REASON_RULE)
	local sg = g:RandomSelect(tp, 1)
	Duel.SendtoHand(sg, nil, REASON_RULE)
	g:Sub(sg)
	Duel.Exile(g, REASON_RULE)
end

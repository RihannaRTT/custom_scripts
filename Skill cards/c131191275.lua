--coded by Lyris
--Cyber Style
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		local lp = Duel.GetLP(tp)
		return lp <= 2000 and Duel.GetLocationCount(tp, LOCATION_MZONE) >= math.min(3, (4000 - lp) // 1000) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil, 70095154) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local ft = Duel.GetLocationCount(tp, LOCATION_MZONE)
	local ct = math.min(ft, (4000 - Duel.GetLP(tp)) // 1000)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	for token in aux.Next(Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK + LOCATION_HAND, 0, ct, ct, nil, 70095154)) do
		local pos = Duel.SelectPosition(tp, token, POS_FACEUP)
		Duel.MoveToField(token, tp, tp, LOCATION_MZONE, pos, true)
		token:SetStatus(STATUS_FORM_CHANGED, true)
		local e0 = Effect.CreateEffect(c)
		e0:SetType(EFFECT_TYPE_SINGLE)
		e0:SetCode(EFFECT_SET_ATTACK)
		e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e0:SetValue(0)
		e0:SetReset(RESET_EVENT + RESETS_STANDARD)
		token:RegisterEffect(e0, true)
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UNRELEASABLE_SUM)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetValue(1)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD)
		token:RegisterEffect(e1, true)
		local e2 = e1:Clone()
		e2:SetCode(EFFECT_UNRELEASABLE_NONSUM)
		token:RegisterEffect(e2, true)
		local e4 = Effect.CreateEffect(c)
		e4:SetType(EFFECT_TYPE_FIELD)
		e4:SetCode(EFFECT_CANNOT_ATTACK)
		e4:SetRange(LOCATION_MZONE)
		e4:SetTargetRange(LOCATION_MZONE, 0)
		e4:SetTarget(aux.TargetBoolFunction(aux.NOT(Card.IsType), TYPE_FUSION))
		e4:SetReset(RESET_EVENT + RESETS_STANDARD)
		token:RegisterEffect(e4, true)
		local e3 = e4:Clone()
		e3:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
		e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e3:SetAbsoluteRange(tp, 1, 0)
		e3:SetTargetRange(1, 0)
		token:RegisterEffect(e3, true)
	end
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetTargetRange(1, 0)
	e3:SetTarget(aux.TargetBoolFunction(aux.NOT(Card.IsCode), 1546123))
	e3:SetReset(RESET_PHASE + PHASE_END + RESET_OPPO_TURN)
	Duel.RegisterEffect(e3, tp)
end

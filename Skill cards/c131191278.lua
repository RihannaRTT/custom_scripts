--coded by Lyris
--Destiny Board
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetCondition(s.con(c))
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE + PHASE_END)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCountLimit(1)
	e1:SetOperation(s.checkop)
	c:RegisterEffect(e1)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_ADJUST)
	e4:SetRange(LOCATION_REMOVED)
	e4:SetOperation(s.flip(c))
	c:RegisterEffect(e4)
	local e3 = e4:Clone()
	e3:SetCode(EVENT_CHAIN_SOLVED)
	c:RegisterEffect(e3)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLP(tp) <= 2000 and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_GRAVE, 0, 1, nil, 31829185) and c:GetFlagEffect(id) == 0 and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	c:RegisterFlagEffect(id, 0, 0, 1)
end
function s.checkop(e, tp)
	local c = e:GetOwner()
	if not c:IsOriginalCodeRule(id) or Duel.GetTurnPlayer() ~= tp then
		return
	end
	local ct = Duel.GetFlagEffect(tp, id)
	Duel.RegisterFlagEffect(tp, 95308449, RESET_PHASE + PHASE_END, 0, 5 - ct)
	c:SetHint(CHINT_TURN, ct)
	if ct == 5 then
		Duel.Win(tp, 0x15)
	end
end
function s.flip(c)
	return function(e, tp)
		if not Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_GRAVE, 0, 1, nil, 31829185) and c:IsOriginalCodeRule(id) then
			c:SetEntityCode(111004001)
		end
	end
end

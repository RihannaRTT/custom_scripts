--coded by Lyris
--Elements Unite!
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetOperation(s.rop(e0))
	c:RegisterEffect(e1)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		Duel.SetLP(tp, 500)
		if Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 3 then
			Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0), nil, SEQ_DECKBOTTOM, REASON_RULE)
		end
		local tk = Duel.CreateToken(tp, 25833572)
		e:SetLabelObject(tk)
		Duel.SendtoHand(tk, nil, REASON_RULE)
		local codes = { 25955164, 62340868, 98434877 }
		for _, code in ipairs(codes) do
			local token = Duel.CreateToken(tp, code)
			Duel.MoveToField(token, tp, tp, LOCATION_MZONE, POS_FACEUP_ATTACK, true)
			local e2 = Effect.CreateEffect(c)
			e2:SetType(EFFECT_TYPE_SINGLE)
			e2:SetCode(EFFECT_DISABLE)
			e2:SetReset(RESET_EVENT + RESETS_STANDARD)
			token:RegisterEffect(e2, true)
			local e3 = e2:Clone()
			e3:SetCode(EFFECT_DISABLE_EFFECT)
			local e4 = e2:Clone()
			e4:SetCode(EFFECT_CANNOT_ATTACK)
			token:RegisterEffect(e4, true)
		end
	end
end
function s.rop(ef)
	return function(e, tp)
		if Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 then
			Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0) - ef:GetLabelObject(), nil, SEQ_DECKBOTTOM, REASON_RULE)
		end
	end
end

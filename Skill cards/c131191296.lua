--coded by Lyris
--Extra, Extra
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
end
function s.count(e, tp, eg, ep, ev, re, r, rp)
	local plp0, plp1 = e:GetLabel()
	local clp0, clp1 = Duel.GetLP(0), Duel.GetLP(1)
	e:SetLabel(clp0, clp1)
	if not plp0 or not plp1 then
		return
	end
	if plp0 > clp0 then
		s[0] = s[0] - clp0 + plp0
	end
	if plp1 > clp1 then
		s[1] = s[1] - clp1 + plp1
	end
end
function s.con(c)
	return function(e, tp, eg)
		return s[tp] >= 2000 and not c:IsOriginalCodeRule(id) and Duel.GetDrawCount(tp) == 1 and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	s[tp] = 0
	Duel.Hint(HINT_CARD, 0, id)
	Duel.SendtoHand(Duel.CreateToken(tp, Duel.GetDecktopGroup(tp, 1):GetFirst():GetOriginalCode()), nil, REASON_RULE)
end

--coded by Lyris
--Fiend Farewell
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	if not s.global_check then
		s.global_check = true
		local e2 = Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e2:SetCode(EVENT_BATTLE_DESTROYED)
		e2:SetCondition(s.regcon)
		e2:SetOperation(s.reg)
		Duel.RegisterEffect(e2, 0)
	end
end
function s.regcon(e, tp, eg)
	e:SetLabelObject(eg:GetFirst())
	return #eg > 0
end
function s.reg(e)
	Duel.RegisterFlagEffect(e:GetLabelObject():GetPreviousControler(), id, RESET_PHASE + PHASE_END + RESET_OPPO_TURN, 0, 2)
end
function s.con(e, tp)
	return Duel.GetFlagEffect(tp, id) > 0 and Duel.IsExistingMatchingCard(Card.IsRace, tp, LOCATION_DECK, 0, 1, nil, RACE_FIEND)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	local tc = Duel.GetMatchingGroup(Card.IsRace, tp, LOCATION_DECK, 0, nil, RACE_FIEND):RandomSelect(tp, 1):GetFirst()
	Duel.SendtoGrave(tc, REASON_RULE)
end

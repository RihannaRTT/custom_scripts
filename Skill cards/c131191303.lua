--coded by Lyris
--Fusion Time!
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
end
function s.count(e, tp)
	local plp0, plp1 = e:GetLabel()
	local clp0, clp1 = Duel.GetLP(0), Duel.GetLP(1)
	e:SetLabel(clp0, clp1)
	if not plp0 or not plp1 then
		return
	end
	if plp0 > clp0 then
		s[0] = s[0] - clp0 + plp0
	end
	if plp1 > clp1 then
		s[1] = s[1] - clp1 + plp1
	end
end
function s.con(c)
	return function(e, tp)
		return s[tp] >= 1500 and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	s[tp] = 0
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local cc = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil):GetFirst()
	Duel.SendtoDeck(cc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	local tk = Duel.CreateToken(tp, 24094653)
	Duel.SendtoHand(tk, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, tk)
	local mg = Duel.GetMatchingGroup(s.filter, tp, LOCATION_GRAVE, 0, 1, nil)
	if #mg > 0 and Duel.SelectEffectYesNo(tp, c) then
		Duel.BreakEffect()
		local sc = mg:Select(tp, 1, 1, nil):GetFirst()
		Duel.SendtoHand(sc, nil, REASON_RULE)
	end
end
function s.filter(c)
	return c:IsLevelBelow(4) and c:IsSetCard(0x3008) and c:IsType(TYPE_MONSTER)
end

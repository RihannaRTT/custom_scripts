--coded by Lyris
--Heavy Starter
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		local g = Duel.GetMatchingGroup(Card.IsLevelAbove, tp, LOCATION_DECK, 0, nil, 5)
		if g:GetClassCount(Card.GetCode) < 3 then
			return
		end
		c:SetEntityCode(id)
		local tc = g:RandomSelect(tp, 1):GetFirst()
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND)
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.SendtoHand(tc, nil, REASON_RULE + REASON_DRAW)
			Duel.Draw(tp, #hg - 1, REASON_RULE)
		else
			Duel.MoveSequence(tc, 0)
		end
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetTargetRange(1, 0)
		e1:SetReset(RESET_PHASE + PHASE_MAIN1 + RESET_SELF_TURN)
		Duel.RegisterEffect(e1, tp)
		local e2 = e1:Clone()
		e2:SetCode(EFFECT_CANNOT_ACTIVATE)
		e2:SetTarget(aux.TargetBoolFunction(Effect.IsActiveType, TYPE_MONSTER))
		Duel.RegisterEffect(e2, tp)
	end
end

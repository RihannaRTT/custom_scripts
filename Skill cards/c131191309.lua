--coded by Lyris
--Holy Guard
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD)
	e4:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
	e4:SetRange(LOCATION_REMOVED)
	e4:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e4:SetTargetRange(1, 0)
	e4:SetCondition(s.con)
	e4:SetValue(1)
	c:RegisterEffect(e4)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
	end
end
function s.con(e)
	return Duel.GetTurnPlayer() == e:GetOwner():GetControler()
end

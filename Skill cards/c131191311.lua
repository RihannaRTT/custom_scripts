--coded by Lyris
--Insect Uprising
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsFaceup() and c:IsRace(RACE_INSECT)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 3, nil) and c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	for tc in aux.Next(Duel.GetMatchingGroup(Card.IsFaceup, tp, 0, LOCATION_MZONE, nil)) do
		if tc:IsControler(tp) or not tc:IsHasEffect(121191258) then
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_UPDATE_ATTACK)
			e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
			e1:SetValue(-800)
			tc:RegisterEffect(e1, true)
		end
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

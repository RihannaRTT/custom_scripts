--coded by Lyris
--Labyrinth Builder
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 1 and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetActivityCount(tp, ACTIVITY_SUMMON) == 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 2, 2, nil)
	Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.ShuffleDeck(tp)
	local tk = Duel.CreateToken(tp, 67284908)
	Duel.MoveToField(tk, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e1:SetReset(RESET_PHASE + PHASE_END)
	e1:SetTargetRange(1, 0)
	Duel.RegisterEffect(e1, tp)
	tk:SetStatus(STATUS_FORM_CHANGED, true)
end

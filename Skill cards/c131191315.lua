--coded by Lyris
--Light Barrier
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and not c:IsOriginalCodeRule(id)
	end
end
function s.act(e, tp)
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.MoveToField(Duel.CreateToken(tp, 73206827), tp, tp, LOCATION_FZONE, POS_FACEDOWN, true)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

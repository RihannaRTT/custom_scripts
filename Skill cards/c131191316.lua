--coded by Lyris
--Love is Pain
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_DAMAGE)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	if ep ~= tp or Duel.GetLP(tp) + ev > 2000 or rp == tp or r & REASON_EFFECT == 0 then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Damage(1 - tp, ev, REASON_RULE)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

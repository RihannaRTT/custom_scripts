--coded by Lyris
--LP Boost Beta
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e0:SetRange(LOCATION_REMOVED)
	e0:SetCountLimit(1)
	e0:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e0:SetOperation(s.rop)
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		Duel.SetLP(tp, Duel.GetLP(tp) + 1500)
	end
end
function s.rop(e, tp)
	if Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 then
		Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0):GetFirst(), nil, SEQ_DECKBOTTOM, REASON_RULE)
	end
end

--coded by Lyris
--Master of Destiny
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCode(EVENT_TOSS_COIN_NEGATE)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.op)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 3
		s[1] = 3
	end
end
function s.cfilter(c)
	return c.toss_coin
end
function s.act(c)
	return function(e, tp)
		if Duel.GetMatchingGroup(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil):GetClassCount(Card.GetCode) < 7 then
			return
		end
		c:SetEntityCode(id)
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetCode(EFFECT_DRAW_COUNT)
		e1:SetTargetRange(1, 0)
		e1:SetReset(RESET_PHASE + PHASE_DRAW + RESET_OPPO_TURN, 2)
		e1:SetValue(0)
		Duel.RegisterEffect(e1, tp)
	end
end
function s.con(c)
	return function(e, tp, eg, ep)
		return ep == tp and c:IsOriginalCodeRule(id)
	end
end
function s.op(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	local t = { Duel.GetCoinResult() }
	local ct = s[tp]
	for i = 1, math.min(ev, ct) do
		t[i] = 1
		s[tp] = s[tp] - 1
	end
	Duel.SetCoinResult(table.unpack(t))
end

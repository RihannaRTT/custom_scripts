--coded by Lyris
--Meet My Family!
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		local codes = { 79856792, 7093411, 95600067, 32710364, 21698716, 68215963, 69937550, 32933942 }
		for _, code in ipairs(codes) do
			Duel.SendtoDeck(Duel.CreateToken(tp, code), nil, SEQ_DECKBOTTOM, REASON_RULE)
		end
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.Draw(tp, #hg, REASON_RULE)
		else
			Duel.ShuffleDeck(tp)
		end
	end
end

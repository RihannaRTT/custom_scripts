--coded by Lyris
--Ojama Overflow
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnCount() > 4 and Duel.GetLP(tp) <= 500 and Duel.GetLocationCount(1 - tp, LOCATION_MZONE) > 0 and Duel.IsPlayerCanSpecialSummonMonster(tp, 29843092, 0xf, 0x4011, 0, 1000, 2, RACE_BEAST, ATTRIBUTE_LIGHT, POS_FACEUP_DEFENSE, 1 - tp) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local ft = Duel.GetLocationCount(1 - tp, LOCATION_MZONE)
	for i = 0, ft do
		local token = Duel.CreateToken(tp, 29843092 + i)
		if Duel.SpecialSummonStep(token, 0, tp, 1 - tp, false, false, POS_FACEUP_DEFENSE) then
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_UNRELEASABLE_SUM)
			e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
			e1:SetReset(RESET_EVENT + RESETS_STANDARD)
			e1:SetValue(1)
			token:RegisterEffect(e1, true)
			local e2 = Effect.CreateEffect(c)
			e2:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
			e2:SetCode(EVENT_LEAVE_FIELD)
			e2:SetOperation(s.damop)
			token:RegisterEffect(e2, true)
		end
	end
	Duel.SpecialSummonComplete()
end
function s.damop(e, tp)
	local c = e:GetOwner()
	if c:IsReason(REASON_DESTROY) then
		Duel.Damage(c:GetPreviousControler(), 300, REASON_EFFECT)
	end
	e:Reset()
end

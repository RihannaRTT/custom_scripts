--coded by Lyris
--Posthumous Army
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
end
function s.count(e, tp, eg, ep, ev, re, r, rp)
	local plp0, plp1 = e:GetLabel()
	local clp0, clp1 = Duel.GetLP(0), Duel.GetLP(1)
	e:SetLabel(clp0, clp1)
	if not plp0 or not plp1 then
		return
	end
	if plp0 > clp0 then
		s[0] = s[0] - clp0 + plp0
	end
	if plp1 > clp1 then
		s[1] = s[1] - clp1 + plp1
	end
end
function s.con(c)
	return function(e, tp)
		return s[tp] >= 1000 and c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp, chk)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	s[tp] = 0
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetTargetRange(LOCATION_MZONE + LOCATION_GRAVE, LOCATION_MZONE + LOCATION_GRAVE)
	e2:SetCode(EFFECT_CHANGE_RACE)
	e2:SetTarget(s.tg(tp))
	e2:SetValue(RACE_ZOMBIE)
	e2:SetReset(RESET_PHASE + PHASE_END + RESET_OPPO_TURN)
	Duel.RegisterEffect(e2, tp)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.tg(tp)
	return function(e, tc)
		return tc:IsControler(tp) or not tc:IsHasEffect(121191258)
	end
end

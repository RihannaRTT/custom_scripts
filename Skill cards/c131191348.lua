--coded by Lyris
--Restart
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e0:SetCountLimit(1)
	e0:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e0:SetCondition(s.con)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.con(e, tp)
	return Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0
end
function s.act(c)
	return function(e, tp)
		Duel.Hint(HINT_CARD, 0, id)
		local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
		Duel.SendtoDeck(hg, nil, SEQ_DECKBOTTOM, REASON_RULE)
		Duel.ShuffleDeck(tp)
		Duel.Draw(tp, ht - 1, REASON_RULE)
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetTargetRange(1, 0)
		e1:SetReset(RESET_PHASE + PHASE_MAIN1 + RESET_SELF_TURN)
		Duel.RegisterEffect(e1, tp)
		local e2 = e1:Clone()
		e2:SetCode(EFFECT_CANNOT_ACTIVATE)
		e2:SetTarget(aux.TargetBoolFunction(Effect.IsActiveType, TYPE_MONSTER))
		Duel.RegisterEffect(e2, tp)
	end
end

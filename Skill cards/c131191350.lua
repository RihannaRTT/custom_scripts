--coded by Lyris
--Right Back at You
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_BATTLE_DAMAGE)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCondition(s.regcon)
	e2:SetOperation(s.op)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetFlagEffect(tp, id) > 0 and Duel.IsExistingMatchingCard(Card.IsFaceup, tp, LOCATION_MZONE, 0, 1, nil) and c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_MZONE, 0, nil)
	local tc = g:GetFirst()
	if #g > 1 then
		tc = g:Select(tp, 1, 1, nil):GetFirst()
	end
	Duel.HintSelection(Group.FromCards(tc))
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetValue(math.ceil(Duel.GetFlagEffectLabel(tp, id) / 2))
	e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
	tc:RegisterEffect(e1, true)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.regcon(e, tp, eg, ep)
	return ep == tp
end
function s.op(e, tp, eg, ep, ev, re, r, rp)
	if Duel.GetFlagEffect(tp, id) > 0 then
		Duel.SetFlagEffectLabel(tp, id, Duel.GetFlagEffectLabel(tp, id) + ev)
	else
		Duel.RegisterFlagEffect(tp, id, RESET_PHASE + PHASE_END, 0, 2, ev)
	end
end

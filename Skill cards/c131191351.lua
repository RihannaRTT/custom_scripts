--coded by Lyris
--Right Side Up!
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCode(EVENT_TOSS_COIN_NEGATE)
	e2:SetCondition(s.con)
	e2:SetOperation(s.op)
	c:RegisterEffect(e2)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
	end
end
function s.con(e, tp, eg, ep, ev, re, r, rp)
	local rc = re:GetHandler()
	return ep == tp and rc:IsType(TYPE_MONSTER) and rc:IsSetCard(0x5)
end
function s.op(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	local t = {  }
	for i = 1, ev do
		table.insert(t, 1)
	end
	Duel.SetCoinResult(table.unpack(t))
end

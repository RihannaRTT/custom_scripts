--coded by Lyris
--Sealed Tombs
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLP(tp) + 1000 <= Duel.GetLP(1 - tp) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD)
	e4:SetCode(EFFECT_CANNOT_REMOVE)
	e4:SetTargetRange(LOCATION_GRAVE, LOCATION_GRAVE)
	e4:SetReset(RESET_PHASE + PHASE_END + RESET_OPPO_TURN)
	Duel.RegisterEffect(e4, tp)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 1)
	e1:SetTarget(aux.TargetBoolFunction(Card.IsLocation, LOCATION_GRAVE))
	e1:SetReset(RESET_PHASE + PHASE_END + RESET_OPPO_TURN)
	Duel.RegisterEffect(e1, tp)
end

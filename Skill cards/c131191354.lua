--coded by Lyris
--Shadow Game
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_TURN_END)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.act(e)
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	local tp = Duel.GetTurnPlayer()
	if Duel.GetLP(tp) > 1000 then
		Duel.Damage(tp, math.min(Duel.GetFieldGroupCount(tp, LOCATION_GRAVE, 0) * 100, 400), REASON_RULE)
	end
end

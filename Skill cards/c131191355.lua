--coded by Lyris
--Show of Nightmares
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingTarget(Card.IsType, tp, LOCATION_GRAVE, 0, 3, nil, TYPE_SPELL) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local gc = Duel.GetMatchingGroup(Card.IsType, tp, LOCATION_GRAVE, 0, nil, TYPE_SPELL):RandomSelect(tp, 1):GetFirst()
	Duel.SendtoHand(gc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, gc)
	Duel.ShuffleHand(tp)
	Duel.BreakEffect()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil):GetFirst()
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
end

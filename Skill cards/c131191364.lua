--coded by Lyris
--Tether of Defeat
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_BATTLE_DESTROYED)
	e2:SetCondition(s.drcon)
	e2:SetOperation(s.drop)
	c:RegisterEffect(e2)
end
function s.drcon(e, tp, eg, ep, ev, re, r, rp)
	return eg:IsExists(Card.IsPreviousControler, 1, nil, 1 - tp)
end
function s.drop(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	local p = Duel.SelectOption(tp, aux.Stringid(id, 0), aux.Stringid(id, 1))
	local tc = Duel.GetDecktopGroup(p, 1):GetFirst()
	Duel.DisableShuffleCheck()
	Duel.SendtoGrave(tc, REASON_RULE)
end

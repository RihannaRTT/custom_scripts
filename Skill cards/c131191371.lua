--coded by Lyris
--Training Wheels
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
end
function s.count(e, tp)
	local plp0, plp1 = e:GetLabel()
	local clp0, clp1 = Duel.GetLP(0), Duel.GetLP(1)
	e:SetLabel(clp0, clp1)
	if not plp0 or not plp1 then
		return
	end
	if plp0 > clp0 then
		s[0] = s[0] - clp0 + plp0
	end
	if plp1 > clp1 then
		s[1] = s[1] - clp1 + plp1
	end
end
function s.con(e, tp)
	return s[tp] >= 1000 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	s[tp] = 0
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_BATTLE_END)
	e1:SetCondition(s.rmcon)
	e1:SetOperation(s.rmop)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
end
function s.rmcon()
	return Duel.GetAttacker():IsOriginalCodeRule(45945685, 16114248)
end
function s.rmop(e, tp)
	local tc = Duel.GetAttacker()
	if Duel.Remove(tc, tc:GetPosition(), REASON_RULE + REASON_TEMPORARY) == 0 then
		return
	end
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e1:SetReset(RESET_PHASE + PHASE_DRAW + RESET_SELF_TURN)
	e1:SetCondition(s.retcon)
	e1:SetOperation(s.retop(tc))
	Duel.RegisterEffect(e1, tp)
end
function s.retcon(e, tp)
	return tp == Duel.GetTurnPlayer()
end
function s.retop(tc)
	return function()
		Duel.ReturnToField(tc)
	end
end

--coded by Lyris
--Transcendent Crystals
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetCondition(s.con(c, e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.plfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil))
end
function s.con(c, ef)
	return function(e, tp)
		return (Duel.GetTurnCount() > 2 or ef:GetLabel() > 6) and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and Duel.IsExistingMatchingCard(s.plfilter, tp, LOCATION_DECK, 0, 1, nil) and e:GetLabel() < 2 and c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local ft = Duel.GetLocationCount(tp, LOCATION_SZONE)
	local g = Duel.GetMatchingGroup(s.plfilter, tp, LOCATION_DECK, 0, nil)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local cg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, math.min(ft, #g, 3), nil)
	local ct = #cg
	Duel.SendtoGrave(cg, REASON_RULE)
	Duel.BreakEffect()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	for tc in aux.Next(g:Select(tp, ct, ct, nil)) do
		Duel.MoveToField(tc, tp, tp, LOCATION_SZONE, POS_FACEUP, true)
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_CHANGE_TYPE)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetValue(TYPE_SPELL + TYPE_CONTINUOUS)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD - RESET_TURN_SET)
		tc:RegisterEffect(e1, true)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end
function s.plfilter(c)
	return c:IsSetCard(0x1034) and c:IsType(TYPE_MONSTER)
end

--coded by Lyris
--Avenger from the Void
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		if Duel.IsExistingMatchingCard(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil) then
			return
		end
		c:SetEntityCode(id)
		Duel.SendtoGrave(Duel.CreateToken(tp, 85475641), nil, 1, REASON_RULE)
	end
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsAttribute(ATTRIBUTE_DARK)
end

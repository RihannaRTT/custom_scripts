--coded by Lyris
--Black Rose Gale
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.op)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(73580471)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsRace(RACE_PLANT) and not c:IsSetCard(0x1123)
end
function s.act(c)
	return function(e, tp)
		if Duel.IsExistingMatchingCard(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil) then
			return
		end
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e1:SetCode(EVENT_SPSUMMON_SUCCESS)
		e1:SetCountLimit(1)
		e1:SetCondition(s.regcon)
		e1:SetOperation(s.reg(c))
		Duel.RegisterEffect(e1, 0)
	end
end
function s.regcon(e, tp, eg)
	return eg:IsExists(s.filter, 1, nil)
end
function s.reg(c)
	return function()
		c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	end
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_ONFIELD, 0, 1, nil) and c:GetFlagEffect(id) > 0 and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.op(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Destroy(Duel.GetMatchingGroup(s.dfilter, tp, LOCATION_ONFIELD, LOCATION_ONFIELD, nil), REASON_RULE)
end
function s.dfilter(c)
	return c:IsControler(tp) or not (c:IsHasEffect(121191258) or c:IsHasEffect(121191250))
end

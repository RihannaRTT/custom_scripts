--coded by Lyris
--Blackwing Rising
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil) and c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.filter(c)
	return c:IsFaceup() and (c:IsSetCard(0x33) and c:IsType(TYPE_SYNCHRO) or c:IsCode(9012916))
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsSetCard(0x33)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil):GetFirst()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
	e1:SetValue(Duel.GetMatchingGroupCount(s.cfilter, tp, LOCATION_MZONE, 0, nil) * 200)
	tc:RegisterEffect(e1, true)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

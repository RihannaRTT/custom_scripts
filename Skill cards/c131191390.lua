--coded by Lyris
--Call of the Magician Girls
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x20a2)
end
function s.act(c)
	return function(e, tp)
		local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK, 0, nil)
		if g:GetClassCount(Card.GetCode) < 4 then
			return
		end
		c:SetEntityCode(id)
		local tc = g:RandomSelect(tp, 1):GetFirst()
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND)
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.SendtoHand(tc, nil, REASON_RULE + REASON_DRAW)
			Duel.Draw(tp, #hg - 1, REASON_RULE)
		else
			Duel.MoveSequence(tc, 0)
		end
	end
end

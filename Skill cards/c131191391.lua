--coded by Lyris
--Cheer Up Fairies
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLP(tp) <= 3000 and Duel.IsExistingMatchingCard(s.filter1, tp, LOCATION_MZONE, 0, 1, nil, tp) and Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_EXTRA, 0, 1, nil, e, tp) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.filter1(c, tp)
	return c:IsFaceup() and c:IsCode(45939611) and c:IsCanOverlay(tp)
end
function s.filter2(c, e, tp)
	return c:IsCode(51960178) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_XYZ, tp, false, false)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_XMATERIAL)
	local mg = Duel.SelectMatchingCard(tp, s.filter1, tp, LOCATION_MZONE, 0, 1, 1, nil, tp)
	Duel.HintSelection(mg)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	local sc = Duel.SelectMatchingCard(tp, s.filter2, tp, LOCATION_EXTRA, 0, 1, 1, nil, e, tp):GetFirst()
	Duel.Overlay(sc, mg)
	Duel.SpecialSummon(sc, SUMMON_TYPE_XYZ, tp, tp, false, false, POS_FACEUP)
end

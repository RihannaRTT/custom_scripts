--coded by Lyris
--Curse of the Remnant
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_EQUIP)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetValue(1000)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_GRANT)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetTargetRange(LOCATION_SZONE, 0)
	e2:SetTarget(aux.TargetBoolFunction(Card.IsCode, 22610082))
	e2:SetLabelObject(e1)
	c:RegisterEffect(e2)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
	end
end

--coded by Lyris
--Cyber Energy Amplified
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_EQUIP)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetLabel(0)
	e1:SetValue(s.val)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_GRANT)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetTargetRange(LOCATION_SZONE, 0)
	e2:SetTarget(aux.TargetBoolFunction(Card.IsCode, 22610082))
	e2:SetLabelObject(e1)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e3:SetRange(LOCATION_REMOVED)
	e3:SetCondition(s.con)
	e3:SetOperation(s.op(e1))
	c:RegisterEffect(e3)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(22610082)
end
function s.val(e, c)
	return 500 + e:GetLabel() * 300
end
function s.con(e, tp)
	return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_SZONE, 0, 1, nil)
end
function s.op(ef)
	return function(e, tp)
		for tc in aux.Next(Duel.GetMatchingGroup(s.filter, tp, LOCATION_SZONE, 0, nil)) do
			ef:SetLabel(ef:GetLabel() + tc:GetEquipTarget():GetEquipGroup():FilterCount(Card.IsType, nil, TYPE_SPELL))
		end
	end
end

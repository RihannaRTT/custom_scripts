--coded by Lyris
--Dark Horizon
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		s[2] = 0
		s[3] = 0
		s[4] = 0
		s[5] = 0
		local e0 = Effect.CreateEffect(c)
		e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e0:SetCode(EVENT_DAMAGE)
		e0:SetOperation(s.reg)
		Duel.RegisterEffect(e0, 0)
		local e2 = Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e2:SetCode(EVENT_TURN_END)
		e2:SetOperation(s.reset)
		Duel.RegisterEffect(e2, 0)
	end
end
function s.reg(e, tp, eg, ep, ev)
	s[ep] = s[ep] + ev
end
function s.reset()
	local p = Duel.GetTurnPlayer()
	s[4 + p] = s[p]
	s[3 - p] = s[1 - p]
	s[p] = 0
	s[1 - p] = 0
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil, tp, s) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.filter(c, tp, d)
	local codes = {  }
	if d[4 + tp] >= 2500 then
		table.insert(codes, 46986414)
		table.insert(codes, 120130000)
	end
	if d[2 + tp] >= 2000 then
		table.insert(codes, 38033121)
	end
	return #codes > 0 and c:IsCode(table.unpack(codes))
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local sc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil, tp, s):GetFirst()
	Duel.MoveToField(sc, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	sc:SetStatus(STATUS_FORM_CHANGED, true)
end

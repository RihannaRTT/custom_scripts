--coded by Lyris
--Dino DNA!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Recover(tp, 200, REASON_RULE)
end

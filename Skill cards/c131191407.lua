--coded by Lyris
--Draw Loan
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK, 0, 1, nil, 67955331) and c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK, 0, 1, 1, nil, 67955331)
	Duel.SendtoHand(g, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, g)
	local dt = Duel.GetDrawCount(tp)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_DRAW_COUNT)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_DRAW)
	e1:SetValue(math.max(dt - 1, 0))
	Duel.RegisterEffect(e1, tp)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

--coded by Lyris
--Equipment Exchange
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLP(tp) <= 3000 and Duel.IsPlayerCanDraw(tp, 1) and Duel.IsExistingMatchingCard(Card.IsType, tp, LOCATION_HAND, 0, 1, nil, TYPE_EQUIP) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):SelectSubGroup(tp, Group.IsExists, false, 1, 2, Card.IsType, 1, nil, TYPE_EQUIP) or Group.CreateGroup()
	Duel.ConfirmCards(1 - tp, g)
	Duel.SendtoDeck(g, nil, SEQ_DECKBOTTOM, REASON_RULE)
	Duel.BreakEffect()
	Duel.Draw(tp, #g, REASON_RULE)
end

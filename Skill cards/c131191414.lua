--coded by Lyris
--Extra Pain
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_BATTLE_DAMAGE)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(e, tp, eg, ep)
	return ep ~= tp and Duel.GetLP(1 - tp) > 1000
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Damage(1 - tp, 100, REASON_RULE)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

--coded by Lyris
--Fleeting Hand
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLP(tp) + 2000 <= Duel.GetLP(1 - tp) and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.check(g)
	if #g == 1 then
		return true
	elseif #g == 2 then
		return Duel.GetLocationCount(tp, LOCATION_MZONE) > 0
	else
		return Duel.GetLocationCount(tp, LOCATION_SZONE) > 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):SelectSubGroup(tp, s.check, false, 1, 3) or Group.CreateGroup()
	local ct = #g
	Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.ShuffleDeck(tp)
	if ct == 3 then
		Duel.MoveToField(Duel.CreateToken(tp, 66957584), tp, tp, LOCATION_SZONE, POS_FACEUP, true)
	elseif ct == 2 then
		local tk = Duel.CreateToken(tp, 56209279)
		Duel.MoveToField(tk, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
		tk:SetStatus(STATUS_FORM_CHANGED, true)
	else
		Duel.SendtoDeck(Duel.CreateToken(tp, 99177923), nil, SEQ_DECKTOP, REASON_RULE)
	end
end

--coded by Lyris
--Fortune Overclock
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsSetCard(0x31)
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_CHANGE_LEVEL)
	e1:SetValue(math.min(Duel.GetTurnCount(), 12))
	e1:SetReset(RESET_EVENT + RESETS_STANDARD)
	tc:RegisterEffect(e1, true)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_TURN_END)
	e2:SetOperation(s.reset(e1))
	e2:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_DRAW)
	tc:RegisterEffect(e2, true)
end
function s.reset(ef)
	return function(e)
		local c = e:GetHandler()
		local e3 = ef:Clone()
		ef:Reset()
		e3:SetCode(EFFECT_CHANGE_LEVEL)
		e3:SetValue(c:GetOriginalLevel())
		c:RegisterEffect(e3, true)
	end
end

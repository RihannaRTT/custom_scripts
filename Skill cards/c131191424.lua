--coded by Lyris
--Fusionmorph: Red-Eyes
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsOriginalCodeRule(74677422, 120125001)
end
function s.filter(c)
	return c:IsType(TYPE_NORMAL) and (c:IsLevel(6) and c:IsRace(RACE_FIEND + RACE_DRAGON) or c:IsLevel(4) and c:IsAttribute(ATTRIBUTE_EARTH) and c:IsRace(RACE_WARRIOR))
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.DiscardHand(tp, s.filter, 1, 1, REASON_RULE)
	local dc = Duel.GetOperatedGroup():GetFirst()
	Duel.SendtoGrave(dc, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	local code = 21140872
	if dc:IsRace(RACE_DRAGON) then
		code = 90660762
	elseif dc:IsRace(RACE_FIEND) then
		code = 11901678
	end
	tc:SetEntityCode(code)
	tc:ReplaceEffect(code, 0)
	Duel.SetMetatable(tc, _G["c" .. tostring(code)])
	tc:ResetEffect(code, RESET_CARD)
	tc:ResetEffect(code, RESET_CARD)
	_G["c" .. tostring(code)]["initial_effect"](tc)
end

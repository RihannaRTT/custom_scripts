--coded by Lyris
--Future Fortune
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e1:SetCode(EVENT_FREE_CHAIN)
		e1:SetDescription(1158)
		e1:SetCondition(s.lookcon)
		e1:SetOperation(s.look)
		Duel.RegisterEffect(e1, tp)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsSetCard(0x31)
end
function s.lookcon(e, tp, eg, ep, ev, re, r, rp)
	return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 2, nil) and (Duel.GetFieldCard(tp, LOCATION_DECK, Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) - 1):IsFacedown() or Duel.GetFieldCard(1 - tp, LOCATION_DECK, Duel.GetFieldGroupCount(tp, 0, LOCATION_DECK) - 1):IsFacedown())
end
function s.look(e, tp, eg, ep, ev, re, r, rp, c, sg)
	local g = Duel.GetDecktopGroup(tp, 1) + Duel.GetDecktopGroup(1 - tp, 1)
	Duel.ConfirmCards(tp, g:Filter(Card.IsFacedown, nil))
end

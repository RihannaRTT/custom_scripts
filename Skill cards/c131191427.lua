--coded by Lyris
--Gathering of Disciples
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsCode(46986414, 120130000) and not c:IsPublic()
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local tk = Duel.CreateToken(tp, 38033121)
	if Duel.GetLP(tp) <= 2500 and Duel.SelectOption(tp, 1105, 1190) ~= 0 then
		Duel.SendtoHand(tk, nil, REASON_RULE)
		Duel.ConfirmCards(1 - tp, tk)
	else
		Duel.SendtoDeck(tk, nil, SEQ_DECKBOTTOM, REASON_RULE)
	end
end

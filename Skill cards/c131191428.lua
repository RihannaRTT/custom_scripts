--coded by Lyris
--Gem Dragon Go!
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		local g = Duel.GetMatchingGroup(Card.IsType, tp, LOCATION_DECK, 0, nil, TYPE_NORMAL)
		if g:Filter(Card.IsCode, nil, 62397231, 11091375, 17658803, 43096270):GetClassCount(Card.GetCode) < 3 then
			return
		end
		c:SetEntityCode(id)
		local tc = g:RandomSelect(tp, 1):GetFirst()
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.Draw(tp, #hg, REASON_RULE)
		else
			Duel.MoveSequence(tc, 0)
		end
	end
end

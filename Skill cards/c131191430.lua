--coded by Lyris
--Handless Combo 100
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetOperation(s.ret)
	c:RegisterEffect(e1)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		Duel.MoveToField(Duel.CreateToken(tp, 95453143), tp, tp, LOCATION_MZONE, POS_FACEUP_ATTACK, true)
	end
end
function s.ret(e, tp)
	Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0), nil, SEQ_DECKBOTTOM, REASON_RULE)
end

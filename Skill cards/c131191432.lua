--coded by Lyris
--Happily Ever After
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(e, tp)
	return Duel.GetFieldGroupCount(tp, LOCATION_ONFIELD, 0) == 2 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, tp) and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK, 0, 1, nil, 130000025)
end
function s.filter(c, tp)
	return c:IsFaceup() and c:IsCode(130000038) and c:GetOwner() == tp and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil, tp)
end
function s.cfilter(c, tp)
	return c:GetOwner() ~= tp
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local tc = Duel.GetFirstMatchingCard(s.filter, tp, LOCATION_MZONE, 0, nil, tp)
	Duel.Equip(tp, Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK, 0, 1, 1, nil, 130000025):GetFirst(), tc)
end

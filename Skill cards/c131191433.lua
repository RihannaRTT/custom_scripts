--coded by Lyris
--Have Some Duel Fuel!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return (Duel.GetFirstMatchingCard(Card.IsSetCard, tp, LOCATION_REMOVED + LOCATION_ONFIELD, 0, c, 0x2889) or Duel.GetDuelOptions and Duel.GetDuelOptions() & 0x20 > 0) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e1:SetCondition(s.reccon)
	e1:SetOperation(s.recop)
	e1:SetReset(RESET_PHASE + PHASE_DRAW, 2)
	Duel.RegisterEffect(e1, tp)
end
function s.reccon(e, tp)
	return Duel.GetTurnPlayer() == tp
end
function s.recop(e, tp)
	Duel.Recover(tp, 2000, REASON_RULE)
end

--coded by Lyris
--I'm Always Near You
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_REMOVED, LOCATION_REMOVED, 1, nil) and c:GetFlagEffect(id) == 0
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(78371393, 4779091, 31764700)
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_REMOVED, LOCATION_REMOVED, nil)
	if Duel.SelectOption(tp, 1105, 1103) == 0 then
		Duel.SendtoDeck(g, tp, SEQ_DECKSHUFFLE, REASON_RULE)
	else
		Duel.SendtoGrave(g, REASON_RULE + REASON_RETURN)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

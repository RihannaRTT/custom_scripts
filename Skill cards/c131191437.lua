--coded by Lyris
--I've Seen That Deck Before
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabel(0)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_ONFIELD, 0, 1, nil, tp) and e:GetLabel() < 3 and c:GetFlagEffect(id) == 0
	end
end
function s.cfilter(c, code)
	return c:IsFaceup() and c:IsCode(code)
end
function s.filter(c, tp)
	return c:IsFaceup() and Duel.IsExistingMatchingCard(s.cfilter, tp, 0, LOCATION_ONFIELD, 1, c, c:GetCode())
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Recover(tp, 500 * Duel.GetMatchingGroup(s.filter, tp, LOCATION_ONFIELD, 0, nil, tp):GetClassCount(Card.GetCode), REASON_RULE)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end

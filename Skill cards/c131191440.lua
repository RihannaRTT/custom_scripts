--coded by Lyris
--Infernity Inferno
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.target)
	e1:SetOperation(s.activate)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0xb)
end
function s.target(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnCount(tp) == 2 and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil) and not e:GetOwner():GetFlagEffect(id) == 0
end
function s.check(g)
	return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, #g, nil, 0xb)
end
function s.activate(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DISCARD)
	local ct = Duel.SendtoGrave(Duel.GetFieldGroup(tp, LOCATION_HAND, 0):SelectSubGroup(tp, s.check, false, 1, 2, tp) or Group.CreateGroup(), REASON_DISCARD + REASON_RULE)
	Duel.BreakEffect()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	Duel.SendtoGrave(Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, ct, ct, nil), REASON_RULE)
end

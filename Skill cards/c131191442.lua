--coded by Lyris
--Iron Call
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_CHAIN_SOLVING)
	e1:SetOperation(s.chk)
	e1:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.chk(e, tp, eg, ep, ev, re)
	if re:GetHandler():IsCode(64662453) and re:IsHasType(EFFECT_TYPE_ACTIVATE) then
		Duel.RegisterFlagEffect(ep, id, RESET_PHASE + PHASE_END, 0, 3, Duel.GetTurnCount())
	end
end
function s.con(c)
	return function(e, tp)
		local tid = Duel.GetFlagEffectLabel(tp, id)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and tid and tid ~= Duel.GetTurnCount() and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_GRAVE, 0, 1, nil, 64662453) and Duel.IsExistingMatchingCard(Card.IsRace, tp, LOCATION_HAND, 0, 1, nil, RACE_MACHINE)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.SelectMatchingCard(tp, Card.IsRace, tp, LOCATION_HAND, 0, 1, 1, nil, RACE_MACHINE):GetFirst()
	Duel.ConfirmCards(1 - tp, tc)
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.BreakEffect()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_RTOHAND)
	local sc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_GRAVE, 0, 1, 1, nil, 64662453):GetFirst()
	Duel.SendtoHand(sc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, sc)
end

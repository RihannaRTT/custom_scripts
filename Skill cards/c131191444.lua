--coded by Lyris
--Kuribohmorph
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND + LOCATION_MZONE, 0, 1, nil)
	end
end
function s.filter(c)
	return c:IsSetCard(0xa4) and (c:IsFaceup() or c:IsLocation(LOCATION_HAND))
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND + LOCATION_MZONE, 0, 1, 1, nil):GetFirst()
	if tc:IsFaceup() then
		Duel.HintSelection(Group.FromCards(tc))
	end
	local code = Duel.AnnounceCard(tp, 40640057, OPCODE_ISCODE, 33245030, OPCODE_ISCODE, OPCODE_OR, 50185950, OPCODE_ISCODE, OPCODE_OR, 46613515, OPCODE_ISCODE, OPCODE_OR)
	tc:SetEntityCode(code)
	tc:ReplaceEffect(code, 0)
	Duel.SetMetatable(tc, _G["c" .. tostring(code)])
	tc:ResetEffect(code, RESET_CARD)
	tc:ResetEffect(code, RESET_CARD)
	_G["c" .. tostring(code)]["initial_effect"](tc)
end

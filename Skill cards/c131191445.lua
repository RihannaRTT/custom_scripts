--coded by Lyris
--Let Me Duel Too!
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_SSET)
	e0:SetLabel(0)
	e0:SetOperation(s.chk)
	e0:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c, e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	Duel.AddCustomActivityCounter(id, ACTIVITY_NORMALSUMMON, aux.FilterBoolFunction(aux.NOT(Card.IsRace), RACE_FAIRY))
end
function s.chk(e, tp, eg, ep)
	if ep == tp then
		e:SetLabel(e:GetLabel() + eg:FilterCount(Card.IsPreviousLocation, nil, LOCATION_HAND))
	end
end
function s.con(c, ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetTurnCount() < 3 and Duel.GetCustomActivityCount(id, tp, ACTIVITY_NORMALSUMMON) == 1 and ef:GetLabel() > 1
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Recover(tp, 2000, REASON_RULE)
end

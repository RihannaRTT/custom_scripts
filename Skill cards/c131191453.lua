--coded by Lyris
--Lost and Found
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetFieldGroup(tp, LOCATION_MZONE, 0) > 0 and Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_GRAVE, 0, 1, nil, 0x12e)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetMatchingGroup(Card.IsSetCard, tp, LOCATION_GRAVE, 0, nil, 0x12e)
	local ct = Duel.DiscardHand(tp, 1, math.min(3, #g), REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local sg = g:Select(tp, ct, ct, nil)
	Duel.BreakEffect()
	Duel.SendtoHand(sg, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, sg)
end

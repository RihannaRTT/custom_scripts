--coded by Lyris
--Lucky Stones
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_CHAIN_SOLVING)
	e1:SetOperation(s.chk(c))
	e1:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e1)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.chk(c)
	return function(e, tp, eg, ep)
		local rc = re:GetHandler()
		if ep == tp and rc:IsSetCard(0x12e) then
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
			e1:SetCode(EVENT_SPSUMMON_SUCCESS)
			e1:SetOperation(s.reg(tp))
			e1:SetReset(RESET_CHAIN)
			rc:RegisterEffect(e1, true)
		end
	end
end
function s.reg(tp)
	return function()
		Duel.RegisterFlagEffect(tp, id, RESET_PHASE + PHASE_END, 0, 1)
	end
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetFlagEffect(tp, id) > 0 and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK, 0, 1, nil, 31461282)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	Duel.MoveToField(Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK, 0, 1, 1, nil, 31461282):GetFirst(), tp, tp, LOCATION_SZONE, POS_FACEDOWN, true)
end

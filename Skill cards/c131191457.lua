--coded by Lyris
--Magician Girl Defense
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_CHANGE_DAMAGE)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetTargetRange(1, 0)
	e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e2:SetValue(s.hdval)
	c:RegisterEffect(e2)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsSetCard(0x20a2)
end
function s.hdval(e, re, dam, r, rp, rc)
	if Duel.IsExistingMatchingCard(s.filter, e:GetHandlerPlayer(), LOCATION_MZONE, 0, 2, nil) then
		return dam / 2
	else
		return dam
	end
end

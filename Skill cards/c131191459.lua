--coded by Lyris
--Maiden in Action
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_CONTROL_CHANGED)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(e, tp, eg, ep, ev, re)
	return re and re:GetHandler():IsCode(130000015) and aux.nzatk(eg:GetFirst())
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Recover(tp, eg:GetFirst():GetAttack(), REASON_RULE)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

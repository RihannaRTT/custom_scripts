--coded by Lyris
--Maiden in Love
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(Card.IsCanAddCounter, tp, 0, LOCATION_MZONE, 1, nil, 0x1753, 1) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_ONFIELD, 0, 1, nil)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(130000038)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_COUNTER)
	local g = Duel.SelectMatchingCard(tp, Card.IsCanAddCounter, tp, 0, LOCATION_MZONE, 1, Duel.GetMatchingGroupCount(s.filter, tp, LOCATION_ONFIELD, 0, nil), nil, 0x1753, 1)
	Duel.HintSelection(g)
	for tc in aux.Next(g) do
		if tc:IsControler(tp) or not tc:IsHasEffect(121191258) then
			tc:AddCounter(c, 0x1753, 1)
		end
	end
end

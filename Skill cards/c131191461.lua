--coded by Lyris
--Maiden of Iron
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_CHANGE_BATTLE_DAMAGE)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetTargetRange(1, 0)
	e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e2:SetValue(s.val)
	c:RegisterEffect(e2)
end
function s.act(e, tp)
	e:GetOwner():SetEntityCode(id)
end
function s.val()
	local bc = Duel.GetAttackTarget()
	if Duel.GetAttacker():IsCode(130000038) or bc and bc:IsCode(130000038) then
		return HALF_DAMAGE
	else
		return -1
	end
end

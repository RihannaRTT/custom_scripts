--coded by Lyris
--Mask of Restrict
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(48948935, 49064413)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.MoveToField(Duel.CreateToken(tp, 29549364), tp, tp, LOCATION_SZONE, POS_FACEUP, true)
end

--coded by Lyris
--Miracle Draw
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.set(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	e1:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e1)
end
function s.set(c)
	return function()
		c:RegisterFlagEffect(id, RESET_PHASE + PHASE_DRAW + RESET_SELF_TURN, 0, 3)
	end
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x3008)
end
function s.act(e, tp)
	local c = e:GetOwner()
	local ct = c:GetFlagEffectLabel(id)
	if not ct then
		return
	end
	c:SetFlagEffectLabel(id, ct + 1)
	if ct == 2 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_GRAVE, 0, 1, nil) then
		c:SetEntityCode(id)
		Duel.Hint(HINT_CARD, 0, id)
		Duel.SendtoDeck(Duel.CreateToken(tp, 55144522), nil, SEQ_DECKTOP, REASON_RULE)
	end
end

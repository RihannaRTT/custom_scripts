--coded by Lyris
--Miracle Power
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetTargetRange(LOCATION_MZONE, 0)
	e2:SetTarget(aux.TargetBoolFunction(Card.IsSetCard, 0x12e))
	e2:SetCondition(s.con(c))
	e2:SetValue(s.val)
	c:RegisterEffect(e2)
end
function s.xfilter(c)
	return not (c:IsType(TYPE_MONSTER) and c:IsSetCard(0x12e))
end
function s.act(c)
	return function(e, tp)
		if not Duel.IsExistingMatchingCard(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND + LOCATION_EXTRA, 0, 1, nil) then
			c:SetEntityCode(id)
		end
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(31461282)
end
function s.con(c)
	return function(e)
		return c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, e:GetHandlerPlayer(), LOCATION_ONFIELD, 0, 1, nil)
	end
end
function s.val(e, c)
	return 300 * c:GetLevel()
end

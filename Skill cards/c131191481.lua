--coded by Lyris
--Numbers' Rule
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_INDESTRUCTABLE_COUNT)
	e2:SetReset(RESET_PHASE + PHASE_END, 2)
	e2:SetTargetRange(LOCATION_MZONE, 0)
	e2:SetTarget(s.target)
	e2:SetValue(s.indct)
	Duel.RegisterEffect(e2, tp)
end
function s.target(e, c)
	local bc = c:GetBattleTarget()
	return c:IsSetCard(0x48) and bc and not bc:IsSetCard(0x48)
end
function s.indct(e, re, r, rp)
	return r & REASON_BATTLE > 0 and 1 or 0
end

--coded by Lyris
--Onomatoplay
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c, e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.chk(c)
	return function(e, tp)
		e:SetLabel(Duel.GetMatchingGroupCount(s.dfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil), Duel.GetMatchingGroupCount(s.efilter, tp, LOCATION_EXTRA, 0, nil), Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_EXTRA, 0, nil))
	end
end
function s.dfilter(c)
	return c:IsSetCard(0x8f, 0x54, 0x59, 0x82) and c:IsType(TYPE_MONSTER)
end
function s.efilter(c)
	return c:IsType(TYPE_XYZ) and c:IsSetCard(0x107f)
end
function s.xfilter(c)
	return c:IsRankAbove(5) and not c:IsSetCard(0x48, 0x107e)
end
function s.con(c, ef)
	return function(e, tp)
		local ct1, ct2, xt = ef:GetLabel()
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and ct1 > 11 and ct2 > 1 and xt == 0 and Duel.GetTurnCount() > 2 and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil, tp)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_HAND, 0, 1, 1, nil, tp):GetFirst()
	Duel.ConfirmCards(1 - tp, tc)
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local sc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil, tc:GetCode()):GetFirst()
	Duel.SendtoHand(sc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, sc)
end
function s.cfilter(c, tp)
	return c:IsSetCard(0x8f, 0x54, 0x59, 0x82) and c:IsType(TYPE_MONSTER) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil, c:GetCode())
end
function s.filter(c, code)
	return c:IsSetCard(0x8f, 0x54, 0x59, 0x82) and c:IsType(TYPE_MONSTER) and not c:IsCode(code)
end

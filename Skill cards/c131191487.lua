--coded by Lyris
--Painful Evolution
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, tp, { [78371393] = 4779091, [4779091] = 31764700 })
	end
end
function s.filter(c, tp, t)
	return c:IsCode(78371393, 4779091) and Duel.GetMZoneCount(tp, c) > 0 and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_HAND, 0, 1, nil, t[c:GetCode()])
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local t = { [78371393] = 4779091, [4779091] = 31764700 }
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil, tp, t):GetFirst()
	local code = tc:GetCode()
	tc:ResetEffect(RESET_LEAVE, RESET_EVENT)
	Duel.SendtoGrave(tc, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local sc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_HAND, 0, 1, 1, nil, t[code]):GetFirst()
	Duel.MoveToField(sc, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	sc:SetStatus(STATUS_FORM_CHANGED, true)
end

--coded by Lyris
--Psychic Vision
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e0:SetRange(LOCATION_REMOVED)
	e0:SetCountLimit(1)
	e0:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e0:SetOperation(s.ret)
	e0:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		local e3 = Effect.CreateEffect(c)
		e3:SetType(EFFECT_TYPE_FIELD)
		e3:SetCode(EFFECT_PUBLIC)
		e3:SetTargetRange(0, LOCATION_HAND)
		e3:SetReset(RESET_PHASE + PHASE_END + RESET_SELF_TURN)
		Duel.RegisterEffect(e3, tp)
	end
end
function s.ret(e, tp)
	if Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 then
		Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0):GetFirst(), nil, SEQ_DECKBOTTOM, REASON_RULE)
	end
end

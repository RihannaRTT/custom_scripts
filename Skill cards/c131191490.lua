--coded by Lyris
--Ready For Victory
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.spfilter1(c, tp)
	return c:IsFaceup() and c:IsLevelAbove(1) and Duel.IsExistingMatchingCard(s.spfilter2, tp, LOCATION_MZONE, 0, 1, c, c:GetLevel())
end
function s.spfilter2(c, lv)
	return c:IsFaceup() and c:IsLevel(lv)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.spfilter1, tp, LOCATION_MZONE, 0, 1, nil, tp) and not e:GetOwner():IsOriginalCodeRule(id)
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetReset(RESET_PHASE + PHASE_END)
	e1:SetTargetRange(LOCATION_MZONE, 0)
	e1:SetTarget(aux.TargetBoolFunction(Card.IsType, TYPE_XYZ))
	e1:SetValue(500)
	Duel.RegisterEffect(e1, tp)
end

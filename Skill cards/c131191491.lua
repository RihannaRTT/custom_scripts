--coded by Lyris
--Recycling Reserve
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetMatchingGroupCount(Card.IsRace, tp, LOCATION_GRAVE, 0, nil, RACE_MACHINE) - Duel.GetFieldGroupCount(tp, LOCATION_GRAVE, 0) > 1
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Exile(Duel.GetMatchingGroup(aux.NOT(Card.IsRace), tp, LOCATION_GRAVE, 0, nil, RACE_MACHINE), REASON_RULE)
end

--coded by Lyris
--Red-Eyes Fusion
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetLP(tp) <= 2400 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND + LOCATION_GRAVE, 0, 1, nil)
	end
end
function s.filter(c)
	return c:IsSetCard(0x3b) and (c:IsCode(74677422, 120125001) or c:IsLocation(LOCATION_HAND))
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND + LOCATION_GRAVE, 0, 1, 1, nil):GetFirst()
	if tc:IsLocation(LOCATION_HAND) then
		Duel.ConfirmCards(1 - tp, tc)
	else
		Duel.HintSelection(Group.FromCards(tc))
	end
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.SendtoHand(Duel.CreateToken(tp, 6172122), nil, REASON_RULE)
end

--coded by Lyris
--Return of the Archduke
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		Duel.SendtoDeck(Duel.CreateToken(tp, 66506689), nil, SEQ_DECKTOP, REASON_RULE)
	end
end
function s.con(e, tp)
	return Duel.IsExistingMatchingCard(Card.IsDefensePos, tp, 0, LOCATION_MZONE, 1, nil) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_GRAVE, 0, 1, nil, 66506689)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_GRAVE, 0, 1, 1, nil, 66506689):GetFirst()
	Duel.SendtoDeck(tc, nil, SEQ_DECKTOP, REASON_RULE)
end

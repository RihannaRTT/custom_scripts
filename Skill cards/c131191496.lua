--coded by Lyris
--Scorns of Ultimate Defeat
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetTurnCount() > 5
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsDefenseAbove(3500)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.SendtoDeck(Duel.CreateToken(tp, 55410871), nil, SEQ_DECKTOP, REASON_RULE)
	if Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_MZONE, 1, nil) then
		Duel.SendtoHand(Duel.CreateToken(tp, 21082832), nil, REASON_RULE)
	end
end

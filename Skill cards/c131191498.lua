--coded by Lyris
--Secret Seeker
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabel(0)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(Card.IsFacedown, tp, 0, LOCATION_MZONE, 1, nil) and e:GetLabel() < 2
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	Duel.ConfirmCards(tp, Duel.SelectMatchingCard(tp, Card.IsFacedown, tp, 0, LOCATION_MZONE, 1, 1, nil))
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end

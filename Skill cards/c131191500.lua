--coded by Lyris
--Sentrification
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_GRAVE, 0, 1, nil, 13039848)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	for tc in aux.Next(Duel.GetMatchingGroup(Card.IsCode, tp, LOCATION_GRAVE, 0, nil, 13039848)) do
		tc:SetEntityCode(57354389)
		tc:ReplaceEffect(57354389, 0)
		Duel.SetMetatable(tc, _G["c57354389"])
		tc:ResetEffect(57354389, RESET_CARD)
		tc:ResetEffect(57354389, RESET_CARD)
		c57354389.initial_effect(tc)
	end
end

--coded by Lyris
--Smile bright!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and Duel.GetLP(1 - tp) > 1000
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Damage(1 - tp, 50, REASON_RULE)
end

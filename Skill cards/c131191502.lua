--coded by Lyris
--Spell Counter Boost
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and e:GetLabel() < 2 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 2, nil) and Duel.IsExistingMatchingCard(Card.IsCanAddCounter, tp, LOCATION_ONFIELD, LOCATION_ONFIELD, 1, nil, 0x1, 1) and c:GetFlagEffect(id) == 0
	end
end
function s.filter(c)
	return c:IsType(TYPE_SPELL) and not c:IsPublic()
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND, 0, 2, 2, nil)
	Duel.ConfirmCards(1 - tp, g)
	Duel.SelectMatchingCard(tp, Card.IsCanAddCounter, tp, LOCATION_ONFIELD, LOCATION_ONFIELD, 1, 1, nil, 0x1, 1):GetFirst():AddCounter(0x1, 1)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end

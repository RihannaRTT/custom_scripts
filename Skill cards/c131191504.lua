--coded by Lyris
--Spiral Spear Strike
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c)
	return s.filter(c) or c:IsCode(66889139)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	c:SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.MoveToField(Duel.CreateToken(tp, 49328340), tp, tp, LOCATION_SZONE, POS_FACEUP, true)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.filter(c)
	return c:IsFaceup() and c:IsSetCard(0xbd) and c:IsAttackAbove(2300)
end

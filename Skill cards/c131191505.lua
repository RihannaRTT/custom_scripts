--coded by Lyris
--Spiritual Swords of Revealing Light
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(e, tp)
	return (Duel.GetFirstMatchingCard(Card.IsSetCard, tp, LOCATION_REMOVED + LOCATION_ONFIELD, 0, e:GetOwner(), 0x2889) or Duel.GetDuelOptions and Duel.GetDuelOptions() & 0x20 > 0) and Duel.GetTurnPlayer() == tp and Duel.GetTurnCount(tp) == 1 and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.MoveToField(Duel.CreateToken(tp, 78625592), tp, tp, LOCATION_SZONE, POS_FACEDOWN, true)
end

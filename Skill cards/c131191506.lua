--coded by Lyris
--Sun and Moon
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, tp)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil, tp)
	Duel.HintSelection(g)
	Duel.SendtoGrave(g, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local sc = Duel.SelectMatchingCard(tp, aux.NOT(Card.IsCode), tp, LOCATION_GRAVE, 0, 1, 1, nil, g:GetFirst():GetCode()):GetFirst()
	Duel.MoveToField(sc, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	sc:SetStatus(STATUS_FORM_CHANGED, true)
end
function s.filter(c, tp)
	return c:IsFaceup() and c:IsCode(39823987, 66818682) and Duel.GetMZoneCount(tp, c) > 0 and Duel.IsExistingMatchingCard(aux.NOT(Card.IsCode), tp, LOCATION_GRAVE, 0, 1, nil, c:GetCode())
end

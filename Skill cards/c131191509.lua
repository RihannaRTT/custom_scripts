--coded by Lyris
--The Destroyer of D
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c, e0))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil))
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(83965310)
end
function s.con(c, ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetTurnCount(tp) > 3 and Duel.GetLP(tp) <= 3000 and (Duel.GetLocationCount(1 - tp, LOCATION_MZONE) > 0 or Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_MZONE, 1, nil)) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_HAND, 0, 1, nil, 26964762) and ef:GetLabel() == 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local tc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_HAND, 0, 1, 1, nil, 26964762):GetFirst()
	if Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_MZONE, 1, nil) and (Duel.GetLocationCount(1 - tp, LOCATION_MZONE) <= 0 or Duel.SelectOption(tp, 1002, 1000) ~= 0) then
		Duel.SendtoDeck(tc, 1 - tp, SEQ_DECKTOP, REASON_RULE)
	else
		Duel.MoveToField(tc, tp, 1 - tp, LOCATION_MZONE, POS_FACEUP_DEFENSE, true)
	end
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsSetCard(0xc008)
end

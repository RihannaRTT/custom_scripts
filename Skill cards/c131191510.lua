--coded by Lyris
--The Dino Within
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
		local e4 = e5:Clone()
		Duel.RegisterEffect(e4, 1)
	end
end
function s.count(e, tp)
	local plp, clp = e:GetLabel(), Duel.GetLP(tp)
	e:SetLabel(clp)
	if plp <= clp then
		return
	end
	s[tp] = s[tp] - clp + plp
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and s[tp] >= 1500 and c:GetFlagEffect(id) == 0 and Duel.IsExistingMatchingCard(s.filter1, tp, LOCATION_MZONE, 0, 1, nil, e, tp)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	s[tp] = 0
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local g = Duel.SelectMatchingCard(tp, s.filter1, tp, LOCATION_MZONE, 0, 1, 1, nil, e, tp)
	Duel.HintSelection(g)
	Duel.SendtoGrave(g, REASON_RULE)
	local tc = g:GetFirst()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
	Duel.SpecialSummon(Duel.SelectMatchingCard(tp, s.filter2, tp, LOCATION_DECK, 0, 1, 1, nil, e, tp, tc), 0, tp, tp, false, false, POS_FACEUP)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.filter1(c, e, tp)
	return c:IsFaceup() and c:IsRace(RACE_REPTILE) and not c:IsType(TYPE_TOKEN) and Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_DECK, 0, 1, nil, e, tp, c)
end
function s.filter2(c, e, tp, tc)
	return c:IsRace(RACE_DINOSAUR) and c:IsAttribute(tc:GetOriginalAttribute()) and c:IsLevel(tc:GetLevel() + 3) and c:IsCanBeSpecialSummoned(e, 0, tp, false, false) and c:IsSummonableCard()
end

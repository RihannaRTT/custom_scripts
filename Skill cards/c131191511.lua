--coded by Lyris
--Three Lord Pillars
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c, ef)
	return function(e, tp)
		if Duel.GetMatchingGroup(Card.IsCode, tp, LOCATION_DECK, 0, nil, 130000049, 130000051):GetClassCount(Card.GetCode) < 2 then
			return
		end
		c:SetEntityCode(id)
		Duel.MoveToField(Duel.CreateToken(tp, 130000040), tp, tp, LOCATION_SZONE, POS_FACEDOWN, true)
	end
end

--coded by Lyris
--Time Roulette Go!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_CUSTOM + 71625222)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsFaceup() and c:IsOriginalCodeRule(88819587, 46986414)
end
function s.con(c)
	return function(e, tp, eg, ep)
		return ep == tp and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	local t = { [88819587] = 41462083, [46986414] = 92377303 }
	for tc in aux.Next(Duel.GetMatchingGroup(s.filter, tp, LOCATION_MZONE, 0, nil)) do
		for _, ocode in ipairs { tc:GetOriginalCodeRule() } do
			local code = t[ocode]
			if code then
				tc:SetEntityCode(code)
				tc:ReplaceEffect(code, 0)
				Duel.SetMetatable(tc, _G["c" .. tostring(code)])
				tc:ResetEffect(code, RESET_CARD)
				tc:ResetEffect(code, RESET_CARD)
				_G["c" .. tostring(code)]["initial_effect"](tc)
			end
		end
	end
end

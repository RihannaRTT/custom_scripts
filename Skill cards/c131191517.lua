--coded by Lyris
--Ultimate Dragons
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		for _, i in ipairs { 23995346, 56532353, 2129638 } do
			Duel.SendtoDeck(Duel.CreateToken(tp, i), nil, SEQ_DECKTOP, REASON_RULE)
		end
	end
end
function s.filter(c)
	return c:IsCode(89631139, 120120000) and not c:IsPublic()
end
function s.con(e, tp)
	return Duel.GetTurnCount() > 2 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 2, nil)
end
function s.act(e, tp, eg, ep, ev, re, r, rp, c)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	Duel.ConfirmCards(1 - tp, Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND, 0, 2, 2, nil))
	Duel.SendtoHand(Duel.CreateToken(tp, 24094653), nil, REASON_RULE)
end

--coded by Lyris
--Warrior's Sacrifice
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		local e3 = Effect.CreateEffect(c)
		e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e3:SetCode(EVENT_PHASE_START + PHASE_DRAW)
		e3:SetOperation(s.chk(c, e2))
		Duel.RegisterEffect(e3, 0)
	end
end
function s.chk(c, ef)
	return function()
		ef:SetLabel(Duel.GetLP(c:GetControler()), Duel.GetLP(1 - c:GetControler()))
	end
end
function s.filter(c)
	return c:IsSetCard(0xe7) and aux.nzatk(c)
end
function s.con(c)
	return function(e, tp)
		local lps, lpo = e:GetLabel()
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and lps < lpo and Duel.GetMatchingGroup(aux.NOT(Card.IsSetCard), tp, LOCATION_ONFIELD, 0, nil, 0x2889) == 0 and Duel.IsExistingMatchingCard(Card.IsFaceup, tp, 0, LOCATION_MZONE, 1, nil) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, Card.IsFaceup, tp, 0, LOCATION_MZONE, 1, 1, nil)
	Duel.HintSelection(g)
	local fc = g:GetFirst()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local tc = Duel.SelectMatchingCard(tp, Card.IsSetCard, tp, LOCATION_HAND, 0, 1, 1, nil, 0xe7):GetFirst()
	Duel.SendtoGrave(tc, REASON_RULE)
	if not fc:IsHasEffect(121191258) then
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		e1:SetValue(-tc:GetAttack())
		fc:RegisterEffect(e1, true)
	end
end

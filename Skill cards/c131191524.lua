--coded by Lyris
--Wave-Motion Inferno
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetTargetRange(LOCATION_MZONE, 0)
	e2:SetCondition(s.con)
	e2:SetTarget(aux.TargetBoolFunction(Card.IsSetCard, 0xb))
	e2:SetValue(400)
	c:RegisterEffect(e2)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
	end
end
function s.con(e)
	return Duel.GetFieldGroupCount(e:GetHandlerPlayer(), LOCATION_HAND, 0) == 0
end

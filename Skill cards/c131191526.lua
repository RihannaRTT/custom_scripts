--coded by Lyris
--Wounded Hero
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e0 = Effect.CreateEffect(c)
		e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e0:SetCode(EVENT_DAMAGE)
		e0:SetOperation(s.count)
		Duel.RegisterEffect(e0, 0)
	end
end
function s.count(e, tp, eg, ep, ev, re, r)
	if r & REASON_BATTLE + REASON_EFFECT then
		s[ep] = s[ep] + ev
	end
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil, s[tp])
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local sc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil, s[tp]):GetFirst()
	Duel.MoveToField(sc, tp, tp, LOCATION_MZONE, POS_FACEUP_ATTACK, true)
	sc:SetStatus(STATUS_FORM_CHANGED, true)
end
function s.filter(c, dam)
	return c:IsSetCard(0x3008) and c:IsAttackBelow(dam)
end

--coded by Lyris
--You're Under Arrest!
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
end
function s.act(e, tp)
	e:GetOwner():SetEntityCode(id)
	local g = Group.CreateGroup()
	local toCreate = { 7391448, 58901502, 63364266, 84305651, 98637386 }
	for i, code in ipairs(toCreate) do
		g:AddCard(Duel.CreateToken(tp, code))
	end
	Duel.Remove(g, POS_FACEDOWN, REASON_RULE)
	local sg = g:RandomSelect(tp, 2)
	Duel.SendtoDeck(sg, nil, SEQ_DECKTOP, REASON_RULE)
	g:Sub(sg)
	Duel.Exile(g, REASON_RULE)
end

--coded by Lyris
--Zorc Appears!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_HAND, 0, 1, nil, 97642679)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	if Duel.TossDice(tp, 1) < Duel.GetTurnCount() then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
		local tc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_HAND, 0, 1, 1, nil, 97642679):GetFirst()
		Duel.MoveToField(tc, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
		tc:CompleteProcedure()
		tc:SetStatus(STATUS_FORM_CHANGED, true)
	end
end

--coded by Lyris
--Shell of a Ghost
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.xfilter(c)
	return c:IsFacedown() or not c:IsSetCard(0xb)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) == 0 and not Duel.IsExistingMatchingCard(s.xfilter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK, 0, 1, nil, 66957584)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, aux.Stringid(34086406, 0))
	local tc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK, 0, 1, 1, nil, 66957584):GetFirst()
	Duel.ShuffleDeck(tp)
	Duel.MoveSequence(tc, 0)
	Duel.ConfirmDecktop(tp, 1)
	Duel.BreakEffect()
	Duel.SendtoDeck(Duel.CreateToken(tp, 66957584), nil, SEQ_DECKBOTTOM, REASON_RULE)
end

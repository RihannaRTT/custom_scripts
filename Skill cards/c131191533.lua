--coded by Lyris
--Magic Combo
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	Duel.AddCustomActivityCounter(id, ACTIVITY_NORMALSUMMON, aux.FilterBoolFunction(aux.NOT(Card.IsCode), 51254277))
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetCustomActivityCount(id, tp, ACTIVITY_NORMALSUMMON) > 0 and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and e:GetLabel() < 2 and c:GetFlagEffect(id) == 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.MoveToField(Duel.CreateToken(tp, 7653207), tp, tp, LOCATION_SZONE, POS_FACEDOWN, true)
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end

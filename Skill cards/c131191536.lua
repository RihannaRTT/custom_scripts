--coded by Lyris
--Shark's Revenge
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetLP(tp) < Duel.GetLP(1 - tp)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetTargetRange(LOCATION_MZONE, LOCATION_MZONE)
	e1:SetTarget(s.target)
	e1:SetValue(s.val)
	e1:SetReset(RESET_PHASE + PHASE_END)
	e1:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e1)
end
function s.target(e, c)
	return c:IsFaceup() and c:IsRankBelow(4) and c:IsAttribute(ATTRIBUTE_WATER) and c:IsRace(RACE_SEASERPENT) and c:GetOverlayCount() > 2
end
function s.val(e, c)
	return c:GetOverlayCount() // 3 * 1500
end

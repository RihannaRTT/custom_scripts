--coded by Lyris
--FIRE Reload
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.xfilter(c)
	return not (c:IsRace(RACE_PYRO) and c:IsAttribute(ATTRIBUTE_FIRE))
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsPlayerCanDraw(tp) and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and not Duel.IsExistingMatchingCard(s.xfilter, tp, LOCATION_HAND, 0, 1, nil)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
	Duel.ConfirmCards(1 - tp, hg)
	local ht = #hg
	Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.ShuffleDeck(tp)
	Duel.BreakEffect()
	Duel.Draw(tp, ht, REASON_RULE)
end

--coded by Lyris
--Blaze Accelerator Deployment
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabel(0)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_SZONE, 0, 1, nil) and e:GetLabel() < 2 and c:GetFlagEffect(id) == 0
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(69537999)
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_SZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	tc:SetEntityCode(21420702)
	tc:ReplaceEffect(21420702, 0)
	Duel.SetMetatable(tc, _G["c21420702"])
	tc:ResetEffect(21420702, RESET_CARD)
	tc:ResetEffect(21420702, RESET_CARD)
	c21420702.initial_effect(tc)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end

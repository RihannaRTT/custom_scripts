--coded by Lyris
--Blaze Accelerator Reserve
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x32)
end
function s.check(g, tp)
	return Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_DECK + LOCATION_GRAVE, 0, #g, nil, 0xb9)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil) and Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, nil, 0xb9)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local cg = Duel.GetMatchingGroup(s.cfilter, tp, LOCATION_HAND, 0, nil):SelectSubGroup(tp, s.check, false, 1, 2, tp) or Group.CreateGroup()
	local ct = #cg
	Duel.SendtoGrave(cg, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, Card.IsSetCard, tp, LOCATION_DECK + LOCATION_GRAVE, 0, ct, ct, nil, 0xb9)
	Duel.SendtoHand(g, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, g)
end

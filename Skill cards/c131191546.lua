--coded by Lyris
--Reloading!
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsSetCard(0xb9)
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x32)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, nil) and Duel.IsExistingMatchingCard(Card.IsAttribute, tp, LOCATION_HAND, 0, 1, nil, ATTRIBUTE_FIRE) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil)
	end
end
function s.check(g, tp)
	return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, #g, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local cg = Duel.GetMatchingGroup(Card.IsAttribute, tp, LOCATION_HAND, 0, nil, ATTRIBUTE_FIRE):SelectSubGroup(tp, s.check, false, 1, 2, tp) or Group.CreateGroup()
	local ct = #cg
	Duel.SendtoGrave(cg, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	Duel.SendtoGrave(Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, ct, ct, nil), REASON_RULE)
end

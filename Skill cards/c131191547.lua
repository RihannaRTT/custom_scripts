--coded by Lyris
--Volcanic Burning Deck
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_CHAINING)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCondition(s.atcon)
	e1:SetOperation(aux.chainreg)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_CHAIN_SOLVED)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetLabel(0)
	e2:SetOperation(s.atop)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_ACTIVATE)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetCountLimit(1)
	e3:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e3:SetCondition(s.con(e2))
	e3:SetOperation(s.act)
	c:RegisterEffect(e3)
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		for _, i in ipairs { 33365932, 33365932, 32543380, 69537999, 21420702 } do
			Duel.SendtoDeck(Duel.CreateToken(tp, i), nil, SEQ_DECKSHUFFLE, REASON_RULE)
		end
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.Draw(tp, #hg, REASON_RULE)
		else
			Duel.ShuffleDeck(tp)
		end
	end
end
function s.atcon(e, tp, eg, ep, ev, re, r, rp)
	return re:IsHasType(EFFECT_TYPE_ACTIVATE) and re:GetHandler():IsCode(69537999, 21420702)
end
function s.atop(e, tp, eg, ep, ev, re, r, rp)
	if e:GetOwner():GetFlagEffect(1) == 0 then
		return
	end
	Duel.RegisterFlagEffect(ep, id, RESET_PHASE + PHASE_END, 0, 2, re:GetHandler():GetCode())
	e:SetLabel(Duel.GetTurnCount())
end
function s.filter(c, tp)
	local id = Duel.GetFlagEffectLabel(tp, id)
	if id == 69537999 then
		return c:IsCode(21420702)
	elseif id == 21420702 then
		return c:IsCode(32543380)
	else
		return false
	end
end
function s.con(ef)
	return function(e, tp)
		local ptn = ef:GetLabel()
		return ptn > 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnCount() ~= ptn and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, nil, tp)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, 1, nil, tp):GetFirst()
	Duel.SendtoHand(tc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, tc)
	Duel.ResetFlagEffect(tp, id)
end

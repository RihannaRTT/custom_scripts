--coded by Lyris
--Full Armored Xyz [Black Ray]
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsPlayerCanSpecialSummonMonster(tp, 25853045, 0, 0x800021, 2100, 600, 4, RACE_BEASTWARRIOR, ATTRIBUTE_WATER, POS_FACEUP, tp, SUMMON_TYPE_XYZ) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_ONFIELD, 0, 1, nil, tp)
	end
end
function s.filter(c, tp)
	return s.ofilter(c) and c:IsCanOverlay(tp) and Duel.GetMZoneCount(tp, c) > 0
end
function s.ofilter(c)
	return c:IsFaceup() and c:IsCode(74416224, 5014629)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local tk = Duel.CreateToken(tp, 25853045)
	Duel.Overlay(tk, Duel.SelectMatchingCard(tp, s.ofilter, tp, LOCATION_ONFIELD, 0, 1, 1, nil))
	Duel.MoveToField(tk, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	tk:CompleteProcedure()
	tk:SetStatus(STATUS_FORM_CHANGED, true)
end

--coded by Lyris
--Neutralization
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabel(0)
	e2:SetCondition(s.con(c, e0))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND + LOCATION_EXTRA, 0, 1, nil))
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsCanAddCounter(0x1038, 1)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsAttribute(0xe3)
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0xe3) and not c:IsPublic()
end
function s.con(c, ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and ef:GetLabel() == 0 and e:GetLabel() < 2 and c:GetFlagEffect(id) == 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 3, nil) and Duel.IsExistingMatchingCard(s.cfilter, tp, 0, LOCATION_MZONE, 1, nil)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	Duel.ConfirmCards(1 - tp, Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND, 0, 3, 3, nil))
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_COUNTER)
	local tc = Duel.SelectMatchingCard(tp, s.cfilter, tp, 0, LOCATION_MZONE, 1, 1, nil):GetFirst()
	tc:AddCounter(0x1038, 1)
	if not tc:IsHasEffect(121191258) then
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_CANNOT_ATTACK)
		e1:SetCondition(s.disable)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD)
		tc:RegisterEffect(e1, true)
		local e2 = e1:Clone()
		e2:SetCode(EFFECT_DISABLE)
		tc:RegisterEffect(e2, true)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end
function s.disable(e)
	return e:GetOwner():GetCounter(0x1038) > 0
end

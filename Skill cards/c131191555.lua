--coded by Lyris
--Number Hunt
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnCount(tp) == 4 and not Duel.CheckPhaseActivity()
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local tc = Duel.GetMatchingGroup(Card.IsSetCard, tp, 0, LOCATION_EXTRA, nil, 0x48):RandomSelect(tp, 1):GetFirst()
	if tc then
		Duel.Exile(tc, REASON_RULE)
		Duel.SendtoDeck(Duel.CreateToken(tp, tc:GetOriginalCode()), nil, SEQ_DECKBOTTOM, REASON_RULE)
		Duel.BreakEffect()
		local no = aux.GetXyzNumber(tc)
		if no then
			Duel.Damage(tp, no * 30, REASON_RULE)
		end
	end
end

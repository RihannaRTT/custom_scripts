--coded by Lyris
--Final Mission
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp, chk)
	Duel.Hint(HINT_CARD, 0, id)
	if Duel.GetLP(tp) <= 1000 or Duel.TossCoin(tp, 1) > 0 then
		Duel.SendtoGrave(Duel.CreateToken(tp, 66436257), REASON_RULE)
	end
end

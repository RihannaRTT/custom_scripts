--coded by Lyris
--Tuner Level Balancer
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	Duel.AddCustomActivityCounter(id, ACTIVITY_SPSUMMON, s.slfilter)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and c:GetFlagEffect(id) == 0 and Duel.GetCustomActivityCount(id, tp, ACTIVITY_SPSUMMON) == 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil)
	end
end
function s.slfilter(c)
	return c:GetSummonLocation() ~= LOCATION_EXTRA or c:IsType(TYPE_SYNCHRO)
end
function s.filter(c)
	return c:IsFaceup() and c:IsSetCard(0x27) and c:IsType(TYPE_TUNER)
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_LEVEL)
	e1:SetValue(2 * Duel.SelectOption(tp, aux.Stringid(id, 0), tc:IsLevelAbove(3) and aux.Stringid(id, 1) or nil) - 1)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
	tc:RegisterEffect(e1, true)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e2:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e2:SetReset(RESET_PHASE + PHASE_END)
	e2:SetTargetRange(1, 0)
	e2:SetTarget(s.splimit)
	Duel.RegisterEffect(e2, tp)
	if tc:IsType(TYPE_SYNCHRO) then
		c:SetEntityCode(111004001)
		c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	end
end
function s.splimit(e, c)
	return c:IsLocation(LOCATION_EXTRA) and not c:IsType(TYPE_SYNCHRO)
end

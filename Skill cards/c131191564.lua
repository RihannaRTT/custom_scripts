--coded by Lyris
--Set! Delta Accel!
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	if not s.global_check then
		s.global_check = true
		local ge1 = Effect.CreateEffect(c)
		ge1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		ge1:SetCode(EVENT_SPSUMMON_SUCCESS)
		ge1:SetOperation(s.reg)
		Duel.RegisterEffect(ge1, 0)
	end
end
function s.reg(e, tp, eg)
	for tc in aux.Next(eg:Filter(s.filter, nil)) do
		Duel.RegisterFlagEffect(tc:GetSummonPlayer(), id, RESET_PHASE + PHASE_END, 0, 1)
	end
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		for _, i in ipairs { 24943456, 51447164, 62560742, 97836203, 98558751 } do
			Duel.SendtoDeck(Duel.CreateToken(tp, i), nil, SEQ_DECKTOP, REASON_RULE)
		end
	end
end
function s.con(e, tp)
	return Duel.GetFlagEffect(tp, id) > 1 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_HAND, 0, 1, nil, 0x27) and Duel.GetLocationCountFromEx(tp, tp, nil, TYPE_SYNCHRO) > 0 and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_EXTRA, 0, 1, nil, 62560742)
end
function s.filter(c)
	return c:IsSummonType(SUMMON_TYPE_SYNCHRO) and c:IsSetCard(0x27)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local cc = Duel.SelectMatchingCard(tp, Card.IsSetCard, tp, LOCATION_HAND, 0, 1, 1, nil, 0x27):GetFirst()
	Duel.ConfirmCards(1 - tp, cc)
	Duel.SendtoDeck(cc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	local tc = Duel.GetFirstMatchingCard(Card.IsCode, tp, LOCATION_EXTRA, 0, nil, 62560742)
	Duel.MoveToField(tc, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	tc:CompleteProcedure()
	tc:SetStatus(STATUS_FORM_CHANGED, true)
end

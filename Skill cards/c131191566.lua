--coded by Lyris
--Draw Sense: Dice
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetLabel(0)
	e2:SetCondition(s.con(e0))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK, 0, nil):GetClassCount(Card.GetCode))
end
function s.filter(c)
	return c.toss_dice
end
function s.con(ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and Duel.GetDrawCount(tp) > 0 and ef:GetLabel() > 5 and e:GetLabel() < 2 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_DRAW_COUNT)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_DRAW)
	e1:SetValue(0)
	Duel.RegisterEffect(e1, tp)
	Duel.MoveSequence(Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK, 0, nil):RandomSelect(tp, 1):GetFirst(), SEQ_DECKTOP)
	Duel.Draw(tp, 1, REASON_RULE)
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end

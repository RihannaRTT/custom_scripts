--coded by Lyris
--Dice Booster
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and c:GetFlagEffect(id) == 0 and Duel.IsExistingMatchingCard(Card.IsFaceup, tp, LOCATION_MZONE, 0, 1, nil)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local ct = Duel.TossDice(tp, 1)
	for tc in aux.Next(Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_MZONE, 0, nil)) do
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		e1:SetValue(ct * 50)
		tc:RegisterEffect(e1, true)
	end
	if ct == 1 or ct == 6 then
		c:SetEntityCode(111004001)
		c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	end
end

--coded by Lyris
--Deck Master Effect: Strike Ninja
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetDescription(1112)
	e1:SetCondition(s.tcon(c))
	e1:SetOperation(s.tact)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetDescription(1102)
	e2:SetCondition(s.rcon(c))
	e2:SetOperation(s.ract)
	c:RegisterEffect(e2)
end
function s.filter(c)
	return c:IsFacedown() and c:GetSequence() < 5
end
function s.tcon(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetLP(tp) <= 1500 and Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_HAND, 0, 1, nil, 0x2b, 0x61) and Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_SZONE, 1, nil) and (c:GetFlagEffect(id) == 0 or c:GetFlagEffectLabel(id) & 0x1 == 0)
	end
end
function s.tact(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.DiscardHand(tp, Card.IsSetCard, 1, 1, REASON_RULE, nil, 0x2b, 0x61)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONTROL)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, 0, LOCATION_SZONE, 1, 1, nil):GetFirst()
	if tc then
		Duel.MoveToField(tc, tp, tp, LOCATION_SZONE, tc:GetPosition(), true)
	end
	c:RegisterFlagEffect(id + o * 10, RESET_PHASE + PHASE_END, 0, 1)
	local ct = c:GetFlagEffectLabel(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 0x1)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 0x1)
	end
end
function s.cfilter(c, tp)
	return c:GetOwner() ~= tp and Duel.GetMZoneCount(tp, c) > 0 and (c:IsControler(tp) or not (c:IsLocation(LOCATION_MZONE) and c:IsHasEffect(121191258)))
end
function s.rcon(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetLP(tp) <= 1500 and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, LOCATION_ONFIELD, 1, nil, tp) and (c:GetFlagEffect(id) == 0 or c:GetFlagEffectLabel(id) & 0x2 == 0)
	end
end
function s.ract(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_REMOVE)
	local g = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_ONFIELD, 0, 1, 1, nil, tp)
	Duel.HintSelection(g)
	if Duel.Remove(g, POS_FACEUP, REASON_RULE) > 0 then
		local tk = Duel.CreateToken(tp, 41006930)
		Duel.MoveToField(tk, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
		tk:SetStatus(STATUS_FORM_CHANGED, true)
	end
	c:RegisterFlagEffect(id + o * 10, RESET_PHASE + PHASE_END, 0, 1)
	local ct = c:GetFlagEffectLabel(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 0x2)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 0x2)
	end
end

--coded by Lyris
--Dice Return
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_GRAVE, 0, 1, nil)
	end
end
function s.filter(c)
	return c.toss_dice
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local mg = Duel.GetMatchingGroup(s.filter, tp, LOCATION_GRAVE, 0, nil)
	local ct = math.min(#mg, Duel.TossDice(tp, 1))
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = mg:Select(tp, ct, ct, nil)
	Duel.HintSelection(g)
	Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, REASON_RULE)
end

--coded by Lyris
--Go, Gradius!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetCondition(s.con)
	e1:SetOperation(s.hact)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabel(1)
	e2:SetDescription(aux.Stringid(id, 1))
	e2:SetCondition(s.con)
	e2:SetOperation(s.fact)
	c:RegisterEffect(e2)
end
function s.filter(c)
	return c:IsFaceup() and c:IsOriginalCodeRule(10992251)
end
function s.con(e, tp)
	if e:GetOwner():GetFlagEffect(id) > 0 then
		return false
	end
	local g = Duel.GetFieldGroup(tp, LOCATION_MZONE, 0)
	if e:GetLabel() == 0 then
		return g:IsExists(s.filter, 1, nil) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_HAND, 0, 1, nil, 54289683)
	else
		local tc = g:GetFirst()
		return #g == 1 and tc:IsFaceup() and tc:IsOriginalCodeRule(10992251)
	end
end
function s.hact(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local tc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_HAND, 0, 1, 1, nil, 54289683):GetFirst()
	Duel.ConfirmCards(1 - tp, tc)
	Duel.ShuffleHand(tp)
	local ac = Duel.AnnounceCard(tp, 5494820, OPCODE_ISCODE, 14291024, OPCODE_ISCODE, OPCODE_OR)
	tc:SetEntityCode(ac)
	tc:ReplaceEffect(ac, 0)
	Duel.SetMetatable(tc, _G["c" .. ac])
	tc:ResetEffect(ac, RESET_CARD)
	tc:ResetEffect(ac, RESET_CARD)
	_G["c" .. ac].initial_effect(tc)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.fact(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local ac = Duel.AnnounceCard(tp, 93130021, OPCODE_ISCODE, 10642488, OPCODE_ISCODE, OPCODE_OR)
	local tc = Duel.GetFirstMatchingCard(s.filter, tp, LOCATION_MZONE, 0, nil)
	tc:SetEntityCode(ac)
	tc:ReplaceEffect(ac, 0)
	Duel.SetMetatable(tc, _G["c" .. ac])
	tc:ResetEffect(ac, RESET_CARD)
	tc:ResetEffect(ac, RESET_CARD)
	_G["c" .. ac].initial_effect(tc)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

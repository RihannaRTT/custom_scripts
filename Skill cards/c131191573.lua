--coded by Lyris
--ZEXAL - Zexal Weapon
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
	e2:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e2:SetCondition(s.fcon(c))
	e2:SetOperation(s.flip)
	e2:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e2)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_PREDRAW)
	e0:SetCondition(s.dcon(c))
	e0:SetOperation(s.dact)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.fcon(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and Duel.GetLP(tp) <= 2000 and not c:IsOriginalCodeRule(id)
	end
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		Duel.Hint(HINT_CARD, 0, id)
		Duel.SendtoDeck(Duel.CreateToken(tp, 56840427), nil, SEQ_DECKBOTTOM, REASON_RULE)
		Duel.SetChainLimitTillChainEnd(aux.FALSE)
	end
end
function s.dcon(c)
	return function()
		return c:IsOriginalCodeRule(id)
	end
end
function s.dact(e, tp, eg, ep, ev, re, r, rp)
	if Duel.GetTurnPlayer() ~= tp then
		return
	end
	local tc = Duel.GetDecktopGroup(tp, 1):GetFirst()
	if tc then
		Duel.ConfirmCards(tp, tc)
	end
	if not tc or not Duel.SelectYesNo(tp, e:GetOwner()) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	local ac = Duel.AnnounceCard(tp, 45082499, OPCODE_ISCODE, 87008374, OPCODE_ISCODE, OPCODE_OR, 2648201, OPCODE_ISCODE, OPCODE_OR, 81471108, OPCODE_ISCODE, OPCODE_OR, 18865703, OPCODE_ISCODE, OPCODE_OR, 76080032, OPCODE_ISCODE, OPCODE_OR, 29353756, OPCODE_ISCODE, OPCODE_OR)
	tc:SetEntityCode(ac)
	tc:ReplaceEffect(ac, 0)
	Duel.SetMetatable(tc, _G["c" .. ac])
	tc:ResetEffect(ac, RESET_CARD)
	tc:ResetEffect(ac, RESET_CARD)
	_G["c" .. ac].initial_effect(tc)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_HAND, 0, 1, nil, 0x107e)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp, c, sg)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local sc = Duel.SelectMatchingCard(tp, Card.IsSetCard, tp, LOCATION_HAND, 0, 1, 1, nil, 0x107e):GetFirst()
	Duel.MoveToField(sc, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	sc:SetStatus(STATUS_FORM_CHANGED, true)
end

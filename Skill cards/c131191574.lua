--coded by Lyris
--ZEXAL - Xyz Evolution
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
	e2:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e2:SetCondition(s.fcon)
	e2:SetOperation(s.flip(c))
	e2:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e2)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetCountLimit(1)
	e0:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e0:SetCondition(s.con(c))
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetCondition(s.dcon(c))
	e1:SetOperation(s.dact)
	c:RegisterEffect(e1)
end
function s.fcon(e, tp)
	return Duel.GetTurnPlayer() == tp and Duel.GetLP(tp) <= 2000
end
function s.flip(c)
	return function()
		c:SetEntityCode(id)
		Duel.Hint(HINT_CARD, 0, id)
	end
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp, c, sg)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.MoveToField(Duel.CreateToken(tp, 11705261), tp, tp, LOCATION_SZONE, POS_FACEUP, true)
	if Duel.GetLP(tp) ~= 2000 and Duel.SelectEffectYesNo(tp, e:GetOwner()) then
		Duel.SetLP(tp, 2000)
	end
end
function s.dcon(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_GRAVE, 0, 1, nil, 0x107f) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id)
	end
end
function s.dact(e, tp, eg, ep, ev, re, r, rp, c, sg)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.SendtoDeck(Duel.GetMatchingGroup(Card.IsSetCard, tp, LOCATION_GRAVE, 0, nil, 0x107f), nil, SEQ_DECKBOTTOM, REASON_RULE)
end

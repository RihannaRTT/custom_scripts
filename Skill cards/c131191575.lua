--coded by Lyris
--Shooting Star Road
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsSetCard(0x43) and c:IsType(TYPE_MONSTER) or c:IsSetCard(0x1017) and c:IsType(TYPE_TUNER)
end
function s.flip(c)
	return function(e, tp)
		if (Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, 8, nil) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_EXTRA, 0, 1, nil, 24696097) and Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_EXTRA, 0, 4, nil, 0x43, 0x1017, 0xa3)) then
			c:SetEntityCode(id)
			Duel.SendtoGrave(Duel.CreateToken(tp, 11069680), REASON_RULE)
		end
	end
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsCode(44508094)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, nil) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(s.splimit)
	e1:SetReset(RESET_PHASE + PHASE_END + RESET_OPPO_TURN)
	Duel.RegisterEffect(e1, tp)
	local tk = Duel.CreateToken(tp, 50091196)
	Duel.MoveToField(tk, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	tk:CompleteProcedure()
	tk:SetStatus(STATUS_FORM_CHANGED, true)
	if Duel.SelectEffectYesNo(tp, e:GetOwner()) then
		for _, i in ipairs { 40348946, 96182448, 67270095 } do
			local tc = Duel.CreateToken(tp, i)
			Duel.SendtoDeck(tc, nil, SEQ_DECKTOP, REASON_RULE)
		end
	end
end
function s.splimit(e, c)
	return not ((c:IsSetCard(0x43) or c:IsAttribute(ATTRIBUTE_WIND) and c:IsRace(RACE_DRAGON)) and c:IsType(TYPE_SYNCHRO))
end

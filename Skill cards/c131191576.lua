--coded by Lyris
--Demon's Resonance
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetCondition(s.con(c, e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.cfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsRace(RACE_DRAGON + RACE_FIEND)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(Card.IsSetCard, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, 0x57), Duel.GetMatchingGroupCount(s.cfilter, tp, LOCATION_DECK, 0, nil), Duel.GetMatchingGroup(Card.IsCode, tp, LOCATION_EXTRA, 0, nil, 70902743, 97489701):GetClassCount(Card.GetCode), Duel.GetMatchingGroupCount(aux.NOT(Card.IsType), tp, LOCATION_EXTRA, 0, nil, TYPE_SYNCHRO))
end
function s.afilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x57) and not c:IsPublic()
end
function s.filter(c)
	return c:IsLevelBelow(4) and c:IsRace(RACE_FIEND)
end
function s.con(c, ef)
	return function(e, tp)
		local ct, xt1, ex, xt2 = ef:GetLabel()
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and ct > 7 and xt1 == 0 and ex > 1 and xt2 == 0 and Duel.IsExistingMatchingCard(s.afilter, tp, LOCATION_HAND, 0, 1, nil) and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and Duel.IsExistingMatchingCard(Card.IsLevelBelow, tp, LOCATION_DECK, 0, 1, nil, 4) and c:GetFlagEffect(id) == 0 and e:GetLabel() < 2
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local cc = Duel.SelectMatchingCard(tp, s.afilter, tp, LOCATION_HAND, 0, 1, 1, nil):GetFirst()
	Duel.ConfirmCards(1 - tp, cc)
	Duel.SendtoDeck(cc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil):GetFirst()
	Duel.SendtoHand(tc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, tc)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_CANNOT_ACTIVATE)
	e1:SetTargetRange(1, 0)
	e1:SetValue(s.val(tc))
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	Duel.SendtoDeck(Duel.CreateToken(tp, 66141736), nil, SEQ_DECKTOP, REASON_RULE)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end
function s.val(tc)
	return function(e, rf, p)
		return rf:GetHandler():IsCode(tc:GetCode())
	end
end

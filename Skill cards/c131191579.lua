--coded by Lyris
--Gathering of Ghosts
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_CHAINING)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCondition(s.atcon)
	e1:SetOperation(aux.chainreg)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_CHAIN_SOLVED)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetLabel(0)
	e2:SetOperation(s.atop)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_ACTIVATE)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetLabel(0)
	e3:SetCondition(s.con(c, e0))
	e3:SetOperation(s.act)
	c:RegisterEffect(e3)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND + LOCATION_EXTRA, 0, nil))
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsRace(RACE_MACHINE)
end
function s.atcon(e, tp, eg, ep, ev, re, r, rp)
	return re:IsHasType(EFFECT_TYPE_ACTIVATE) and re:GetHandler():IsCode(56769674)
end
function s.atop(e, tp, eg, ep, ev, re, r, rp)
	if e:GetOwner():GetFlagEffect(1) == 0 then
		return
	end
	Duel.RegisterFlagEffect(ep, id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.con(c, ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and e:GetLabel() < 2 and c:GetFlagEffect(id) == 0 and ef:GetLabel() == 0 and Duel.IsExistingMatchingCard(Card.IsAttribute, tp, LOCATION_HAND, 0, 1, nil, ATTRIBUTE_DARK) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.SelectMatchingCard(tp, Card.IsAttribute, tp, LOCATION_HAND, 0, 1, 1, nil):GetFirst()
	Duel.ConfirmCards(1 - tp, tc)
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	local tk = Duel.CreateToken(tp, 59482302)
	Duel.MoveToField(tk, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	tk:SetStatus(STATUS_FORM_CHANGED, true)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end

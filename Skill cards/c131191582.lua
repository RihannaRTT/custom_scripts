--coded by Lyris
--Parts Separation [Wisel]
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsCode(68140974)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.GetFieldGroupCount(tp, LOCATION_EXTRA, 0) == 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0
	end
end
function s.filter(c)
	return c:IsLevel(1) and c:IsAttribute(ATTRIBUTE_DARK) and c:IsRace(RACE_MACHINE)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, 131191599)
	local ft = math.min(Duel.GetLocationCount(tp, LOCATION_MZONE), 2)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND, 0, 1, ft, nil)
	Duel.ConfirmCards(1 - tp, g)
	Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	local xcode, code = 0
	for i = 1, #g do
		while not code or code == xcode do
			code = Duel.AnnounceCard(tp, 130000059, OPCODE_ISCODE, 130000061, OPCODE_ISCODE, OPCODE_OR, 130000062, OPCODE_ISCODE, OPCODE_OR, 130000064, OPCODE_ISCODE, OPCODE_OR)
			if code == xcode then
				Duel.Hint(HINT_OPSELECTED, tp, aux.Stringid(id, 0))
			end
		end
		local tk = Duel.CreateToken(tp, code)
		Duel.MoveToField(tk, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
		tk:SetStatus(STATUS_FORM_CHANGED, true)
		xcode = code
	end
end

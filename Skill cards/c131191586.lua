--coded by Lyris
--The Dragon Knight's Path
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c, e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil))
end
function s.con(c, ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and ef:GetLabel() > 7 and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK, 0, 1, nil, 2106266)
	end
end
function s.cfilter(c)
	return c:IsType(TYPE_MONSTER) and (c:IsSetCard(0xbd) or c:IsLevel(5) and c:IsRace(RACE_DRAGON))
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(s.splimit)
	e1:SetReset(RESET_PHASE + PHASE_END + RESET_OPPO_TURN)
	Duel.RegisterEffect(e1, tp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local cc = Duel.SelectMatchingCard(tp, Card.IsType, tp, LOCATION_HAND, 0, 1, 1, nil, TYPE_MONSTER):GetFirst()
	Duel.ConfirmCards(1 - tp, cc)
	Duel.SendtoDeck(cc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local tc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK, 0, 1, 1, nil, 2106266):GetFirst()
	local fc = Duel.GetFieldCard(tp, LOCATION_FZONE, 0)
	if fc then
		Duel.SendtoGrave(fc, REASON_RULE)
	end
	Duel.MoveToField(tc, tp, tp, LOCATION_FZONE, POS_FACEUP, true)
end
function s.splimit(e, c)
	return not (c:IsCode(2519690, 15989522, 66889139) or c:IsSetCard(0xbd, 0x10cf) or c:IsLevel(5) and c:IsRace(RACE_DRAGON))
end

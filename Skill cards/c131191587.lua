--coded by Lyris
--Halved Synchro
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c, e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK, 0, nil))
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsSetCard(0x27)
end
function s.con(c, ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and ef:GetLabel() == 0 and Duel.GetLocationCountFromEx(tp, tp, nil, TYPE_SYNCHRO) > 0 and Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_MZONE, 0, 1, nil, Duel.GetMatchingGroup(s.filter1, tp, LOCATION_MZONE, 0, nil))
	end
end
function s.filter1(c)
	return c:IsFaceup() and c:IsSetCard(0x27) and c:GetType() & 0x3000 == 0x3000
end
function s.filter2(c, g)
	return c:IsSetCard(0x27) and c:IsType(TYPE_SYNCHRO) and g:IsExists(s.filter0, 1, nil, c)
end
function s.filter0(tc, c)
	return c:GetLevel() < tc:GetLevel() / 2
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local tc = Duel.SelectMatchingCard(s.filter2, tp, LOCATION_EXTRA, 0, 1, 1, nil, Duel.GetMatchingGroup(s.filter1, tp, LOCATION_MZONE, 0, nil)):GetFirst()
	Duel.MoveToField(tc, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	tc:CompleteProcedure()
	tc:SetStatus(STATUS_FORM_CHANGED, true)
end

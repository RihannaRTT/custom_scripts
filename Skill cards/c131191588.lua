--coded by Lyris
--Puppet - Strings of Fate
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_CHAIN_SOLVING)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetOperation(s.reg)
	c:RegisterEffect(e1)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		Duel.SendtoDeck(Duel.CreateToken(tp, 88120966), nil, SEQ_DECKTOP, REASON_RULE)
	end
end
function s.reg(e, tp, eg, ep, ev, re)
	if re:IsActiveType(TYPE_MONSTER) and re:GetHandler():IsOriginalCodeRule(88120966) then
		Duel.RegisterFlagEffect(ep, id, RESET_PHASE + PHASE_END, 0, 1)
	end
end
function s.con(e, tp)
	return Duel.CheckRemoveOverlayCard(tp, 1, 0, 1, REASON_RULE) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(Card.IsType, tp, 0, LOCATION_GRAVE, 1, nil, TYPE_XYZ) and Duel.GetLocationCount(1 - tp, LOCATION_MZONE) > 0
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.RemoveOverlayCard(tp, 1, 0, 1, 1, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	Duel.MoveToField(Duel.SelectMatchingCard(tp, Card.IsType, tp, 0, LOCATION_GRAVE, 1, 1, nil, TYPE_XYZ):GetFirst(), tp, 1 - tp, LOCATION_MZONE, POS_FACEUP_DEFENSE, true)
end

--coded by Lyris
--Gimmick Puppet - Gear Changer
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsCode(56427559) and not c:IsPublic()
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil) and e:GetLabel() < 2 and c:GetFlagEffect(id) == 0
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	Duel.ConfirmCards(1 - tp, Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND, 0, 1, 1, nil))
	local g = Duel.SelectMatchingCard(tp, c.filter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	local t = {  }
	for i = 4, 8 do
		if i % 4 > 0 or tc:IsLevel(i) then
			table.insert(t, i)
		end
	end
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_CHANGE_LEVEL)
	e1:SetValue(Duel.AnnounceLevel(tp, 4, 8, table.unpack(t)))
	e1:SetReset(RESET_EVENT + RESETS_STANDARD)
	tc:RegisterEffect(e1, true)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end
function s.filter(c)
	return c:IsFaceup() and c:IsSetCard(0x1083) and c:IsLevelAbove(1)
end

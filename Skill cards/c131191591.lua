--coded by Lyris
--Gimmick Puppet - Returning
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsFaceup() and c:IsSetCard(0x1083)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_REMOVED, LOCATION_REMOVED, 1, nil) and e:GetLabel() < 2 and c:GetFlagEffect(id) == 0
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.SendtoGrave(Duel.GetMatchingGroup(s.filter, tp, LOCATION_REMOVED, LOCATION_REMOVED, nil), REASON_RULE + REASON_RETURN)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end

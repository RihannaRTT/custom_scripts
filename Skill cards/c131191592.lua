--coded by Lyris
--Relinquished Puppet
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c, e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND + LOCATION_EXTRA, 0, nil))
end
function s.con(c, ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and ef:GetLabel() == 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, 1, nil):GetFirst()
	Duel.SendtoGrave(tc, REASON_RULE)
	local tk = Duel.CreateToken(tp, 76543119)
	Duel.MoveToField(tk, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	tk:SetStatus(STATUS_FORM_CHANGED, true)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not (c:IsAttribute(ATTRIBUTE_DARK) and c:IsSetCard(0x48) or c:IsSetCard(0x1083))
end
function s.filter(c)
	return c:IsLevel(8) and c:IsSetCard(0x1083)
end

--coded by Lyris
--Summoning Crest
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabel(0)
	e2:SetCondition(s.con(c, e0))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil))
end
function s.con(c, ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK, 0, 1, nil, 15744417) and ef:GetLabel() > 8 and c:GetFlagEffect(id) == 0 and e:GetLabel() < 2
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local tc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK, 0, 1, 1, nil, 15744417):GetFirst()
	Duel.SendtoHand(tc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, tc)
	Duel.ShuffleHand(tp)
	Duel.BreakEffect()
	local ct = 0
	for i = 1, 3 do
		ct = ct + Duel.TossDice(tp, 1)
	end
	if ct > 10 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
		local rc = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil):GetFirst()
		Duel.SendtoDeck(rc, nil, SEQ_DECKBOTTOM, REASON_RULE)
	end
	if ct < 10 then
		for tc in aux.Next(Duel.GetMatchingGroup(s.filter, tp, LOCATION_HAND, 0, nil)) do
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_CHANGE_LEVEL)
			e1:SetValue(4)
			e1:SetReset(RESET_EVENT + RESETS_STANDARD - RESET_TOFIELD + RESET_PHASE + PHASE_END)
			tc:RegisterEffect(e1, true)
		end
	end
	if ct < 7 then
		local e2 = Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_FIELD)
		e2:SetCode(EFFECT_SET_SUMMON_COUNT_LIMIT)
		e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e2:SetTargetRange(1, 0)
		e2:SetValue(2)
		e2:SetReset(RESET_PHASE + PHASE_END)
		Duel.RegisterEffect(e2, tp)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end
function s.cfilter(c)
	return c.toss_dice
end
function s.filter(c)
	return c:IsCode(15744417) and not c:IsLevel(4)
end

--coded by XGlitchy30
--Domination of Darkness
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.checkop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetLabelObject(e0)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
s.expired_codes = {  }
function s.checkdeck(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x6008)
end
function s.checkexc(c)
	return c:IsType(TYPE_MONSTER) and not c:IsSetCard(0x3008, 0x6008)
end
function s.checkexc2(c)
	return c:IsType(TYPE_FUSION) and not c:IsSetCard(0x6008)
end
function s.checkop(e, tp)
	local g1 = Duel.GetMatchingGroup(s.checkdeck, tp, LOCATION_DECK, 0, nil)
	local g2 = Duel.GetMatchingGroup(s.checkdeck, tp, LOCATION_EXTRA, 0, nil)
	if #g1 >= 5 and not Duel.IsExistingMatchingCard(s.checkexc, tp, LOCATION_DECK, 0, 1, nil) and #g2 >= 5 and not Duel.IsExistingMatchingCard(s.checkexc2, tp, LOCATION_EXTRA, 0, 1, nil) then
		e:SetLabel(100)
	end
end
function s.rvfilter(c)
	if not c.material then
		return false
	end
	local matcheck = false
	for code, checkcode in pairs(c.material) do
		if type(code) == "number" and checkcode then
			matcheck = true
		end
	end
	if not matcheck then
		return false
	end
	return c:IsFacedown() and c:IsType(TYPE_FUSION) and c:IsSetCard(0x6008) and (#s.expired_codes == 0 or not c:IsCode(table.unpack(s.expired_codes)))
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and e:GetLabelObject():GetLabel() == 100 and Duel.IsExistingMatchingCard(s.rvfilter, tp, LOCATION_EXTRA, 0, 1, nil) and Duel.GetFlagEffect(tp, id) <= 0
end
function s.act(e, tp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local g = Duel.SelectMatchingCard(tp, s.rvfilter, tp, LOCATION_EXTRA, 0, 1, 1, nil)
	if #g > 0 then
		Duel.ConfirmCards(1 - tp, g)
		for _, cd in ipairs({ g:GetFirst():GetCode() }) do
			table.insert(s.expired_codes, cd)
		end
		local tc = g:GetFirst()
		for code, _ in pairs(tc.material) do
			local tk = Duel.CreateToken(tp, code)
			Duel.SendtoGrave(tk, REASON_RULE)
		end
	end
	Duel.RegisterFlagEffect(tp, id, RESET_PHASE + PHASE_END, 0, 1)
	e:GetOwner():SetEntityCode(111004001)
end

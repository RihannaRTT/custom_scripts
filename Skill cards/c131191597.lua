--coded by Lyris
--Evil Change
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c, e0))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(aux.NOT(Card.IsRace), tp, LOCATION_EXTRA, 0, nil, RACE_FIEND))
end
function s.cfilter(c, tp)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x6008) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil, c:GetCode())
end
function s.filter(c, code)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x6008) and not c:IsCode(id) or c:IsCode(94820406)
end
function s.con(c, ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and ef:GetLabel() == 0 and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil, tp)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_HAND, 0, 1, 1, nil, tp):GetFirst()
	Duel.ConfirmCards(1 - tp, tc)
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local sc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil, tc:GetCode()):GetFirst()
	Duel.SendtoHand(sc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, sc)
end

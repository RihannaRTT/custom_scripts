--coded by Lyris
--Evil Reincarnation
local s, id, o = GetID()
function s.initial_effect(c)
	local ge1 = Effect.CreateEffect(c)
	ge1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	ge1:SetCode(EVENT_DESTROYED)
	ge1:SetCondition(s.regcon)
	ge1:SetOperation(s.reg)
	ge1:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(ge1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c, tp)
	return c:IsReason(REASON_BATTLE) and c:GetPreviousControler() == tp
end
function s.regcon(e, tp, eg, ep, ev, re, r, rp)
	local v = 0
	if eg:IsExists(s.cfilter, 1, nil, 0) then
		v = v + 1
	end
	if eg:IsExists(s.cfilter, 1, nil, 1) then
		v = v + 2
	end
	if v == 0 then
		return false
	end
	e:SetLabel(({ 0, 1, PLAYER_ALL })[v])
	return true
end
function s.reg(e, tp)
	local ep = e:GetLabel()
	if ep == PLAYER_ALL or ep == tp then
		Duel.RegisterFlagEffect(tp, id, RESET_PHASE + PHASE_END, 0, Duel.GetTurnPlayer() == tp and 3 or 2)
	end
	if ep == PLAYER_ALL or ep ~= tp then
		Duel.RegisterFlagEffect(ep, id, RESET_PHASE + PHASE_END, 0, Duel.GetTurnPlayer() ~= tp and 3 or 2)
	end
end
function s.filter(c)
	return c:GetOriginalRace() == RACE_FIEND and c:GetOriginalAttribute() == ATTRIBUTE_EARTH
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetFlagEffect(tp, 131191538) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_GRAVE, 0, 1, nil)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_GRAVE, 0, 1, 1, nil):GetFirst()
	Duel.SendtoHand(tc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, tc)
	Duel.ShuffleHand(tp)
	Duel.BreakEffect()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local dc = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil):GetFirst()
	Duel.SendtoDeck(dc, nil, SEQ_DECKBOTTOM, REASON_RULE)
end

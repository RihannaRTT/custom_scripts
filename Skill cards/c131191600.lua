--coded by Lyris
--Remains of the Heroes
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetTargetRange(LOCATION_MZONE, 0)
	e2:SetTarget(s.target)
	e2:SetValue(s.val(c))
	c:RegisterEffect(e2)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
	end
end
function s.target(e, c)
	return c:IsSetCard(0x6008) and c:IsType(TYPE_FUSION)
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x3008, 0x6008)
end
function s.val(c)
	return function()
		return 100 * Duel.GetMatchingGroupCount(s.filter, c:GetControler(), LOCATION_GRAVE, 0, nil)
	end
end

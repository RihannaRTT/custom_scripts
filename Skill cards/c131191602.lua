--coded by Lyris
--Sinister Calling
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetCondition(s.con(c, e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.cfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x6008) or c:IsCode(94820406, 12071500)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsSetCard(0x3008, 0x6008)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil), Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil), Duel.GetMatchingGroupCount(aux.NOT(Card.IsType), tp, LOCATION_EXTRA, 0, nil, TYPE_FUSION))
end
function s.con(c, ef)
	return function(e, tp)
		local u, ct, xt1, xt2 = e:GetLabel(), ef:GetLabel()
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and ct > 9 and xt1 == 0 and xt2 == 0 and c:GetFlagEffect(id) == 0 and Duel.IsExistingMatchingCard(s.dfilter, tp, LOCATION_HAND, 0, 1, nil) and (not u or u < 0x3 and (u & 0x1 > 0 or Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK, 0, 1, nil, 94820406, 12071500)))
	end
end
function s.dfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x6008)
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DISCARD)
	local dc = Duel.SelectMatchingCard(tp, s.dfilter, tp, LOCATION_HAND, 0, 1, 1, nil):GetFirst()
	Duel.SendtoGrave(dc, REASON_RULE + REASON_DISCARD)
	local ct = e:GetLabel()
	local b1 = Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK, 0, 1, nil, 94820406, 12071500) and (not ct or ct & 0x1 == 0)
	local b2 = not ct or ct & 0x2 == 0
	local op = 0
	if b1 and b2 then
		op = Duel.SelectOption(tp, 1190, 1191)
	elseif b1 then
		op = Duel.SelectOption(tp, 1190)
	else
		op = Duel.SelectOption(tp, 1191) + 1
	end
	if op == 0 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local tc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK, 0, 1, 1, nil, 94820406, 12071500):GetFirst()
		Duel.SendtoHand(tc, nil, REASON_RULE)
		Duel.ConfirmCards(1 - tp, tc)
		e:SetLabel(ct | 0x1)
	else
		local tk = Duel.CreateToken(tp, 45659520)
		Duel.SendtoGrave(tk, REASON_RULE)
		e:SetLabel(ct | 0x2)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

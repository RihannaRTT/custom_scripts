--coded by Lyris
--Supreme King's Followers
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	e2:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
		local e4 = e5:Clone()
		Duel.RegisterEffect(e4, 1)
	end
end
function s.count(e, tp)
	local plp, clp = e:GetLabel(), Duel.GetLP(tp)
	e:SetLabel(clp)
	if plp <= clp then
		return
	end
	s[tp] = s[tp] - clp + plp
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		Duel.SendtoDeck(Duel.CreateToken(tp, 2504891), nil, SEQ_DECKTOP, REASON_RULE)
	end
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0 and Duel.GetDrawCount(tp) > 0 and s[tp] >= 1000
end
function s.act(e, tp)
	if not Duel.SelectEffectYesNo(tp, e:GetOwner()) then
		return
	end
	local dt = Duel.GetDrawCount(tp)
	if dt ~= 0 then
		_replace_count = 0
		_replace_max = dt
		local e1 = Effect.GlobalEffect()
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetCode(EFFECT_DRAW_COUNT)
		e1:SetTargetRange(1, 0)
		e1:SetReset(RESET_PHASE + PHASE_DRAW)
		e1:SetValue(0)
		Duel.RegisterEffect(e1, tp)
	end
	_replace_count = _replace_count + 1
	if _replace_count > _replace_max then
		return
	end
	s[tp] = 0
	Duel.Hint(HINT_CARD, 0, id)
	local g = Group.CreateGroup()
	local toCreate = { 72426662, 73752131, 46363422, 9596126, 73544866, 42431843 }
	for _, code in ipairs(toCreate) do
		g:AddCard(Duel.CreateToken(tp, code))
	end
	Duel.Remove(g, POS_FACEDOWN, REASON_RULE)
	local sg = g:RandomSelect(tp, 1)
	Duel.SendtoDeck(sg, nil, SEQ_DECKTOP, REASON_RULE)
	g:Sub(sg)
	Duel.Exile(g, REASON_RULE)
	Duel.Draw(tp, 1, REASON_RULE)
end

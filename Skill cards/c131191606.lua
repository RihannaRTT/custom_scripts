--coded by XGlitchy30
--ZEXAL - Leo Arms
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
	e0:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e0:SetCountLimit(1)
	e0:SetCondition(s.turncon)
	e0:SetOperation(s.transform)
	e0:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCountLimit(1)
	e1:SetCondition(s.con1)
	e1:SetOperation(s.act1)
	c:RegisterEffect(e1)
	local e2 = e1:Clone()
	e2:SetDescription(aux.Stringid(id, 1))
	e2:SetCondition(s.con2)
	e2:SetOperation(s.act2)
	c:RegisterEffect(e2)
end
function s.turncon(e, tp)
	return Duel.GetTurnPlayer() == tp and Duel.GetLP(tp) <= 2000
end
function s.transform(e, tp)
	c:SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
end
function s.sharedcon(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id)
end
function s.con1(e, tp)
	return s.sharedcon(e, tp) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0
end
function s.act1(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_OPSELECTED, 0, e:GetDescription())
	Duel.Hint(HINT_CARD, 0, id)
	local tc = Duel.CreateToken(tp, 60992364)
	Duel.MoveToField(tc, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	tc:SetStatus(STATUS_FORM_CHANGED, true)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_SET_ATTACK)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e0:SetValue(0)
	e0:SetReset(RESET_EVENT + RESETS_STANDARD)
	tc:RegisterEffect(e0, true)
	local e0x = e0:Clone()
	e0x:SetCode(EFFECT_SET_DEFENSE)
	tc:RegisterEffect(e0x, true)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UNRELEASABLE_SUM)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e1:SetValue(1)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD)
	tc:RegisterEffect(e1, true)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_UNRELEASABLE_NONSUM)
	tc:RegisterEffect(e2, true)
	local e3 = e1:Clone()
	e3:SetCode(EFFECT_CANNOT_BE_XYZ_MATERIAL)
	tc:RegisterEffect(e3, true)
	tc:CompleteProcedure()
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.tofilter(c)
	return c:IsFaceup() and c:IsCode(60992364)
end
function s.fromfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x107e)
end
function s.con2(e, tp)
	return s.sharedcon(e, tp) and Duel.IsExistingMatchingCard(s.tofilter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.IsExistingMatchingCard(s.fromfilter, tp, LOCATION_GRAVE, 0, 3, nil)
end
function s.act1(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_OPSELECTED, 0, e:GetDescription())
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.tofilter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_XMATERIAL)
	local xg = Duel.SelectMatchingCard(tp, s.fromfilter, tp, LOCATION_GRAVE, 0, 3, 3, nil)
	Duel.HintSelection(xg)
	local escape = 0
	while xg:IsExists(aux.NOT(Card.IsLocation), 1, nil, LOCATION_OVERLAY) and escape < 3 do
		local xgf = xg:Filter(aux.NOT(Card.IsLocation), nil, LOCATION_OVERLAY)
		Duel.Overlay(g:GetFirst(), xgf)
		escape = escape + 1
	end
	escape = 0
	local tg = Duel.GetMatchingGroup(s.fromfilter, tp, LOCATION_GRAVE, 0, nil)
	if #tg > 0 and Duel.SelectYesNo(tp, aux.Stringid(id, 2)) then
		Duel.BreakEffect()
		while xg:IsExists(aux.NOT(Card.IsLocation), 1, nil, LOCATION_DECK + LOCATION_EXTRA) and escape < 3 do
			local xgf = xg:Filter(aux.NOT(Card.IsLocation), nil, LOCATION_DECK + LOCATION_EXTRA)
			Duel.SendtoDeck(xgf, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			escape = escape + 1
		end
	end
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

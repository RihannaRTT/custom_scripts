--coded by Lyris
--Chronomaly Resonance
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.filter(c, tp)
	local TYPE_EXTRA = TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK
	return c:IsFaceup() and c:IsSetCard(0x70) and c:IsLevelAbove(1) and Duel.GetMZoneCount(tp, c) > 0 and c:GetOriginalType() & TYPE_EXTRA == 0 and Duel.IsExistingMatchingCard(s.sfilter, tp, LOCATION_HAND, 0, 1, nil, c:GetLevel())
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, tp)
	end
end
function s.sfilter(c, lv)
	return c:IsSetCard(0x70) and c:IsLevelAbove(lv + 1)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_RTOHAND)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil, tp)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	tc:ResetEffect(RESET_LEAVE, RESET_EVENT)
	Duel.SendtoHand(tc, tp, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local sc = Duel.SelectMatchingCard(tp, s.sfilter, tp, LOCATION_HAND, 0, 1, 1, nil, tc:GetLevel()):GetFirst()
	Duel.MoveToField(sc, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	sc:CompleteProcedure()
	sc:SetStatus(STATUS_FORM_CHANGED, true)
end

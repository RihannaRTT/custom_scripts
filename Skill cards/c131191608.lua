--coded by Lyris
--Divine Artifacts
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		Duel.SendtoDeck(Duel.CreateToken(tp, 9161357), nil, SEQ_DECKBOTTOM, REASON_RULE)
	end
end
function s.cfilter(c, tp)
	return c:IsSetCard(0x70) and Duel.IsExistingMatchingCard(c.lfilter, tp, LOCATION_MZONE, 0, 1, nil, c:GetLevel())
end
function s.lfilter(c, lv)
	return c:IsFaceup() and c:IsSetCard(0x70) and c:IsLevelAbove(1) and not c:IsLevel(lv)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil, tp)
end
function s.filter(c)
	return c:IsFaceup() and c:IsSetCard(0x70)
end
function s.act(e, tp, eg, ep, ev, re, r, rp, c, sg)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local lc = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_HAND, 0, 1, 1, nil, tp):GetFirst()
	Duel.ConfirmCards(1 - tp, lc)
	for tc in aux.Next(Duel.GetMatchingGroup(s.filter, tp, LOCATION_MZONE, 0, nil)) do
		if tc:IsLevelAbove(1) then
			local e1 = Effect.CreateEffect(e:GetOwner())
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_CHANGE_LEVEL)
			e1:SetValue(lc:GetLevel())
			e1:SetReset(RESET_EVENT + RESETS_STANDARD)
			tc:RegisterEffect(e1, true)
		end
	end
end

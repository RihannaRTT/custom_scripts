--coded by Lyris
--Duels of Babylon
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsSetCard(0x70, 0x48)
end
function s.act(c)
	return function(e, tp)
		if Duel.IsExistingMatchingCard(s.xfilter, tp, LOCATION_DECK + LOCATION_EXTRA + LOCATION_HAND, 0, 1, nil) then
			return
		end
		c:SetEntityCode(id)
		Duel.MoveToField(Duel.CreateToken(tp, 4357063), tp, tp, LOCATION_FZONE, POS_FACEUP, true)
	end
end

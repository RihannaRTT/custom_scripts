--coded by Lyris
--Family Gift
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_CHAIN_SOLVING)
	e1:SetOperation(s.chk)
	e1:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.reg)
	c:RegisterEffect(e0)
end
function s.chk(e, tp, eg, ep, ev, re)
	if re:IsActiveType(TYPE_SPELL) and re:GetHandler():IsSetCard(0x70) then
		Duel.RegisterFlagEffect(ep, id, RESET_PHASE + PHASE_END, 0, 1)
	end
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetFlagEffect(tp, id) > 0 and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil):GetFirst()
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.SendtoHand(Duel.CreateToken(tp, 94766498), nil, REASON_RULE)
end
function s.reg(e, tp)
	Duel.CreateToken(tp, 94766498)
end

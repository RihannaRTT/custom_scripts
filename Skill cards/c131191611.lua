--coded by Lyris
--Gateway to the Another Dimension
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c, e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not (c:IsSetCard(0x70) or c:IsAttribute(ATTRIBUTE_LIGHT) and c:IsSetCard(0x48))
end
function s.cfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x70)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND + LOCATION_EXTRA, 0, nil), Duel.GetMatchingGroupCount(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil))
end
function s.con(c, ef)
	return function(e, tp)
		local ctx, ctc = ef:GetLabel()
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and ctx == 0 and ctc > 11 and c:GetFlagEffect(id) == 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK + LOCATION_EXTRA, 0, 1, nil, tp)
	end
end
function s.vfilter(c)
	return c:IsFaceup() and c:IsLevelAbove(1)
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local lc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_EXTRA, 0, 1, 1, nil, tp):GetFirst()
	Duel.SendtoGrave(lc, REASON_RULE)
	for tc in aux.Next(Duel.GetMatchingGroup(s.vfilter, tp, LOCATION_MZONE, 0, nil)) do
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_CHANGE_LEVEL)
		e1:SetValue(lc:IsType(TYPE_XYZ) and lc:GetRank() or lc:GetLevel())
		e1:SetReset(RESET_EVENT + RESETS_STANDARD)
		tc:RegisterEffect(e1, true)
	end
	local g = Duel.GetMatchingGroup(Card.IsType, tp, LOCATION_HAND, 0, nil, TYPE_MONSTER)
	if lc:IsPreviousLocation(LOCATION_DECK) and #g > 0 and Duel.SelectYesNo(tp, 1191) then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
		local sc = g:Select(tp, 1, 1, nil):GetFirst()
		Duel.SendtoGrave(sc, REASON_RULE)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.filter(c, tp)
	local lv = c:IsType(TYPE_XYZ) and c:GetRank() or c:GetLevel()
	return c:IsType(TYPE_MONSTER) and lv > 0 and Duel.IsExistingMatchingCard(s.lfilter, tp, LOCATION_MZONE, 0, 1, nil, lv)
end
function s.lfilter(c, lv)
	return c:IsFaceup() and c:IsLevelAbove(1) and not c:IsLevel(lv)
end

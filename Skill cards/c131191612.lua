--coded by Lyris
--Ley Line Energy
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, tp) and Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_MZONE, 1, nil, tp)
	end
end
function s.filter(c, tp)
	return c:IsFaceup() and c:IsAttackAbove(3000) and (c:IsControler(tp) or not c:IsHasEffect(121191258))
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local g = Group.CreateGroup()
	for i = 0, 1 do
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_DESTROY)
		g:Merge(Duel.SelectMatchingCard(tp, s.filter, (tp + i) % 2, LOCATION_MZONE, 0, 1, 1, nil, tp))
	end
	Duel.HintSelection(g)
	local tc1, tc2 = g:GetFirst(), g:GetNext()
	local a1, a2 = tc1:GetAttack(), tc2:GetAttack()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SET_ATTACK)
	e1:SetValue(a2)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD)
	tc1:RegisterEffect(e1, true)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_SET_ATTACK)
	e2:SetValue(a1)
	e2:SetReset(RESET_EVENT + RESETS_STANDARD)
	tc2:RegisterEffect(e2, true)
end

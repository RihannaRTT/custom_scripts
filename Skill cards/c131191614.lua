--coded by Lyris
--Re-Atlandis
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.IsExistingMatchingCard(c9161357.filter, tp, LOCATION_GRAVE, 0, 1, nil)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(9161357)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_EQUIP)
	local qg = Duel.SelectMatchingCard(tp, c9161357.filter, tp, LOCATION_GRAVE, 0, 1, 1, nil)
	Duel.HintSelection(qg)
	Duel.SetTargetCard(qg)
	local te
	for _, ef in ipairs { g:GetFirst():IsHasEffect(EVENT_SPSUMMON_SUCCESS) } do
		if ef:IsHasProperty(EFFECT_FLAG_INITIAL) then
			te = ef:Clone()
			break
		end
	end
	qg:GetFirst():CreateEffectRelation(te)
	c9161357.eqop(te, tp)
end

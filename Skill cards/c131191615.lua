--coded by Lyris
--Gimmick Puppet - 4 or 8
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and e:GetLabel() < 2 and e:GetOwner():GetFlagEffect(id) == 0
end
function s.filter(c, lv)
	if not c:IsSetCard(0x1083) then
		return false
	end
	if lv then
		return c:IsFaceup() and not c:IsLevel(lv)
	else
		return c:IsLevel(8)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	local b1 = Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil)
	local b2 = Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, 8)
	if b1 and (not b2 or Duel.SelectOption(tp, 1001, 1002) == 0) then
		for tc in aux.Next(Duel.GetMatchingGroup(s.filter, tp, LOCATION_HAND, 0, nil)) do
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_CHANGE_LEVEL)
			e1:SetReset(RESET_EVENT + RESETS_STANDARD - RESET_TOFIELD + RESET_PHASE + PHASE_END)
			e1:SetValue(4)
			tc:RegisterEffect(e1, true)
		end
	else
		for tc in aux.Next(Duel.GetMatchingGroup(s.filter, tp, LOCATION_MZONE, 0, nil, 8)) do
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_CHANGE_LEVEL)
			e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
			e1:SetValue(8)
			tc:RegisterEffect(e1, true)
		end
	end
	e:SetLabel(e:GetLabel() + 1)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

--coded by Lyris
--Dueltaining Recast
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.pfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_PENDULUM)
end
function s.con(c)
	return function(e, tp)
		local g, pc = Duel.GetMatchingGroup(s.cfilter, tp, LOCATION_MZONE, 0, nil), Duel.GetMatchingGroupCount(s.pfilter, tp, LOCATION_MZONE, 0, nil)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and g:GetClassCount(Card.GetLevel) == 3 and g:CheckSubGroup(s.gchk, pc, pc, tp)
	end
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_PENDULUM) and c:IsLevelAbove(1)
end
function s.filter(c)
	return c:IsType(TYPE_PENDULUM) and c:IsSetCard(0x98, 0x99, 0x9f) and c:IsSummonableCard()
end
function s.gchk(g, tp)
	return Duel.GetMatchingGroup(s.filter, tp, LOCATION_HAND, 0, nil):CheckWithSumEqual(Card.GetLevel, g:GetSum(Card.GetOriginalLevel), 1, (Duel.GetMZoneCount(tp, g)))
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetMatchingGroup(s.cfilter, tp, LOCATION_MZONE, 0, nil, TYPE_PENDULUM)
	Duel.SendtoExtraP(g, tp, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	for tc in aux.Next(Duel.GetMatchingGroup(s.filter, tp, LOCATION_HAND, 0, nil):SelectWithSumEqual(tp, Card.GetLevel, g:GetSum(Card.GetOriginalLevel), 1, (Duel.GetLocationCount(tp, LOCATION_MZONE)))) do
		Duel.MoveToField(tc, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
		tc:SetStatus(STATUS_FORM_CHANGED, true)
		tc:CompleteProcedure()
	end
end

--coded by Lyris
--Extra Balloons
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not (c:IsSetCard(0x9f, 0x99) or c:IsType(TYPE_PENDULUM) and c:IsSetCard(0x98))
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		if not Duel.IsExistingMatchingCard(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil) then
			Duel.MoveToField(Duel.CreateToken(tp, 78574395), tp, tp, LOCATION_SZONE, POS_FACEUP, true)
		end
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(78574395) and c:GetCounter(0x32) == 0
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_SZONE, LOCATION_SZONE, 1, nil)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_COUNTER)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_SZONE, LOCATION_SZONE, 1, 1, nil)
	Duel.HintSelection(g)
	g:GetFirst():AddCounter(0x32, 1)
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

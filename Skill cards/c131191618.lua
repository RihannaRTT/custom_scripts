--coded by XGlitchy30
--Fusion or Advent
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.checkop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetLabelObject(e0)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.checkdeck(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x9f, 0x99) or c:IsType(TYPE_PENDULUM) and c:IsSetCard(0x98)
end
function s.checkop(e, tp)
	local g1 = Duel.GetMatchingGroup(s.checkdeck, tp, LOCATION_DECK, 0, nil)
	if #g1 >= 9 then
		e:SetLabel(100)
	end
end
function s.thfilter1(c)
	return c:IsFaceup() and c:IsCode(16178681)
end
function s.thfilter2(c)
	return c:IsCode(24094653) or c:IsType(TYPE_SPELL) and c:IsType(TYPE_RITUAL)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and e:GetLabelObject():GetLabel() == 100 and Duel.IsExistingMatchingCard(s.thfilter1, tp, LOCATION_PZONE, 0, 1, nil) and Duel.IsExistingMatchingCard(thfilter2, tp, LOCATION_HAND, 0, 1, nil)
end
function s.act(e, tp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g1 = Duel.SelectMatchingCard(tp, s.thfilter1, tp, LOCATION_PZONE, 0, 1, 1, nil)
	Duel.HintSelection(g1)
	local g2 = Duel.SelectMatchingCard(tp, s.thfilter2, tp, LOCATION_HAND, 0, 1, 1, nil)
	g1:Merge(g2)
	local escape = 0
	while g1:IsExists(aux.NOT(Card.IsLocation), 1, nil, LOCATION_DECK + LOCATION_EXTRA) and escape < 3 do
		Duel.SendtoDeck(g1:Filter(aux.NOT(Card.IsLocation), nil, LOCATION_DECK + LOCATION_EXTRA), nil, SEQ_DECKSHUFFLE, REASON_RULE)
		escape = escape + 1
	end
	escape = 0
	local code = (g1:Filter(Card.IsLocation, nil, LOCATION_DECK + LOCATION_EXTRA):IsExists(Card.IsCode, 1, nil, 24094653)) and 48144509 or 16494704
	local tk = Duel.CreateToken(tp, code)
	Duel.SendtoHand(tk, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, Group.FromCards(tk))
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

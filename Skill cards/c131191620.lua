--coded by XGlitchy30
--Pendulum Gazers
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.checkop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetLabelObject(e0)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.checkdeck(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x9f, 0x99) or c:IsType(TYPE_PENDULUM) and c:IsSetCard(0x98)
end
function s.checkop(e, tp)
	local g1 = Duel.GetMatchingGroup(s.checkdeck, tp, LOCATION_DECK, 0, nil)
	if #g1 >= 9 then
		local c = e:GetOwner()
		c:SetEntityCode(id)
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(131191624)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetTargetRange(1, 0)
		Duel.RegisterEffect(e1, tp)
		c:SetEntityCode(111004001)
		e:SetLabel(100)
	end
end
function s.rvfilter(c)
	return c:IsFaceup() and c:IsCode(94415058, 20409757)
end
function s.thfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_PENDULUM) and not c:IsExtraDeckMonster()
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnCount() >= 3 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and e:GetLabelObject():GetLabel() == 100 and Duel.IsExistingMatchingCard(s.rvfilter, tp, LOCATION_PZONE, 0, 1, nil) and Duel.IsExistingMatchingCard(s.thfilter, tp, LOCATION_EXTRA, 0, 1, nil)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local tc = Duel.SelectMatchingCard(tp, s.thfilter, tp, LOCATION_EXTRA, 0, 1, 1, nil):GetFirst()
	local escape = 0
	while not tc:IsLocation(LOCATION_HAND) and escape < 3 do
		Duel.SendtoHand(tc, nil, REASON_RULE)
		escape = escape + 1
	end
	Duel.ConfirmCards(1 - tp, tc)
	local code = tc:GetCode()
	tc:RegisterFlagEffect(id, RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END, EFFECT_FLAG_SET_AVAILABLE + EFFECT_FLAG_IGNORE_IMMUNE, 2)
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SUMMON)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(s.sumlimit0)
	e1:SetLabel(code)
	e1:SetReset(RESET_PHASE + PHASE_END, 2)
	Duel.RegisterEffect(e1, tp)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e2:SetTarget(s.sumlimit1)
	Duel.RegisterEffect(e2, tp)
	local e3 = e1:Clone()
	e3:SetCode(EFFECT_CANNOT_FLIP_SUMMON)
	Duel.RegisterEffect(e3, tp)
	local e4 = e1:Clone()
	e4:SetCode(EFFECT_CANNOT_ACTIVATE)
	e4:SetValue(s.aclimit)
	Duel.RegisterEffect(e4, tp)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.sumlimit0(e, c, sump, sumtype, sumpos, targetp, se)
	return c:GetFlagEffect(id) > 0 or c:IsCode(e:GetLabel())
end
function s.sumlimit1(e, c, sump, sumtype, sumpos, targetp, se)
	return (c:GetFlagEffect(id) > 0 or c:IsCode(e:GetLabel())) and sumtype & SUMMON_TYPE_PENDULUM == 0
end
function s.aclimit(e, re, tp)
	return (re:GetHandler():GetFlagEffect(id) > 0 or re:GetHandler():IsCode(e:GetLabel())) and re:IsActiveType(TYPE_MONSTER)
end

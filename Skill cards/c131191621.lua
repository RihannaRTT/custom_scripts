--coded by Lyris
--Pendulum Shuffle
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsFaceup() and c:IsType(TYPE_PENDULUM)
end
function s.con(c)
	return function(e, tp)
		return not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetFieldGroupCount(tp, LOCATION_PZONE, 0) == 2 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_EXTRA, 0, 2, nil)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.SendtoExtraP(Duel.GetFieldGroup(tp, LOCATION_PZONE, 0), tp, REASON_RULE)
	for pc in aux.Next(Duel.GetMatchingGroup(s.filter, tp, LOCATION_EXTRA, 0, nil):RandomSelect(tp, 2)) do
		Duel.MoveToField(pc, tp, tp, LOCATION_PZONE, POS_FACEUP, true)
	end
end

--coded by Lyris
--Swing Into Action: Pendulum Summon!
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(id)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetTargetRange(1, 0)
		Duel.RegisterEffect(e1, tp)
	end
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_PENDULUM)
end
function s.filter(c, tp)
	return c:GetSequence() > 4 and Duel.CheckLocation(tp, LOCATION_MZONE, 1 + (c:GetSequence() - 5) * 2)
end
function s.con(e, tp)
	return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_EXTRA, 0, 1, nil) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, tp)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOZONE)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil, tp)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	Duel.MoveSequence(tc, 1 + (tc:GetSequence() - 5) * 2)
end

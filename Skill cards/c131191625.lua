--coded by Lyris
--Turning Up the Heat
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not (c:IsType(TYPE_PENDULUM) and c:IsSetCard(0x98) or c:IsSetCard(0x99, 0x9f))
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		if not Duel.IsExistingMatchingCard(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil) then
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_FIELD)
			e1:SetCode(131191624)
			e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
			e1:SetTargetRange(1, 0)
			Duel.RegisterEffect(e1, tp)
		end
		local codes = { 20409757, 94415058 }
		if not Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK + LOCATION_HAND, 0, 3, nil, 16178681) then
			table.insert(codes, 16178681)
		end
		for _, i in ipairs(codes) do
			Duel.SendtoDeck(Duel.CreateToken(tp, i), nil, SEQ_DECKSHUFFLE, REASON_RULE)
		end
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.Draw(tp, #hg, REASON_RULE)
		else
			Duel.ShuffleDeck(tp)
		end
	end
end

--coded by Lyris
--Draw Reserves
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetCurrentPhase() == PHASE_MAIN1 and not Duel.CheckPhaseActivity() and Duel.GetTurnCount(tp) == 1 and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 2
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 3, 3, nil), nil, SEQ_DECKSHUFFLE, REASON_RULE)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetOperation(s.con(c))
	e2:SetReset(RESET_PHASE + PHASE_DRAW + RESET_SELF_TURN)
	e2:SetCountLimit(1)
	Duel.RegisterEffect(e2, tp)
end
function s.con(c)
	return function(e, tp)
		if Duel.SelectEffectYesNo(tp, c) then
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_FIELD)
			e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
			e1:SetCode(EFFECT_DRAW_COUNT)
			e1:SetTargetRange(1, 0)
			e1:SetReset(RESET_PHASE + PHASE_DRAW)
			e1:SetValue(3)
			Duel.RegisterEffect(e1, tp)
		end
	end
end

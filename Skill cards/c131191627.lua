--coded by Lyris
--Duel Recital
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.con(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetTargetRange(LOCATION_MZONE, 0)
	e2:SetTarget(aux.TargetBoolFunction(Card.IsSetCard, 0x9b))
	e2:SetValue(s.val(c))
	c:RegisterEffect(e2)
end
function s.con(c)
	return function()
		c:SetEntityCode(id)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsSetCard(0x9b)
end
function s.val(c)
	return function()
		return Duel.GetMatchingGroupCount(s.filter, c:GetControler(), LOCATION_MZONE, 0, nil) * 100
	end
end

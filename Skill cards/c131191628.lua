--coded by Lyris
--From Songstress to Maestra
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x9b)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND + LOCATION_EXTRA, 0, nil))
end
function s.cfilter(c, tp)
	return c:IsFaceup() and c:IsOriginalSetCard(0x9b) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, nil, c:GetLevel()) and Duel.GetMZoneCount(tp, c) > 0
end
function s.filter(c, lv)
	return c:IsSetCard(0x109b) and c:GetOriginalLevel() > lv
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil, tp)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local g = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_MZONE, 0, 1, 1, nil, tp)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	Duel.SendtoGrave(g, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local sc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, 1, nil, tc:GetLevel()):GetFirst()
	Duel.MoveToField(sc, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	sc:CompleteProcedure()
	sc:SetStatus(STATUS_FORM_CHANGED, true)
end

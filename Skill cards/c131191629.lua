--coded by Lyris
--Fusioning Maestra
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		for _, i in ipairs { 62895219, 79531196 } do
			Duel.SendtoDeck(Duel.CreateToken(tp, i), nil, SEQ_DECKSHUFFLE, REASON_RULE)
		end
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.Draw(tp, #hg, REASON_RULE)
		else
			Duel.ShuffleDeck(tp)
		end
		Duel.BreakEffect()
		Duel.SendtoDeck(Duel.CreateToken(tp, 84988419), nil, SEQ_DECKTOP, REASON_RULE)
	end
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsCode(79531196)
end
function s.filter(c)
	return c:IsFaceup() and c:IsType(TYPE_FUSION)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, nil) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_REMOVED, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.SendtoDeck(Duel.GetMatchingGroup(s.filter, tp, LOCATION_REMOVED, 0, nil), nil, SEQ_DECKTOP, REASON_RULE)
end

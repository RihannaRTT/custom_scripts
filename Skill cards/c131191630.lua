--coded by Lyris
--Pendulum Extra Reset
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.filter(c)
	return c:IsFaceup() and c:IsType(TYPE_PENDULUM)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_EXTRA, 0, 1, nil)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_EXTRA, 0, nil)
	local TYPE_EXTRA = TYPE_FUSION + TYPE_SYNCHRO + TYPE_XYZ + TYPE_LINK
	local xg = g:Filter(Card.IsType, nil, TYPE_EXTRA)
	Duel.SendtoDeck(g:Clone() - xg, SEQ_DECKSHUFFLE, nil, REASON_RULE)
	for xc in aux.Next(xg) do
		Duel.Exile(xc, REASON_RULE)
		Duel.SendtoDeck(xc, SEQ_DECKSHUFFLE, nil, REASON_RULE)
	end
end

--coded by Lyris
--Pendulum Recovery
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_SPSUMMON_SUCCESS)
	e0:SetLabel(0)
	e0:SetOperation(s.reg)
	e0:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.reg(e, tp, eg)
	for _ in aux.Next(eg:Filter(Card.IsSummonType, nil, SUMMON_TYPE_PENDULUM)) do
		Duel.RegisterFlagEffect(tp, id, RESET_PHASE + PHASE_END, 0, 1)
	end
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetFlagEffect(tp, id) > 0
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Recover(tp, 500 * Duel.GetFlagEffect(tp, id), REASON_RULE)
end

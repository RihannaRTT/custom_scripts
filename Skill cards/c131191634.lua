--coded by XGlitchy30
--Benkei the Guardian
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.checkop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetLabelObject(e0)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.checkdeck(c)
	return c:IsType(TYPE_SPELL + TYPE_TRAP)
end
function s.checkop(e, tp)
	local g = Duel.GetMatchingGroup(s.checkdeck, tp, LOCATION_DECK, 0, nil)
	if #g == 0 then
		e:SetLabel(100)
	end
end
function s.tgfilter(c)
	return c:IsFaceup() and c:IsCode(3117804) and c:GetEquipGroup():IsExists(Card.IsSetCard, 1, nil, 0x109a)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and e:GetLabelObject():GetLabel() == 100 and Duel.IsExistingMatchingCard(s.tgfilter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.GetActivityCount(tp, ACTIVITY_ATTACK) == 0
end
function s.act(e, tp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.tgfilter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	local tc = g:GetFirst()
	Duel.HintSelection(g)
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_IGNORE_IMMUNE)
	e1:SetCondition(s.turncon)
	e1:SetValue(1)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END + RESET_OPPO_TURN)
	tc:RegisterEffect(e1)
	local e3 = Effect.CreateEffect(e:GetOwner())
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CANNOT_SELECT_BATTLE_TARGET)
	e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_IGNORE_IMMUNE)
	e3:SetRange(LOCATION_MZONE)
	e3:SetTargetRange(0, LOCATION_MZONE)
	e3:SetCondition(s.turncon)
	e3:SetValue(s.atlimit)
	e3:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END + RESET_OPPO_TURN)
	tc:RegisterEffect(e3)
	local g1 = Effect.CreateEffect(e:GetOwner())
	g1:SetType(EFFECT_TYPE_FIELD)
	g1:SetCode(EFFECT_CANNOT_ATTACK_ANNOUNCE)
	g1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	g1:SetTargetRange(1, 0)
	g1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(g1, tp)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.turncon(e, tp)
	return Duel.GetTurnPlayer() == 1 - tp
end
function s.atlimit(e, c)
	return c ~= e:GetOwner()
end

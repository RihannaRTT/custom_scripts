--coded by Lyris
--Heavystrong Strategy
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return e:GetLabel() < 2 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and not Duel.IsExistingMatchingCard(Card.IsType, tp, LOCATION_GRAVE, 0, 1, nil, TYPE_SPELL + TYPE_TRAP) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsAttribute(ATTRIBUTE_EARTH) and c:IsRace(RACE_MACHINE) and c:IsCanChangePosition()
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.ChangePosition(Duel.GetMatchingGroup(s.filter, tp, LOCATION_MZONE, 0, nil), POS_FACEUP_DEFENSE)
	Duel.BreakEffect()
	for tc in aux.Next(Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_MZONE, 0, 1, nil)) do
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_DEFENSE)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD)
		e1:SetValue(200 * Duel.GetMatchingGroupCount(Card.IsPosition, tp, LOCATION_MZONE, 0, nil, POS_FACEUP_DEFENSE))
		tc:RegisterEffect(e1, true)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end

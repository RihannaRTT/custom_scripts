--coded by Lyris
--Heavystrong Style
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		Duel.SendtoDeck(Duel.CreateToken(tp, 3117804), nil, SEQ_DECKSHUFFLE, REASON_RULE)
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.Draw(tp, #hg, REASON_RULE)
		else
			Duel.ShuffleDeck(tp)
		end
	end
end
function s.con(e, tp)
	local g = Duel.GetMatchingGroup(Card.IsDefensePos, tp, LOCATION_GRAVE, 0, nil)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and #g > 1 and g:Filter(Card.IsFaceup, nil):GetSum(Card.GetDefense) >= 3500 and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_GRAVE, 0, 1, nil, 3117804)
end
function s.filter(c)
	return c:IsFaceup() and c:IsDefenseAbove(3500)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local tc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_GRAVE, 0, 1, 1, nil, 3117804):GetFirst()
	Duel.SendtoHand(tc, nil, REASON_RULE)
	if not Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil) then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
		local dc = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil):GetFirst()
		Duel.BreakEffect()
		Duel.SendtoDeck(dc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	end
end

--coded by Lyris
--Style Evolution
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c, e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	if not s.global_check then
		s.global_check = true
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e1:SetCode(EVENT_SPSUMMON_SUCCESS)
		e1:SetOperation(s.count)
		Duel.RegisterEffect(e1, 0)
		local e2 = Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e2:SetCode(EVENT_PHASE_START + PHASE_END)
		e2:SetCountLimit(1)
		e2:SetOperation(s.reg(c))
		Duel.RegisterEffect(e2, 0)
	end
end
function s.count(e, tp, eg)
	for p = 0, 1 do
		if eg:IsExists(s.cfilter, 1, nil, p) then
			Duel.RegisterFlagEffect(p, id, RESET_PHASE + PHASE_END, 0, 1)
		end
	end
end
function s.reg(c)
	return function()
		local p = Duel.GetTurnPlayer()
		if Duel.GetFlagEffect(p, id) == 0 then
			Duel.RaiseSingleEvent(c, EVENT_CUSTOM + id, e2, 0, p, p, 0)
		end
	end
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(Card.IsType, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, TYPE_SPELL + TYPE_TRAP))
end
function s.filter(c)
	return c:IsLevelBelow(4) and c:IsSetCard(0x9a) and c:IsPosition(POS_FACEUP_DEFENSE) and not c:IsType(TYPE_TUNER)
end
function s.cfilter(c, tp)
	return c:IsSummonPlayer(tp) and c:IsSummonType(SUMMON_TYPE_SYNCHRO)
end
function s.con(c, ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and ef:GetLabel() == 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_CUSTOM + id)
	e1:SetCountLimit(1)
	e1:SetOperation(s.op)
	e1:SetReset(RESET_PHASE + PHASE_END)
	c:RegisterEffect(e1, true)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_ADD_TYPE)
	e0:SetReset(RESET_EVENT + RESETS_STANDARD)
	e0:SetValue(TYPE_TUNER)
	g:GetFirst():RegisterEffect(e0, true)
end
function s.op(e, tp)
	Duel.SendtoGrave(Duel.GetMatchingGroup(Card.IsDefensePos, tp, LOCATION_MZONE, 0, nil), REASON_RULE)
end

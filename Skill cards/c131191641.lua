--coded by Lyris
--Blowing Up Yosen
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.op(c))
	c:RegisterEffect(e0)
end
function s.cfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0xb3)
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(27918963)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_SZONE, 0, 1, nil)
end
function s.tfilter(c)
	return c:IsFaceup() and s.cfilter(c)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_COUNTER)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_SZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	g:GetFirst():AddCounter(0x33, Duel.GetMatchingGroupCount(s.tfilter, tp, LOCATION_MZONE, 0, nil))
end
function s.op(c)
	return function(e, tp)
		if Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, 9, nil) then
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_FIELD)
			e1:SetCode(131191624)
			e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
			e1:SetTargetRange(1, 0)
			Duel.RegisterEffect(e1, tp)
		end
	end
end

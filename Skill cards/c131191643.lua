--coded by Lyris
--Neo Sylvio
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(Card.IsSummonType, tp, LOCATION_MZONE, 0, 1, nil, SUMMON_TYPE_ADVANCE) and Duel.IsExistingMatchingCard(Card.IsType, tp, LOCATION_HAND, 0, 1, nil, TYPE_SPELL + TYPE_TRAP)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local tc = Duel.SelectMatchingCard(tp, Card.IsType, tp, LOCATION_HAND, 0, 1, 1, nil, TYPE_SPELL + TYPE_TRAP):GetFirst()
	Duel.ConfirmCards(1 - tp, tc)
	Duel.ShuffleHand(tp)
	local ac = 99940363
	if tc:IsType(TYPE_TRAP) then
		ac = 26822796
	end
	tc:SetEntityCode(ac)
	tc:ReplaceEffect(ac, 0)
	Duel.SetMetatable(tc, _G["c" .. ac])
	tc:ResetEffect(ac, RESET_CARD)
	tc:ResetEffect(ac, RESET_CARD)
	_G["c" .. ac].initial_effect(tc)
end

--coded by Lyris
--Pendulum Draft
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
end
function s.count(e, tp, eg, ep, ev, re, r, rp)
	local plp0, plp1 = e:GetLabel()
	local clp0, clp1 = Duel.GetLP(0), Duel.GetLP(1)
	e:SetLabel(clp0, clp1)
	if not plp0 or not plp1 then
		return
	end
	if plp0 > clp0 then
		s[0] = s[0] - clp0 + plp0
	end
	if plp1 > clp1 then
		s[1] = s[1] - clp1 + plp1
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsType(TYPE_PENDULUM) and not c:IsExtraDeckMonster()
end
function s.con(c)
	return function(e, tp)
		return s[tp] >= 2000 and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_EXTRA, 0, 1, nil)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	s[tp] = 0
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_EXTRA, 0, 1, 1, nil):GetFirst()
	Duel.SendtoHand(tc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, tc)
	local code = tc:GetCode()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SUMMON)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(aux.TargetBoolFunction(Card.IsCode, code))
	e1:SetLabel(code)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e2:SetTarget(s.sumlimit)
	Duel.RegisterEffect(e2, tp)
	local e3 = e1:Clone()
	e3:SetCode(EFFECT_CANNOT_MSET)
	Duel.RegisterEffect(e3, tp)
	local e4 = e1:Clone()
	e4:SetCode(EFFECT_CANNOT_ACTIVATE)
	e4:SetValue(s.aclimit)
	Duel.RegisterEffect(e4, tp)
end
function s.sumlimit(e, c, sp, st)
	return st & SUMMON_TYPE_PENDULUM ~= SUMMON_TYPE_PENDULUM or not c:IsSummonType(SUMMON_TYPE_PENDULUM)
end
function s.aclimit(e, re, tp)
	return re:GetHandler():IsCode(e:GetLabel()) and re:IsActiveType(TYPE_MONSTER)
end

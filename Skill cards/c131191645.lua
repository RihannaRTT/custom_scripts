--coded by Lyris
--Pendulum Extra Charge
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	Duel.AddCustomActivityCounter(id, ACTIVITY_SPSUMMON, aux.FilterBoolFunction(aux.NOT(Card.IsSummonType), SUMMON_TYPE_PENDULUM))
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetFieldGroupCount(tp, LOCATION_PZONE, 0) == 2 and (Duel.CheckLocation(tp, LOCATION_PZONE, 0) or Duel.CheckLocation(tp, LOCATION_PZONE, 1)) and Duel.GetCustomActivityCount(id, tp, ACTIVITY_SPSUMMON) == 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, aux.Stringid(id, 0))
	local tc = Duel.GetFieldGroup(tp, LOCATION_PZONE, 0):Select(tp, 1, 1, nil):GetFirst()
	Duel.SendtoExtraP(tc, tc:GetOwner(), REASON_RULE)
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e1:SetReset(RESET_PHASE + PHASE_END)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(s.splimit)
	Duel.RegisterEffect(e1, tp)
end
function s.splimit(e, c, sp, st, spos, tp, se)
	return st & SUMMON_TYPE_PENDULUM ~= SUMMON_TYPE_PENDULUM
end

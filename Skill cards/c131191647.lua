--coded by Lyris
--Sylvio's Showstopping Performance
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	e2:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e2)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
		local e4 = e5:Clone()
		Duel.RegisterEffect(e4, 1)
	end
end
function s.cfilter(c)
	return c:IsType(TYPE_MONSTER) and (c:IsSetCard(0xb3) and c:IsType(TYPE_PENDULUM) or c:IsSetCard(0x10ec))
end
function s.count(e, tp)
	local plp, clp = e:GetLabel(), Duel.GetLP(tp)
	e:SetLabel(clp)
	if plp <= clp then
		return
	end
	s[tp] = s[tp] - clp + plp
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0xb3, 0x10ec)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and Duel.GetDrawCount(tp) > 0 and s[tp] >= 1500 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil) and (not c:IsOriginalCodeRule(id) or c:GetFlagEffect(id) == 0)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	if c:IsOriginalCodeRule(id) and not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_DRAW_COUNT)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_DRAW)
	e1:SetValue(0)
	Duel.RegisterEffect(e1, tp)
	s[tp] = 0
	Duel.MoveSequence(Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil):GetFirst(), SEQ_DECKTOP)
	Duel.Draw(tp, 1, REASON_RULE)
	c:RegisterFlagEffect(id, 0, 0, 1)
end
function s.flip(c)
	return function(e, tp)
		if Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, 9, nil) then
			c:SetEntityCode(id)
			Duel.Hint(HINT_CARD, 0, id)
			local e2 = Effect.CreateEffect(c)
			e2:SetType(EFFECT_TYPE_FIELD)
			e2:SetCode(131191624)
			e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
			e2:SetTargetRange(1, 0)
			Duel.RegisterEffect(e2, tp)
		end
	end
end

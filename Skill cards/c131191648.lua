--coded by Lyris
--Waiting for Abyss Actors
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.flip(c)
	return function(e, tp)
		if Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, 9, nil) then
			c:SetEntityCode(id)
			Duel.Hint(HINT_CARD, 0, id)
			local e2 = Effect.CreateEffect(c)
			e2:SetType(EFFECT_TYPE_FIELD)
			e2:SetCode(131191624)
			e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
			e2:SetTargetRange(1, 0)
			Duel.RegisterEffect(e2, tp)
		end
	end
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x10ec)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and e:GetLabel() ~= 1 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, aux.Stringid(id, 0))
	Duel.SendtoExtraP(Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND, 0, 1, 2, nil), nil, REASON_RULE)
	e:SetLabel(1)
end

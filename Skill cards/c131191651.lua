--coded by Lyris
--Black and White Contract
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetCondition(s.con(c, e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsSetCard(0xaf)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND + LOCATION_EXTRA, 0, nil))
end
function s.filter(c, set)
	return c:IsFaceup() and c:IsSetCard(set)
end
function s.con(c, ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and e:GetLabel() < 2 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_ONFIELD, 0, 1, nil, 0xae) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, 0xaf) and ef:GetLabel() == 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local cc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_ONFIELD, 0, 1, 1, nil, 0xae):GetFirst()
	Duel.SendtoGrave(cc, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil, 0xaf)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	local off = 1
	local ops = {  }
	local opval = {  }
	if not tc:IsType(TYPE_TUNER) then
		ops[off] = 1063
		opval[off - 1] = 1
		off = off + 1
	end
	if tc:IsLevelAbove(1) and not tc:IsLevel(4) then
		ops[off] = aux.Stringid(id, 0)
		opval[off - 1] = 2
		off = off + 1
	end
	if tc:IsLevelAbove(1) and not tc:IsLevel(8) then
		ops[off] = aux.Stringid(id, 1)
		opval[off - 1] = 3
		off = off + 1
	end
	local op = Duel.SelectOption(tp, table.unpack(ops))
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_SINGLE)
	if opval[op] == 1 then
		e1:SetCode(EFFECT_ADD_TYPE)
		e1:SetValue(TYPE_TUNER)
	else
		e1:SetCode(EFFECT_CHANGE_LEVEL)
		if opval[op] == 2 then
			e1:SetValue(4)
		else
			e1:SetValue(8)
		end
	end
	e1:SetReset(RESET_EVENT + RESETS_STANDARD)
	tc:RegisterEffect(e1, true)
	e:SetLabel(e:GetLabel() + 1)
end

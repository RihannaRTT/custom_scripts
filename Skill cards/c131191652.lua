--coded by Lyris
--Command of the Doom King
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.cfilter(c)
	return c:IsType(TYPE_PENDULUM) and c:IsSetCard(0x10af)
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		local g = Duel.GetMatchingGroup(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil)
		if #g > 2 then
			local e2 = Effect.CreateEffect(c)
			e2:SetType(EFFECT_TYPE_FIELD)
			e2:SetCode(131191624)
			e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
			e2:SetTargetRange(1, 0)
			Duel.RegisterEffect(e2, tp)
		end
	end
end
function s.filter(c)
	return c:IsType(TYPE_PENDULUM) and c:IsSetCard(0xaf) and not c:IsLocation(LOCATION_PZONE) and (not c:IsLocation(LOCATION_SZONE) or c:GetSequence() > 0 and c:GetSequence() < 4)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_HAND, 0, 1, nil, 47198668) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK + LOCATION_ONFIELD, 0, 1, nil) and (Duel.CheckLocation(tp, LOCATION_PZONE, 0) or Duel.CheckLocation(tp, LOCATION_PZONE, 1))
end
function s.chk(g)
	return g:GetClassCount(Card.GetLocation) == #g
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, aux.Stringid(id, 0))
	local tc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_HAND, 0, 1, 1, nil, 47198668):GetFirst()
	Duel.SendtoExtraP(tc, tp, REASON_RULE)
	local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK + LOCATION_ONFIELD, 0, nil)
	local ct = 0
	if Duel.CheckLocation(tp, LOCATION_PZONE, 0) then
		ct = ct + 1
	end
	if Duel.CheckLocation(tp, LOCATION_PZONE, 1) then
		ct = ct + 1
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	for sc in aux.Next(g:SelectSubGroup(tp, s.chk, false, 1, ct) or Group.CreateGroup()) do
		Duel.MoveToField(sc, tp, tp, LOCATION_PZONE, POS_FACEUP, true)
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_DISABLE)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD)
		sc:RegisterEffect(e1, true)
		local e3 = Effect.CreateEffect(c)
		e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e3:SetCode(EVENT_PHASE_START + PHASE_DRAW)
		e3:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
		e3:SetCountLimit(1)
		e3:SetOperation(s.op)
		e3:SetReset(RESET_PHASE + PHASE_DRAW + RESET_SELF_TURN)
		Duel.RegisterEffect(e3, tp)
	end
end
function s.op(e, tp)
	if Duel.GetTurnPlayer() == tp then
		Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_PZONE, 0), nil, SEQ_DECKSHUFFLE, REASON_RULE)
	end
end

--coded by Lyris
--Contract Procrastination
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
	e2:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	e2:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e2)
end
function s.filter(c)
	return c:IsFaceup() and c:IsSetCard(0xae)
end
function s.con(c)
	return function(e, tp)
		return not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_ONFIELD, 0, 1, nil)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	c:SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetCode(EFFECT_SKIP_SP)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
end

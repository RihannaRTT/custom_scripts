--coded by Lyris
--Emergency Contract Laundering
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c, tp)
	return (c:IsCode(46259438) or c:IsSetCard(0xae)) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, c:IsFacedown() and c or nil)
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsSetCard(0xae)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, 0xef, 0, 1, nil, tp)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, 0xef, 0, 1, 1, nil, tp)
	local tc = g:GetFirst()
	if tc:IsFaceup() or tc:IsLocation(LOCATION_GRAVE) then
		Duel.HintSelection(g)
	else
		Duel.ConfirmCards(1 - tp, g)
	end
	Duel.SendtoGrave(g, REASON_RULE)
	local tg, ct = Duel.GetMatchingGroup(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, nil), 0
	for i = 0, 3 do
		ct = ct + Duel.Destroy(tg, REASON_RULE)
		if not tg:IsExists(Card.IsOnField, 1, nil) then
			break
		end
	end
	Duel.BreakEffect()
	Duel.Recover(tp, ct * 1000, REASON_RULE)
end

--coded by XGlitchy30
--End of the World
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.pendop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(id, 1))
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCondition(s.dstcon)
	e2:SetOperation(s.dstact)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		local g1 = Effect.CreateEffect(c)
		g1:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
		g1:SetCode(EVENT_SPSUMMON_SUCCESS)
		g1:SetOperation(s.registersum)
		Duel.RegisterEffect(g1, 0)
	end
end
function s.pfilter(c, tp)
	return c:IsSummonPlayer(tp) and c:IsSummonType(SUMMON_TYPE_PENDULUM) and c:GetSummonLocation() & LOCATION_HAND > 0 and c:IsFaceup() and c:IsCode(47198668)
end
function s.registersum(e, tp, eg, ep, ev, re, r, rp)
	for p = 0, 1 do
		if eg:IsExists(s.pfilter, 3, nil, p) then
			Duel.RegisterFlagEffect(p, id, RESET_PHASE + PHASE_END, 0, 1)
		end
	end
end
function s.checkdeck(c)
	return c:IsSetCard(0x10af) and c:IsType(TYPE_MONSTER) or c:IsSetCard(0xae) and c:IsType(TYPE_SPELL + TYPE_TRAP)
end
function s.pendop(e, tp)
	if Duel.IsExistingMatchingCard(s.checkdeck, tp, LOCATION_DECK + LOCATION_EXTRA, 0, 12, nil) then
		e:GetOwner():SetEntityCode(id)
		Duel.Hint(HINT_CARD, 0, id)
		local e1 = Effect.CreateEffect(e:GetOwner())
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(131191624)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetTargetRange(1, 0)
		Duel.RegisterEffect(e1, tp)
	end
	e:GetOwner():SetEntityCode(111004001)
end
function s.rvfilter(c, tp)
	return c:IsCode(47198668) and not c:IsPublic() and Duel.IsExistingMatchingCard(s.thfilter, tp, LOCATION_EXTRA, 0, 1, nil)
end
function s.thfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_PENDULUM) and c:IsSetCard(0x10af)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.rvfilter, tp, LOCATION_HAND, 0, 1, nil, tp)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_OPSELECTED, 0, e:GetDescription())
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local g = Duel.SelectMatchingCard(tp, s.rvfilter, tp, LOCATION_HAND, 0, 1, 1, nil, tp)
	Duel.ConfirmCards(1 - tp, g)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local sg = Duel.SelectMatchingCard(tp, s.thfilter, tp, LOCATION_EXTRA, 0, 1, 1, nil)
	local tc = sg:GetFirst()
	local escape = 0
	while sg:IsExists(aux.NOT(Card.IsLocation), 1, nil, LOCATION_HAND) and escape < 3 do
		Duel.SendtoHand(sg:Filter(aux.NOT(Card.IsLocation), nil, LOCATION_HAND), nil, REASON_RULE)
		escape = escape + 1
	end
	Duel.ConfirmCards(1 - tp, sg)
	local ct = (Duel.GetTurnPlayer() == tp) and 2 or 1
	tc:RegisterFlagEffect(id, RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END + RESET_SELF_TURN, EFFECT_FLAG_SET_AVAILABLE + EFFECT_FLAG_IGNORE_IMMUNE, ct)
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SUMMON)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(s.sumlimit0)
	e1:SetLabel(tc:GetCode())
	e1:SetReset(RESET_PHASE + PHASE_END + RESET_SELF_TURN, ct)
	Duel.RegisterEffect(e1, tp)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e2:SetTarget(s.sumlimit1)
	Duel.RegisterEffect(e2, tp)
	local e3 = e1:Clone()
	e3:SetCode(EFFECT_CANNOT_FLIP_SUMMON)
	Duel.RegisterEffect(e3, tp)
	local e4 = e1:Clone()
	e4:SetCode(EFFECT_CANNOT_ACTIVATE)
	e4:SetValue(s.aclimit)
	Duel.RegisterEffect(e4, tp)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.sumlimit0(e, c, sump, sumtype, sumpos, targetp, se)
	return c:GetFlagEffect(id) > 0 or c:IsCode(e:GetLabel())
end
function s.sumlimit1(e, c, sump, sumtype, sumpos, targetp, se)
	return (c:GetFlagEffect(id) > 0 or c:IsCode(e:GetLabel())) and sumtype & SUMMON_TYPE_PENDULUM == 0
end
function s.aclimit(e, re, tp)
	return (re:GetHandler():GetFlagEffect(id) > 0 or re:GetHandler():IsCode(e:GetLabel())) and re:IsActiveType(TYPE_MONSTER)
end
function s.spare(c)
	return c:IsFacedown() or not c:IsCode(47198668)
end
function s.dstcon(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetFlagEffect(tp, id) > 0 and Duel.IsExistingMatchingCard(s.spare, tp, LOCATION_MZONE, LOCATION_MZONE, 1, nil) and e:GetOwner():GetFlagEffect(id) <= 0
end
function s.dstact(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_OPSELECTED, 0, e:GetDescription())
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetMatchingGroup(s.spare, tp, LOCATION_MZONE, LOCATION_MZONE, nil)
	if #g > 0 then
		Duel.Destroy(g, REASON_EFFECT)
	end
	e:GetOwner():SetEntityCode(111004001)
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, EFFECT_FLAG_SET_AVAILABLE + EFFECT_FLAG_IGNORE_IMMUNE, 1)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

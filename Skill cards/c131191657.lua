--coded by XGlitchy30
--King of the Underworld
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.checkop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.checkdeck(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x10af) or c:IsType(TYPE_SPELL + TYPE_TRAP) and c:IsSetCard(0xae)
end
function s.checkop(e, tp)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	local tk = Duel.CreateToken(tp, 47198668)
	Duel.SendtoDeck(tk, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	if Duel.GetTurnCount() > 1 then
		local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
		local escape = 0
		while hg:IsExists(aux.NOT(Card.IsLocation), 1, nil, LOCATION_DECK) and escape < 3 do
			Duel.SendtoDeck(hg:Filter(aux.NOT(Card.IsLocation), nil, LOCATION_DECK), nil, SEQ_DECKSHUFFLE, REASON_RULE)
			escape = escape + 1
		end
		Duel.ShuffleDeck(tp)
		Duel.Draw(tp, #hg, REASON_RULE)
	end
	local g1 = Duel.GetMatchingGroup(s.checkdeck, tp, LOCATION_DECK + LOCATION_EXTRA, 0, nil)
	if #g1 >= 12 then
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetCode(131191624)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetTargetRange(1, 0)
		Duel.RegisterEffect(e1, tp)
	end
	c:SetEntityCode(111004001)
end
function s.rvfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_PENDULUM) and c:IsSetCard(0xaf)
end
function s.thfilter(c)
	return c:IsCode(47198668) and (c:IsFaceup() or not c:IsLocation(LOCATION_EXTRA))
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.thfilter, tp, LOCATION_DECK + LOCATION_EXTRA, 0, 1, nil) and Duel.IsExistingMatchingCard(s.rvfilter, tp, LOCATION_PZONE, 0, 2, nil)
end
function s.act(e, tp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local tc = Duel.SelectMatchingCard(tp, s.thfilter, tp, LOCATION_DECK + LOCATION_EXTRA, 0, 1, 1, nil):GetFirst()
	local escape = 0
	while not tc:IsLocation(LOCATION_HAND) and escape < 3 do
		Duel.SendtoHand(tc, nil, REASON_RULE)
		escape = escape + 1
	end
	escape = 0
	Duel.ConfirmCards(1 - tp, Group.FromCards(tc))
	Duel.ShuffleHand(tp)
	Duel.BreakEffect()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc2 = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil):GetFirst()
	while not tc2:IsLocation(LOCATION_DECK) and escape < 3 do
		Duel.SendtoDeck(tc2, nil, SEQ_DECKSHUFFLE, REASON_RULE)
		escape = escape + 1
	end
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

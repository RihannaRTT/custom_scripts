--coded by Lyris
--Return from Another Dimension
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c, e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	Duel.AddCustomActivityCounter(id, ACTIVITY_CHAIN, s.cfilter)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsSetCard(0xaf)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND + LOCATION_EXTRA, 0, nil))
end
function s.cfilter(re, tp, cid)
	return not (re:IsActiveType(TYPE_SPELL + TYPE_TRAP) and re:IsHasType(EFFECT_TYPE_ACTIVATE))
end
function s.filter(c)
	return c:IsFaceup() and c:IsType(TYPE_MONSTER) and c:IsSetCard(0xaf)
end
function s.con(c, ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and ef:GetLabel() == 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_REMOVED, 0, 20, nil) and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and Duel.GetCustomActivityCount(id, tp, ACTIVITY_CHAIN) == 0
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_ACTIVATE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(aux.TargetBoolFunction(aux.NOT(s.cfilter)))
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	Duel.MoveToField(Duel.CreateToken(tp, 27174286), tp, tp, LOCATION_SZONE, POS_FACEDOWN, true)
end

--coded by Lyris
--Reverse of the Underworld and D.D.
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0xaf) and (c:IsFaceup() or c:IsLocation(LOCATION_GRAVE))
end
function s.con(c)
	return function(e, tp)
		local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_GRAVE + LOCATION_REMOVED, 0, nil)
		return not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and g:FilterCount(Card.IsLocation, nil, LOCATION_GRAVE) == g:FilterCount(Card.IsLocation, nil, LOCATION_REMOVED)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local g1 = Duel.GetMatchingGroup(s.filter, tp, LOCATION_GRAVE + LOCATION_REMOVED, 0, nil)
	local g2 = g1:Filter(Card.IsLocation, nil, LOCATION_REMOVED)
	g1:Sub(g2)
	Duel.Remove(g1, POS_FACEUP, REASON_RULE)
	Duel.SendtoGrave(g2, POS_FACEUP, REASON_RULE)
end

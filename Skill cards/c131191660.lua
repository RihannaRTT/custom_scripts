--coded by Lyris
--Clock of Destiny
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c, e0))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_ACTIVATE)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetCondition(s.ccon(c, e0))
	e3:SetOperation(s.cact)
	c:RegisterEffect(e3)
end
function s.cfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0xc008)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsSetCard(0x3008, 0xc008)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil), Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil))
end
function s.sfilter(c)
	return c:IsLevelBelow(4) and c:IsSetCard(0xc008)
end
function s.con(c, ef)
	return function(e, tp)
		local ct, xt = ef:GetLabel()
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and ct > 7 and xt == 0 and Duel.IsExistingMatchingCard(s.sfilter, tp, LOCATION_HAND, 0, 1, nil) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK, 0, 1, nil, 75041269) and (c:GetFlagEffect(id) == 0 or c:GetFlagEffectLabel(id) & 0x1 == 0)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local cc = Duel.SelectMatchingCard(tp, s.sfilter, tp, LOCATION_HAND, 0, 1, 1, nil):GetFirst()
	Duel.SendtoGrave(cc, REASON_RULE)
	Duel.BreakEffect()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local tc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK, 0, 1, 1, nil, 75041269):GetFirst()
	Duel.SendtoHand(tc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, tc)
	local ct = c:GetFlagEffectLabel(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 0x1)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 0x1)
	end
end
function s.ccon(c, ef)
	return function(e, tp)
		local tc, ct, xt = Duel.GetFieldCard(tp, LOCATION_FZONE, 0), ef:GetLabel()
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and ct > 7 and xt == 0 and tc and tc:IsFaceup() and tc:IsCode(75041269) and (c:GetFlagEffect(id) == 0 or c:GetFlagEffectLabel(id) & 0x2 == 0)
	end
end
function s.cact(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_COUNTER)
	local tc = Duel.GetFieldCard(tp, LOCATION_FZONE, 0)
	Duel.HintSelection(Group.FromCards(tc))
	tc:AddCounter(0x1b, 3)
	local tid = Duel.GetTurnCount()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE + PHASE_END)
	e1:SetRange(LOCATION_FZONE)
	e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e1:SetCountLimit(1)
	e1:SetOperation(s.tdop(tid))
	e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END + RESET_SELF_TURN, 2)
	tc:RegisterEffect(e1, true)
	local g = Duel.GetMatchingGroup(Card.IsCode, tp, LOCATION_DECK, 0, nil, 5318639)
	if #g > 0 and Duel.SelectYesNo(tp, 1190) then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local sc = g:Select(tp, 1, 1, nil):GetFirst()
		Duel.SendtoHand(sc, nil, REASON_RULE)
		Duel.ConfirmCards(1 - tp, sc)
	end
	local ct = c:GetFlagEffectLabel(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 0x2)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 0x2)
	end
end
function s.tdop(tid)
	return function(e, tp)
		if Duel.GetTurnPlayer() == tp and Duel.GetTurnCount() ~= tid then
			Duel.SendtoDeck(e:GetHandler(), nil, SEQ_DECKSHUFFLE, REASON_RULE)
		end
	end
end

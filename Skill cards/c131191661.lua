--coded by Lyris
--Dueltaining Stage Change
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.cfilter(c)
	return c:IsType(TYPE_PENDULUM) and c:IsSetCard(0x9f, 0x98, 0x99)
end
function s.filter(c)
	return c:IsFaceup() and c:IsType(TYPE_PENDULUM) and c:IsOriginalSetCard(0x9f, 0x98, 0x99)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetLP(tp) <= 2000 and Duel.GetMatchingGroupCount(s.cfilter, tp, LOCATION_PZONE, 0, nil) == 2
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.GetFieldGroup(tp, LOCATION_PZONE, 0):Select(tp, 1, 1, nil):GetFirst()
	Duel.SendtoExtraP(tc, nil, REASON_RULE)
	local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_MZONE, 0, nil)
	if #g > 0 and (Duel.CheckLocation(tp, LOCATION_PZONE, 0) or Duel.CheckLocation(tp, LOCATION_PZONE, 1)) and Duel.SelectEffectYesNo(tp, c) then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
		Duel.MoveToField(g:Select(tp, 1, 1, nil):GetFirst(), tp, tp, LOCATION_PZONE, POS_FACEUP, true)
	end
end

--coded by Lyris
--Pendulum Booster
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetTargetRange(LOCATION_MZONE, 0)
	e1:SetCondition(s.con)
	e1:SetValue(s.val(c))
	c:RegisterEffect(e1)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
	end
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and Duel.GetFieldGroup(tp, LOCATION_PZONE, 0) == 2
end
function s.filter(c)
	return c:GetOriginalType() & TYPE_MONSTER > 0
end
function s.val(c)
	return function()
		local _, hs = Duel.GetMatchingGroup(s.filter, c:GetControler(), LOCATION_PZONE, 0, nil):GetMaxGroup(Card.GetCurrentScale)
		return 50 * hs
	end
end

--coded by XGlitchy30
--Master of Rites: Super Soldier
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.checkop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetLabelObject(e0)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.checkdeck(c)
	return c:IsType(TYPE_RITUAL) and c:IsType(TYPE_MONSTER) and c:IsSetCard(0x10cf) or c:IsCode(29904964, 6628343, 38695361, 32013448, 79234734)
end
function s.checkop(e, tp)
	local g = Duel.GetMatchingGroup(s.checkdeck, tp, LOCATION_DECK, 0, nil)
	if #g >= 6 and g:GetClassCount(Card.GetOriginalCode) >= 4 then
		e:SetLabel(100)
	end
end
function s.tgfilter(c)
	return c:IsType(TYPE_RITUAL) and c:IsType(TYPE_MONSTER)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and e:GetLabelObject():GetLabel() == 100 and Duel.GetTurnCount() > 1 and Duel.IsExistingMatchingCard(s.tgfilter, tp, LOCATION_HAND, 0, 1, nil)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local g = Duel.SelectMatchingCard(tp, s.tgfilter, tp, LOCATION_HAND, 0, 1, 1, nil)
	local escape = 0
	while g:IsExists(aux.NOT(Card.IsLocation), 1, nil, LOCATION_GRAVE) and escape < 3 do
		Duel.SendtoGrave(g:Filter(aux.NOT(Card.IsLocation), nil, LOCATION_GRAVE), REASON_RULE)
		escape = escape + 1
	end
	local og1 = Duel.GetOperatedGroup()
	local th = Duel.CreateToken(tp, 45948430)
	if Duel.SendtoHand(th, nil, REASON_RULE) > 0 and th:IsLocation(LOCATION_HAND) then
		Duel.ConfirmCards(1 - tp, Group.FromCards(th))
		Duel.ShuffleHand(tp)
	end
	if og1:IsExists(Card.IsCode, 1, nil, 5405694) and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and Duel.SelectYesNo(tp, aux.Stringid(id, 0)) then
		local tf = Duel.CreateToken(tp, 73694478)
		Duel.MoveToField(tf, tp, tp, LOCATION_SZONE, POS_FACEDOWN, true)
		if tf:IsOnField() and tf:IsFacedown() then
			Duel.ConfirmCards(1 - tp, Group.FromCards(tf))
		end
	end
	local e0 = Effect.CreateEffect(e:GetOwner())
	e0:SetType(EFFECT_TYPE_FIELD)
	e0:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e0:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e0:SetTargetRange(1, 0)
	e0:SetTarget(s.sumlimit)
	e0:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e0, tp)
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_CANNOT_ACTIVATE)
	e1:SetTargetRange(1, 0)
	e1:SetCondition(s.actcon)
	e1:SetValue(s.actlimit)
	e1:SetReset(RESET_PHASE + Duel.GetCurrentPhase())
	Duel.RegisterEffect(e1, tp)
	local e2 = Effect.CreateEffect(e:GetOwner())
	e2:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
	e2:SetCode(EVENT_CHAINING)
	e2:SetOperation(s.aclimit1)
	e2:SetReset(RESET_PHASE + Duel.GetCurrentPhase())
	Duel.RegisterEffect(e2, tp)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.sumlimit(e, c)
	return not c:IsSetCard(0x10cf)
end
function s.actcon(e)
	return Duel.GetFlagEffect(e:GetOwnerPlayer(), id) ~= 0
end
function s.actlimit(e, re, tp)
	return re:IsActiveType(TYPE_MONSTER)
end
function s.aclimit1(e, tp, eg, ep, ev, re, r, rp)
	local tp = e:GetHandlerPlayer()
	if ep ~= tp or not re:IsActiveType(TYPE_MONSTER) then
		return
	end
	Duel.RegisterFlagEffect(tp, id, RESET_PHASE + Duel.GetCurrentPhase(), 0, 1)
end

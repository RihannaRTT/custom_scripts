--coded by Lyris
--Orichalcum Chain
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c, tp)
	return c:IsFaceup() and c:IsSetCard(0x70) and c:IsLevelAbove(1) and Duel.IsExistingMatchingCard(s.sfilter, tp, LOCATION_EXTRA, 0, 1, nil, c:GetLevel())
end
function s.sfilter(c, lv)
	return c:IsSetCard(0x70) and c:IsType(TYPE_XYZ) and c:IsRank(lv)
end
function s.con(c)
	return function(e, tp)
		return not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, tp) and Duel.GetActivityCount(tp, ACTIVITY_SUMMON) + Duel.GetActivityCount(tp, ACTIVITY_FLIPSUMMON) + Duel.GetActivityCount(tp, ACTIVITY_SPSUMMON) == 0
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SUMMON)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_OATH)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	Duel.RegisterEffect(e2, tp)
	local e3 = e1:Clone()
	e3:SetCode(EFFECT_CANNOT_FLIP_SUMMON)
	Duel.RegisterEffect(e3, tp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil, tp):GetFirst()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local sc = Duel.SelectMatchingCard(tp, s.sfilter, tp, LOCATION_EXTRA, 0, 1, 1, nil, tc:GetLevel()):GetFirst()
	Duel.Overlay(sc, tc)
	Duel.MoveToField(sc, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	tc:CompleteProcedure()
	tc:SetStatus(STATUS_FORM_CHANGED, true)
end

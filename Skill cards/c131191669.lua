--coded by XGlitchy30
--Level Cannon / Lock-On Laser
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.checkop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetLabelObject(e0)
	e1:SetCondition(s.con1)
	e1:SetOperation(s.act1)
	c:RegisterEffect(e1)
	local e2 = e1:Clone()
	e2:SetDescription(aux.Stringid(id, 1))
	e2:SetLabelObject(e0)
	e2:SetCondition(s.con2)
	e2:SetOperation(s.act2)
	c:RegisterEffect(e2)
end
function s.checkdeck(c)
	return c:IsType(TYPE_MONSTER) and not c:IsSetCard(0x13)
end
function s.checkop(e, tp)
	local g1 = Duel.GetMatchingGroup(s.checkdeck, tp, LOCATION_DECK, 0, nil)
	if #g1 == 0 then
		e:SetLabel(100)
	end
end
function s.fieldfilter(c)
	return c:IsFaceup() and c:IsCode(86997073)
end
function s.synfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_SYNCHRO) and c:GetLevel() > 0
end
function s.sharedcon(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and e:GetLabelObject():GetLabel() == 100 and Duel.IsExistingMatchingCard(s.fieldfilter, tp, LOCATION_FZONE, 0, 1, nil) and Duel.GetLP(1 - tp) >= 3000
end
function s.con1(e, tp)
	return s.sharedcon(e, tp) and Duel.IsExistingMatchingCard(s.synfilter, tp, 0, LOCATION_MZONE, 1, nil)
end
function s.act1(e, tp)
	Duel.Hint(HINT_OPSELECTED, 0, e:GetDescription())
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.synfilter, tp, 0, LOCATION_MZONE, 1, 1, nil)
	Duel.HintSelection(g)
	local res = Duel.GetLP(1 - tp) - g:GetFirst():GetLevel() * 100
	if res < 0 then
		res = 0
	end
	Duel.SetLP(1 - tp, res)
	e:GetOwner():SetEntityCode(111004001)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.stfilter(c)
	return c:IsType(TYPE_SPELL + TYPE_TRAP) and (c:IsFaceup() or not c:IsLocation(LOCATION_MZONE))
end
function s.con2(e, tp)
	return s.sharedcon(e, tp) and Duel.IsExistingMatchingCard(s.stfilter, tp, 0, LOCATION_ONFIELD, 1, nil)
end
function s.act2(e, tp)
	Duel.Hint(HINT_OPSELECTED, 0, e:GetDescription())
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetMatchingGroup(s.stfilter, tp, 0, LOCATION_ONFIELD, nil)
	local res = Duel.GetLP(1 - tp) - g:GetCount() * 100
	if res < 0 then
		res = 0
	end
	Duel.SetLP(1 - tp, res)
	e:GetOwner():SetEntityCode(111004001)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

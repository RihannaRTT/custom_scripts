--coded by Lyris
--Meklord Army Regeneration
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_SPSUMMON_SUCCESS)
	e0:SetRange(LOCATION_REMOVED)
	e0:SetCountLimit(1)
	e0:SetCondition(s.regcon)
	e0:SetOperation(s.reg)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.cfilter(c, tp)
	return c:IsFaceup() and c:IsCode(63468625) and c:IsSummonLocation(LOCATION_HAND) and c:IsSummonPlayer(tp)
end
function s.regcon(e, tp, eg)
	return eg:IsExists(s.cfilter, 1, nil, tp)
end
function s.reg(e, tp, eg)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 3, eg:GetFirst():GetTurnID())
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x6013)
end
function s.con(c)
	return function(e, tp)
		if not (Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetMatchingGroup(s.filter, tp, LOCATION_GRAVE, 0, nil):GetClassCount(Card.GetCode) > 1) then
			return false
		end
		for _, tid in ipairs { c:GetFlagEffectLabel(id) } do
			if tid < Duel.GetTurnCount() then
				return true
			end
		end
		return false
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_RTOHAND)
	local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_GRAVE, 0, nil):SelectSubGroup(tp, aux.dncheck, false, 2, 2) or Group.CreateGroup()
	Duel.SendtoHand(g, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, g)
	for _, ef in ipairs { c:IsHasEffect(0x10000000 + id) } do
		if ef:GetLabel() < Duel.GetTurnCount() then
			ef:Reset()
		end
	end
end

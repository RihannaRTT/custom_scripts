--coded by Lyris
--Meklord Astro Genesis
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_SPSUMMON_SUCCESS)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCountLimit(1)
	e1:SetCondition(s.regcon)
	e1:SetOperation(s.reg)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsSetCard(0x13)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil))
end
function s.rfilter(c)
	return c:IsFaceup() and c:IsSetCard(0x3013)
end
function s.regcon(e, tp, eg)
	return eg:IsExists(s.rfilter, 1, nil)
end
function s.reg(e, tp, eg)
	for tc in aux.Next(eg) do
		Duel.RegisterFlagEffect(tc:GetSummonPlayer(), id, 0, 0, 1)
	end
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetFlagEffect(tp, id) > 2 and Duel.GetMatchingGroup(Card.IsCode, tp, LOCATION_GRAVE, 0, nil, 4545683, 31930787, 68140974):CheckSubGroup(aux.dncheck, 3, 3) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil, 63468625)
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_REMOVE)
	local g = Duel.GetMatchingGroup(Card.IsCode, tp, LOCATION_GRAVE, 0, nil, 4545683, 31930787, 68140974):SelectSubGroup(tp, aux.dncheck, false, 3, 3) or Group.CreateGroup()
	Duel.Remove(g, POS_FACEUP, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local sc = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, 1, nil, 63468625):GetFirst()
	Duel.MoveToField(sc, tp, tp, LOCATION_MZONE, POS_FACEUP_ATTACK, true)
	sc:SetStatus(STATUS_FORM_CHANGED, true)
	sc:CompleteProcedure()
end

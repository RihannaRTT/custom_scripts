--coded by XGlitchy30
--Meklord Convert
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.checkop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetLabelObject(e0)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(id, 1))
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetLabelObject(e0)
	e2:SetCondition(s.con2)
	e2:SetOperation(s.act2)
	c:RegisterEffect(e2)
end
function s.checkexc(c)
	return c:IsType(TYPE_MONSTER) and not c:IsSetCard(0x13)
end
function s.checkop(e, tp)
	local g1 = Duel.GetMatchingGroup(Card.IsType, tp, LOCATION_DECK, 0, nil, TYPE_MONSTER)
	if #g1 >= 12 and not Duel.IsExistingMatchingCard(s.checkexc, tp, LOCATION_DECK, 0, 1, nil) then
		e:SetLabel(100)
	end
end
function s.rvfilter(c)
	return c:IsCode(63468625) and not c:IsPublic()
end
function s.thfilter(c)
	return c:IsFaceup() and c:IsSetCard(0x13)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and e:GetLabelObject():GetLabel() == 100 and Duel.IsExistingMatchingCard(s.rvfilter, tp, LOCATION_HAND, 0, 1, nil) and Duel.IsExistingMatchingCard(s.thfilter, tp, LOCATION_MZONE, 0, 2, nil) and e:GetOwner():GetFlagEffect(id) <= 0 and (Duel.GetFlagEffect(tp, id) <= 0 or Duel.GetFlagEffectLabel(tp, id) & 0x1 == 0)
end
function s.act(e, tp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local g = Duel.SelectMatchingCard(tp, s.rvfilter, tp, LOCATION_HAND, 0, 1, 1, nil)
	if #g > 0 then
		Duel.ConfirmCards(1 - tp, g)
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local sg = Duel.SelectMatchingCard(tp, s.thfilter, tp, LOCATION_MZONE, 0, 2, 2, nil)
		local escape = 0
		while sg:IsExists(aux.NOT(Card.IsLocation), 1, nil, LOCATION_HAND) and escape < 3 do
			Duel.SendtoHand(sg:Filter(aux.NOT(Card.IsLocation), nil, LOCATION_HAND), nil, REASON_RULE)
			escape = escape + 1
		end
		local tk = Duel.CreateToken(tp, 66594927)
		Duel.SendtoGrave(tk, REASON_RULE)
	end
	e:GetOwner():SetEntityCode(111004001)
	if Duel.GetFlagEffect(tp, id) <= 0 then
		Duel.RegisterFlagEffect(tp, id, 0, 0, 1)
	else
		Duel.SetFlagEffectLabel(tp, id, Duel.GetFlagEffectLabel(tp, id) | 0x1)
	end
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, EFFECT_FLAG_SET_AVAILABLE + EFFECT_FLAG_IGNORE_IMMUNE, 1)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.tdfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x13)
end
function s.con2(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and e:GetLabelObject():GetLabel() == 100 and Duel.IsExistingMatchingCard(s.tdfilter, tp, LOCATION_GRAVE, 0, 2, nil) and e:GetOwner():GetFlagEffect(id) <= 0 and (Duel.GetFlagEffect(tp, id) <= 0 or Duel.GetFlagEffectLabel(tp, id) & 0x2 == 0)
end
function s.act2(e, tp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = Duel.SelectMatchingCard(tp, s.tdfilter, tp, LOCATION_GRAVE, 0, 2, 2, nil)
	if #g > 0 then
		Duel.HintSelection(g)
		local escape = 0
		while g:IsExists(aux.NOT(Card.IsLocation), 1, nil, LOCATION_DECK + LOCATION_EXTRA) and escape < 3 do
			Duel.SendtoDeck(g:Filter(aux.NOT(Card.IsLocation), nil, LOCATION_DECK + LOCATION_EXTRA), nil, SEQ_DECKSHUFFLE, REASON_RULE)
			escape = escape + 1
		end
		escape = 0
		if g:IsExists(Card.IsLocation, 1, nil, LOCATION_DECK) then
			Duel.ShuffleDeck(tp)
		end
		if Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK, 0, 1, nil, 63468625) and Duel.SelectYesNo(tp, aux.Stringid(id, 2)) then
			Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
			local sg = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK, 0, 1, 1, nil, 63468625)
			Duel.BreakEffect()
			while sg:IsExists(aux.NOT(Card.IsLocation), 1, nil, LOCATION_HAND) and escape < 3 do
				Duel.SendtoHand(sg:Filter(aux.NOT(Card.IsLocation), nil, LOCATION_HAND), nil, REASON_RULE)
				escape = escape + 1
			end
			Duel.ConfirmCards(1 - tp, sg)
		end
	end
	e:GetOwner():SetEntityCode(111004001)
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, EFFECT_FLAG_SET_AVAILABLE + EFFECT_FLAG_IGNORE_IMMUNE, 1)
	if Duel.GetFlagEffect(tp, id) <= 0 then
		Duel.RegisterFlagEffect(tp, id, 0, 0, 1)
	else
		Duel.SetFlagEffectLabel(tp, id, Duel.GetFlagEffectLabel(tp, id) | 0x2)
	end
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

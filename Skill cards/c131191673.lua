--coded by XGlitchy30
--Soldier to Lord
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.checkop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(id, 0))
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetLabelObject(e0)
	e1:SetCondition(s.con1)
	e1:SetOperation(s.act1)
	c:RegisterEffect(e1)
end
function s.checkdeck(c)
	return c:IsType(TYPE_MONSTER) and not c:IsSetCard(0x13)
end
function s.checkop(e, tp)
	local g1 = Duel.GetMatchingGroup(s.checkdeck, tp, LOCATION_DECK, 0, nil)
	if #g1 == 0 then
		e:SetLabel(100)
	end
end
function s.sharedcon(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and e:GetLabelObject():GetLabel() == 100
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(39648965, 2137678, 75733063)
end
function s.con1(e, tp)
	local g = Duel.GetFieldGroup(tp, LOCATION_MZONE, 0)
	return s.sharedcon(e, tp) and #g == 1 and s.filter(g:GetFirst())
end
function s.act1(e, tp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_REMOVE)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil):GetFirst()
	local code = tc:GetCode()
	local escape = 0
	while not tc:IsLocation(LOCATION_GRAVE) and escape < 3 do
		Duel.SendtoGrave(tc, REASON_RULE)
		escape = escape + 1
	end
	escape = 0
	local newc = (code == 39648965) and 68140974 or (code == 75733063) and 31930787 or 4545683
	local tk = Duel.CreateToken(tp, newc)
	Duel.MoveToField(tk, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	tc:CompleteProcedure()
	tc:SetStatus(STATUS_FORM_CHANGED, true)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

--coded by XGlitchy30
--The Embodiment of Despair: Meklord Astro Mekanikle
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.checkop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
s.rchecks = aux.CreateChecks(Card.IsCode, { 39648965, 75733063, 2137678 })
function s.checkdeck(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x10af) or c:IsType(TYPE_SPELL + TYPE_TRAP) and c:IsSetCard(0xae)
end
function s.checkop(e, tp)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	local tk1, tk2, tk3, tk4 = Duel.CreateToken(tp, 63468625), Duel.CreateToken(tp, 39648965), Duel.CreateToken(tp, 75733063), Duel.CreateToken(tp, 2137678)
	Duel.SendtoDeck(Group.FromCards(tk1, tk2, tk3, tk4), nil, SEQ_DECKSHUFFLE, REASON_RULE)
	if Duel.GetTurnCount() > 1 then
		local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
		local escape = 0
		while hg:IsExists(aux.NOT(Card.IsLocation), 1, nil, LOCATION_DECK) and escape < 3 do
			Duel.SendtoDeck(hg:Filter(aux.NOT(Card.IsLocation), nil, LOCATION_DECK), nil, SEQ_DECKSHUFFLE, REASON_RULE)
			escape = escape + 1
		end
		Duel.ShuffleDeck(tp)
		Duel.Draw(tp, #hg, REASON_RULE)
	end
	c:SetEntityCode(111004001)
end
function s.rvfilter(c)
	return c:IsCode(63468625) and not c:IsPublic()
end
function s.thfilter0(c)
	return c:IsCode(39648965, 75733063, 2137678)
end
function s.con(e, tp)
	local g = Duel.GetMatchingGroup(s.thfilter0, tp, LOCATION_GRAVE, 0, nil)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.rvfilter, tp, LOCATION_HAND, 0, 1, nil) and g:CheckSubGroupEach(s.rchecks)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local tc = Duel.SelectMatchingCard(tp, s.rvfilter, tp, LOCATION_HAND, 0, 1, 1, nil):GetFirst()
	Duel.ConfirmCards(1 - tp, Group.FromCards(tc))
	Duel.ShuffleHand(tp)
	local g = Duel.GetMatchingGroup(s.thfilter0, tp, LOCATION_GRAVE, 0, nil)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local sg = g:SelectSubGroupEach(tp, s.rchecks)
	local escape = 0
	while sg:IsExists(aux.NOT(Card.IsLocation), 1, nil, LOCATION_HAND) and escape < 3 do
		Duel.SendtoHand(sg:Filter(aux.NOT(Card.IsLocation), nil, LOCATION_HAND), nil, REASON_RULE)
		escape = escape + 1
	end
	Duel.ConfirmCards(1 - tp, sg)
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_CANNOT_ACTIVATE)
	e1:SetTargetRange(1, 0)
	e1:SetValue(1)
	Duel.RegisterEffect(e1, tp)
	local e5 = Effect.CreateEffect(e:GetOwner())
	e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e5:SetCode(EVENT_SPSUMMON_SUCCESS)
	e5:SetLabelObject(e1)
	e5:SetCondition(s.sumcon)
	e5:SetOperation(s.sumop)
	Duel.RegisterEffect(e5, tp)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.sumfilter(c, tp)
	return c:IsFaceup() and c:IsCode(63468625) and c:IsSummonPlayer(tp)
end
function s.sumcon(e, tp, eg, ep, ev, re, r, rp)
	return eg:IsExists(s.sumfilter, 1, nil, tp)
end
function s.sumop(e, tp, eg, ep, ev, re, r, rp)
	local ce = e:GetLabelObject()
	if ce and ce.SetLabelObject then
		ce:Reset()
	end
	e:Reset()
end

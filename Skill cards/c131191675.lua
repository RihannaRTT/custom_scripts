--coded by XGlitchy30
--Mark of the Dragon - Heart
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.checkop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	if not s.global_check then
		s.global_check = true
		local g1 = Effect.CreateEffect(c)
		g1:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
		g1:SetCode(EVENT_SPSUMMON_SUCCESS)
		g1:SetOperation(s.registersum)
		Duel.RegisterEffect(g1, 0)
	end
	Duel.AddCustomActivityCounter(id, ACTIVITY_SPSUMMON, s.counterfilter)
end
function s.counterfilter(c)
	return c:IsType(TYPE_SYNCHRO) and aux.IsMaterialListType(c, TYPE_SYNCHRO)
end
function s.pfilter(c, tp)
	return c:IsSummonPlayer(tp) and c:IsSummonType(SUMMON_TYPE_SYNCHRO) and c:IsFaceup() and c:IsCode(2403771)
end
function s.registersum(e, tp, eg, ep, ev, re, r, rp)
	for p = 0, 1 do
		if eg:IsExists(s.pfilter, 1, nil, p) then
			Duel.RegisterFlagEffect(p, id, RESET_PHASE + PHASE_END, 0, 1)
		end
	end
end
function s.checkop(e, tp)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	local tk1, tk2, tk3, tk4 = Duel.CreateToken(tp, 76865611), Duel.CreateToken(tp, 76865611), Duel.CreateToken(tp, 2403771), Duel.CreateToken(tp, 25165047)
	Duel.SendtoDeck(Group.FromCards(tk1, tk2), nil, SEQ_DECKSHUFFLE, REASON_RULE)
	if Duel.GetTurnCount() > 1 then
		local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
		local escape = 0
		while hg:IsExists(aux.NOT(Card.IsLocation), 1, nil, LOCATION_DECK) and escape < 3 do
			Duel.SendtoDeck(hg:Filter(aux.NOT(Card.IsLocation), nil, LOCATION_DECK), nil, SEQ_DECKSHUFFLE, REASON_RULE)
			escape = escape + 1
		end
		Duel.ShuffleDeck(tp)
		Duel.Draw(tp, #hg, REASON_RULE)
	end
	Duel.BreakEffect()
	Duel.SendtoDeck(Group.FromCards(tk3, tk4), nil, SEQ_DECKSHUFFLE, REASON_RULE)
	c:SetEntityCode(111004001)
end
function s.playfilter(c, tp)
	return c:IsCode(76865611) and not c:IsForbidden() and c:CheckUniqueOnField(tp)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.playfilter, tp, LOCATION_HAND, 0, 1, nil, tp) and Duel.GetFlagEffect(tp, id) > 0 and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	if Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
		local tc = Duel.SelectMatchingCard(tp, s.playfilter, tp, LOCATION_HAND, 0, 1, 1, nil, tp):GetFirst()
		Duel.MoveToField(tc, tp, tp, LOCATION_MZONE, POS_FACEUP_DEFENSE, true)
		tc:CompleteProcedure()
		tc:SetStatus(STATUS_FORM_CHANGED, true)
	end
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e1:SetReset(RESET_PHASE + PHASE_END)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(s.splimit)
	Duel.RegisterEffect(e1, tp)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.splimit(e, c)
	return not s.counterfilter(c)
end

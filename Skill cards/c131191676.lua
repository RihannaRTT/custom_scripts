--coded by XGlitchy30
--Burning Soul
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.checkop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetLabelObject(e0)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	Duel.AddCustomActivityCounter(id, ACTIVITY_SUMMON, s.counterfilter)
	Duel.AddCustomActivityCounter(id, ACTIVITY_SPSUMMON, s.counterfilter)
end
function s.counterfilter(c)
	return c:IsType(TYPE_SYNCHRO) and aux.IsMaterialListType(c, TYPE_SYNCHRO)
end
function s.checkdeck(c)
	return c:IsType(TYPE_TUNER) and not c:IsSetCard(0x57)
end
function s.checkop(e, tp)
	if not Duel.IsExistingMatchingCard(s.checkdeck, tp, LOCATION_DECK, 0, 1, nil) then
		e:SetLabel(100)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsType(TYPE_SYNCHRO) and c:IsAttribute(ATTRIBUTE_DARK) and c:IsRace(RACE_DRAGON) and c:IsAttackAbove(3000)
end
function s.playfilter(c, tp)
	return c:IsType(TYPE_TUNER) and c:GetLevel() > 0 and c:IsLevelBelow(4) and not c:IsForbidden() and c:CheckUniqueOnField(tp)
end
function s.con(e, tp)
	local g = Duel.GetFieldGroup(tp, LOCATION_MZONE, 0)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and e:GetLabelObject():GetLabel() == 100 and g:FilterCount(s.filter, nil) == 1 and Duel.IsExistingMatchingCard(s.playfilter, tp, LOCATION_GRAVE, 0, 1, nil, tp) and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.GetCustomActivityCount(id, tp, ACTIVITY_SUMMON) + Duel.GetCustomActivityCount(id, tp, ACTIVITY_SPSUMMON) == 0
end
function s.act(e, tp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil):GetFirst()
	local escape = 0
	while not tc:IsLocation(LOCATION_DECK + LOCATION_EXTRA) and escape < 3 do
		Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
		escape = escape + 1
	end
	local ft = (s.bonuscheck(e, tp)) and 2 or 1
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local sg = Duel.SelectMatchingCard(tp, s.playfilter, tp, LOCATION_GRAVE, 0, 1, ft, nil, tp)
	local sc = sg:GetFirst()
	while sc do
		Duel.MoveToField(sc, tp, tp, LOCATION_MZONE, POS_FACEUP_DEFENSE, true)
		sc:CompleteProcedure()
		sc:SetStatus(STATUS_FORM_CHANGED, true)
		local e1 = Effect.CreateEffect(e:GetOwner())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_DISABLE)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD)
		sc:RegisterEffect(e1, true)
		local e2 = e1:Clone()
		e2:SetCode(EFFECT_UNRELEASABLE_SUM)
		e2:SetValue(1)
		sc:RegisterEffect(e2, true)
		local e3 = e2:Clone()
		e3:SetCode(EFFECT_UNRELEASABLE_NONSUM)
		sc:RegisterEffect(e3, true)
		sc = sg:GetNext()
	end
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e1:SetReset(RESET_PHASE + PHASE_END)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(s.splimit)
	Duel.RegisterEffect(e1, tp)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_CANNOT_SUMMON)
	Duel.RegisterEffect(e2, tp)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.splimit(e, c)
	return not s.counterfilter(c)
end
function s.bonuscheck(e, tp)
	local tc = Duel.GetFieldGroup(tp, LOCATION_MZONE, 0):Filter(s.filter, nil):GetFirst()
	return tc:IsCode(70902743) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 1 and not Duel.IsPlayerAffectedByEffect(tp, 59822133)
end

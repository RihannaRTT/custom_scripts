--coded by Lyris
--Forbidden Contract
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(73360025)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetLP(tp) <= 2000 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_ONFIELD, 0, 1, nil)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_ONFIELD, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	tc:SetEntityCode(10833828)
	tc:ReplaceEffect(10833828, 0)
	Duel.SetMetatable(tc, _G["c10833828"])
	tc:ResetEffect(10833828, RESET_CARD)
	tc:ResetEffect(10833828, RESET_CARD)
	c10833828.initial_effect(tc)
end

--coded by Lyris
--Destiny Draw: Monster Reborn
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.filter(c)
	return c:IsFaceup() and c:IsAttackAbove(3000)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and Duel.GetDrawCount(tp) > 0 and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK, 0, 1, nil, 83764718) and not c:IsOriginalCodeRule(id) and (Duel.GetLP(tp) <= 2500 or Duel.IsExistingMatchingCard(s.filter, tp, 0 < LOCATION_MZONE, 0, 1, nil))
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_DRAW_COUNT)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_DRAW)
	e1:SetValue(0)
	Duel.RegisterEffect(e1, tp)
	Duel.MoveSequence(Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK, 0, 1, 1, nil, 83764718):GetFirst(), SEQ_DECKTOP)
	Duel.Draw(tp, 1, REASON_RULE)
end

--coded by Lyris
--Master of Fusion: Paladin
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabel(0)
	e2:SetCondition(s.con(c, e0))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e1:SetCode(EVENT_SPSUMMON_SUCCESS)
		e1:SetOperation(s.count)
		Duel.RegisterEffect(e1, 0)
	end
end
function s.count(e, tp, eg)
	for p = 0, 1 do
		local g = eg:Filter(Card.IsSummonPlayer, nil, p)
		if #g > 1 then
			Duel.RegisterFlagEffect(p, id, RESET_PHASE + PHASE_END, 0, 1)
		end
	end
end
function s.hfilter(c)
	return not (c:IsAttribute(ATTRIBUTE_DARK) and c:IsRace(RACE_SPELLCASTER))
end
function s.xfilter(c)
	return c:IsType(TYPE_FUSION) and not c:IsCode(98502113)
end
function s.chk(e, tp)
	local g = Duel.GetMatchingGroup(s.hfilter, tp, LOCATION_DECK + LOCATION_HAND + LOCATION_EXTRA, 0, nil)
	e:SetLabel(g:GetClassCount(Card.GetCode), #g, Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_EXTRA, 0, nil))
end
function s.filter(c)
	return c:IsCode(46986414, 120130000) or c:IsType(TYPE_SPELL + TYPE_TRAP) and c:IsSetCard(0x10a2)
end
function s.con(c, ef)
	return function(e, tp)
		local nt, ct, xt = ef:GetLabel()
		return e:GetLabel() < 2 and c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and nt == ct and xt == 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil) and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and Duel.IsExistingMatchingCard(Card.IsCode, tp, 0xf3, 0, 1, nil, 24094653) and Duel.GetActivityCount(tp, ACTIVITY_SPSUMMON) < 2 and Duel.GetFlagEffect(tp, id) == 0
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(59822133)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e2:SetTargetRange(1, 0)
	e2:SetTarget(s.limtg(tp))
	e2:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e2, tp)
	local tid = Duel.GetTurnCount()
	local e3 = e2:Clone()
	e3:SetCondition(s.limcon(tid))
	e3:SetTarget(1)
	e3:SetReset(RESET_PHASE + PHASE_END + RESET_OPPO_TURN)
	Duel.RegisterEffect(e3, tp)
	local et = Effect.CreateEffect(c)
	et:SetType(EFFECT_TYPE_FIELD)
	et:SetCode(EFFECT_LEFT_SPSUMMON_COUNT)
	et:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	et:SetTargetRange(1, 0)
	et:SetValue(s.limval(tp))
	et:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(et, tp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND, 0, 1, 1, nil):GetFirst()
	Duel.ConfirmCards(1 - tp, tc)
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.BreakEffect()
	Duel.SendtoHand(Duel.CreateToken(tp, 78193831), nil, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SET)
	Duel.SSet(tp, Duel.SelectMatchingCard(tp, Card.IsCode, tp, 0xf3, 0, 1, 1, nil, 24094653), tp, false)
	e:SetLabel(e:GetLabel() + 1)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.limtg(tp)
	return function()
		return Duel.GetActivityCount(tp, ACTIVITY_SPSUMMON) >= 1
	end
end
function s.limcon(tid)
	return function()
		return Duel.GetTurnCount() ~= tid
	end
end
function s.limval(tp)
	return function()
		return math.max(1 - Duel.GetActivityCount(tp, ACTIVITY_SPSUMMON), 0)
	end
end

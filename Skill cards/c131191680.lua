--coded by Lyris
--Fangs of Rebellion: Dark Rebellion
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.flip(e, tp)
	e:GetOwner():SetEntityCode(id)
	for i = 0, 1 do
		Duel.SendtoDeck(Duel.CreateToken(tp, 77462146), nil, SEQ_DECKSHUFFLE, REASON_RULE)
	end
	if Duel.GetTurnCount() > 1 then
		local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
		Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
		Duel.ShuffleDeck(tp)
		Duel.Draw(tp, #hg, REASON_RULE)
	else
		Duel.ShuffleDeck(tp)
	end
	Duel.SendtoDeck(Duel.CreateToken(tp, 16195942), nil, SEQ_DECKTOP, REASON_RULE)
end
function s.cfilter(c, tp)
	return c:IsFaceup() and c:IsCode(16195942) and Duel.IsExistingMatchingCard(s.afilter, tp, 0, LOCATION_MZONE, 1, nil, c:GetAttack())
end
function s.afilter(c, atk)
	return c:IsFaceup() and c:GetAttack() > atk
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil, tp) and Duel.IsExistingMatchingCard(Card.IsCanOverlay, tp, LOCATION_HAND, 0, 1, nil)
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(16195942)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_XMATERIAL)
	Duel.Overlay(g:GetFirst(), Duel.SelectMatchingCard(Card.IsCanOverlay, tp, LOCATION_HAND, 0, 1, 2, nil))
end

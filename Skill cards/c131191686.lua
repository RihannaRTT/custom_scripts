--coded by Lyris
--Out of Control!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.flip(e, tp)
	e:GetOwner():SetEntityCode(id)
	for _, code in ipairs { 79850798, 24731453, 51126152 } do
		Duel.SendtoDeck(Duel.CreateToken(tp, code), nil, SEQ_DECKSHUFFLE, REASON_RULE)
	end
	if Duel.GetTurnCount() > 1 then
		local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
		Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
		Duel.ShuffleDeck(tp)
		Duel.Draw(tp, #hg, REASON_RULE)
	else
		Duel.ShuffleDeck(tp)
	end
	Duel.SendtoDeck(Duel.CreateToken(tp, 56910167), nil, SEQ_DECKTOP, REASON_RULE)
end
function s.filter(c)
	return c:IsLevel(10) and c:IsAttribute(ATTRIBUTE_EARTH) and c:IsRace(RACE_MACHINE)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and Duel.GetDrawCount(tp) > 0 and Duel.GetFieldGroup(tp, LOCATION_DECK, 0) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_GRAVE, 0, 2, nil) and c:GetFlagEffect(id) == 0
end
function s.act(e, tp)
	local c = e:GetOwner()
	if not Duel.SelectEffectYesNo(tp, c) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	Duel.SendtoDeck(Duel.CreateToken(tp, 97520701), nil, SEQ_DECKTOP, REASON_RULE)
	c:RegisterFlagEffect(id, 0, 0, 1)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

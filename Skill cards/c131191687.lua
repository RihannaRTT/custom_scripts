--coded by Lyris
--Unstoppable Train!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.chk)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabel(0)
	e2:SetLabelObject(e1)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.xfilter(c)
	return not (c:IsLevel(4, 10) and c:IsAttribute(ATTRIBUTE_EARTH) and c:IsRace(RACE_MACHINE) and c:IsAttackAbove(1800))
end
function s.chk(e, tp)
	local g = Duel.GetMatchingGroup(Card.IsType, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil, TYPE_MONSTER)
	e:SetLabel(#g, g:FilterCount(s.xfilter, nil))
end
function s.con(e, tp)
	local c = e:GetOwner()
	local ct, xt = e:GetLabelObject():GetLabel()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and e:GetLabel() < 2
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SUMMON_PROC)
	e1:SetDescription(90)
	e1:SetTargetRange(LOCATION_HAND, 0)
	e1:SetCondition(s.ntcon)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EVENT_SUMMON_SUCCESS)
	e2:SetOperation(s.disop)
	e2:SetReset(RESET_PHASE + PHASE_END)
	c:RegisterEffect(e2)
	local e3 = e2:Clone()
	e3:SetCode(EVENT_MSET)
	Duel.RegisterEffect(e3, tp)
	e:SetLabel(e:GetLabel() + 1)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.ntcon(e, c, minc)
	if c == nil then
		return true
	end
	local c = e:GetOwner()
	local tp = c:GetControler()
	return minc == 0 and c:IsLevelAbove(5) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0
end
function s.disop(e, tp, eg)
	local tc = eg:GetFirst()
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_DISABLE)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD)
	tc:RegisterEffect(e1, true)
	local e2 = e3:Clone()
	e2:SetCode(EFFECT_DISABLE_EFFECT)
	e2:SetValue(RESET_TURN_SET)
	tc:RegisterEffect(e2, true)
end

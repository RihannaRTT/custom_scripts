--coded by Lyris
--Super Express!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_PREDRAW)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsLevel(10) and c:IsAttribute(ATTRIBUTE_EARTH) and c:IsRace(RACE_MACHINE) and c:IsSummonableCard()
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and not c:IsOriginalCodeRule(id) and Duel.GetDrawCount(tp) > 0 and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0 and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil) and Duel.IsAbleToEnterBP()
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_DRAW_COUNT)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_DRAW)
	e1:SetValue(0)
	Duel.RegisterEffect(e1, tp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND, 0, 1, 1, nil):GetFirst()
	local e2 = Effect.CreateEffect(e:GetOwner())
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_DISABLE)
	e2:SetReset(RESET_EVENT + RESETS_STANDARD - RESET_TOFIELD)
	tc:RegisterEffect(e2, true)
	local e4 = e2:Clone()
	e4:SetCode(EFFECT_DISABLE_EFFECT)
	e4:SetValue(RESET_TURN_SET)
	tc:RegisterEffect(e4, true)
	Duel.MoveToField(tc, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	tc:SetStatus(STATUS_JUST_POS, true)
	Duel.SkipPhase(tp, PHASE_DRAW, RESET_PHASE + PHASE_END, 1)
	Duel.SkipPhase(tp, PHASE_STANDBY, RESET_PHASE + PHASE_END, 1)
	Duel.SkipPhase(tp, PHASE_MAIN1, RESET_PHASE + PHASE_END, 1)
	local e3 = Effect.GlobalEffect()
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CANNOT_EP)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetTargetRange(1, 0)
	e3:SetReset(RESET_PHASE + PHASE_MAIN1)
	Duel.RegisterEffect(e3, tp)
end

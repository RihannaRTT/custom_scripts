--coded by Lyris
--Devotion
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_PREDRAW)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return (Duel.GetFirstMatchingCard(Card.IsSetCard, tp, LOCATION_REMOVED + LOCATION_ONFIELD, 0, c, 0x2889) or Duel.GetDuelOptions and Duel.GetDuelOptions() & 0x20 > 0) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and Duel.GetDrawCount(tp) > 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e0 = Effect.GlobalEffect()
	e0:SetType(EFFECT_TYPE_FIELD)
	e0:SetCode(EFFECT_DRAW_COUNT)
	e0:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e0:SetTargetRange(1, 0)
	e0:SetValue(0)
	e0:SetReset(RESET_PHASE + PHASE_DRAW)
	Duel.RegisterEffect(e0, tp)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE_START + PHASE_STANDBY)
	e1:SetOperation(s.op(e0))
	e1:SetReset(RESET_PHASE + PHASE_STANDBY)
	Duel.RegisterEffect(e1, tp)
end
function s.op(ef)
	return function(e, tp)
		local e2 = ef:Clone()
		e2:SetValue(2)
		Duel.RegisterEffect(e2, tp)
	end
end

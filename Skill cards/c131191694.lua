--coded by Lyris
--Building a Diorama
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.chk)
	c:RegisterEffect(e1)
	if not s.global_check then
		s.global_check = true
		local ge1 = Effect.CreateEffect(c)
		ge1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		ge1:SetCode(EVENT_SPSUMMON_SUCCESS)
		ge1:SetCountLimit(1)
		ge1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
		ge1:SetCondition(s.ctcon)
		ge1:SetOperation(s.count)
		Duel.RegisterEffect(ge1, 0)
	end
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabelObject(e1)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c, typ)
	return c:GetType() & TYPE_RITUAL + typ == TYPE_RITUAL + typ
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, TYPE_MONSTER), Duel.GetMatchingGroupCount(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, TYPE_SPELL))
end
function s.filter(c, tp)
	return c:IsSummonPlayer(tp) and c:IsSummonType(SUMMON_TYPE_RITUAL) and c:GetType() & 0x81 == 0x81 and c:GetOriginalLevel() > 7
end
function s.ctcon(e, tp, eg)
	return eg:FilterCount(s.filter, nil, tp) == 1
end
function s.count(e, tp)
	Duel.RegisterFlagEffect(tp, id, 0, 0, 1)
end
function s.con(e, tp)
	local c = e:GetOwner()
	local ct1, ct2 = e:GetLabelObject():GetLabel()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and ct1 > 3 and ct2 > 3 and Duel.GetFlagEffect(tp, id) > 0 and Duel.IsExistingMatchingCard(Card.IsType, tp, LOCATION_DECK, 0, 1, nil, TYPE_FIELD)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, Card.IsType, tp, LOCATION_DECK, 0, 1, 1, nil, TYPE_FIELD)
	Duel.SendtoHand(g, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, g)
	Duel.ShuffleHand(tp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil), nil, SEQ_DECKBOTTOM, REASON_RULE)
end

--coded by Lyris
--Double Normal Summon
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
end
function s.count(e, tp)
	local plp0, plp1 = e:GetLabel()
	local clp0, clp1 = Duel.GetLP(0), Duel.GetLP(1)
	e:SetLabel(clp0, clp1)
	if not plp0 or not plp1 then
		return
	end
	if plp0 > clp0 then
		s[0] = s[0] - clp0 + plp0
	end
	if plp1 > clp1 then
		s[1] = s[1] - clp1 + plp1
	end
end
function s.con(c)
	return function(e, tp)
		return c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and s[tp] >= 1500 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND + LOCATION_MZONE, 0, 1, nil)
	end
end
function s.act(e, tp)
	s[tp] = 0
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SUMMON)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND + LOCATION_MZONE, 0, 1, 1, nil):GetFirst()
	Duel.Summon(tp, tc, true, nil)
	if not tc:IsOnField() then
		Duel.MoveToField(tc, tp, tp, LOCATION_MZONE, POS_FACEUP_ATTACK, true)
		Duel.Hint(HINT_CARD, 0, tc:GetOriginalCode())
	end
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.filter(c)
	return c:IsSummonable(true, nil) and c:IsType(TYPE_NORMAL)
end

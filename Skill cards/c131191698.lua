--coded by Lyris
--White Magician's Defensive Spell
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabel(0)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsRace(RACE_SPELLCASTER)
end
function s.filter(c)
	return c:IsFaceup() and c:IsDefenseAbove(0)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and e:GetLabel() < 2 and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_MZONE, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local d = Duel.TossDice(tp, 2)
	local def = 1000
	local c = e:GetOwner()
	if d > 2 and d < 8 then
		def = 700
	elseif d > 7 and d < 12 then
		def = 500
	elseif d == 12 then
		def = 100
	end
	for tc in aux.Next(Duel.GetMatchingGroup(s.filter, tp, 0, LOCATION_MZONE, nil)) do
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_DEFENSE)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD)
		e1:SetValue(-def)
		tc:RegisterEffect(e1, true)
	end
	e:SetLabel(e:GetLabel() + 1)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

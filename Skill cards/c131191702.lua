--coded by Lyris
--Supernatural Tactics
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	Duel.AddCustomActivityCounter(id, ACTIVITY_SPSUMMON, aux.FilterBoolFunction(Card.IsRace, RACE_MACHINE))
end
function s.cfilter(c, tp)
	return c:GetOwner() == 1 - tp or c:IsFaceup() and aux.IsCodeListed(c, 77585513) and Duel.GetMZoneCount(tp, c) > 0
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and Duel.GetCustomActivityCount(id, tp, ACTIVITY_SPSUMMON) == 0 and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil, tp)
end
function s.filter(c)
	return c:IsFaceup() and c:IsType(TYPE_TRAP)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(aux.TargetBoolFunction(aux.NOT(Card.IsRace), RACE_MACHINE))
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	Duel.SendtoGrave(Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_MZONE, 0, 1, 1, nil, tp), REASON_RULE)
	local tk = Duel.CreateToken(tp, 66362965)
	Duel.MoveToField(tk, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
	tk:SetStatus(STATUS_FORM_CHANGED, true)
	if not (Duel.GetLocationCount(1 - tp, LOCATION_SZONE, tp) > 0 and not Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_ONFIELD, 1, nil) and Duel.SelectEffectYesNo(tp, c)) then
		return
	end
	local tc = Duel.CreateToken(tp, 39978267)
	Duel.MoveToField(tc, tp, 1 - tp, LOCATION_SZONE, POS_FACEUP, true)
	local e2 = Effect.CreateEffect(c)
	e2:SetCode(EFFECT_CHANGE_TYPE)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e2:SetReset(RESET_EVENT + RESETS_STANDARD - RESET_TURN_SET)
	e2:SetValue(TYPE_TRAP + TYPE_CONTINUOUS)
	tc:RegisterEffect(e2, true)
end

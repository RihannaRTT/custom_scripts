--coded by Lyris
--Bear Parade
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.qcon)
	e2:SetOperation(s.qact)
	c:RegisterEffect(e2)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsCode(85545073)
end
function s.filter(c, tp)
	return c:IsType(TYPE_MONSTER) and c:CheckUniqueOnField(tp) and not c:IsForbidden()
end
function s.qcon(e, tp)
	local c = e:GetOwner()
	local ct = c:GetFlagEffectLabel(id)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil) and (not ct or ct & 1 == 0) and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_GRAVE, 1, nil, tp)
end
function s.qact(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	local ef = tc:IsHasEffect(EVENT_BATTLE_DESTROYING)
	tc:CreateEffectRelation(ef)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_EQUIP)
	local tg = Duel.SelectMatchingCard(tp, s.filter, tp, 0, LOCATION_GRAVE, 1, 1, nil, tp)
	Duel.HintSelection(tg)
	Duel.SetTargetCard(tg)
	ef:GetOperation()(e, tp)
	tc:ReleaseEffectRelation(ef)
	local c = e:GetOwner()
	local ct = c:GetFlagEffectLabel(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 1)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 1)
	end
end
function s.con(e, tp)
	local c = e:GetOwner()
	local ct = c:GetFlagEffectLabel(id)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil) and (not ct or ct & 2 == 0)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	local c = e:GetOwner()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_EXTRA_ATTACK)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
	e1:SetValue(tc:GetEquipCount())
	tc:RegisterEffect(e1, true)
	local ct = c:GetFlagEffectLabel(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 2)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 2)
	end
end

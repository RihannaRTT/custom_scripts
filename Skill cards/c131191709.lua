--coded by Lyris
--The Most Unbearable Monster
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	Duel.AddCustomActivityCounter(id, ACTIVITY_SPSUMMON, s.xfilter)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
end
function s.count(e, tp)
	local plp0, plp1 = e:GetLabel()
	local clp0, clp1 = Duel.GetLP(0), Duel.GetLP(1)
	e:SetLabel(clp0, clp1)
	if not plp0 or not plp1 then
		return
	end
	if plp0 > clp0 then
		s[0] = s[0] - clp0 + plp0
	end
	if plp1 > clp1 then
		s[1] = s[1] - clp1 + plp1
	end
end
function s.xfilter(c)
	return c:IsSetCard(0xad) and c:IsType(TYPE_FUSION)
end
function s.flip(e, tp)
	e:GetOwner():SetEntityCode(id)
	Duel.SendtoDeck(Duel.CreateToken(tp, 85545073), nil, SEQ_DECKTOP, REASON_RULE)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and Duel.GetDrawCount(tp) > 0 and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0 and s[tp] >= 1500
end
function s.act(e, tp)
	local c = e:GetOwner()
	if not Duel.SelectEffectYesNo(tp, c) then
		return
	end
	Duel.Hint(HINT_CARD, 0, id)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD)
	e0:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e0:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e0:SetTargetRange(1, 0)
	e0:SetTarget(aux.TargetBoolFunction(aux.NOT(s.xfilter)))
	e0:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e0, tp)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_DRAW_COUNT)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_DRAW)
	e1:SetValue(0)
	Duel.RegisterEffect(e1, tp)
	s[tp] = 0
	local g = Group.CreateGroup()
	local toCreate = { 30068120, 3841833, 24094653, 6077601 }
	for _, code in ipairs(toCreate) do
		g:AddCard(Duel.CreateToken(tp, code))
	end
	Duel.Remove(g, POS_FACEDOWN, REASON_RULE)
	local sg = g:RandomSelect(tp, 1)
	Duel.SendtoDeck(sg, nil, SEQ_DECKTOP, REASON_RULE)
	g:Sub(sg)
	Duel.Exile(g, REASON_RULE)
	Duel.Draw(tp, 1, REASON_RULE)
end

--coded by Lyris
--The Phantom Knights Order
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsFaceup() and c:IsSetCard(0x10db) and c:IsLevelAbove(1)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and e:GetOwner():GetFlagEffect(id) == 0 and e:GetLabel() < 2 and Duel.GetTurnCount() > 1 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil)
end
function s.act(e, tp, eg, ep, ev, re, r, rp, chk)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	for tc in aux.Next(g) do
		local e2 = Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE)
		e2:SetCode(EFFECT_XYZ_LEVEL)
		e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
		e2:SetRange(LOCATION_MZONE)
		e2:SetValue(s.xyzlv)
		e2:SetLabel(2)
		tc:RegisterEffect(e2, true)
		local e3 = e2:Clone()
		e3:SetLabel(3)
		tc:RegisterEffect(e3, true)
		local e4 = e2:Clone()
		e4:SetLabel(4)
		tc:RegisterEffect(e4, true)
	end
	e:SetLabel(e:GetLabel() + 1)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.xyzlv(e, c, rc)
	if c:IsSetCard(0x10db) then
		return c:GetLevel() + 0x10000 * e:GetLabel()
	else
		return c:GetLevel()
	end
end

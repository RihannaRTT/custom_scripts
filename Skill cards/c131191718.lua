--coded by Lyris
--Train Uncoupling!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.chk)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetLabelObject(e1)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.sfilter(c)
	return c:IsLevel(10) and c:IsAttribute(ATTRIBUTE_EARTH) and c:IsRace(RACE_MACHINE)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.sfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil))
end
function s.cfilter(c, tp)
	return c:IsLevel(10) and c:IsAttribute(ATTRIBUTE_EARTH) and c:IsRace(RACE_MACHINE) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_GRAVE, 0, 1, nil, c:GetAttack())
end
function s.filter(c, atk)
	return c:IsLevelBelow(4) and c:IsAttribute(ATTRIBUTE_EARTH) and c:IsRace(RACE_MACHINE) and (not atk or c:IsAttackBelow(atk))
end
function s.con(e, tp)
	local c = e:GetOwner()
	local ct = e:GetLabelObject():GetLabel()
	return Duel.GetTurnPlayer() == tp and Duel.GetDrawCount(tp) > 0 and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0 and ct > 2 and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil, tp)
end
function s.chk(g, atk)
	return g:GetSum(Card.GetAttack) <= atk
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_DRAW_COUNT)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_DRAW)
	e1:SetValue(0)
	Duel.RegisterEffect(e1, tp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local tc = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_HAND, 0, 1, 1, nil, tp):GetFirst()
	Duel.SendtoGrave(tc, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_GRAVE, 0, nil):SelectSubGroup(tp, s.chk, false, 1, 2, tc:GetAttack())
	Duel.SendtoHand(g, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, g)
end

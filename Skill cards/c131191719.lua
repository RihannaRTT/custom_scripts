--coded by Lyris
--Earth Golem
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and Duel.IsExistingMatchingCard(Card.IsRace, tp, LOCATION_GRAVE, 0, 1, nil, RACE_ROCK) and Duel.IsExistingMatchingCard(Card.IsRace, tp, LOCATION_HAND, 0, 1, nil, RACE_ROCK) and (Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK, 0, 1, nil, 14258627) or Duel.IsExistingMatchingCard(Card.IsRace, tp, LOCATION_HAND, 0, 2, nil, RACE_ROCK))
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetMatchingGroup(Card.IsCode, tp, LOCATION_DECK, 0, nil, 14258627)
	local minc = 1
	if #g == 0 then
		minc = 2
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local ct = Duel.DiscardHand(tp, Card.IsRace, minc, 2, REASON_RULE, nil, RACE_ROCK)
	local tc = Duel.CreateToken(tp, 14258627)
	if ct < 2 then
		tc = g:Select(tp, 1, 1, nil):GetFirst()
	end
	Duel.SendtoHand(tc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, tc)
end

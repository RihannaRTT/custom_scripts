--coded by Lyris
--Fossil Warriors
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.flip(e, tp)
	e:GetOwner():SetEntityCode(id)
	for _, code in ipairs { 23147658, 14258627, 59419719 } do
		Duel.SendtoDeck(Duel.CreateToken(tp, code), nil, SEQ_DECKSHUFFLE, REASON_RULE)
	end
	if Duel.GetTurnCount() > 1 then
		local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
		Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
		Duel.ShuffleDeck(tp)
		Duel.Draw(tp, #hg, REASON_RULE)
	else
		Duel.ShuffleDeck(tp)
	end
	for _, code in ipairs { 96897184, 59531356, 86520461 } do
		Duel.SendtoDeck(Duel.CreateToken(tp, code), nil, SEQ_DECKTOP, REASON_RULE)
	end
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetLP(tp) <= 2000 and Duel.IsExistingMatchingCard(Card.IsRace, tp, LOCATION_HAND, 0, 1, nil, RACE_ROCK) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, nil, 59419719)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	Duel.DiscardHand(tp, Card.IsRace, 1, 1, REASON_RULE, nil, RACE_ROCK)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, 1, nil, 59419719)
	Duel.SendtoHand(g, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, g)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

--coded by Lyris
--Let's Go Shirley!
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and Duel.GetDrawCount(tp) > 0 and c:IsOriginalCodeRule(111004001) and Duel.GetLP(tp) <= 2500 and Duel.IsExistingMatchingCard(Card.IsRace, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, nil, RACE_REPTILE)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.GetMatchingGroup(Card.IsRace, tp, LOCATION_GRAVE, 0, nil, RACE_REPTILE):SelectSubGroup(tp, aux.TRUE, true, 1, 1) or Duel.GetMatchingGroup(Card.IsRace, tp, LOCATION_DECK, 0, nil, RACE_REPTILE):RandomSelect(tp, 1)
	local tc = g:GetFirst()
	if tc:IsLocation(LOCATION_DECK) then
		Duel.MoveSequence(tc, SEQ_DECKTOP)
	else
		Duel.SendtoDeck(tc, nil, SEQ_DECKTOP, REASON_RULE)
	end
end

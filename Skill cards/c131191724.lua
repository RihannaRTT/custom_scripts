--coded by Lyris
--Sacred Defense Barrier
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_FREE_CHAIN)
	e0:SetCondition(s.con(c))
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(Card.IsRace, tp, LOCATION_HAND, 0, 1, nil, RACE_ROCK)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	Duel.DiscardHand(tp, Card.IsRace, 1, 1, REASON_RULE, nil, RACE_ROCK)
	Duel.SendtoHand(Duel.CreateToken(tp, 130000048), nil, REASON_RULE)
end

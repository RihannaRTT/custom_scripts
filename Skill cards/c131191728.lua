--coded by Lyris
--Ice Fire Egg
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
	if not s.global_check then
		s.global_check = true
		local e0 = Effect.GlobalEffect()
		e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e0:SetCode(EVENT_DESTROYED)
		e0:SetOperation(s.reg)
		Duel.RegisterEffect(e0, 0)
	end
end
function s.reg(e, tp, eg)
	for p = 0, 1 do
		if eg:IsExists(s.cfilter, 1, nil, p) then
			e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END + RESET_SELF_TURN, 0, 2, Duel.GetTurnCount())
		end
	end
end
function s.cfilter(c, tp)
	return c:IsType(TYPE_TRAP) and c:IsPreviousControler(tp)
end
function s.filter(c)
	return c:IsAttribute(ATTRIBUTE_WATER) and c:IsRace(RACE_WINDBEAST) and c:IsType(TYPE_XYZ)
end
function s.con(c)
	return function(e, tp)
		local ct = c:GetFlagEffectLabel(id)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and ct and ct ~= Duel.GetTurnCount()
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetRange(LOCATION_EXTRA)
	e1:SetProperty(EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_IGNORE_IMMUNE)
	e1:SetCondition(s.sprcon)
	e1:SetOperation(s.sprop)
	e1:SetReset(RESET_PHASE + PHASE_END)
	e1:SetValue(SUMMON_TYPE_XYZ)
	local e2 = Effect.GlobalEffect()
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_GRANT)
	e2:SetTargetRange(LOCATION_EXTRA, 0)
	e2:SetTarget(aux.TargetBoolFunction(s.filter))
	e2:SetLabelObject(e1)
	e2:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e2, tp)
	e1:SetLabelObject(e2)
end
function s.mfilter(c, xc)
	return c:IsFaceup() and c:IsAttribute(ATTRIBUTE_WATER) and c:IsType(TYPE_XYZ) and c:IsRank(xc:GetRank() - 1)
end
function s.sprcon(e, c, og, min, max)
	if c == nil then
		return true
	end
	local tp = c:GetControler()
	return Duel.IsExistingMatchingCard(s.mfilter, tp, LOCATION_MZONE, 0, 1, nil, c)
end
function s.sprop(e, tp, eg, ep, ev, re, r, rp, c, og, min, max)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_XMATERIAL)
	local mg = Duel.SelectMatchingCard(tp, s.mfilter, tp, LOCATION_MZONE, 0, 1, 1, nil, c)
	c:SetMaterial(mg)
	Duel.SendtoGrave(mg:GetFirst():GetOverlayGroup(), REASON_RULE + REASON_LOST_OVERLAY)
	Duel.Overlay(c, mg)
	e:GetLabelObject():Reset()
end

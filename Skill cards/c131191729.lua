--coded by Lyris
--Twins Together
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabel(0)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.filter(c)
	return c:IsFaceup() and c:IsLevelAbove(3) and c:IsLevelBelow(5) and c:IsAttribute(ATTRIBUTE_WATER) and c:IsRace(RACE_FISH) and not c:IsType(TYPE_TOKEN)
end
function s.chk(g, tp)
	return Duel.GetMZoneCount(tp, g) > 0
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and e:GetLabel() < 2 and Duel.GetMatchingGroup(s.filter, tp, LOCATION_MZONE, 0, nil):CheckSubGroup(s.chk, 2, 2, tp)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_MZONE, 0, nil):SelectSubGroup(tp, s.chk, false, 2, 2, tp)
	if g then
		Duel.HintSelection(g)
		Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, REASON_RULE)
		for _, code in ipairs { 70089580, 43694481 } do
			local tk = Duel.CreateToken(tp, code)
			Duel.MoveToField(tk, tp, tp, LOCATION_MZONE, POS_FACEUP, true)
			tk:SetStatus(STATUS_FORM_CHANGED, true)
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_XYZ_LEVEL)
			e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
			e1:SetValue(s.xlv)
			tk:RegisterEffect(e1, true)
		end
	end
	e:SetLabel(e:GetLabel() + 1)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end
function s.xlv(e, c, rc)
	return rc:IsAttribute(ATTRIBUTE_WATER) and c:GetLevel() + 5 * 0x10000 or c:GetLevel()
end

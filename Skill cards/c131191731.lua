--coded by Lyris
--You'll Pay for This!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetFieldGroupCount(tp, 0, LOCATION_MZONE) > 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_OPPO)
	local g = Duel.GetFieldGroup(tp, 0, LOCATION_MZONE):Select(tp, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	if tc:IsHasEffect(121191258) then
		return
	end
	tc:RegisterFlagEffect(id, RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_DRAW + RESET_SELF_TURN, 0, 1)
	local e0 = Effect.GlobalEffect()
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e0:SetCondition(s.atkcon)
	e0:SetOperation(s.atkop(c, tc))
	e0:SetReset(RESET_PHASE + PHASE_DRAW + RESET_SELF_TURN)
	Duel.RegisterEffect(e0, tp)
end
function s.atkcon(e, tp)
	return Duel.GetTurnPlayer() == tp
end
function s.atkop(c, tc)
	return function(e, tp)
		if tc:GetFlagEffect(id) > 0 and (tc:IsControler(tp) or not tc:IsHasEffect(121191258)) then
			if tc:IsFacedown() then
				tc:ResetFlagEffect(id)
				return
			end
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_SINGLE)
			e1:SetCode(EFFECT_SET_ATTACK_FINAL)
			e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
			e1:SetValue(math.ceil(tc:GetBaseAttack() / 2))
			tc:RegisterEffect(e1, true)
			local e2 = Effect.CreateEffect(c)
			e2:SetType(EFFECT_TYPE_SINGLE)
			e2:SetCode(EFFECT_SET_DEFENSE_FINAL)
			e2:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
			e2:SetValue(math.ceil(tc:GetBaseDefense() / 2))
			tc:RegisterEffect(e2, true)
		end
	end
end

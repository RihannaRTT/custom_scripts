--coded by Lyris
--Black Wind to the Top
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.flip(e, tp)
	if not Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_EXTRA, 0, 3, nil, 0x33) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.MoveToField(Duel.CreateToken(tp, 50913601), tp, tp, LOCATION_FZONE, POS_FACEUP, true)
	Duel.SendtoDeck(Duel.CreateToken(tp, 9012916), nil, SEQ_DECKTOP, REASON_RULE)
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(9012916)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, 1, s.filter, tp, LOCATION_MZONE, 0, 1, nil)
	Duel.HintSelection(g)
	g:GetFirst():AddCounter(0x10, 1)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

--coded by Lyris
--Burning Draw
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.chk)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabelObject(e1)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(Card.IsSetCard, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, 0x119))
end
function s.con(e, tp)
	local c = e:GetOwner()
	local lp = Duel.GetLP(tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and e:GetLabelObject():GetLabel() > 9 and Duel.GetTurnCount(tp) > 4 and lp <= 2300 and lp >= 1100 and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) >= (lp - 100) // 1000
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local cost = Duel.GetLP(tp) - 100
	Duel.PayLPCost(tp, cost, true)
	Duel.SendtoHand(Duel.GetDecktopGroup(tp, cost // 1000), nil, REASON_RULE)
end

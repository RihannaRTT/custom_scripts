--coded by Lyris
--Code Extended
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsLink(3) and c:IsSetCard(0x101)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	local b = tc:IsCode(1861629) and tc:GetSequence() > 4
	tc:SetEntityCode(30822527)
	tc:ReplaceEffect(30822527, 0)
	Duel.SetMetatable(tc, _G["c30822527"])
	tc:ResetEffect(30822527, RESET_CARD)
	tc:ResetEffect(30822527, RESET_CARD)
	c30822527.initial_effect(tc)
	local tg = Duel.GetFieldGroup(tp, 0, LOCATION_MZONE)
	if b and tc:GetColumnZone(LOCATION_MZONE, 0, 0, 1 - tp) > 0 and #tg > 0 and Duel.SelectEffectYesNo(tp, e:GetOwner()) then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_OPPO)
		local vg = tg:Select(tp, 1, 1, nil)
		Duel.HintSelection(vg)
		local vc = vg:GetFirst()
		if not vc:IsHasEffect(121191258) then
			Duel.MoveSequence(vc, 4 - ((tc:GetSequence() - 5) * 2 + 1))
		end
	end
end

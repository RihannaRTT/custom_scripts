--coded by Lyris
--Code Talker Alive
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.chk)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabelObject(e1)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c)
	return c:IsRace(RACE_CYBERSE) and c:IsType(TYPE_MONSTER) or c:IsSetCard(0x118) and c:IsType(TYPE_SPELL + TYPE_TRAP)
end
function s.chk(e, tp)
	local g = Duel.GetFieldGroup(tp, LOCATION_EXTRA, 0)
	e:SetLabel(Duel.GetMatchingGroupCount(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil), #g, g:GetClassCount(Card.GetCode))
end
function s.filter(c)
	return c:IsCode(1861629) and c:GetSequence() > 4
end
function s.con(e, tp)
	local c = e:GetOwner()
	local ct, xt, xc = e:GetLabel()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and ct > 11 and xt == xc and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.SSet(tp, Duel.CreateToken(tp, 70238111), tp, false)
end

--coded by Lyris
--Double Transcendence
local s, id, o = GetID()
function s.initial_effect(c)
	if not s.global_check then
		s.global_check = true
		local ge1 = Effect.CreateEffect(c)
		ge1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		ge1:SetCode(EVENT_SPSUMMON_SUCCESS)
		ge1:SetCountLimit(1)
		ge1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
		ge1:SetCondition(s.ctcon)
		ge1:SetOperation(s.count)
		Duel.RegisterEffect(ge1, 0)
	end
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c, tp)
	local g = c:GetMaterial()
	return c:IsSummonPlayer(tp) and c:IsSummonType(SUMMON_TYPE_LINK) and g and #g == 1 and g:GetFirst():IsCode(c:GetCode())
end
function s.ctcon(e, tp, eg)
	return eg:FilterCount(s.cfilter, nil, tp) == 1
end
function s.count(e, tp)
	Duel.RegisterFlagEffect(tp, id, 0, 0, 1)
end
function s.filter(c)
	return c:IsSetCard(0x119) and c:IsType(TYPE_SPELL + TYPE_TRAP)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and Duel.GetFlagEffect(tp, id) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND, 0, 1, 1, nil)
	Duel.SendtoHand(Duel.CreateToken(tp, 54529134), nil, REASON_RULE)
	Duel.ShuffleHand(tp)
	local c = e:GetOwner()
	local lg = Duel.GetMatchingGroup(Card.IsType, tp, LOCATION_GRAVE, 0, nil, TYPE_LINK)
	if #lg > 0 and Duel.SelectEffectYesNo(tp, c) then
		Duel.SendtoDeck(lg, nil, SEQ_DECKTOP, REASON_RULE)
	end
end

--coded by Lyris
--Extra Heat-Up
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetTargetRange(LOCATION_MZONE, 0)
	e2:SetCondition(s.con)
	e2:SetTarget(aux.TargetBoolFunction(Card.IsSetCard, 0xfc, 0x11a))
	e2:SetValue(300)
	c:RegisterEffect(e2)
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
	end
end
function s.con(e)
	local tp, tc = e:GetHandlerPlayer()
	for i = 5, 6 do
		tc = Duel.GetFieldCard(tp, LOCATION_MZONE, i)
		if tc then
			break
		end
	end
	return Duel.GetTurnPlayer() == tp and tc and tc:IsFaceup() and tc:IsSetCard(0xfc, 0x11a)
end

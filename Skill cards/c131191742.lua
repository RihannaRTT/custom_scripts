--coded by Lyris
--Fighting Spirit of the Avenger
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.chk)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabelObject(e1)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(Card.IsSetCard, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, 0xfc, 0x11a))
end
function s.filter(c, tp)
	return c:IsSetCard(0xfc, 0x11a) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil, c:GetLevel()) and c:IsDefenseAbove(0)
end
function s.cfilter(c, lv)
	return c:IsSetCard(0xfc, 0x11a) and c:IsType(TYPE_MONSTER) and c:IsLevel(lv)
end
function s.con(e, tp)
	local c = e:GetOwner()
	local ct = e:GetLabelObject():GetLabel()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and ct > 9 and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_GRAVE, 0, 1, nil, tp)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_GRAVE, 0, 1, 1, nil, tp)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	Duel.DiscardHand(tp, s.cfilter, 1, 1, REASON_RULE, nil, tc:GetLevel())
	Duel.MoveToField(tc, tp, tp, LOCATION_MZONE, POS_FACEUP_DEFENSE, true)
end

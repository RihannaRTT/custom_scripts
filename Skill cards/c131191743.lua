--coded by Lyris
--Fusion Excavation
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsSetCard(0x46) and c:IsType(TYPE_SPELL)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetLP(tp) <= 2000 and Duel.IsExistingMatchingCard(Card.IsRace, tp, LOCATION_HAND, 0, 1, nil, RACE_ROCK) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_GRAVE, 0, 1, nil)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	Duel.DiscardHand(tp, Card.IsRace, 1, 1, REASON_RULE, nil, RACE_ROCK)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_GRAVE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local sc = g:GetFirst()
	Duel.SendtoHand(sc, nil, REASON_RULE)
	if not sc:IsCode(59419719) and Duel.SelectEffectYesNo(tp, e:GetOwner()) then
		Duel.BreakEffect()
		sc:SetEntityCode(59419719)
		sc:ReplaceEffect(59419719, 0)
		Duel.SetMetatable(sc, _G["c59419719"])
		sc:ResetEffect(59419719, RESET_CARD)
		sc:ResetEffect(59419719, RESET_CARD)
		c59419719.initial_effect(sc)
		Duel.ConfirmCards(1 - tp, sc)
	end
end

--coded by Lyris
--Link Booster
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetTargetRange(LOCATION_MZONE, 0)
	e2:SetTarget(aux.TargetBoolFunction(Card.IsType, TYPE_LINK))
	e2:SetValue(s.val)
	c:RegisterEffect(e2)
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
	end
end
function s.val(e, c)
	return c:GetLinkedGroupCount() * 100
end

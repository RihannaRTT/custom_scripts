--coded by Lyris
--Malefic Dragons
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(111004001)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.act(e, tp)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	for _, code in ipairs { 37115575, 74509280, 36521459, 27564031 } do
		Duel.SendtoDeck(Duel.CreateToken(tp, code), nil, SEQ_DECKSHUFFLE, REASON_RULE)
	end
	if Duel.GetTurnCount() > 1 then
		local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
		Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
		Duel.ShuffleDeck(tp)
		Duel.Draw(tp, #hg, REASON_RULE)
	else
		Duel.ShuffleDeck(tp)
	end
	for _, code in ipairs { 44508094, 8310162 } do
		Duel.SendtoDeck(Duel.CreateToken(tp, code), nil, SEQ_DECKTOP, REASON_RULE)
	end
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e1:SetTargetRange(LOCATION_FZONE, 0)
	e1:SetTarget(aux.TargetBoolFunction(Card.IsCode, 27564031))
	e1:SetValue(aux.indoval)
	Duel.RegisterEffect(e1, tp)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e2:SetTargetRange(1, 0)
	e2:SetTarget(aux.TargetBoolFunction(aux.NOT(Card.IsSetCard), 0x23))
	Duel.RegisterEffect(e2, tp)
end

--coded by Lyris
--Monster Move
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c, tp)
	local seq = c:GetSequence()
	return seq < 5 or Duel.CheckLocation(tp, LOCATION_MZONE, (seq - 5) * 2 + 1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and Duel.GetLocationCount(tp, LOCATION_MZONE, PLAYER_NONE, 0) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, tp)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SELF)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil, tp)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	local seq = tc:GetSequence()
	if seq < 5 then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOZONE)
		Duel.MoveSequence(tc, math.log(Duel.SelectDisableField(tp, 1, LOCATION_MZONE, 0, 0xe0), 2))
	else
		Duel.MoveSequence(tc, (seq - 5) * 2 + 1)
	end
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

--coded by Lyris
--Rise from the Valley of Flames
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.flip(e, tp)
	e:GetOwner():SetEntityCode(id)
	Duel.SendtoDeck(Duel.CreateToken(tp, 41463181), nil, SEQ_DECKTOP, REASON_RULE)
end
function s.filter(c)
	return c:IsSetCard(0x119) and c:IsType(TYPE_LINK)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetLP(tp) <= 2000 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, 1, nil)
	Duel.HintSelection(g)
	Duel.SendtoDeck(Duel.CreateToken(tp, g:GetFirst():GetCode()), nil, SEQ_DECKTOP, REASON_RULE)
	local fc = Duel.GetFieldCard(tp, LOCATION_FZONE, 0)
	if fc then
		Duel.SendtoGrave(fc, REASON_RULE)
	end
	Duel.MoveToField(Duel.CreateToken(tp, 1295111), tp, tp, LOCATION_FZONE, POS_FACEUP, true)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

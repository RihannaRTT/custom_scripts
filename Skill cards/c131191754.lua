--coded by Lyris
--Salamangreat Booster
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetTargetRange(LOCATION_MZONE, 0)
	e2:SetTarget(s.tg)
	e2:SetValue(300)
	c:RegisterEffect(e2)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
	end
end
function s.tg(e, c)
	return c:IsSetCard(0x119) and c:IsType(TYPE_LINK) and Duel.IsExistingMatchingCard(Card.IsCode, c:GetControler(), LOCATION_GRAVE, 0, 1, nil, c:GetCode())
end

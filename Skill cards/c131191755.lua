--coded by Lyris
--Salamangreat Reincarnation
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c, tp)
	return c:IsOriginalCodeRule(41463181, 87871125) and not (c:IsSummonType(SUMMON_TYPE_LINK) and c:GetMaterial():IsExists(Card.IsLinkCode, 1, nil, c:GetOriginalCodeRule())) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_GRAVE, 0, 1, nil, c:GetOriginalCodeRule())
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, tp)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_OPERATECARD)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil, tp)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	for _, ef in ipairs { tc:IsHasEffect(EVENT_SPSUMMON_SUCCESS) } do
		if ef:IsHasType(EFFECT_TYPE_CONTINUOUS) and ef:IsHasProperty(EFFECT_FLAG_INITIAL) then
			ef:GetOperation()(ef)
			break
		end
	end
end

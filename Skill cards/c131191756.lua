--coded by Lyris
--Playmaker's "Storm Access"
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		for _, code in ipairs { 1861629, 5043010 } do
			Duel.SendtoDeck(Duel.CreateToken(tp, code), nil, SEQ_DECKTOP, REASON_RULE)
		end
	end
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetLP(tp) <= 1000
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Group.CreateGroup()
	for _, code in ipairs { 1861629, 6622715, 15844566, 30822527, 33897356, 40669071, 45462149, 46947713, 53413628, 61245672, 86066372, 58036229 } do
		g:AddCard(Duel.CreateToken(tp, code))
	end
	Duel.Remove(g, POS_FACEDOWN, REASON_RULE)
	local sg = g:RandomSelect(tp, 1)
	Duel.SendtoDeck(sg, nil, SEQ_DECKTOP, REASON_RULE)
	g:Sub(sg)
	Duel.Exile(g, REASON_RULE)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

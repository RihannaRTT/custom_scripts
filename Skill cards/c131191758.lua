--coded by Lyris
--Territory of Malice
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.chk)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabelObject(e1)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not (c:IsSetCard(0x23) or c:IsRace(RACE_DRAGON))
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil))
end
function s.con(e, tp)
	local xt = e:GetLabelObject():GetLabel()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and xt == 0 and Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_HAND, 0, 1, nil, 0x23)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.SelectMatchingCard(tp, Card.IsSetCard, tp, LOCATION_HAND, 0, 1, 1, nil, 0x23):GetFirst()
	Duel.ConfirmCards(1 - tp, tc)
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.ShuffleDeck(tp)
	Duel.SendtoHand(Duel.CreateToken(tp, 75223115), nil, REASON_RULE)
	if Duel.SelectEffectYesNo(tp, e:GetOwner()) then
		Duel.SendtoDeck(Duel.CreateToken(tp, 27564031), nil, SEQ_DECKBOTTOM, REASON_RULE)
	end
end

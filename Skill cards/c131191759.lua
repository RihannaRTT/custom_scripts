--coded by Lyris
--The Blank Card
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetTurnCount(tp) == 2 and not Duel.CheckPhaseActivity()
end
function s.filter(c)
	return c:IsFacedown() and c:IsRace(RACE_DRAGON) and c:IsType(TYPE_SYNCHRO)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	local tc = Duel.GetMatchingGroup(s.filter, tp, 0, LOCATION_EXTRA, nil):RandomSelect(tp, 1):GetFirst()
	if not tc then
		return
	end
	local code = tc:GetOriginalCode()
	Duel.Exile(tc, REASON_RULE)
	local tk = Duel.CreateToken(tp, code)
	Duel.SendtoDeck(tk, nil, SEQ_DECKTOP, REASON_RULE)
	local r = 1
	if Duel.GetCurrentPhase() == PHASE_MAIN1 then
		r = 2
	end
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE_START + PHASE_MAIN1)
	e1:SetCondition(s.retcon)
	e1:SetOperation(s.op)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_MAIN1 + RESET_SELF_TURN, r)
	tk:RegisterEffect(e1, true)
end
function s.retcon(e, tp)
	return Duel.GetTurnCount(tp) > 2 and Duel.GetTurnPlayer() == tp
end
function s.op(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetHandler()
	local code = c:GetOriginalCode()
	Duel.Exile(c, REASON_RULE)
	Duel.SendtoDeck(Duel.CreateToken(1 - tp, code), nil, SEQ_DECKTOP, REASON_RULE)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

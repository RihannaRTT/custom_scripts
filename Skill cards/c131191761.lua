--coded by Lyris
--The Opposites
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c, tp)
	return c:IsFaceupEx() and c:IsLevelAbove(7) and c:IsRace(RACE_DRAGON) and Duel.IsExistingMatchingCard(s.filter, tp, 0x34, 0, 1, nil, c)
end
function s.filter(c, tc)
	return c:IsFaceupEx() and c:IsLevel(tc:GetLevel()) and c:IsAttack(tc:GetAttack()) and c:IsDefense(tc:GetDefense()) and not c:IsCode(tc:GetCode())
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_GRAVE + LOCATION_REMOVED, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local tc = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_GRAVE + LOCATION_REMOVED, 0, 1, 1, nil):GetFirst()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, 0x34, 0, 1, 1, nil, tc) + tc
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local sc = g:Select(tp, 1, 1, nil):GetFirst()
	Duel.SendtoHand(sc, nil, REASON_RULE)
	g:RemoveCard(sc)
	local dg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil) + g
	aux.PlaceCardsOnDeckBottom(tp, sg, REASON_RULE)
end

--coded by Lyris
--Three Effects!
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and Duel.IsExistingMatchingCard(Card.IsType, tp, LOCATION_MZONE, 0, 1, nil, TYPE_LINK)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, Card.IsType, tp, LOCATION_MZONE, 0, 1, 1, nil, TYPE_LINK)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	local op = aux.SelectFromOptions(tp, { true, aux.Stringid(id, 0) }, { true, aux.Stringid(id, 1) }, { aux.nzatk(tc), aux.Stringid(id, 2) })
	local c = e:GetOwner()
	if op == 1 then
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_INDESTRUCTABLE_COUNT)
		e1:SetCountLimit(1)
		e1:SetValue(s.indval)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END + RESET_OPPO_TURN)
		tc:RegisterEffect(e1, true)
	elseif op == 2 then
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END + RESET_OPPO_TURN)
		e1:SetValue(1)
		tc:RegisterEffect(e1, true)
	else
		Duel.Recover(tp, tc:GetAttack(), REASON_RULE)
	end
end
function s.indval(e, re, r, rp)
	return r & REASON_BATTLE > 0
end

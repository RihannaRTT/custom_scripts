--coded by Lyris
--World Full of Malice
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		local tk = Duel.CreateToken(tp, 27564031)
		if Duel.GetMatchingGroup(Card.IsSetCard, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, 0x23):GetClassCount(Card.GetCode) > 4 then
			Duel.MoveToField(tk, tp, tp, LOCATION_FZONE, POS_FACEUP, true)
		else
			Duel.SSet(tp, tk, tp, false)
		end
	end
end

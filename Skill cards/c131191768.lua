--coded by Lyris
--Heart Creation
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsLinkAbove(2) and c:IsSetCard(0x12b) and c:GetSequence() > 4
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	for tc in aux.Next(Duel.GetMatchingGroup(s.filter, tp, LOCATION_MZONE, 0, nil)) do
		tc:SetEntityCode(67712104)
		tc:ReplaceEffect(67712104, 0)
		Duel.SetMetatable(tc, _G["c67712104"])
		tc:ResetEffect(67712104, RESET_CARD)
		tc:ResetEffect(67712104, RESET_CARD)
		c67712104.initial_effect(tc)
	end
end

--coded by Lyris
--Marincess Transcend
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.chk)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabelObject(e1)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	Duel.AddCustomActivityCounter(id, ACTIVITY_SUMMON, aux.FilterBoolFunction(Card.IsSetCard, 0x12b))
	Duel.AddCustomActivityCounter(id, ACTIVITY_SPSUMMON, aux.FilterBoolFunction(Card.IsSetCard, 0x12b))
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not (c:IsAttribute(ATTRIBUTE_WATER) and c:IsRace(RACE_CYBERSE))
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_EXTRA + LOCATION_HAND, 0, nil))
end
function s.cfilter(c, tp)
	return c:IsFaceup() and c:IsSetCard(0x12b) and c:IsType(TYPE_EFFECT) and c:IsLevelAbove(1) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil, c:GetOriginalLevel())
end
function s.filter(c, lv)
	return c:IsSetCard(0x12b) and c:IsLevel(lv + 1)
end
function s.con(e, tp)
	local c = e:GetOwner()
	local ct = e:GetLabelObject():GetLabel()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and ct == 0 and Duel.GetCustomActivityCount(id, tp, ACTIVITY_SUMMON) + Duel.GetCustomActivityCount(id, tp, ACTIVITY_SPSUMMON) == 0 and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil, tp)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SUMMON)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(aux.TargetBoolFunction(aux.NOT(Card.IsSetCard), 0x12b))
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, tp)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	Duel.RegisterEffect(e2, tp)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local g = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_MZONE, 0, 1, 1, nil, tp)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	Duel.SendtoGrave(g, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local sg = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil, tc:GetOriginalLevel())
	Duel.SendtoHand(sg, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, sg)
end

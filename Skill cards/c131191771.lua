--coded by Lyris
--Trickstar Gig
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetLP(tp) <= 2000 and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local t = {  }
	for i = 1, math.min(3, Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0)) do
		table.insert(t, i)
	end
	local tg = Duel.GetDecktopGroup(tp, Duel.AnnounceNumber(tp, table.unpack(t)))
	Duel.DisableShuffleCheck()
	Duel.SendtoGrave(tg, REASON_RULE)
	local g = Duel.GetMatchingGroup(Card.IsSetCard, tp, LOCATION_GRAVE, 0, 1, nil)
	if #g > 0 and Duel.SelectEffectYesNo(tp, e:GetOwner()) then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local tc = g:Select(tp, 1, 1, nil):GetFirst()
		Duel.SendtoHand(tc, nil, REASON_RULE)
		Duel.ConfirmCards(1 - tp, tc)
	end
end

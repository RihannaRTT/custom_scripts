--coded by Lyris
--Trickstars Together
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.flip(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.flip(c)
	return function(e, tp)
		c:SetEntityCode(id)
		for _, code in ipairs { 61283655, 98700941 } do
			Duel.SendtoDeck(Duel.CreateToken(tp, code), nil, SEQ_DECKSHUFFLE, REASON_RULE)
		end
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.Draw(tp, #hg, REASON_RULE)
		else
			Duel.ShuffleDeck(tp)
		end
		Duel.BreakEffect()
		Duel.SendtoDeck(Duel.CreateToken(tp, 32448765), nil, SEQ_DECKTOP, REASON_RULE)
	end
end
function s.filter(c)
	return not c:IsType(TYPE_LINK) and c:IsSetCard(0xfb)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetLocationCount(tp, LOCATION_MZONE, PLAYER_NONE, 0) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_OPERATECARD)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOZONE)
	Duel.MoveSequence(g:GetFirst(), math.log(Duel.SelectDisableField(tp, 1, LOCATION_MZONE, 0, 0), 2))
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

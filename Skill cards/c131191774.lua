--coded by Lyris
--Revolution with Courage & Strength
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.dcon)
	e1:SetOperation(s.dact)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.dcon(e, tp)
	local c = e:GetOwner()
	local ct = c:GetFlagEffectLabel(id)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not ct or ct & 1 == 0) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_EXTRA, 0, 1, nil, 25165047)
end
function s.dact(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.ConfirmCards(1 - tp, Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_EXTRA, 0, 1, 1, nil, 25165047))
	for i = 0, 2 do
		Duel.SendtoDeck(Duel.CreateToken(tp, 63730624), nil, SEQ_DECKBOTTOM, REASON_RULE)
	end
	local c = e:GetOwner()
	local ct = c:GetFlagEffectLabel(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 1)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 1)
	end
end
function s.filter(c, tp)
	return c:IsFaceup() and c:IsCode(63730624) and Duel.GetMZoneCount(tp, c) > 0
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not ct or ct & 2 == 0) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_ONFIELD, 0, 1, nil, tp) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	Duel.SendtoGrave(Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_ONFIELD, 0, 1, 1, nil, tp), REASON_RULE)
	local tk = Duel.CreateToken(tp, 76865611)
	Duel.MoveToField(tk, tp, tp, LOCATION_MZONE, POS_FACEUP_ATTACK, true)
	tk:SetStatus(STATUS_FORM_CHANGED, true)
	local c = e:GetOwner()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e1:SetRange(LOCATION_MZONE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetTarget(s.lim)
	c:RegisterEffect(e1, true)
	local ct = c:GetFlagEffectLabel(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 2)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 2)
	end
end
function s.lim(e, c)
	return not ((c:IsSetCard(0xc2) or c:IsLevel(7, 8) and c:IsRace(RACE_DRAGON)) and c:IsType(TYPE_SYNCHRO))
end

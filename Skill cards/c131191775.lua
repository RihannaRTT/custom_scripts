--coded by Lyris
--Pendulum Change: Basic
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.filter(c,...)
	return c:IsType(TYPE_NORMAL) and not c:IsCode(...)
end
function s.flip(e, tp)
	if Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, 0):GetClassCount(Card.GetCode) < 3 then
		return
	end
	local c = e:GetOwner()
	c:SetEntityCode(id)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(131191624)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	Duel.RegisterEffect(e1, tp)
end
function s.cfilter(c, tp)
	return c:GetOriginalType() & TYPE_NORMAL > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil, c:GetCode())
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_PZONE, 0, 1, nil, tp)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = Duel.SelectMatchingCard(tp, 1, nil, tp)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	local sg = Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK, 0, nil, tc:GetCode()):RandomSelect(tp, 1)
	Duel.SendtoHand(sg, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, sg)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

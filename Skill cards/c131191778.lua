--coded by Lyris
--Crush the World with the Mighty Fist!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCountLimit(1)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.flip(e, tp)
	e:GetOwner():SetEntityCode(id)
	for _, code in ipairs { 68535320, 95929069 } do
		if not Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil) then
			Duel.SendtoDeck(Duel.CreateToken(tp, code), nil, SEQ_DECKSHUFFLE, REASON_RULE)
		end
	end
	if Duel.GetTurnCount() > 1 then
		local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
		Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
		Duel.ShuffleDeck(tp)
		Duel.Draw(tp, #hg, REASON_RULE)
	else
		Duel.ShuffleDeck(tp)
	end
	Duel.SendtoDeck(Duel.CreateToken(tp, 63746411), nil, SEQ_DECKTOP, REASON_RULE)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetLP(tp) <= 2000 and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0):Select(tp, 1, 1, nil), nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.SendtoHand(Duel.CreateToken(tp, 47660516), nil, REASON_RULE)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

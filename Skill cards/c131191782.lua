--coded by Lyris
--Starter Hand
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.act(e, tp)
	local g = Duel.GetFieldGroup(tp, LOCATION_DECK + LOCATION_HAND, 0)
	local m = g:FilterCount(Card.IsType, nil, TYPE_MONSTER)
	local sg = g:Filter(Card.IsCode, nil, 22530212, 68535320, 95929069, 21414674)
	if not (m > #g - m and sg:GetClassCount(Card.GetCode) == 4 and Duel.GetMatchingGroupCount(aux.NOT(Card.IsType), tp, LOCATION_EXTRA, 0, nil, TYPE_XYZ) == 0) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	local tg = sg:RandomSelect(tp, 1)
	sg:Remove(Card.IsCode, nil, tg:GetFirst():GetCode())
	tg:Merge(sg:RandomSelect(tp, 1))
	local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
	if Duel.GetTurnCount() > 1 then
		Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
		Duel.ShuffleDeck(tp)
	end
	for tc in aux.Next(tg) do
		Duel.MoveSequence(tc, SEQ_DECKTOP)
	end
	if Duel.GetTurnCount() > 1 then
		Duel.Draw(tp, #hg, REASON_RULE)
	end
end

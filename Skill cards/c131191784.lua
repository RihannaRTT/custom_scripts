--coded by Lyris
--Everything's Ready!
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_PREDRAW)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
end
function s.count(e, tp)
	local plp, clp = e:GetLabel(), Duel.GetLP(tp)
	e:SetLabel(clp)
	if plp <= clp then
		return
	end
	s[tp] = s[tp] - clp + plp
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0xfb, 0x12b)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and Duel.GetDrawCount(tp) > 0 and s[tp] >= 1500 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	s[tp] = 0
	Duel.MoveSequence(Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK, 0, nil):Select(tp, 1, 1, nil):GetFirst(), SEQ_DECKTOP)
end

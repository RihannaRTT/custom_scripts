--coded by Lyris
--Anti-Glare Shield
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(_, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (c:IsFacedown() or not c:IsOriginalCodeRule(id))
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.CreateEffect(e:GetOwner())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 0)
	e1:SetCondition(s.dcon)
	e1:SetReset(RESET_PHASE + PHASE_END + RESET_OPPO_TURN)
	Duel.RegisterEffect(e1, tp)
end
function s.dcon(e)
	local ac = Duel.GetAttacker()
	if ac and ac:IsControler(1 - e:GetHandlerPlayer()) and ac:IsAttribute(ATTRIBUTE_LIGHT) then
		e:SetReset(RESET_PHASE + PHASE_DAMAGE_CAL)
		return true
	else
		return false
	end
end

--coded by Lyris
--Blasting Borrel
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCountLimit(2)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.flip(e, tp)
	e:GetOwner():SetEntityCode(id)
	Duel.SendtoDeck(Duel.CreateToken(tp, 31833038), nil, SEQ_DECKTOP, REASON_RULE)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and Duel.GetLP(tp) <= 3000
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.SendtoDeck(Duel.CreateToken(tp, Duel.AnnounceCard(tp, 73341839, OPCODE_ISCODE, 29296344, OPCODE_ISCODE, OPCODE_OR)), nil, SEQ_DECKTOP, REASON_RULE)
end

--coded by Lyris
--Borrel Booster
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.act(c))
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetTargetRange(LOCATION_MZONE, 0)
	e2:SetCondition(s.con(c))
	e2:SetTarget(aux.TargetBoolFunction(Card.IsSetCard, 0x10f, 0x102))
	e2:SetValue(200)
	c:RegisterEffect(e2)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsSetCard(0x10f)
end
function s.con(c)
	return function()
		return Duel.IsExistingMatchingCard(s.filter, c:GetControler(), LOCATION_MZONE, 0, 1, nil)
	end
end

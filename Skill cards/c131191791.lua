--coded by Lyris
--Rokket Reload
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0x102)
end
function s.con(c)
	return function(_, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not c:IsOriginalCodeRule(id) or c:IsFacedown()) and Duel.GetLP(tp) <= 3000 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 2, nil)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.DiscardHand(tp, s.filter, 1, 1, REASON_RULE)
	Duel.SendtoHand(Duel.CreateToken(tp, 6556178), nil, REASON_RULE)
end

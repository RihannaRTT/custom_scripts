--coded by Lyris
--Varis' "Storm Access"
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(_, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetLP(tp) <= 1000 and (not c:IsOriginalCodeRule(id) or c:IsFacedown())
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Group.CreateGroup()
	for _, code in ipairs { 5821478, 22593417, 66403530, 72529749 } do
		g:AddCard(Duel.CreateToken(tp, code))
	end
	Duel.Remove(g, POS_FACEDOWN, REASON_RULE)
	local sg = g:RandomSelect(tp, 1)
	Duel.SendtoDeck(sg, nil, SEQ_DECKTOP, REASON_RULE)
	g:Sub(sg)
	Duel.Exile(g, REASON_RULE)
end

--coded by Lyris
--Hand Reloading
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.hand(c)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and Duel.GetLP(tp) <= 2000 and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_GRAVE + LOCATION_HAND, 0, 1, nil, 22530212, 68535320, 95929069, 21414674, 63746411) and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = Duel.SelectMatchingCard(tp, Card.IsCode, tp, LOCATION_GRAVE + LOCATION_HAND, 0, 1, 1, nil, 22530212, 68535320, 95929069, 21414674, 63746411)
	Duel.HintSelection(g)
	local ct = Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.ShuffleDeck(tp)
	Duel.SendtoHand(Duel.GetDecktopGroup(tp, ct), nil, REASON_RULE)
end

--coded by Lyris
--Wings of Rebellion
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EVENT_BATTLE_DAMAGE)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetOperation(s.count)
	c:RegisterEffect(e2)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.count(e, tp, eg, ep, ev)
	if Duel.GetTurnPlayer() == ep then
		return
	end
	local ct = Duel.GetFlagEffectLabel(ep, id)
	if ct then
		Duel.SetFlagEffectLabel(tp, id, ct + ev)
	else
		Duel.RegisterFlagEffect(ep, id, RESET_PHASE + PHASE_END, 0, 2, ev)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsSetCard(0xba) and c:IsType(TYPE_XYZ)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and Duel.GetFlagEffect(tp, id) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil)
	Duel.HintSelection(g)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
	e1:SetValue(Duel.GetFlagEffectLabel(tp, id))
	g:GetFirst():RegisterEffect(e1, true)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

--coded by Lyris
--Emptiness Transforms into the Infinite!
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCountLimit(2)
	e2:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not c:IsLevel(10)
end
function s.flip(e, tp)
	if Duel.IsExistingMatchingCard(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, 1, nil) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.SendtoDeck(Duel.CreateToken(tp, 8967776), nil, SEQ_DECKSHUFFLE, REASON_RULE)
	if Duel.GetTurnCount() > 1 then
		local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
		Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
		Duel.ShuffleDeck(tp)
		Duel.Draw(tp, #hg, REASON_RULE)
	else
		Duel.ShuffleDeck(tp)
	end
end
function s.con(e, tp)
	local c = e:GetOwner()
	return
end
function s.filter(c)
	return c:IsLevel(10) and c:IsRace(RACE_FAIRY)
end
function s.chk(g, tp)
	return Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, #g, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local ct = Duel.SendtoGrave(Duel.GetFieldGroup(tp, LOCATION_HAND, 0):SelectSubGroup(tp, s.chk, false, 1, 2), REASON_RULE)
	Duel.SendtoGrave(Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK, 0, nil):RandomSelect(tp, ct), REASON_RULE)
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

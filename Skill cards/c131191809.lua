--coded by Lyris
--Infinite Power of the Timelords
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsCode(CARD_INFINITE_LIGHT)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_ONFIELD, 0, 1, nil) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_HAND, 0, 1, nil, 0x4a)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local tc = Duel.SelectMatchingCard(tp, 1, nil, 0x4a):GetFirst()
	Duel.MoveToField(tc, tp, tp, LOCATION_MZONE, POS_FACEUP_ATTACK, true)
	tc:SetStatus(STATUS_FORM_CHANGED, true)
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

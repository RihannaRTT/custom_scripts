--coded by Lyris
--No Hope in the Future - Only Despair
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabel(0)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.filter(c)
	return c:IsCode(92435533) and not c:IsPublic()
end
function s.con(e, tp)
	local ct = e:GetOwner():GetFlagEffectLabel(id)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not ct or ct & 1 == 0 and ct & 2 == 0) and e:GetLabel() < 2 and (Duel.GetFieldGroupCount(tp, 0, LOCATION_DECK) > 1 or Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil) and Duel.GetFieldGroupCount(tp, 0, LOCATION_GRAVE) > 2)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	local op = aux.SelectFromOptions(tp, { Duel.GetFieldGroupCount(tp, 0, LOCATION_DECK) > 1, aux.Stringid(id, 0) }, { Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil) and Duel.GetFieldGroupCount(tp, 0, LOCATION_GRAVE) > 2, HINTMSG_TODECK })
	if op == 2 then
		Duel.ConfirmCards(1 - tp, Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND, 0, 1, 1, nil))
		local g = Duel.GetFieldGroup(tp, 0, LOCATION_GRAVE):RandomSelect(tp, 3)
		Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	else
		Duel.ShuffleDeck(1 - tp)
	end
	e:SetLabel(e:GetLabel() + 1)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1, 1)
	if op == 2 then
		c:SetFlagEffectLabel(id, 3)
	end
end

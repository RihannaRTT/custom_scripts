--coded by Lyris
--Neos Tribute
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_DECREASE_TRIBUTE)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetTargetRange(LOCATION_HAND, LOCATION_HAND)
	e2:SetTarget(aux.TargetBoolFunction(Card.IsCode, 89943723))
	e2:SetValue(s.decval)
	c:RegisterEffect(e2)
end
function s.flip(e)
	e:GetOwner():SetEntityCode(id)
end
function s.decval()
	return 0x1, id
end

--coded by Lyris
--Gravity Blaster
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c, tp)
	return c:IsFaceup() and c:IsRace(RACE_MACHINE) and (c:IsCode(1992816) or Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, nil, c)
end
function s.filter(c, tc)
	return c:IsCode(35220244) and tc:CheckEquipTarget(c)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and Duel.GetLocationCount(tp, LOCATION_SZONE) > 0 and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil, tp)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_EQUIP)
	local g = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_MZONE, 0, 1, 1, nil, tp)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	local mg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
	if not tc:IsCode(1992816) or #mg == 0 or Duel.SelectEffectYesNo(tp, c, 1193) then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
		local mc = mg:Select(tp, 1, 1, nil):GetFirst()
		Duel.SendtoDeck(mc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	Duel.Equip(tp, tc, Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK + LOCATION_GRAVE, 0, 1, 1, nil, tc):GetFirst(), true)
end

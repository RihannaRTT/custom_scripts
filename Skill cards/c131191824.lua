--coded by Lyris
--Megamachine Construction
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.chk(g)
	return g:CheckWithSumEqual(Card.GetLevel, 9, #g, #g)
end
function s.filter(c)
	return c:IsLevel(9) and c:IsRace(RACE_MACHINE)
end
function s.con(e, tp)
	local c = e:GetOwner()
	local g = Duel.GetMatchingGroup(Card.IsRace, tp, LOCATION_HAND, 0, 1, nil, RACE_MACHINE)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and g:CheckSubGroup(s.chk, 1) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetMatchingGroup(Card.IsRace, tp, LOCATION_HAND, 0, 1, nil, RACE_MACHINE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local mg = g:SelectSubGroup(tp, s.chk, false, 1)
	Duel.ConfirmCards(1 - tp, mg)
	Duel.SendtoDeck(mg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 0, 1, 1, nil):GetFirst()
	Duel.SendtoHand(tc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, tc)
end

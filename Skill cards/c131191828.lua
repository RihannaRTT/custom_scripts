--coded by Lyris
--Hi-Speedroid Reboot
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c, tp)
	return c:IsFaceup() and c:IsSetCard(0x2016) and Duel.GetMZoneCount(tp, c) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_GRAVE, 0, 1, nil, c:GetOriginalLevel())
end
function s.filter(c, lv)
	return c:IsSetCard(0x2016) and c:IsType(TYPE_SYNCHRO) and c:GetOriginalLevel() == lv
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and Duel.GetActivityCount(tp, ACTIVITY_SPSUMMON) == 0 and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil, tp)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	local mc = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_MZONE, 0, 1, 1, nil, tp):GetFirst()
	Duel.SendtoGrave(mc, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOFIELD)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_GRAVE, 0, 1, 1, nil, mc:GetOriginalLevel()):GetFirst()
	Duel.MoveToField(tc, tp, tp, LOCATION_MZONE, POS_FACEUP_ATTACK, true)
	tc:SetStatus(STATUS_FORM_CHANGED, true)
end

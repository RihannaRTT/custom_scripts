--coded by Lyris
--Hi-Speedroid Recycle
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.cfilter(c)
	return c:IsFaceup() and c:IsSetCard(0x2016) and c:IsType(TYPE_SYNCHRO)
end
function s.filter(c)
	return c:IsSetCard(0x2016) and c:IsType(TYPE_MONSTER)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_MZONE, 0, 1, nil) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_GRAVE, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local _, lv = Duel.GetMatchingGroup(s.cfilter, tp, LOCATION_MZONE, 0, nil):GetMaxGroup(Card.GetLevel)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_GRAVE, 0, 1, lv, nil)
	Duel.HintSelection(g)
	Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, REASON_RULE)
end

--coded by Lyris
--Speedroid Tune
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c, tp)
	return c:IsSetCard(0x2016) and c:IsLevelAbove(1) and not c:IsPublic() and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, c)
end
function s.filter(c, tc)
	return c:IsFaceup() and c:IsSetCard(0x2016) and c:IsLevelAbove(1) and (not c:IsLevel(tc:GetOriginalLevel()) or tc:IsType(TYPE_TUNER) and not c:IsType(TYPE_TUNER))
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil, tp)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local tc = Duel.SelectMatchingCard(tp, s.cfilter, tp, LOCATION_HAND, 0, 1, 1, nil, tp):GetFirst()
	Duel.ConfirmCards(1 - tp, tc)
	Duel.SendtoDeck(tc, nil, SEQ_DECKTOP, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil, tc)
	Duel.HintSelection(g)
	local sc = g:GetFirst()
	local c = e:GetOwner()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_CHANGE_LEVEL)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
	e1:SetValue(tc:GetOriginalLevel())
	sc:RegisterEffect(e1, true)
	if tc:IsType(TYPE_TUNER) and not c:IsType(TYPE_TUNER) and Duel.SelectEffectYesNo(tp, c) then
		local e2 = e1:Clone()
		e2:SetCode(EFFECT_ADD_TYPE)
		e2:SetValue(TYPE_TUNER)
		sc:RegisterEffect(e2, true)
	end
end

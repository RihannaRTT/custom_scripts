--coded by Lyris
--Born from the Infinite
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c, tp)
	return c:IsSetCard(0x4a) and not Duel.IsExistingMatchingCard(Card.IsAttribute, tp, LOCATION_GRAVE, 0, 1, nil, c:GetAttribute())
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:GetFlagEffect(id) == 0 and e:GetLabel() < 2 and Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_HAND, 0, 1, nil, 0x4a)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	Duel.DiscardHand(tp, Card.IsSetCard, 1, 1, REASON_RULE, nil, 0x4a)
	local g = Duel.GetMatchingGroup(s.filter, tp, LOCATION_DECK, 0, nil, tp)
	if #g > 0 and Duel.SelectEffectYesNo(tp, c) then
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		local sg = g:Select(tp, 1, 1, nil)
		Duel.SendtoHand(sg, nil, REASON_RULE)
		Duel.ConfirmCards(1 - tp, sg)
	end
	e:SetLabel(e:GetLabel() + 1)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

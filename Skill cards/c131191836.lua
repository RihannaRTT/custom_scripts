--coded by Lyris
--Salamandra & Phoenix
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_SUMMON_PROC)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetDescription(90)
	e2:SetTargetRange(LOCATION_HAND, 0)
	e2:SetCondition(s.ntcon)
	e2:SetTarget(aux.TargetBoolFunction(Card.IsCode, 69488544))
	c:RegisterEffect(e2)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_FREE_CHAIN)
	e4:SetRange(LOCATION_REMOVED)
	e4:SetCondition(s.con)
	e4:SetOperation(s.act)
	c:RegisterEffect(e4)
end
function s.cfilter(c)
	return c:IsAttribute(ATTRIBUTE_FIRE) and c:IsRace(RACE_WARRIOR)
end
function s.flip(e, tp)
	if Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, 10, nil) then
		e:GetOwner():SetEntityCode(id)
	end
end
function s.ntcon(e, c, minc)
	if c == nil then
		return true
	end
	local c = e:GetOwner()
	local tp = c:GetControler()
	return c:IsOriginalCodeRule(id) and minc == 0 and c:IsLevelAbove(5) and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0
end
function s.filter(c)
	return c:IsCode(22091647) and not c:IsPublic()
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_HAND, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	Duel.ConfirmCards(1 - tp, Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_HAND, 0, 1, 1, nil))
	Duel.SSet(tp, Duel.CreateToken(tp, 32268901))
	Duel.SendtoGrave(Duel.CreateToken(tp, 61854111), REASON_RULE)
end

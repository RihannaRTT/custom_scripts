--coded by Lyris
--A Burning Roar
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.flip)
	c:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCountLimit(1)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.filter(c)
	return c:IsSetCard(0x119) and c:IsType(TYPE_MONSTER)
end
function s.flip(e, tp)
	if not (Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK + LOCATION_EXTRA + LOCATION_HAND, 0, 10, nil) and Duel.IsExistingMatchingCard(Card.IsCode, tp, LOCATION_EXTRA, 0, 1, nil, 41463181)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	for _, code in ipairs { 41463181, 87871125 } do
		Duel.SendtoDeck(Duel.CreateToken(tp, code), nil, SEQ_DECKTOP, REASON_RULE)
	end
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(id) and c:GetFlagEffect(id) == 0 and Duel.IsExistingMatchingCard(Card.IsFaceup, tp, LOCATION_MZONE, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local c = e:GetOwner()
	for tc in aux.Next(Duel.GetMatchingGroup(Card.IsFaceup, tp, LOCATION_MZONE, 0, nil)) do
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		e1:SetValue(Duel.GetFieldGroupCount(tp, LOCATION_MZONE, 0) * 100)
		tc:RegisterEffect(e1, true)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

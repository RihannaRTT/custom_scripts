--coded by Lyris
--Extra Curse 1
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_BATTLE_DESTROYED)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c, tp)
	return c:IsPreviousControler(1 - tp) and c:GetPreviousSequence() > 4
end
function s.con(e, tp, eg)
	return eg:IsExists(s.filter, 1, nil, tp)
end
function s.act(e, tp)
	if Duel.GetFieldGroupCount(tp, LOCATION_EXTRA, 0) > 0 or Duel.GetLP(1 - tp) < 1000 then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Damage(1 - tp, 500, REASON_RULE)
end

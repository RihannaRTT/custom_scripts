--coded by Lyris
--Extra Curse 2
local s, id, o = GetID()
function s.initial_effect(c)
	if not s.global_check then
		s.global_check = true
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e1:SetCode(EVENT_SPSUMMON_SUCCESS)
		e1:SetOperation(s.chk)
		Duel.RegisterEffect(e1, 0)
	end
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c, tp)
	return c:IsSummonLocation(LOCATION_EXTRA) and c:IsSummonPlayer(tp)
end
function s.chk(e, tp, eg, ep)
	if eg:FilterCount(s.cfilter, nil, 0) == 1 then
		Duel.RegisterFlagEffect(1, id, RESET_PHASE + PHASE_END + RESET_SELF_TURN, 0, 1)
	end
	if eg:FilterCount(s.cfilter, nil, 1) == 1 then
		Duel.RegisterFlagEffect(0, id, RESET_PHASE + PHASE_END + RESET_SELF_TURN, 0, 1)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:GetSequence() > 4
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and Duel.GetFlagEffect(tp, id) > 0 and Duel.GetFieldGroupCount(tp, LOCATION_EXTRA, 0) < Duel.GetFieldGroupCount(tp, 0, LOCATION_EXTRA) and Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_MZONE, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, 0, LOCATION_MZONE, 1, 1, nil)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	if tc:IsHasEffect(121191258) then
		return
	end
	local diff = Duel.GetFieldGroupCount(tp, 0, LOCATION_EXTRA) - Duel.GetFieldGroupCount(tp, LOCATION_EXTRA, 0)
	local c = e:GetOwner()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
	e1:SetValue(-diff * 100)
	c:RegisterEffect(e1, true)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_UPDATE_DEFENSE)
	c:RegisterEffect(e2, true)
end

--coded by Lyris
--Sinister Serpent Surprise♪
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_PREDRAW)
	e1:SetCondition(s.dcon)
	e1:SetOperation(s.dact)
	c:RegisterEffect(e1)
	local e2 = e1:Clone()
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
end
function s.count(e, tp)
	local plp0, plp1 = e:GetLabel()
	local clp0, clp1 = Duel.GetLP(0), Duel.GetLP(1)
	e:SetLabel(clp0, clp1)
	if not plp0 or not plp1 then
		return
	end
	if plp0 > clp0 then
		s[0] = s[0] - clp0 + plp0
	end
	if plp1 > clp1 then
		s[1] = s[1] - clp1 + plp1
	end
end
function s.dcon(e, tp)
	local c = e:GetOwner()
	local ct = c:GetFlagEffect(id)
	return Duel.GetTurnPlayer() == tp and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0 and Duel.GetDrawCount(tp) > 0 and (not ct or ct & 2 == 0) and c[tp] >= 1000
end
function s.dact(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_DRAW_COUNT)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_DRAW)
	e1:SetValue(0)
	Duel.RegisterEffect(e1, tp)
	s[tp] = 0
	Duel.SendtoGrave(Duel.CreateToken(tp, 8131171), REASON_RULE)
	local c = e:GetOwner()
	local ct = c:GetFlagEffect(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 1)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 1)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsCode(8131171)
end
function s.con(e, tp)
	local c = e:GetOwner()
	local ct = c:GetFlagEffect(id)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and (not ct or ct & 1 == 0) and s[tp] >= 1000 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_REMOVED, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	s[tp] = 0
	Duel.SendtoGrave(Duel.GetMatchingGroup(s.filter, tp, LOCATION_REMOVED, 0, nil), REASON_RETURN + REASON_RULE)
	local c = e:GetOwner()
	local ct = c:GetFlagEffect(id)
	if ct then
		c:SetFlagEffectLabel(id, ct | 2)
	else
		c:RegisterFlagEffect(id, 0, 0, 1, 2)
	end
end

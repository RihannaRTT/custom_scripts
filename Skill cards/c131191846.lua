--coded by Lyris
--Yowza! A Divine Card!
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.filter(c,...)
	return c:IsFaceup() and c:IsCode(...)
end
function s.con(e, tp)
	local c = e:GetOwner()
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and c:IsOriginalCodeRule(111004001) and Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_ONFIELD, 1, nil, 10000000, 513000135, 10000020, 513000136, 10000010, 513000134)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	for _, t in ipairs { { 10000000, 513000135 }, { 10000020, 513000136 }, { 10000010, 513000134 } } do
		for _, code in ipairs(t) do
			if Duel.IsExistingMatchingCard(s.filter, tp, 0, LOCATION_MZONE, 1, nil, code) then
				Duel.SendtoHand(Duel.CreateToken(tp, code), nil, REASON_RULE)
			end
		end
	end
end

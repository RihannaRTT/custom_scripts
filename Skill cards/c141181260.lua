--coded by Lyris
--"Balance" v2
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		local n1 = Duel.GetMatchingGroupCount(Card.IsType, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, TYPE_MONSTER)
		local n2 = Duel.GetMatchingGroupCount(Card.IsType, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, TYPE_SPELL)
		local n3 = Duel.GetMatchingGroupCount(Card.IsType, tp, LOCATION_DECK + LOCATION_HAND, 0, nil, TYPE_TRAP)
		c:SetEntityCode(id)
		local sum = n1 + n2 + n3
		local g = Duel.GetFieldGroup(tp, LOCATION_DECK, 0)
		local mg = g:Filter(Card.IsType, nil, TYPE_MONSTER):RandomSelect(tp, 3 * n1 // sum) + g:Filter(Card.IsType, nil, TYPE_SPELL):RandomSelect(tp, 3 * n2 // sum) + g:Filter(Card.IsType, nil, TYPE_TRAP):RandomSelect(tp, 3 * n3 // sum)
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND)
			if #mg < #hg then
				mg:Merge(g:RandomSelect(tp, #hg - #mg))
			end
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			for mc in aux.Next(mg) do
				Duel.MoveSequence(mc, 0)
			end
			Duel.Draw(tp, #hg, REASON_RULE)
		else
			for mc in aux.Next(mg) do
				Duel.MoveSequence(mc, 0)
			end
		end
	end
end

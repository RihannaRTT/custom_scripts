--coded by Lyris
--"Parasite Infestation" v1
local s, id, o = GetID()
function s.initial_effect(c)
	Duel.EnableGlobalFlag(GLOBALFLAG_DECK_REVERSE_CHECK)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCountLimit(1)
	e1:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	for i = 1, math.random(3) do
		local tc = Duel.CreateToken(tp, 27911549)
		Duel.SendtoDeck(tc, 1 - tp, SEQ_DECKSHUFFLE, REASON_EFFECT)
		if tc:IsLocation(LOCATION_DECK) then
			tc:ReverseInDeck()
			local e1 = Effect.CreateEffect(tc)
			e1:SetDescription(aux.Stringid(27911549, 1))
			e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
			e1:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_TRIGGER_F)
			e1:SetCode(EVENT_DRAW)
			e1:SetTarget(s.sptg)
			e1:SetOperation(s.spop)
			e1:SetReset(RESET_EVENT + 0x1de0000)
			tc:RegisterEffect(e1, true)
		end
	end
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.sptg(e, tp, eg, ep, ev, re, r, rp, chk)
	if chk == 0 then
		return e:GetOwner():IsRelateToEffect(e)
	end
	Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, e:GetOwner(), 1, 0, 0)
end
function s.spop(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	if c:IsRelateToEffect(e) then
		if Duel.SpecialSummon(c, 0, tp, tp, false, false, POS_FACEUP_DEFENSE) > 0 then
			Duel.Damage(tp, 1000, REASON_EFFECT)
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_FIELD)
			e1:SetCode(EFFECT_CHANGE_RACE)
			e1:SetRange(LOCATION_MZONE)
			e1:SetTargetRange(LOCATION_MZONE, 0)
			e1:SetValue(RACE_INSECT)
			e1:SetReset(RESET_EVENT + RESETS_STANDARD)
			c:RegisterEffect(e1, true)
		end
	end
end

--coded by Lyris
--"Three-Star Demotion" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(Card.IsLevelAbove, tp, LOCATION_HAND, 0, 1, nil, 2) and c:GetFlagEffect(id) == 0 and Duel.CheckLPCost(tp, 2000) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	local c = e:GetOwner()
	c:SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.PayLPCost(tp, 2000)
	for tc in aux.Next(Duel.GetMatchingGroup(Card.IsLevelAbove, tp, LOCATION_HAND, 0, nil, 2)) do
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_LEVEL)
		e1:SetValue(-3)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		tc:RegisterEffect(e1, true)
	end
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

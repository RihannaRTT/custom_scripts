--coded by Lyris
--"Prescience" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(111004001)
	e2:SetOperation(s.act(c))
	c:RegisterEffect(e2)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		if c:IsOnField() then
			Duel.ChangePosition(c, POS_FACEUP)
		end
		local e3 = Effect.CreateEffect(c)
		e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e3:SetCode(EVENT_FREE_CHAIN)
		e3:SetDescription(1158)
		e3:SetCondition(s.lookcon)
		e3:SetOperation(s.look)
		Duel.RegisterEffect(e3, tp)
	end
end
function s.lookcon(e, tp, eg, ep, ev, re, r, rp)
	return Duel.GetLP(tp) * 2 < Duel.GetLP(1 - tp) and (Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0) > 0 and Duel.GetDecktopGroup(tp, 1):GetFirst():IsFacedown() or Duel.GetFieldGroupCount(tp, 0, LOCATION_DECK) > 0 and Duel.GetDecktopGroup(1 - tp, 1):GetFirst():IsFacedown())
end
function s.look(e, tp, eg, ep, ev, re, r, rp, c, sg)
	local g = Duel.GetDecktopGroup(tp, 1) + Duel.GetDecktopGroup(1 - tp, 1)
	Duel.ConfirmCards(tp, g:Filter(Card.IsFacedown, nil))
end

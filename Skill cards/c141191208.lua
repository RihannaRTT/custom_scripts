--coded by Lyris
--"Grit" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act1(c))
	c:RegisterEffect(e0)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
	e2:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act2(c))
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_CHANGE_DAMAGE)
	e3:SetRange(LOCATION_REMOVED)
	e3:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e3:SetTargetRange(1, 0)
	e3:SetLabel(0)
	e3:SetCondition(s.ndcon(c))
	e3:SetValue(s.val)
	c:RegisterEffect(e3)
	local e4 = Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_PHASE + PHASE_END)
	e4:SetRange(LOCATION_REMOVED)
	e4:SetCountLimit(1)
	e4:SetOperation(s.flip(c))
	c:RegisterEffect(e4)
end
function s.act1(c)
	return function(e, tp)
		if Duel.GetLP(tp) >= 4000 then
			c:SetEntityCode(id)
		end
	end
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLP(tp) >= 4000 and not c:IsOriginalCodeRule(id)
	end
end
function s.act2(c)
	return function()
		c:SetEntityCode(id)
		Duel.Hint(HINT_CARD, 0, id)
	end
end
function s.ndcon(c)
	return function()
		return c:IsOriginalCodeRule(id)
	end
end
function s.val(e, re, val)
	local c = e:GetOwner()
	local tp = e:GetOwnerPlayer()
	local lp = Duel.GetLP(tp)
	if val >= lp and e:GetLabel() == 0 then
		Duel.Hint(HINT_NUMBER, 0, val)
		e:SetLabel(1)
		return lp - 1
	else
		return val
	end
end
function s.flip(c)
	return function()
		if c:IsOriginalCodeRule(id) then
			c:SetEntityCode(111004001)
		end
	end
end

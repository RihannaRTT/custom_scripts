--coded by Lyris
--"Last Gamble" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
	e2:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCondition(s.fcon)
	e2:SetOperation(s.flip(c))
	c:RegisterEffect(e2)
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_ACTIVATE)
	e3:SetCode(EVENT_FREE_CHAIN)
	e3:SetCountLimit(1)
	e3:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e3:SetCondition(s.con(c))
	e3:SetOperation(s.act)
	c:RegisterEffect(e3)
end
function s.fcon(c)
	return Duel.GetTurnCount() == 5
end
function s.flip(c)
	return function()
		c:SetEntityCode(id)
		Duel.Hint(HINT_CARD, 0, id)
	end
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLP(tp) > 100 and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 1 and c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.SetLP(tp, 100)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	Duel.DiscardHand(tp, aux.TRUE, 2, 2, REASON_RULE)
	Duel.Draw(tp, Duel.TossDice(tp, 1), REASON_RULE)
end

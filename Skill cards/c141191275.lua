--coded by Lyris
--"Cyber Style" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLP(tp) <= 3000 and Duel.GetLocationCount(tp, LOCATION_MZONE) > 0 and not c:IsOriginalCodeRule(id)
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	local ft = Duel.GetLocationCount(tp, LOCATION_MZONE)
	for i = 1, math.min(ft, (4000 - Duel.GetLP(tp)) // 1000) do
		local token = Duel.CreateToken(tp, 26439287)
		Duel.MoveToField(token, tp, tp, LOCATION_MZONE, POS_FACEUP_ATTACK, true)
		ft = ft - 1
		if ft <= 0 then
			break
		end
	end
end

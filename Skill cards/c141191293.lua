--coded by Lyris
--"Duel, standby!" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		Duel.Draw(tp, 1, REASON_RULE)
		Duel.Draw(1 - tp, 1, REASON_RULE)
	end
end

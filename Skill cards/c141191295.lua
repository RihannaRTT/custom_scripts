--coded by Lyris
--"Endless Trap Hell" v2
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(Card.IsType, tp, LOCATION_GRAVE, 0, 3, nil, TYPE_TRAP) and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local g = Duel.GetMatchingGroup(Card.IsType, tp, LOCATION_GRAVE, 0, nil, TYPE_TRAP)
	local sc = g:RandomSelect(tp, 1):GetFirst()
	Duel.SendtoHand(sc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, sc)
	g:RemoveCard(sc)
	Duel.BreakEffect()
	Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, REASON_RULE)
end

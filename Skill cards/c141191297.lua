--coded by Lyris
--"Fairy's Smile" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_DRAW)
	e2:SetRange(LOCATION_REMOVED)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.filter(c)
	return c:IsRace(RACE_FAIRY) and c:IsReason(REASON_RULE)
end
function s.con(e, tp, eg)
	return eg:IsExists(s.filter, 1, nil)
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.ConfirmCards(1 - tp, eg:Filter(s.filter, nil):RandomSelect(tp, 1))
	Duel.Recover(tp, 500, REASON_RULE)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

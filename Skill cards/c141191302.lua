--coded by Lyris
--"Fusion Reserves - Roids" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
end
function s.count(e, tp)
	local plp0, plp1 = e:GetLabel()
	local clp0, clp1 = Duel.GetLP(0), Duel.GetLP(1)
	e:SetLabel(clp0, clp1)
	if not plp0 or not plp1 then
		return
	end
	if plp0 > clp0 then
		s[0] = s[0] - clp0 + plp0
	end
	if plp1 > clp1 then
		s[1] = s[1] - clp1 + plp1
	end
end
function s.con(c)
	return function(e, tp)
		return Duel.IsExistingMatchingCard(s.filter1, tp, LOCATION_EXTRA, 0, 1, nil) and s[tp] >= 1800 and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.filter1(c, tp)
	return c:IsSetCard(0x16) and Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_DECK, 0, 1, nil, c)
end
function s.filter2(c, fc)
	return aux.IsMaterialListCode(fc, c:GetCode())
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	s[tp] = 0
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
	local cg = Duel.SelectMatchingCard(tp, s.filter1, tp, LOCATION_EXTRA, 0, 1, 1, nil, tp)
	Duel.ConfirmCards(1 - tp, cg)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, s.filter2, tp, LOCATION_DECK, 0, 1, 1, nil, cg:GetFirst())
	Duel.SendtoHand(g, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, g)
	e:SetLabel(e:GetLabel() + 1)
end

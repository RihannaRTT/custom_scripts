--coded by Lyris
--"Master of Rites" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabel(0)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
end
function s.count(e, tp)
	local plp0, plp1 = e:GetLabel()
	local clp0, clp1 = Duel.GetLP(0), Duel.GetLP(1)
	e:SetLabel(clp0, clp1)
	if not plp0 or not plp1 then
		return
	end
	if plp0 > clp0 then
		s[0] = s[0] - clp0 + plp0
	end
	if plp1 > clp1 then
		s[1] = s[1] - clp1 + plp1
	end
end
function s.con(e, tp)
	return s[tp] >= 1800 and Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0 and e:GetLabel() < 2 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	s[tp] = 0
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_DRAW_COUNT)
	e1:SetTargetRange(1, 0)
	e1:SetReset(RESET_PHASE + PHASE_DRAW)
	e1:SetValue(0)
	Duel.RegisterEffect(e1, tp)
	local mg = Duel.GetMatchingGroup(Card.IsType, tp, LOCATION_DECK, 0, nil, TYPE_RITUAL)
	local b1, b2, typ = mg:IsExists(Card.IsType, 1, nil, TYPE_MONSTER), mg:IsExists(Card.IsType, 1, nil, TYPE_SPELL)
	if b1 and (not b2 or Duel.SelectOption(tp, 70, 71) == 0) then
		typ = TYPE_MONSTER
	else
		typ = TYPE_SPELL
	end
	Duel.MoveSequence(mg:Filter(Card.IsType, nil, typ):RandomSelect(tp, 1):GetFirst(), SEQ_DECKTOP)
	Duel.Draw(tp, 1, REASON_RULE)
	e:SetLabel(e:GetLabel() + 1)
end

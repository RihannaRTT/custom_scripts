--coded by Lyris
--"Middle Age Mechs" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(111004001)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.act(e, tp)
	e:GetOwner():SetEntityCode(id)
	Duel.MoveToField(Duel.CreateToken(tp, 92001300), tp, tp, LOCATION_SZONE, POS_FACEUP, true)
end

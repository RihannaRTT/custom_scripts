--coded by Lyris
--"Moth to the Flame" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetRange(LOCATION_HAND)
	e1:SetCondition(s.spcon(2))
	e1:SetOperation(s.spop(2))
	local e2 = e1:Clone()
	e2:SetCondition(s.spcon(3))
	e2:SetOperation(s.spop(3))
	local e6 = e1:Clone()
	e6:SetCondition(s.spcon(1))
	e6:SetOperation(s.spop(1))
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_GRANT)
	e3:SetRange(LOCATION_REMOVED)
	e3:SetTargetRange(LOCATION_HAND, 0)
	e3:SetTarget(aux.TargetBoolFunction(Card.IsCode, 14141448))
	e3:SetLabelObject(e1)
	c:RegisterEffect(e3)
	local e4 = e3:Clone()
	e4:SetTarget(aux.TargetBoolFunction(Card.IsCode, 48579379))
	e4:SetLabelObject(e2)
	c:RegisterEffect(e4)
	local e7 = e3:Clone()
	e7:SetTarget(aux.TargetBoolFunction(Card.IsCode, 87756343))
	e7:SetLabelObject(e6)
	c:RegisterEffect(e7)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
	end
end
function s.eqfilter(c, n)
	return c:IsCode(40240595) and c:GetTurnCounter() >= n
end
function s.rfilter(c, n)
	return c:IsCode(58192742) and c:GetEquipGroup():IsExists(s.eqfilter, 1, nil, n)
end
function s.spcon(n)
	return function(e, c)
		if c == nil then
			return true
		end
		local tp = c:GetControler()
		return Duel.GetLocationCount(tp, LOCATION_MZONE) > -1 and Duel.CheckReleaseGroup(tp, s.rfilter, 1, nil, n)
	end
end
function s.spop(n)
	return function(e, tp, eg, ep, ev, re, r, rp, c)
		local g = Duel.SelectReleaseGroup(c:GetControler(), s.rfilter, 1, 1, nil, n)
		Duel.Release(g, REASON_COST)
	end
end

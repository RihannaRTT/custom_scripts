--coded by Lyris
--"No Mortal Can Resist" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.con(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	return Duel.GetLP(tp) + 1000 <= Duel.GetLP(1 - tp) and Duel.IsExistingMatchingCard(Card.IsType, tp, 0, LOCATION_GRAVE, 1, nil, TYPE_MONSTER) and c:GetFlagEffect(id) == 0 and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
end
function s.act(e, tp, eg, ep, ev, re, r, rp, chk)
	Duel.Hint(HINT_CARD, 0, id)
	for tc in aux.Next(Duel.GetMatchingGroup(Card.IsType, tp, 0, LOCATION_GRAVE, nil, TYPE_MONSTER)) do
		tc:ResetEffect(tc:GetOriginalCode(), RESET_CARD)
		tc:SetEntityCode(32274490)
	end
	e:GetOwner():RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
end

--coded by Lyris
--"Ojama Go!" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
	if not s.global_check then
		s.global_check = true
		s[0] = 0
		s[1] = 0
		local e5 = Effect.CreateEffect(c)
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetCode(EVENT_ADJUST)
		e5:SetOperation(s.count)
		Duel.RegisterEffect(e5, 0)
	end
end
function s.count(e, tp)
	local plp0, plp1 = e:GetLabel()
	local clp0, clp1 = Duel.GetLP(0), Duel.GetLP(1)
	e:SetLabel(clp0, clp1)
	if not plp0 or not plp1 then
		return
	end
	if plp0 > clp0 then
		s[0] = s[0] - clp0 + plp0
	end
	if plp1 > clp1 then
		s[1] = s[1] - clp1 + plp1
	end
end
function s.con(e, tp)
	return s[tp] >= 1800 and Duel.GetLocationCount(1 - tp, LOCATION_MZONE) > 0 and Duel.IsPlayerCanSpecialSummonMonster(tp, 29843092, 0xf, 0x4011, 0, 1000, 2, RACE_BEAST, ATTRIBUTE_LIGHT, POS_FACEUP_DEFENSE, 1 - tp) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	s[tp] = 0
	if Duel.SpecialSummonStep(Duel.CreateToken(tp, 29843092), tp, 1 - tp, false, false, POS_FACEUP_DEFENSE) then
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UNRELEASABLE_SUM)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD)
		e1:SetValue(1)
		token:RegisterEffect(e1, true)
		local e2 = Effect.CreateEffect(c)
		e2:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
		e2:SetCode(EVENT_LEAVE_FIELD)
		e2:SetOperation(s.damop)
		token:RegisterEffect(e2, true)
	end
	Duel.SpecialSummonComplete()
end
function s.damop(e, tp)
	local c = e:GetOwner()
	if c:IsReason(REASON_DESTROY) then
		Duel.Damage(c:GetPreviousControler(), 300, REASON_EFFECT)
	end
	e:Reset()
end

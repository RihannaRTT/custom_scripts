--coded by Lyris
--"Restart" v2
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_ACTIVATE)
	e0:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e0:SetCountLimit(1)
	e0:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e0:SetCondition(s.con)
	e0:SetOperation(s.act)
	c:RegisterEffect(e0)
end
function s.con(_, tp)
	return Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0) > 0
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
	Duel.ConfirmCards(1 - tp, hg)
	Duel.SendtoDeck(hg, nil, SEQ_DECKBOTTOM, REASON_RULE)
	Duel.ShuffleDeck(tp)
	Duel.Draw(tp, #hg, REASON_RULE)
end

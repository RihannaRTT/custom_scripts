--coded by Lyris
--"Shadow Game" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_TURN_END)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.act(e)
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	local tp = Duel.GetTurnPlayer()
	Duel.Damage(tp, Duel.GetFieldGroupCount(tp, LOCATION_GRAVE, 0) * 100, REASON_RULE)
end

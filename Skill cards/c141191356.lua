--coded by Lyris
--"Silent as WATER" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetLP(tp) <= 2000 and not c:IsOriginalCodeRule(id) and Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2)
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	local fc = Duel.GetFieldCard(tp, LOCATION_FZONE, 0)
	if fc then
		Duel.SendtoHand(fc, tp, REASON_RULE)
	end
	Duel.MoveToField(Duel.CreateToken(tp, 82999629), tp, tp, LOCATION_FZONE, POS_FACEUP, true)
end

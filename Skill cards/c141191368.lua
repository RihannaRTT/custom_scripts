--coded by Lyris
--"Titan Showdown" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e5 = Effect.CreateEffect(c)
	e5:SetType(EFFECT_TYPE_FIELD)
	e5:SetCode(EFFECT_CHANGE_BATTLE_DAMAGE)
	e5:SetRange(LOCATION_REMOVED)
	e5:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e5:SetTargetRange(1, 1)
	e5:SetValue(s.val)
	c:RegisterEffect(e5)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
	end
end
function s.val(e)
	local p = e:GetHandlerPlayer()
	if Duel.GetLP(p) >= Duel.GetLP(1 - p) * 2 then
		return aux.ChangeBattleDamage(1, DOUBLE_DAMAGE)(e, p)
	elseif Duel.GetLP(1 - p) >= Duel.GetLP(p) * 2 then
		return aux.ChangeBattleDamage(0, DOUBLE_DAMAGE)(e, p)
	end
end

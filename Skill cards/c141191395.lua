--coded by Lyris
--"Cubic Seeds" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.xfilter(c)
	return not c:IsSetCard(0xe3)
end
function s.act(c)
	return function(e, tp)
		if Duel.IsExistingMatchingCard(s.xfilter, tp, LOCATION_DECK + LOCATION_EXTRA + LOCATION_HAND, 0, 1, nil) then
			return
		end
		c:SetEntityCode(id)
		for i = 1, 3 do
			Duel.SendtoDeck(Duel.CreateToken(tp, 15610297), nil, SEQ_DECKSHUFFLE, REASON_RULE)
		end
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.Draw(tp, #hg, REASON_RULE)
		else
			Duel.ShuffleDeck(tp)
		end
	end
end

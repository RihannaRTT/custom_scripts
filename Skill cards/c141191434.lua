--coded by Lyris
--"Hero Flash!!" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetCondition(s.con(e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter(c)
	return c:IsType(TYPE_NORMAL) and c:IsSetCard(0x8)
end
function s.chk(e, tp)
	e0:SetLabel(Duel.GetMatchingGroupCount(s.filter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil))
end
function s.con(ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and Duel.GetTurnCount(tp) % 2 == 0 and Duel.GetTurnCount(tp) < 11 and e0:GetLabel() > 3
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	c:SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	local t = { 74825788, 213326, 37318031, 63703130, 191749 }
	Duel.SendtoHand(Duel.CreateToken(tp, t[Duel.GetTurnCount(tp) / 2]), nil, REASON_RULE)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

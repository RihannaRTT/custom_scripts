--coded by Lyris
--"Let's Go Goyo!" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		for i = 1, 2 do
			Duel.SendtoDeck(Duel.CreateToken(tp, 60410769), nil, SEQ_DECKSHUFFLE, REASON_RULE)
		end
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.Draw(tp, #hg, REASON_RULE)
		else
			Duel.ShuffleDeck(tp)
		end
		Duel.BreakEffect()
		Duel.Exile(Duel.GetFieldGroup(tp, LOCATION_EXTRA, 0), REASON_RULE)
		Duel.SendtoDeck(Duel.CreateToken(tp, 7391448), nil, SEQ_DECKTOP, REASON_RULE)
	end
end

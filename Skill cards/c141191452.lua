--coded by Lyris
--"Light and Dark" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetLabel(0)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.filter1(c, tp)
	return c:IsAttribute(ATTRIBUTE_DARK + ATTRIBUTE_LIGHT) and Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_DECK, 0, 1, nil, c)
end
function s.filter2(c, tc)
	return not c:IsAttribute(tc:GetAttribute()) and c:IsRace(tc:GetRace()) and c:IsLevel(tc:GetLevel())
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter1, tp, LOCATION_HAND, 0, 1, nil, tp) and e:GetLabel() < 2 and c:GetFlagEffect(id) == 0
	end
end
function s.act(e, tp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.SelectMatchingCard(tp, s.filter1, tp, LOCATION_HAND, 0, 1, 1, nil, tp):GetFirst()
	Duel.ConfirmCards(1 - tp, tc)
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local g = Duel.SelectMatchingCard(tp, s.filter2, tp, LOCATION_DECK, 0, 1, 1, nil, tc)
	Duel.SendtoHand(g, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, g)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end

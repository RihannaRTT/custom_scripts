--coded by Lyris
--"Mark of the Dragon - Head" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.con(c))
	c:RegisterEffect(e0)
end
function s.con(c)
	return function(e, tp)
		c:SetEntityCode(id)
		for i = 1, 2 do
			Duel.SendtoDeck(Duel.CreateToken(tp, 63977008), nil, SEQ_DECKBOTTOM, REASON_RULE)
		end
		if Duel.GetTurnCount() > 1 then
			local hg = Duel.GetFieldGroup(tp, LOCATION_HAND, 0)
			Duel.SendtoDeck(hg, nil, SEQ_DECKSHUFFLE, REASON_RULE)
			Duel.ShuffleDeck(tp)
			Duel.Draw(tp, #hg, REASON_RULE)
		else
			Duel.ShuffleDeck(tp)
		end
		Duel.BreakEffect()
		Duel.SendtoDeck(Duel.CreateToken(tp, 44508094), nil, SEQ_DECKTOP, REASON_RULE)
	end
end

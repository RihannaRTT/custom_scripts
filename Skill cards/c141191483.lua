--coded by Lyris
--"One-Card Wonder" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e0:SetCode(EVENT_PHASE_START + PHASE_DRAW)
	e0:SetCountLimit(1)
	e0:SetProperty(EFFECT_FLAG_NO_TURN_RESET)
	e0:SetOperation(s.ret)
	e0:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetCode(EFFECT_DRAW_COUNT)
		e1:SetTargetRange(1, 0)
		e1:SetValue(2)
		Duel.RegisterEffect(e1, tp)
	end
end
function s.ret(e, tp)
	local ht = Duel.GetFieldGroupCount(tp, LOCATION_HAND, 0)
	if ht > 1 then
		Duel.SendtoDeck(Duel.GetFieldGroup(tp, LOCATION_HAND, 0):RandomSelect(tp, ht - 1), nil, SEQ_DECKBOTTOM, REASON_RULE)
	end
end

--coded by Lyris
--"Onomatoplay" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetMatchingGroup(Card.IsSetCard, tp, LOCATION_HAND, 0, nil):GetClassCount(Card.GetCode) > 1 and Duel.GetMatchingGroup(Card.IsSetCard, tp, LOCATION_DECK, 0, nil, 0x8f, 0x54, 0x59, 0x82):GetClassCount(Card.GetCode) > 1
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
	local tc = Duel.GetMatchingGroup(Card.IsSetCard, tp, LOCATION_HAND, 0, nil, 0x8f, 0x54, 0x59, 0x82):SelectSubGroup(tp, aux.dncheck, false, 2, 2) or Group.CreateGroup()
	Duel.ConfirmCards(1 - tp, tc)
	Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local sc = Duel.GetMatchingGroup(Card.IsSetCard, tp, LOCATION_DECK, 0, nil, 0x8f, 0x54, 0x59, 0x82):SelectSubGroup(tp, aux.dncheck, false, 2, 2) or Group.CreateGroup()
	Duel.SendtoHand(sc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, sc)
end

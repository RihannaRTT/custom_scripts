--coded by Lyris
--"Three Lord Pillars" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		local g = Duel.GetMatchingGroup(Card.IsCode, tp, LOCATION_DECK, 0, nil, 130000049, 130000051)
		if g:GetClassCount(Card.GetCode) < 2 then
			return
		end
		c:SetEntityCode(id)
		Duel.MoveToField(Duel.CreateToken(tp, 130000040), tp, tp, LOCATION_SZONE, POS_FACEDOWN, true)
		for tc in aux.Next(g) do
			Duel.MoveSequence(tc, SEQ_DECKBOTTOM)
		end
	end
end

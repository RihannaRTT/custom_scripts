--coded by Lyris
--"Ultimate Dragons" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		c:SetEntityCode(id)
		for _, i in ipairs { 23995346, 56532353 } do
			Duel.SendtoDeck(Duel.CreateToken(tp, i), nil, SEQ_DECKTOP, REASON_RULE)
		end
	end
end

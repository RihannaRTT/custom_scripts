--coded by Lyris
--"You're Getting On My Nerves!" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local ge1 = Effect.CreateEffect(c)
	ge1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	ge1:SetCode(EVENT_DESTROYED)
	ge1:SetCondition(s.regcon)
	ge1:SetOperation(s.reg)
	ge1:SetRange(LOCATION_REMOVED)
	c:RegisterEffect(ge1)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetLabel(0)
	e2:SetCondition(s.con(c))
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.cfilter(c, tp)
	return c:IsReason(REASON_BATTLE + REASON_EFFECT) and c:GetPreviousControler() == tp
end
function s.regcon(e, tp, eg, ep, ev, re, r, rp)
	local v = 0
	if eg:IsExists(s.cfilter, 1, nil, 0) then
		v = v + 1
	end
	if eg:IsExists(s.cfilter, 1, nil, 1) then
		v = v + 2
	end
	if v == 0 then
		return false
	end
	e:SetLabel(({ 0, 1, PLAYER_ALL })[v])
	return true
end
function s.reg(e, tp)
	local ep = e:GetLabel()
	if ep == PLAYER_ALL or ep == tp then
		Duel.RegisterFlagEffect(tp, id, RESET_PHASE + PHASE_END, 0, Duel.GetTurnPlayer() == tp and 3 or 2)
	end
	if ep == PLAYER_ALL or ep ~= tp then
		Duel.RegisterFlagEffect(ep, id, RESET_PHASE + PHASE_END, 0, Duel.GetTurnPlayer() ~= tp and 3 or 2)
	end
end
function s.filter(c)
	return c:IsFaceup() and c:IsRankBelow(4) and c:IsAttribute(ATTRIBUTE_WATER) and c:IsType(TYPE_XYZ)
end
function s.con(c)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.GetFlagEffect(tp, id) > 0 and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, LOCATION_MZONE, 1, nil) and e:GetLabel() < 2 and c:GetFlagEffect(id) == 0
	end
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetOwner()
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, LOCATION_MZONE, 1, 1, nil):GetFirst()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetValue(500)
	e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
	tc:RegisterEffect(e1, true)
	c:RegisterFlagEffect(id, RESET_PHASE + PHASE_END, 0, 1)
	e:SetLabel(e:GetLabel() + 1)
end

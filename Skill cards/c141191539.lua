--coded by Lyris
--"Pathway to Chaos" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_ACTIVATE)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCondition(s.con)
	e2:SetOperation(s.act)
	c:RegisterEffect(e2)
end
function s.filter(c, tp)
	return c:IsFaceup() and (c:IsOriginalSetCard(0xbd) or c:IsOriginalCodeRule(5405694))
end
function s.cfilter(c)
	return c:IsType(TYPE_MONSTER) and c:IsSetCard(0xa4)
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_MZONE, 0, 1, nil, tp) or Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil)
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.DiscardHand(tp, s.cfilter, 1, 1, REASON_RULE)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_FACEUP)
	local g = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_MZONE, 0, 1, 1, nil, tp)
	Duel.HintSelection(g)
	local tc = g:GetFirst()
	local code
	if tc:IsOriginalSetCard(0xbd) then
		code = 5405694
	elseif tc:IsOriginalCodeRule(5405694) then
		code = 54484652
	else
		return
	end
	tc:SetEntityCode(code)
	tc:ReplaceEffect(code, 0)
	Duel.SetMetatable(tc, _G["c" .. tostring(code)])
	tc:ResetEffect(code, RESET_CARD)
	tc:ResetEffect(code, RESET_CARD)
	_G["c" .. tostring(code)]["initial_effect"](tc)
end

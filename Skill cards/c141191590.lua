--coded by Lyris
--"Gimmick Puppet - Replacement" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.chk)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.con(c, e0))
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.chk(e, tp)
	e:SetLabel(Duel.GetMatchingGroupCount(s.xfilter, tp, LOCATION_DECK + LOCATION_HAND, 0, nil))
end
function s.con(c, ef)
	return function(e, tp)
		return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not c:IsOriginalCodeRule(id) and Duel.GetMatchingGroup(Card.IsLevelAbove, tp, LOCATION_HAND, 0, nil):CheckSubGroup(s.check, 1, 3) and Duel.IsExistingMatchingCard(s.filter, tp, LOCATION_DECK, 0, 1, nil) and ef:GetLabel() == 0
	end
end
function s.act(e, tp)
	Duel.Hint(HINT_CARD, 0, id)
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
	for cc in aux.Next(Duel.GetMatchingGroup(Card.IsLevelAbove, tp, LOCATION_HAND, 0, nil, 1):SelectSubGroup(tp, s.check, false, 1, 3) or Group.CreateGroup()) do
		Duel.SendtoGrave(cc, REASON_RULE)
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
	local tc = Duel.SelectMatchingCard(tp, s.filter, tp, LOCATION_DECK, 1, 1, nil):GetFirst()
	Duel.BreakEffect()
	Duel.SendtoHand(tc, nil, REASON_RULE)
	Duel.ConfirmCards(1 - tp, tc)
end
function s.xfilter(c)
	return c:IsType(TYPE_MONSTER) and not (c:IsAttribute(ATTRIBUTE_DARK) and c:IsRace(RACE_MACHINE))
end
function s.check(g)
	return g:CheckWithSumEqual(Card.GetLevel, 8, #g, #g)
end
function s.filter(c)
	return c:IsLevel(8) and c:IsSetCard(0x1083)
end

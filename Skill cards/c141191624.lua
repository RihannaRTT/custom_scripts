--coded by Lyris
--"Swing Into Action: Pendulum Summon!" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
end
function s.act(c)
	return function(e, tp)
		if Duel.IsExistingMatchingCard(Card.IsType, tp, LOCATION_HAND + LOCATION_DECK, 0, 12, nil, TYPE_PENDULUM) then
			c:SetEntityCode(id)
			local e1 = Effect.CreateEffect(c)
			e1:SetType(EFFECT_TYPE_FIELD)
			e1:SetCode(id)
			e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
			e1:SetTargetRange(1, 0)
			Duel.RegisterEffect(e1, tp)
		end
	end
end

--coded by XGlitchy30
--"Memories of a Pharaoh: Obelisk the Tormentor" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PREDRAW)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetLabelObject(e0)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.con(e, tp)
	return Duel.GetTurnCount() >= 3 and Duel.GetTurnPlayer() == tp and not e:GetOwner():IsOriginalCodeRule(id) and Duel.GetDrawCount(tp) > 0
end
function s.act(e, tp, eg, ep, ev, re, r, rp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	local dt = Duel.GetDrawCount(tp)
	if dt ~= 0 then
		_replace_count = 0
		_replace_max = dt
		local e1 = Effect.GlobalEffect()
		e1:SetType(EFFECT_TYPE_FIELD)
		e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
		e1:SetCode(EFFECT_DRAW_COUNT)
		e1:SetTargetRange(1, 0)
		e1:SetReset(RESET_PHASE + PHASE_DRAW)
		e1:SetValue(0)
		Duel.RegisterEffect(e1, tp)
	end
	_replace_count = _replace_count + 1
	if _replace_count > _replace_max then
		return
	end
	local obelisk = Duel.CreateToken(tp, 10000000)
	Duel.SendtoHand(obelisk, nil, REASON_RULE + REASON_DRAW)
	Duel.ConfirmCards(1 - tp, Group.FromCards(obelisk))
	local e4 = Effect.CreateEffect(e:GetOwner())
	e4:SetType(EFFECT_TYPE_FIELD)
	e4:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE + EFFECT_FLAG_SET_AVAILABLE)
	e4:SetCode(EFFECT_TRIPLE_TRIBUTE)
	e4:SetTargetRange(LOCATION_MZONE, LOCATION_MZONE)
	e4:SetCountLimit(1)
	e4:SetCondition(s.dtcon)
	e4:SetTarget(s.dttg)
	e4:SetValue(s.dtval)
	e4:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e4, tp)
	local e5 = Effect.CreateEffect(e:GetOwner())
	e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e5:SetCode(EVENT_BE_MATERIAL)
	e5:SetCondition(s.sumcon)
	e5:SetOperation(s.sumop)
	e5:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e5, tp)
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end
function s.dtcon(e)
	return Duel.GetFlagEffect(e:GetOwnerPlayer(), id) <= 0
end
function s.dttg(e, c)
	return c:IsOriginalCodeRule(89631139, 120120000) and (c:IsFaceup() or c:IsControler(e:GetOwnerPlayer()))
end
function s.dtval(e, c)
	return c:IsCode(10000000) and c:IsControler(e:GetOwnerPlayer())
end
function s.prefilter(c)
	local code1, code2 = c:GetPreviousCodeOnField()
	return code1 == 89631139 or code2 == 89631139 and code1 == 120120000 or code2 == 120120000
end
function s.sumfilter(c)
	return s.prefilter(c) and r == REASON_SUMMON and c:GetReasonCard():IsCode(10000000) and c:GetReasonCard():GetMaterial():FilterCount(s.prefilter, nil) == 1
end
function s.sumcon(e, tp, eg, ep, ev, re, r, rp)
	return eg:IsExists(s.sumfilter, 1, nil)
end
function s.sumop(e, tp, eg, ep, ev, re, r, rp)
	local rc = eg:Filter(s.sumfilter, nil):GetFirst():GetReasonCard()
	if rc and rc:IsOnField() and rc:IsFaceup() then
		rc:RegisterFlagEffect(id, RESET_EVENT + RESETS_STANDARD, 0, 1)
		local e5 = Effect.CreateEffect(e:GetOwner())
		e5:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
		e5:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
		e5:SetCode(EVENT_PHASE + PHASE_END)
		e5:SetCountLimit(1)
		e5:SetLabelObject(rc)
		e5:SetCondition(s.tgcon)
		e5:SetOperation(s.tgop)
		Duel.RegisterEffect(e5, tp)
	end
	if rc:GetMaterial():GetCount() == 1 and rc:GetMaterial():IsExists(s.prefilter, 1, nil) then
		Duel.RegisterFlagEffect(tp, id, RESET_PHASE + PHASE_END, 0, 1)
	end
end
function s.tgcon(e, tp, eg, ep, ev, re, r, rp)
	if Duel.GetTurnPlayer() ~= tp then
		return false
	end
	local tc = e:GetLabelObject()
	if tc:GetFlagEffect(id) ~= 0 then
		return true
	else
		e:Reset()
		return false
	end
end
function s.tgop(e, tp, eg, ep, ev, re, r, rp)
	local tc = e:GetLabelObject()
	Duel.SendtoGrave(tc, REASON_RULE)
end

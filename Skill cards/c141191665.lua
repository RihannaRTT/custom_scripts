--coded by XGlitchy30
--"Red-Eyes Roulette" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.checkop)
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetLabelObject(e0)
	e1:SetCondition(s.con)
	e1:SetOperation(s.act)
	c:RegisterEffect(e1)
end
function s.checkop(e, tp)
	local g1 = Duel.GetMatchingGroup(Card.IsSetCard, tp, LOCATION_DECK, 0, nil, 0x3b)
	local g2 = g1:Filter(Card.IsType, nil, TYPE_MONSTER)
	if #g1 >= 10 and #g2 >= 5 then
		e:SetLabel(100)
	end
end
function s.rvfilter(c)
	return c:IsFaceup() and c:IsCode(94415058, 20409757)
end
function s.thfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_PENDULUM) and not c:IsExtraDeckMonster()
end
function s.con(e, tp)
	return Duel.GetTurnPlayer() == tp and (Duel.GetCurrentPhase() == PHASE_MAIN1 or Duel.GetCurrentPhase() == PHASE_MAIN2) and not e:GetOwner():IsOriginalCodeRule(id) and e:GetLabelObject():GetLabel() == 100 and Duel.GetTurnCount() > 3
end
function s.act(e, tp)
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 15)) then
		return
	end
	e:GetOwner():SetEntityCode(id)
	Duel.Hint(HINT_CARD, 0, id)
	local codes = { 39357122, OPCODE_ISCODE, 17871506, OPCODE_ISCODE, OPCODE_OR, 85651167, OPCODE_ISCODE, OPCODE_OR }
	if Duel.IsExistingMatchingCard(Card.IsSetCard, tp, LOCATION_DECK, 0, 1, nil, 0x3b) then
		local extracodes = { 70781052, 64271667, 423705 }
		for i = 1, #extracodes do
			table.insert(codes, extracodes[i])
			table.insert(codes, OPCODE_ISCODE)
			table.insert(codes, OPCODE_OR)
		end
	end
	Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CODE)
	local code = Duel.AnnounceCard(tp, table.unpack(codes))
	local tc = Duel.CreateToken(tp, code)
	if tc and Duel.SendtoDeck(tc, nil, SEQ_DECKSHUFFLE, REASON_RULE) > 0 and tc:IsLocation(LOCATION_DECK + LOCATION_EXTRA) then
		if tc:IsLocation(LOCATION_DECK) then
			Duel.ShuffleDeck(tp)
		end
		local g1 = Duel.GetMatchingGroup(Card.IsSetCard, tp, LOCATION_DECK, 0, nil, 0x3b)
		if #g1 > 0 then
			local rng = g1:RandomSelect(tp, 1):GetFirst()
			Duel.SendtoHand(rng, nil, REASON_RULE)
			Duel.ConfirmCards(1 - tp, rng)
			Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
			local g2 = Duel.SelectMatchingCard(tp, Card.IsSetCard, tp, LOCATION_HAND, 0, 1, 1, nil, 0x3b)
			if #g2 > 0 then
				Duel.ConfirmCards(1 - tp, g2)
				Duel.SendtoDeck(g2:GetFirst(), nil, SEQ_DECKSHUFFLE, REASON_RULE)
			end
		end
	end
	Duel.SetChainLimitTillChainEnd(aux.FALSE)
end

--coded by Lyris
--"Holly Angel Trick" v1
local s, id, o = GetID()
function s.initial_effect(c)
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(111004001)
	e0:SetOperation(s.act(c))
	c:RegisterEffect(e0)
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CHANGE_DAMAGE)
	e1:SetRange(LOCATION_REMOVED)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 1)
	e1:SetValue(s.damval)
	c:RegisterEffect(e1)
end
function s.act(c)
	return function()
		c:SetEntityCode(id)
	end
end
function s.damval(e, re, val, r, rp, rc)
	if (re and re:GetHandler():IsOriginalCodeRule(32448765) or rc and rc:IsOriginalCodeRule(32448765)) and r & REASON_EFFECT > 0 then
		return val * 2
	end
	return val
end

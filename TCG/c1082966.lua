--Pyro Clock of Destiny
function c1082966.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(c1082966.target)
	e1:SetOperation(c1082966.activate)
	c:RegisterEffect(e1)
end
function c1082966.filter(c)
	return c:GetFlagEffect(1082946)~=0
end
function c1082966.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c1082966.filter,tp,0x3f,0x3f,1,nil) end
end
function c1082966.activate(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,aux.Stringid(1082966,0))
	local g=Duel.GetMatchingGroup(c1082966.filter,tp,0x3f,0x3f,1,1,nil)
	for tc in aux.Next(g) do
		local turne=tc[tc]
		local op=turne:GetOperation()
		if op then op(turne,turne:GetOwnerPlayer(),nil,0,0,0,0,0) end
	end
end

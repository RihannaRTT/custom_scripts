--Tornado Wall
function c18605155.initial_effect(c)
	aux.AddCodeList(c,22702055)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(c18605155.actcon)
	c:RegisterEffect(e1)
	--avoid battle damage
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
	e2:SetRange(LOCATION_SZONE)
	e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e2:SetTargetRange(1,0)
	e2:SetCondition(c18605155.abdcon)
	c:RegisterEffect(e2)
	--self destroy
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e2:SetRange(LOCATION_SZONE)
	e2:SetCode(EVENT_LEAVE_FIELD)
	e2:SetCondition(c18605155.sdcon)
	e2:SetOperation(function() Duel.Destroy(c,REASON_EFFECT) end)
	c:RegisterEffect(e2)
end
function c18605155.check()
	return Duel.IsEnvironment(22702055)
end
function c18605155.actcon(e,tp,eg,ep,ev,re,r,rp)
	return c18605155.check()
end
function c18605155.abdcon(e)
	local at=Duel.GetAttackTarget()
	return c18605155.check() and (at==nil or at:IsAttackPos() or Duel.GetAttacker():GetAttack()>at:GetDefense())
end
function c18605155.filter(c)
	return c:IsPreviousPosition(POS_FACEUP) and c:IsCode(22702055)
end
function c18605155.sdcon(e,tp,eg)
	return eg:IsExists(c18605155.filter,1,nil)
end

--Second Coin Toss
function c36562647.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	c:RegisterEffect(e1)
	--coin
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e2:SetRange(LOCATION_SZONE)
	e2:SetCode(EVENT_TOSS_COIN_NEGATE)
	e2:SetCondition(c36562647.coincon)
	e2:SetOperation(c36562647.coinop)
	c:RegisterEffect(e2)
end
c36562647.toss_coin=true
function c36562647.coincon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetFlagEffect(tp,36562647)==0
end
function c36562647.coinop(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetFlagEffect(tp,36562647)~=0 then return end
	if Duel.SelectYesNo(tp,aux.Stringid(36562647,0)) then
		Duel.Hint(HINT_CARD,0,36562647)
		Duel.RegisterFlagEffect(tp,36562647,RESET_PHASE+PHASE_END,0,1)
		Duel.TossCoin(ep,ev)
	end
end

--Mistaken Arrest
function c4227116.initial_effect(c)
	--activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetOperation(c4227116.activate)
	c:RegisterEffect(e1)
end
function c4227116.activate(e,tp,eg,ep,ev,re,r,rp)
	local e1=Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_TO_HAND)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetLabel(tp)
	e1:SetTargetRange(1,0)
	e1:SetTarget(c4227116.target)
	if Duel.GetTurnPlayer()==tp then
		e1:SetReset(RESET_PHASE+PHASE_END+RESET_SELF_TURN,2)
	else
		e1:SetReset(RESET_PHASE+PHASE_END+RESET_SELF_TURN)
	end
	Duel.RegisterEffect(e1,tp)
	local e2=e1:Clone()
	e2:SetLabel(1-tp)
	e2:SetTargetRange(0,1)
	Duel.RegisterEffect(e2,tp)
end
function c4227116.target(e,c)
	return c:IsLocation(LOCATION_DECK) and c:IsControler(e:GetLabel())
end

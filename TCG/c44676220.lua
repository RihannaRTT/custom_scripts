--Hero Barrier
function c44676220.initial_effect(c)
	--negate attack
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(c44676220.condition)
	e1:SetOperation(c44676220.operation)
	c:RegisterEffect(e1)
end
function c44676220.filter(c)
	return c:IsFaceup() and c:IsSetCard(0x3008)
end
function c44676220.condition(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetTurnPlayer()~=tp and Duel.GetAttacker()
		and Duel.IsExistingMatchingCard(c44676220.filter,tp,LOCATION_MZONE,0,1,nil)
end
function c44676220.operation(e,tp,eg,ep,ev,re,r,rp)
	if Duel.IsExistingMatchingCard(c44676220.filter,tp,LOCATION_MZONE,0,1,nil) then
		Duel.NegateAttack()
	end
end

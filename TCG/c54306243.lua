--Venom Swamp
function c54306243.initial_effect(c)
	--Activate
	local t={}
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(function(_,tp,eg,ep,ev,re,r,rp,chk) if chk==0 then return true end t={} for tc in aux.Next(Duel.GetFieldGroup(tp,LOCATION_MZONE,LOCATION_MZONE)) do t[tc]=tc:GetAttack() end end)
	e1:SetOperation(function(_,tp)
		Duel.Destroy(Duel.GetMatchingGroup(c54306243.filter,tp,LOCATION_MZONE,LOCATION_MZONE,nil,t),REASON_EFFECT)
	end)
	c:RegisterEffect(e1)
	--Add counter
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(54306243,0))
	e2:SetCategory(CATEGORY_COUNTER)
	e2:SetType(EFFECT_TYPE_TRIGGER_F+EFFECT_TYPE_FIELD)
	e2:SetCode(EVENT_PHASE+PHASE_END)
	e2:SetCountLimit(1)
	e2:SetRange(LOCATION_FZONE)
	e2:SetOperation(c54306243.acop)
	c:RegisterEffect(e2)
	--atk down
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_UPDATE_ATTACK)
	e3:SetRange(LOCATION_FZONE)
	e3:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE)
	e3:SetValue(c54306243.atkval)
	c:RegisterEffect(e3)
	--destroy
	local e4=Effect.CreateEffect(c)
	e4:SetCategory(CATEGORY_DESTROY)
	e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e4:SetCode(EVENT_CUSTOM+54306223)
	e4:SetRange(LOCATION_FZONE)
	e4:SetOperation(c54306243.desop)
	c:RegisterEffect(e4)
end
function c54306243.atkval(e,c)
	return c:GetCounter(0x1009)*-500
end
function c54306243.acop(e,tp,eg,ep,ev,re,r,rp)
	local g=Group.CreateGroup()
	local tg=Duel.GetFieldGroup(tp,LOCATION_MZONE,LOCATION_MZONE)
	local tc=tg:GetFirst()
	while tc do
		if tc:IsCanAddCounter(0x1009,1) and not tc:IsSetCard(0x50) then
			local atk=tc:GetAttack()
			tc:AddCounter(0x1009,1)
			if atk>0 and tc:IsAttack(0) then
				g:AddCard(tc)
			end
		end
		tc=tg:GetNext()
	end
	Duel.Destroy(g,REASON_EFFECT)
end
function c54306243.filter(c,t)
	return c:GetCounter(0x1009)*500>=t[c]
end
function c54306243.desop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Destroy(eg,REASON_EFFECT)
end

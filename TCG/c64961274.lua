--Goddess Verdande's Guidance
function c64961274.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_TOHAND+CATEGORY_SEARCH)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1,64961274+EFFECT_COUNT_CODE_OATH)
	e1:SetOperation(c64961274.activate)
	c:RegisterEffect(e1)
	--see top
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(64961274,1))
	e3:SetType(EFFECT_TYPE_IGNITION)
	e3:SetCountLimit(1)
	e3:SetRange(LOCATION_SZONE)
	e3:SetTarget(c64961274.target)
	e3:SetOperation(c64961274.operation)
	c:RegisterEffect(e3)
end
function c64961274.thcfilter(c)
	return c:IsFacedown() or not c:IsSetCard(0x122)
end
function c64961274.thcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetFieldGroupCount(tp,LOCATION_MZONE,0)>0
		and not Duel.IsExistingMatchingCard(c64961274.thcfilter,tp,LOCATION_MZONE,0,1,nil)
end
function c64961274.thfilter(c)
	return c:IsCode(91969909) and c:IsAbleToHand()
end
function c64961274.activate(e,tp,eg,ep,ev,re,r,rp)
	local g=Duel.GetMatchingGroup(c64961274.thfilter,tp,LOCATION_DECK,0,nil)
	if #g>0 and c64961274.thcon(e,tp,eg,ep,ev,re,r,rp) and
		Duel.SelectYesNo(tp,aux.Stringid(64961274,0)) then
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
		local sg=g:Select(tp,1,1,nil)
		Duel.SendtoHand(sg,nil,REASON_EFFECT)
		Duel.ConfirmCards(1-tp,sg)
	end
end
function c64961274.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetFieldGroupCount(tp,0,LOCATION_DECK)>0 end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_CARDTYPE)
	Duel.SetTargetParam(Duel.AnnounceType(tp))
end
function c64961274.operation(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetFieldGroupCount(tp,0,LOCATION_DECK)<=0 then return end
	Duel.ConfirmDecktop(1-tp,1)
	local g=Duel.GetDecktopGroup(1-tp,1)
	local tc=g:GetFirst()
	Duel.DisableShuffleCheck()
	local opt=Duel.GetChainInfo(0,CHAININFO_TARGET_PARAM)
	if opt==0 and tc:IsType(TYPE_MONSTER) then
		Duel.SpecialSummon(tc,0,1-tp,1-tp,false,false,POS_FACEDOWN_DEFENSE)
	elseif opt==1 and tc:IsType(TYPE_SPELL) then
		Duel.SSet(1-tp,tc)
	elseif opt==2 and tc:IsType(TYPE_TRAP) then
		Duel.SSet(1-tp,tc)
	else
		Duel.SendtoHand(g,1-tp,REASON_EFFECT)
		Duel.ShuffleHand(1-tp)
	end
end

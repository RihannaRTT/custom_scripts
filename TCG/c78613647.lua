--Des Kangaroo
function c78613647.initial_effect(c)
	--destroy
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(78613647,0))
	e1:SetCategory(CATEGORY_DESTROY)
	e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_DAMAGE_STEP_END)
	e1:SetCondition(c78613647.condition)
	e1:SetOperation(c78613647.operation)
	c:RegisterEffect(e1)
end
function c78613647.condition(e,tp,eg,ep,ev,re,r,rp)
	return aux.dsercon(e) and Duel.GetAttackTarget()==e:GetHandler()
		and Duel.GetAttacker():GetAttack()<e:GetHandler():GetDefense()
end
function c78613647.operation(e,tp,eg,ep,ev,re,r,rp)
	local a=Duel.GetAttacker()
	if a:IsRelateToBattle() then Duel.Destroy(a,REASON_EFFECT) end
end

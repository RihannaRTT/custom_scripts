--Time Machine
function c80987716.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_BATTLE_DESTROYED)
	e1:SetTarget(c80987716.target)
	e1:SetOperation(c80987716.activate)
	c:RegisterEffect(e1)
end
function c80987716.filter(c,e,tp)
	return Duel.GetLocationCount(c:GetPreviousControler(),LOCATION_MZONE)>0
		and c:IsLocation(LOCATION_GRAVE) and c:IsReason(REASON_BATTLE)
		and c:IsCanBeSpecialSummoned(e,0,tp,false,false,c:GetPreviousPosition(),c:GetPreviousControler())
end
function c80987716.target(e,tp,eg,ep,ev,re,r,rp,chk)
	local g=eg:Filter(c80987716.filter,nil,e,tp)
	if chk==0 then return #g>0 end
	Duel.SetTargetCard(g)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,eg,1,0,0)
end
function c80987716.activate(e,tp,eg,ep,ev,re,r,rp)
	local g=eg:Filter(Card.IsRelateToEffect,nil,e)
	local tc=g:GetFirst()
	if #g>1 then Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	tc=g:Select(tp,1,1,nil):GetFirst() end
	if tc then
		Duel.SpecialSummon(tc,0,tp,tc:GetPreviousControler(),false,false,tc:GetPreviousPosition())
	end
end

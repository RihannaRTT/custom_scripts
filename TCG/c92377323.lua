--Dark Sage
function c92377323.initial_effect(c)
	aux.AddCodeList(c,46986414)
	c:EnableReviveLimit()
	--special summon
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(92377323,0))
	e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e1:SetRange(LOCATION_HAND+LOCATION_DECK)
	e1:SetCode(EVENT_CUSTOM+71625222)
	e1:SetCondition(c92377323.spcon)
	e1:SetOperation(c92377323.spop)
	c:RegisterEffect(e1)
	--to hand
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(92377323,1))
	e3:SetCategory(CATEGORY_TOHAND+CATEGORY_SEARCH)
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e3:SetRange(LOCATION_MZONE)
	e3:SetCode(EVENT_SPSUMMON_SUCCESS)
	e3:SetCondition(c92377323.thcon)
	e3:SetTarget(c92377323.thtg)
	e3:SetOperation(c92377323.thop)
	c:RegisterEffect(e3)
end
function c92377323.spcon(e,tp,eg,ep,ev,re,r,rp)
	return ep==tp
end
function c92377323.spop(e,tp,eg,ep,ev,re,r,rp)
	if not Duel.CheckReleaseGroup(tp,Card.IsCode,1,nil,46986414) then return end
	local g=Duel.SelectReleaseGroup(tp,Card.IsCode,1,1,nil,46986414)
	local c=e:GetHandler()
	if Duel.Release(g,REASON_COST)>0 and Duel.SpecialSummon(c,0,tp,tp,true,false,POS_FACEUP)~=0 then
		c:CompleteProcedure()
	end
end
function c92377323.thcon(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler()==re:GetHandler()
end
function c92377323.thfilter(c)
	return c:IsType(TYPE_SPELL) and c:IsAbleToHand()
end
function c92377323.thtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c92377323.thfilter,tp,LOCATION_DECK,0,1,nil) end
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,tp,LOCATION_DECK)
end
function c92377323.thop(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
	local g=Duel.SelectMatchingCard(tp,c92377323.thfilter,tp,LOCATION_DECK,0,1,1,nil)
	if g:GetCount()>0 then
		Duel.SendtoHand(g,nil,REASON_EFFECT)
		Duel.ConfirmCards(1-tp,g)
	end
end

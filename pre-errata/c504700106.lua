--サクリファイス
function c504700106.initial_effect(c)
	c:EnableReviveLimit()
	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_CHANGE_CODE)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE+EFFECT_FLAG_UNCOPYABLE)
	e0:SetValue(64631466)
	c:RegisterEffect(e0)
	--equip
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(504700106,0))
	e1:SetCategory(CATEGORY_EQUIP)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCountLimit(1)
	e1:SetCondition(c504700106.eqcon)
	e1:SetTarget(c504700106.eqtg)
	e1:SetOperation(c504700106.eqop)
	c:RegisterEffect(e1)
	--atk/def
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCode(EFFECT_SET_ATTACK)
	e2:SetCondition(c504700106.adcon)
	e2:SetValue(c504700106.atkval)
	c:RegisterEffect(e2)
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE)
	e3:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e3:SetRange(LOCATION_MZONE)
	e3:SetCode(EFFECT_SET_DEFENSE)
	e3:SetCondition(c504700106.adcon)
	e3:SetValue(c504700106.defval)
	c:RegisterEffect(e3)
	--substitute
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e2:SetCode(EVENT_CUSTOM+504700106)
	e2:SetCategory(CATEGORY_DESTROY+CATEGORY_DAMAGE)
	e2:SetTarget(c504700106.repval)
	e2:SetOperation(c504700106.rep)
	c:RegisterEffect(e2)
end
function c504700106.eqcon(e,tp,eg,ep,ev,re,r,rp)
	return c504700106.can_equip_monster(e:GetHandler())
end
function c504700106.eqfilter(c)
	return c:GetFlagEffect(504700106)~=0
end
function c504700106.can_equip_monster(c)
	local g=c:GetEquipGroup():Filter(c504700106.eqfilter,nil)
	return g:GetCount()==0
end
function c504700106.eqtg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsLocation(LOCATION_MZONE) and chkc:IsControler(1-tp) and chkc:IsAbleToChangeControler() end
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_SZONE)>0
		and Duel.IsExistingTarget(Card.IsAbleToChangeControler,tp,0,LOCATION_MZONE,1,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_EQUIP)
	local g=Duel.SelectTarget(tp,Card.IsAbleToChangeControler,tp,0,LOCATION_MZONE,1,1,nil)
	Duel.SetOperationInfo(0,CATEGORY_EQUIP,g,1,0,0)
end
function c504700106.eqlimit(e,c)
	return e:GetOwner()==c
end
function c504700106.equip_monster(c,tp,tc)
	local l=nil
	if tc:IsType(TYPE_TRAPMONSTER) then
		l=3
		if tc:GetTextAttack()<0 then l=l~1 end
		if tc:GetTextDefense()<0 then l=l~2 end
	end
	if not Duel.Equip(tp,tc,c,false) then return end
	--Add Equip limit
	tc:RegisterFlagEffect(504700106,RESET_EVENT+RESETS_STANDARD,0,0,l)
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_OWNER_RELATE)
	e1:SetCode(EFFECT_EQUIP_LIMIT)
	e1:SetReset(RESET_EVENT+RESETS_STANDARD)
	e1:SetValue(c504700106.eqlimit)
	tc:RegisterEffect(e1)
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_EQUIP)
	e2:SetCode(EFFECT_DESTROY_REPLACE)
	e2:SetTarget(function() return tc:GetFlagEffect(504700106)~=0 end)
	e2:SetOperation(function(e) if c:GetBattleTarget() then Duel.RaiseSingleEvent(c,EVENT_CUSTOM+504700106,Effect.GlobalEffect(),0,c:GetBattleTarget():GetControler(),tp,e:GetLabel() or 0) end end)
	e2:SetReset(RESET_EVENT+RESETS_STANDARD)
	tc:RegisterEffect(e2)
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_BATTLE_DAMAGE)
	e3:SetRange(LOCATION_SZONE)
	e3:SetOperation(function(e,tp,eg,ep,ev) if ep==tp then e2:SetLabel(ev) end end)
	e3:SetReset(RESET_EVENT+RESETS_STANDARD)
	tc:RegisterEffect(e3)
end
function c504700106.eqop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local tc=Duel.GetFirstTarget()
	if tc:IsRelateToEffect(e) and tc:IsType(TYPE_MONSTER) and tc:IsControler(1-tp) then
		c504700106.equip_monster(c,tp,tc)
	end
end
function c504700106.repval(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,e:GetHandler():GetEquipGroup():Filter(c504700106.eqfilter,nil),1,0,0)
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,ev)
end
function c504700106.rep(e,tp,eg,ep,ev,re,r,rp,chk)
	if Duel.Destroy(e:GetHandler():GetEquipGroup():Filter(c504700106.eqfilter,nil),REASON_EFFECT)==0 then Duel.Destroy(e:GetHandler(),REASON_BATTLE) return end
	Duel.Damage(1-tp,ev,REASON_EFFECT)
end
function c504700106.adcon(e)
	local c=e:GetHandler()
	local g=c:GetEquipGroup():Filter(c504700106.eqfilter,nil)
	return g:GetCount()>0
end
function c504700106.atkval(e,c)
	local c=e:GetHandler()
	local g=c:GetEquipGroup():Filter(c504700106.eqfilter,nil)
	local atk=g:GetFirst():GetTextAttack()
	if g:GetFirst():IsFacedown() or bit.band(g:GetFirst():GetOriginalType(),TYPE_MONSTER)==0 or atk<0 then
		local l=g:GetFirst():GetFlagEffectLabel(64631466)
		if l then return atk*(l&1)
		else return 0 end
	else
		return atk
	end
end
function c504700106.defval(e,c)
	local c=e:GetHandler()
	local g=c:GetEquipGroup():Filter(c504700106.eqfilter,nil)
	local def=g:GetFirst():GetTextDefense()
	if g:GetFirst():IsFacedown() or bit.band(g:GetFirst():GetOriginalType(),TYPE_MONSTER)==0 or def<0 then
		local l=g:GetFirst():GetFlagEffectLabel(64631466)
		if l then return (l&2)//2*def
		else return 0 end
	else
		return def
	end
end

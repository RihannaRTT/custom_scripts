Auxiliary.SushipMentionsTable={61027400,78362751,42377643}
EFFECT_FLAG2_MILLENNIUM_RESTRICT	=0x0001 --卡名為X的卡的效果無效（千年眼納祭神）
EFFECT_FLAG2_CONST_VALUE			=0x0010	--Sets ATK/DEF to a value independently of the card's ATK/DEF.
EFFECT_CANNOT_LOSE_LP				=401
EFFECT_CANNOT_LOSE_DECK				=402
EFFECT_UNSTOPPABLE_ATTACK			=403
EFFECT_BECOME_QUICK					=404
EFFECT_CANNOT_LOSE_EFFECT			=170000170
CARDDATA_CODE			=1
CARDDATA_ALIAS			=2
CARDDATA_SETCODE		=3
CARDDATA_TYPE			=4
CARDDATA_LEVEL			=5
CARDDATA_ATTRIBUTE		=6
CARDDATA_RACE			=7
CARDDATA_ATTACK			=8
CARDDATA_DEFENSE		=9
CARDDATA_LSCALE			=10
CARDDATA_RSCALE			=11
CARDDATA_LINK_MARKER	=12
CARDDATA_FLAGS			=13
CARDDATA_COVER			=14
function Auxiliary.ChangeCodeCondition(check,condition)
	return	function(e)
				if condition and not condition(e) then return false end
				local le={e:GetHandler():IsHasEffect(EFFECT_DISABLE)}
				for _,te in ipairs(le) do
					if not te:IsHasProperty(0,EFFECT_FLAG2_MILLENNIUM_RESTRICT) then return check end
				end
				return not check
			end
end
local SProc, CFProc, NameProc = Auxiliary.AddSynchroProcedure, Auxiliary.AddContactFusionProcedure, Auxiliary.EnableChangeCode
function Auxiliary.EnableChangeCode(c,code,location,condition)
	local e1=NameProc(c,code,location,condition)
	local e2=e1:Clone()
	e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE+EFFECT_FLAG_CANNOT_DISABLE)
	e2:SetCondition(Auxiliary.ChangeCodeCondition(false,condition))
	c:RegisterEffect(e2)
	return e1,e2
end
function Auxiliary.SpElimFilter(c,mustbefaceup,includemzone)
	--includemzone - contains MZONE in original requirement
	--NOTE: Should only check LOCATION_MZONE+LOCATION_GRAVE
	if c:IsType(TYPE_MONSTER) then
		if mustbefaceup and c:IsLocation(LOCATION_MZONE) and c:IsFacedown() then return false end
		if includemzone then return c:IsLocation(LOCATION_MZONE) or not Duel.IsPlayerAffectedByEffect(c:GetControler(),69832741) end
		if Duel.IsPlayerAffectedByEffect(c:GetControler(),69832741) then
			return c:IsLocation(LOCATION_MZONE)
		else
			return c:IsLocation(LOCATION_GRAVE)
		end
	else
		return includemzone or c:IsLocation(LOCATION_GRAVE)
	end
end
function Auxiliary.XyzLevelFreeOperation(f,gf,minct,maxct)
	return	function(e,tp,eg,ep,ev,re,r,rp,c,og,min,max)
				if og and not min then
					local sg=Group.CreateGroup()
					local tc=og:GetFirst()
					while tc do
						local sg1=tc:GetOverlayGroup()
						Duel.Overlay(c,sg1)
						sg:Merge(sg1)
						tc=og:GetNext()
					end
					c:SetMaterial(og)
					Duel.Overlay(c,og)
					Duel.SendtoGrave(sg,REASON_RULE)
				else
					local mg=e:GetLabelObject()
					if e:GetLabel()==1 then
						local mg2=mg:GetFirst():GetOverlayGroup()
						if mg2:GetCount()~=0 then
							Duel.Overlay(c,mg2)
						end
						c:SetMaterial(mg)
						Duel.Overlay(c,mg)
					else
						local sg=Group.CreateGroup()
						local tc=mg:GetFirst()
						while tc do
							local sg1=tc:GetOverlayGroup()
							Duel.Overlay(c,sg1)
							sg:Merge(sg1)
							tc=mg:GetNext()
						end
						c:SetMaterial(mg)
						Duel.Overlay(c,mg)
						Duel.SendtoGrave(sg,REASON_RULE)
					end
					mg:DeleteGroup()
				end
			end
end
function Auxiliary.XyzLevelFreeOperationAlter(f,gf,minct,maxct,alterf,alterdesc,alterop)
	return	function(e,tp,eg,ep,ev,re,r,rp,c,og,min,max)
				if og and not min then
					local sg=Group.CreateGroup()
					local tc=og:GetFirst()
					while tc do
						local sg1=tc:GetOverlayGroup()
						Duel.Overlay(c,sg1)
						sg:Merge(sg1)
						tc=og:GetNext()
					end
					Duel.SendtoGrave(sg,REASON_RULE)
					c:SetMaterial(og)
					Duel.Overlay(c,og)
				else
					local mg=e:GetLabelObject()
					if e:GetLabel()==1 then
						local mg2=mg:GetFirst():GetOverlayGroup()
						if mg2:GetCount()~=0 then
							Duel.Overlay(c,mg2)
						end
						c:SetMaterial(mg)
						Duel.Overlay(c,mg)
					else
						local sg=Group.CreateGroup()
						local tc=mg:GetFirst()
						while tc do
							local sg1=tc:GetOverlayGroup()
							Duel.Overlay(c,sg1)
							sg:Merge(sg1)
							tc=mg:GetNext()
						end
						c:SetMaterial(mg)
						Duel.Overlay(c,mg)
						Duel.SendtoGrave(sg,REASON_RULE)
					end
					mg:DeleteGroup()
				end
			end
end
function Auxiliary.AddSynchroProcedure(c,f1,f2,minc,maxc)
	if f2==nil then f2=Auxiliary.TRUE end
	SProc(c,f1,f2,minc,maxc)
end
function Auxiliary.RitualUltimateOperation(filter,level_function,greater_or_equal,summon_location,grave_filter,mat_filter,extra_operation)
	return	function(e,tp,eg,ep,ev,re,r,rp)
				::cancel::
				local mg=Duel.GetRitualMaterial(tp)
				if mat_filter then mg=mg:Filter(mat_filter,nil,e,tp) end
				local exg=nil
				if grave_filter then
					exg=Duel.GetMatchingGroup(Auxiliary.RitualExtraFilter,tp,LOCATION_GRAVE,0,nil,grave_filter)
				end
				Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
				local tg=Duel.SelectMatchingCard(tp,Auxiliary.NecroValleyFilter(Auxiliary.RitualUltimateFilter),tp,summon_location,0,1,1,nil,filter,e,tp,mg,exg,level_function,greater_or_equal)
				local tc=tg:GetFirst()
				local mat
				if tc then
					mg=mg:Filter(Card.IsCanBeRitualMaterial,tc,tc)
					if exg then
						mg:Merge(exg)
					end
					if tc.mat_filter then
						mg=mg:Filter(tc.mat_filter,tc,tp)
					else
						mg:RemoveCard(tc)
					end
					Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_RELEASE)
					local lv=level_function(tc)
					Auxiliary.GCheckAdditional=Auxiliary.RitualCheckAdditional(tc,lv,greater_or_equal)
					mat=mg:SelectSubGroup(tp,Auxiliary.RitualCheck,true,1,lv,tp,tc,lv,greater_or_equal)
					if not mat then goto cancel end
					Auxiliary.GCheckAdditional=nil
					tc:SetMaterial(mat)
					Duel.ReleaseRitualMaterial(mat)
					Duel.BreakEffect()
					Duel.SpecialSummon(tc,SUMMON_TYPE_RITUAL,tp,tp,false,true,POS_FACEUP)
					tc:CompleteProcedure()
				end
				if extra_operation then
					extra_operation(e,tp,eg,ep,ev,re,r,rp,tc,mat)
				end
			end
end
function Auxiliary.FOperationMix(insf,sub,...)
	local funs={...}
	return function(e,tp,eg,ep,ev,re,r,rp,gc,chkfnf)
		local c=e:GetHandler()
		local tp=c:GetControler()
		local notfusion=chkfnf&0x100>0
		local concat_fusion=chkfnf&0x200>0
		local sub=(sub or notfusion) and not concat_fusion
		local mg=eg:Filter(Auxiliary.FConditionFilterMix,c,c,sub,concat_fusion,table.unpack(funs))
		if gc then Duel.SetSelectedCard(Group.FromCards(gc)) end
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
		local sg=mg:SelectSubGroup(tp,Auxiliary.FCheckMixGoal,true,#funs,#funs,tp,c,sub,chkfnf,table.unpack(funs))
		if sg and #sg>=#funs then
			Duel.SetFusionMaterial(sg or Group.CreateGroup())
		else Duel.SetFusionMaterial(Group.CreateGroup()) end
	end
end
function Auxiliary.FOperationMixRep(insf,sub,fun1,minc,maxc,...)
	local funs={...}
	return	function(e,tp,eg,ep,ev,re,r,rp,gc,chkfnf)
				local c=e:GetHandler()
				local tp=c:GetControler()
				local notfusion=chkfnf&0x100>0
				local concat_fusion=chkfnf&0x200>0
				local sub=(sub or notfusion) and not concat_fusion
				local mg=eg:Filter(Auxiliary.FConditionFilterMix,c,c,sub,concat_fusion,fun1,table.unpack(funs))
				local sg=Group.CreateGroup()
				if gc then sg:AddCard(gc) end
				while sg:GetCount()<maxc+#funs do
					local cg=mg:Filter(Auxiliary.FSelectMixRep,sg,tp,mg,sg,c,sub,chkfnf,fun1,minc,maxc,table.unpack(funs))
					if cg:GetCount()==0 then break end
					local finish=Auxiliary.FCheckMixRepGoal(tp,sg,c,sub,chkfnf,fun1,minc,maxc,table.unpack(funs))
					local cancel_group=sg:Clone()
					if gc then cancel_group:RemoveCard(gc) end
					Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
					local tc=cg:SelectUnselect(cancel_group,tp,finish,#sg==0,minc+#funs,maxc+#funs)
					if not tc then break end
					if sg:IsContains(tc) then
						sg:RemoveCard(tc)
					else
						sg:AddCard(tc)
					end
				end
				Duel.SetFusionMaterial(sg)
			end
end
function Auxiliary.FShaddollFilter(c,fc,attr)
	return (Auxiliary.FShaddollFilter1(c) or Auxiliary.FShaddollFilter2(c,attr) or (c:IsFusionAttribute(attr) or  c:IsHasEffect(4904633) and c:IsControler(fc:GetControler()))) and c:IsCanBeFusionMaterial(fc) and not c:IsHasEffect(6205579)
end
function Auxiliary.FShaddollOperation(attr)
	return	function(e,tp,eg,ep,ev,re,r,rp,gc,chkf)
				local c=e:GetHandler()
				local mg=eg:Filter(Auxiliary.FShaddollFilter,nil,c,attr)
				local fc=Duel.GetFieldCard(tp,LOCATION_FZONE,0)
				local exg=nil
				if fc and fc:IsHasEffect(81788994) and fc:IsCanRemoveCounter(tp,0x16,3,REASON_EFFECT) then
					local fe=fc:IsHasEffect(81788994)
					exg=Duel.GetMatchingGroup(Auxiliary.FShaddollExFilter,tp,0,LOCATION_MZONE,mg,c,attr,fe)
				end
				local g=nil
				if gc then
					g=Group.FromCards(gc)
					mg:RemoveCard(gc)
				else
					Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
					g=mg:Filter(Auxiliary.FShaddollSpFilter1,nil,c,tp,mg,exg,attr,chkf):SelectSubGroup(tp,Auxiliary.TRUE,true,1,1)
					if g then mg:Sub(g) else Duel.SetFusionMaterial(Group.CreateGroup()) return end
				end
				if exg and exg:IsExists(Auxiliary.FShaddollSpFilter2,1,nil,c,tp,g:GetFirst(),attr,chkf)
					and (mg:GetCount()==0 or (exg:GetCount()>0 and Duel.SelectYesNo(tp,Auxiliary.Stringid(81788994,0)))) then
					fc:RemoveCounter(tp,0x16,3,REASON_EFFECT)
					Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
					local sg=exg:Filter(Auxiliary.FShaddollSpFilter2,nil,c,tp,g:GetFirst(),attr,chkf):SelectSubGroup(tp,Auxiliary.TRUE,true,1,1)
					if sg then g:Merge(sg) else Duel.SetFusionMaterial(Group.CreateGroup()) return end
				else
					Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FMATERIAL)
					local sg=mg:Filter(Auxiliary.FShaddollSpFilter2,nil,c,tp,g:GetFirst(),attr,chkf):SelectSubGroup(tp,Auxiliary.TRUE,true,1,1)
					if sg then g:Merge(sg) else Duel.SetFusionMaterial(Group.CreateGroup()) return end
				end
				Duel.SetFusionMaterial(g)
			end
end
function Auxiliary.AddContactFusionProcedure(c,filter,self_location,opponent_location,mat_operation,...)
	local e=CFProc(c,filter,self_location,opponent_location,mat_operation,...)
	e:SetTarget(Auxiliary.ContactFusionTarget(filter,self_location,opponent_location))
	e:SetOperation(Auxiliary.ContactFusionOperation(filter,self_location,opponent_location,mat_operation,{...}))
	return e
end
function Auxiliary.ContactFusionTarget(filter,self_location,opponent_location)
	return	function(e,tp,eg,ep,ev,re,r,rp,chk,c)
				local mg=Duel.GetMatchingGroup(Auxiliary.ContactFusionMaterialFilter,tp,self_location,opponent_location,c,c,filter)
				local g=Duel.SelectFusionMaterial(tp,c,mg,nil,tp|0x200)
				if g and #g>0 then
					g:KeepAlive()
					e:SetLabelObject(g)
					return true
				else return false end
			end
end
function Auxiliary.ContactFusionOperation(filter,self_location,opponent_location,mat_operation,operation_params)
	return	function(e,tp,eg,ep,ev,re,r,rp,c)
				local g=e:GetLabelObject()
				c:SetMaterial(g)
				mat_operation(g,table.unpack(operation_params))
				g:DeleteGroup()
			end
end

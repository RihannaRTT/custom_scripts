-- 新种族
RACE_CYBORG           = 0x4000000 -- 电子人
RACE_MAGICALKNIGHT    = 0x8000000 -- 魔导骑士
RACE_HYDRAGON         = 0x10000000 -- 多头龙
RACE_OMEGAPSYCHO      = 0x20000000 -- 欧米茄念动力
RACE_CELESTIALWARRIOR = 0x40000000 -- 天界战士
RACE_GALAXY           = 0x80000000 -- 银河
RACE_ALL = 0xffffffff
-- 特殊调整
EFFECT_LEGEND_CARD = 120000000 -- 传说卡标识(改变卡名不影响)
EFFECT_CANNOT_SINGLE_TRIBUTE = 120170045 -- 监狱岛 大姐头巨岩 (与下面的效果结合变成不能上级召唤)
EFFECT_CANNOT_DOUBLE_TRIBUTE = 120120029 -- 魔将 雅灭鲁拉(不能使用：双重解放)
EFFECT_PLAYER_CANNOT_ATTACK = 120155054 -- 幻刃封锁 (对方不能攻击时不能发动)
EFFECT_PLAYER_RACE_CANNOT_ATTACK = 120155055 -- 幻刃封锁 (不能选择不能攻击的种族)
-- Base
RushDuel = {}
RD = RushDuel
-- 创建效果: 玩家对象的全局效果
function RushDuel.CreatePlayerTargetGlobalEffect(code, value)
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(code)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTargetRange(1, 1)
	if value ~= nil then
		e1:SetValue(value)
	end
	Duel.RegisterEffect(e1, 0)
	return e1
end
-- 创建效果: 影响全场的全局效果
function RushDuel.CreateFieldGlobalEffect(is_continuous, code, operation)
	local e1 = Effect.GlobalEffect()
	if is_continuous then
		e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	else
		e1:SetType(EFFECT_TYPE_FIELD)
	end
	e1:SetCode(code)
	e1:SetOperation(operation)
	Duel.RegisterEffect(e1, 0)
	return e1
end
-- 初始化规则
function RushDuel.InitRule()
	-- 场上的怪兽的效果强制1回合1次
	local function get_effect_owner_code(e)
		if e:GetType() & EFFECT_TYPE_XMATERIAL == EFFECT_TYPE_XMATERIAL then
			-- 极大怪兽的L/R部分的效果分开计算
			return e:GetLabel()
		else
			return e:GetOwner():GetOriginalCode()
		end
	end
	RushDuel.CreatePlayerTargetGlobalEffect(EFFECT_CANNOT_ACTIVATE, function(e, re, tp)
		return re:GetHandler():GetFlagEffect(get_effect_owner_code(re)) ~= 0
	end)
	RushDuel.CreateFieldGlobalEffect(true, EVENT_CHAIN_SOLVING, function(e, tp, eg, ep, ev, re, r, rp)
		local te = Duel.GetChainInfo(ev, CHAININFO_TRIGGERING_EFFECT)
		local code = get_effect_owner_code(te)
		te:GetHandler():RegisterFlagEffect(code, RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END, 0, 1)
	end)
	RushDuel.InitMaximum()
end
-- 创建效果: 在LP槽显示提示信息
function RushDuel.CreateHintEffect(e, desc, player, s_range, o_range, reset)
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetDescription(desc)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_CLIENT_HINT)
	e1:SetTargetRange(s_range, o_range)
	e1:SetReset(reset)
	Duel.RegisterEffect(e1, player)
	return e1
end
-- 创建效果: 只能用特定类型的怪兽攻击 (对玩家效果)
function RushDuel.CreateAttackLimitEffect(e, target, player, s_range, o_range, reset)
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_ATTACK)
	e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e1:SetTargetRange(s_range, o_range)
	if target ~= nil then
		e1:SetTarget(target)
	end
	e1:SetReset(reset)
	Duel.RegisterEffect(e1, player)
	return e1
end
-- 创建效果: 不能攻击 (对玩家效果)
function RushDuel.CreateCannotAttackEffect(e, hint, player, s_range, o_range, reset)
	local s_target, o_traget = 0, 0
	if s_range == 1 then
		s_target = LOCATION_MZONE
	end
	if o_range == 1 then
		o_traget = LOCATION_MZONE
	end
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_ATTACK)
	e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e1:SetTargetRange(s_target, o_traget)
	e1:SetReset(reset)
	Duel.RegisterEffect(e1, player)
	local e2 = Effect.CreateEffect(e:GetHandler())
	e2:SetDescription(hint)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_PLAYER_CANNOT_ATTACK)
	e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_CLIENT_HINT)
	e2:SetTargetRange(s_range, o_range)
	e2:SetReset(reset)
	Duel.RegisterEffect(e2, player)
	return e1, e2
end
-- 创建效果: 不能用某些种族攻击 (对玩家效果)
function RushDuel.CreateRaceCannotAttackEffect(e, hint, race, player, s_range, o_range, reset)
	local s_target, o_traget = 0, 0
	if s_range == 1 then
		s_target = LOCATION_MZONE
	end
	if o_range == 1 then
		o_traget = LOCATION_MZONE
	end
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_ATTACK)
	e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e1:SetTargetRange(s_target, o_traget)
	e1:SetTarget(function(e, c)
		return c:IsRace(race)
	end)
	e1:SetReset(reset)
	Duel.RegisterEffect(e1, player)
	local e2 = Effect.CreateEffect(e:GetHandler())
	e2:SetDescription(hint)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_PLAYER_RACE_CANNOT_ATTACK)
	e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_CLIENT_HINT)
	e2:SetTargetRange(s_range, o_range)
	e2:SetValue(race)
	e2:SetReset(reset)
	Duel.RegisterEffect(e2, player)
	return e1, e2
end
-- 创建效果: 不能直接攻击 (对玩家效果)
function RushDuel.CreateCannotDirectAttackEffect(e, target, player, s_range, o_range, reset)
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_DIRECT_ATTACK)
	e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e1:SetTargetRange(s_range, o_range)
	if target ~= nil then
		e1:SetTarget(target)
	end
	e1:SetReset(reset)
	Duel.RegisterEffect(e1, player)
	return e1
end
-- 创建效果: 不能选择攻击目标 (对玩家效果)
function RushDuel.CreateCannotSelectBattleTargetEffect(e, target, player, s_range, o_range, reset)
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SELECT_BATTLE_TARGET)
	e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e1:SetTargetRange(s_range, o_range)
	if target ~= nil then
		e1:SetTarget(target)
	end
	e1:SetReset(reset)
	Duel.RegisterEffect(e1, player)
	return e1
end
-- 创建效果: 不能召唤怪兽 (对玩家效果)
function RushDuel.CreateCannotSummonEffect(e, desc, target, player, s_range, o_range, reset)
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetDescription(desc)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SUMMON)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_CLIENT_HINT)
	e1:SetTargetRange(s_range, o_range)
	if target ~= nil then
		e1:SetTarget(target)
	end
	e1:SetReset(reset)
	Duel.RegisterEffect(e1, player)
	return e1
end
-- 创建效果: 不能盖放怪兽 (对玩家效果)
function RushDuel.CreateCannotSetMonsterEffect(e, desc, target, player, s_range, o_range, reset)
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetDescription(desc)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_MSET)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_CLIENT_HINT)
	e1:SetTargetRange(s_range, o_range)
	if target ~= nil then
		e1:SetTarget(target)
	end
	e1:SetReset(reset)
	Duel.RegisterEffect(e1, player)
	return e1
end
-- 创建效果: 不能特殊召唤怪兽 (对玩家效果)
function RushDuel.CreateCannotSpecialSummonEffect(e, desc, target, player, s_range, o_range, reset)
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetDescription(desc)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_CLIENT_HINT)
	e1:SetTargetRange(s_range, o_range)
	if target ~= nil then
		e1:SetTarget(target)
	end
	e1:SetReset(reset)
	Duel.RegisterEffect(e1, player)
	return e1
end
-- 创建效果: 只能用1只怪兽进行攻击 (对玩家效果)
function RushDuel.CreateOnlySoleAttackEffect(e, code, player, s_range, o_range, reset)
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_ATTACK_ANNOUNCE)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e1:SetOperation(function(e, tp, eg, ep, ev, re, r, rp)
		if Duel.GetFlagEffect(tp, code) == 0 then
			e:GetLabelObject():SetLabel(eg:GetFirst():GetFieldID())
			Duel.RegisterFlagEffect(tp, code, reset, 0, 1)
		end
	end)
	e1:SetReset(reset)
	Duel.RegisterEffect(e1, player)
	local e2 = Effect.CreateEffect(e:GetHandler())
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_CANNOT_ATTACK_ANNOUNCE)
	e2:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e2:SetTargetRange(s_range, o_range)
	e2:SetCondition(function(e)
		return Duel.GetFlagEffect(e:GetHandlerPlayer(), code) ~= 0
	end)
	e2:SetTarget(function(e, c)
		return c:GetFieldID() ~= e:GetLabel()
	end)
	e1:SetLabelObject(e2)
	e2:SetReset(reset)
	Duel.RegisterEffect(e2, player)
end
-- 创建效果: 只能用这张卡进行攻击 (对玩家效果)
function RushDuel.CreateOnlyThisAttackEffect(e, code, player, s_range, o_range, reset)
	local c = e:GetHandler()
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_ATTACK_ANNOUNCE)
	e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e1:SetLabelObject(c)
	e1:SetTargetRange(s_range, o_range)
	e1:SetTarget(function(e, c)
		return not (c == e:GetLabelObject() and c:GetFlagEffect(code) ~= 0)
	end)
	e1:SetReset(reset)
	Duel.RegisterEffect(e1, player)
	c:RegisterFlagEffect(code, RESET_EVENT + RESETS_STANDARD - RESET_TURN_SET + reset, 0, 1)
end
-- 创建效果: 不能把效果发动 (对玩家效果)
function RushDuel.CreateCannotActivateEffect(e, desc, value, player, s_range, o_range, reset)
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetDescription(desc)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_ACTIVATE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_CLIENT_HINT)
	e1:SetTargetRange(s_range, o_range)
	if value ~= nil then
		e1:SetValue(value)
	end
	e1:SetReset(reset)
	Duel.RegisterEffect(e1, player)
	return e1
end
-- 创建效果: Buff类效果
function RushDuel.CreateSingleEffect(e, desc, card, code, value, reset, forced)
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(code)
	if value ~= nil then
		e1:SetValue(value)
	end
	if desc ~= nil then
		e1:SetDescription(desc)
		e1:SetProperty(EFFECT_FLAG_CLIENT_HINT)
	end
	if reset ~= nil then
		e1:SetReset(reset)
	end
	card:RegisterEffect(e1, forced)
	return e1
end
-- 创建效果: 选择效果
function RushDuel.CreateMultiChooseEffect(card, condition, cost, hint1, target1, operation1, hint2, target2, operation2, category1, category2)
	local e1 = Effect.CreateEffect(card)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_MZONE)
	local e2 = e1:Clone()
	e1:SetDescription(hint1)
	e2:SetDescription(hint2)
	e1:SetCategory(category1 or 0)
	e2:SetCategory(category2 or 0)
	if condition ~= nil then
		e1:SetCondition(condition)
		e2:SetCondition(condition)
	end
	if cost ~= nil then
		e1:SetCost(cost)
		e2:SetCost(cost)
	end
	if target1 ~= nil then
		e1:SetTarget(target1)
	end
	if target2 ~= nil then
		e2:SetTarget(target2)
	end
	e1:SetOperation(operation1)
	e2:SetOperation(operation2)
	card:RegisterEffect(e1)
	card:RegisterEffect(e2)
	return e1, e2
end
-- 添加通常召唤手续
function RushDuel.AddSummonProcedure(card, desc, condition, operation, value)
	local e1 = Effect.CreateEffect(card)
	e1:SetDescription(desc)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SUMMON_PROC)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
	e1:SetCondition(condition)
	e1:SetOperation(operation)
	e1:SetValue(value)
	card:RegisterEffect(e1)
	return e1
end
-- 添加手卡特殊召唤手续
function RushDuel.AddHandSpecialSummonProcedure(card, desc, condition, target, operation, value, position)
	local e1 = Effect.CreateEffect(card)
	e1:SetDescription(desc)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	if position == nil then
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
	else
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_SPSUM_PARAM)
		e1:SetTargetRange(position, 0)
	end
	e1:SetRange(LOCATION_HAND)
	if condition ~= nil then
		e1:SetCondition(condition)
	end
	if target ~= nil then
		e1:SetTarget(target)
	end
	if operation ~= nil then
		e1:SetOperation(operation)
	end
	if value ~= nil then
		e1:SetValue(value)
	end
	card:RegisterEffect(e1)
	return e1
end
-- 上级召唤时的解放怪兽检测
function RushDuel.CreateAdvanceCheck(card, filter, count, flag)
	local e1 = Effect.CreateEffect(card)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_MATERIAL_CHECK)
	e1:SetValue(function(e, c)
		local mg = c:GetMaterial()
		local ct = math.min(count, mg:GetCount())
		local label = 0
		if c:IsLevelBelow(6) and count == 2 then
			-- 当前等级小于解放要求
		elseif ct > 0 and mg:IsExists(filter, ct, nil, e) then
			label = flag
		end
		e:SetLabel(label)
	end)
	card:RegisterEffect(e1)
	local e2 = Effect.CreateEffect(card)
	e2:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
	e2:SetCode(EVENT_SUMMON_SUCCESS)
	e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e2:SetLabelObject(e1)
	e2:SetCondition(RushDuel.AdvanceCheckCondition)
	e2:SetOperation(RushDuel.AdvanceCheckOperation)
	card:RegisterEffect(e2)
end
function RushDuel.AdvanceCheckCondition(e, tp, eg, ep, ev, re, r, rp)
	return e:GetLabelObject():GetLabel() ~= 0 and e:GetHandler():IsSummonType(SUMMON_TYPE_ADVANCE)
end
function RushDuel.AdvanceCheckOperation(e, tp, eg, ep, ev, re, r, rp)
	e:GetHandler():RegisterFlagEffect(e:GetLabelObject():GetLabel(), RESET_EVENT + RESETS_STANDARD, 0, 1)
end
-- 记录召唤的解放数量
function RushDuel.CreateAdvanceCount(card, self_value)
	local e1 = Effect.CreateEffect(card)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_MATERIAL_CHECK)
	e1:SetValue(function(e, c)
		if c:IsSummonType(SUMMON_VALUE_SELF) then
			e:SetLabel(self_value)
		elseif c:IsLevelBelow(4) then
			e:SetLabel(0)
		elseif c:IsLevelBelow(6) then
			e:SetLabel(1)
		else
			e:SetLabel(2)
		end
	end)
	card:RegisterEffect(e1)
	return e1
end
-- 记录攻击表示上级召唤的状态
function RushDuel.CreateAdvanceSummonFlag(card, flag)
	local e1 = Effect.CreateEffect(card)
	e1:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_SUMMON_SUCCESS)
	e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e1:SetLabel(flag)
	e1:SetCondition(RushDuel.AdvanceSummonFlagCondition)
	e1:SetOperation(RushDuel.AdvanceSummonFlagOperation)
	card:RegisterEffect(e1)
end
function RushDuel.AdvanceSummonFlagCondition(e, tp, eg, ep, ev, re, r, rp)
	return e:GetHandler():IsSummonType(SUMMON_TYPE_ADVANCE)
end
function RushDuel.AdvanceSummonFlagOperation(e, tp, eg, ep, ev, re, r, rp)
	e:GetHandler():RegisterFlagEffect(e:GetLabel(), RESET_EVENT + RESETS_STANDARD, 0, 1)
end
-- 获取效果值列表
function RushDuel.GetEffectValues(card, code)
	local effects = {card:IsHasEffect(code)}
	local values = {}
	for i, effect in ipairs(effects) do
		values[i] = effect:GetValue()
	end
	return values
end
-- 抹平表
function RushDuel.FlatTable(...)
	local result = {}
	for _, item in ipairs({...}) do
		if type(item) == "table" then
			local datas = RushDuel.FlatTable(table.unpack(item))
			for _, data in ipairs(datas) do
				table.insert(result, data)
			end
		else
			table.insert(result, item)
		end
	end
	return result
end
-- 递归检查表
function RushDuel.FlatCheck(check, ...)
	for _, item in ipairs({...}) do
		if type(item) == "table" then
			if RushDuel.FlatCheck(check, table.unpack(item)) then
				return true
			end
		elseif check(item) then
			return true
		end
	end
	return false
end
-- Select Effect
function RushDuel.BaseSelectEffect(c, eff1hint, eff1con, eff1op, eff2hint, eff2con, eff2op)
	local e = Effect.CreateEffect(c)
	e:SetType(EFFECT_TYPE_IGNITION)
	e:SetRange(LOCATION_MZONE)
	e:SetTarget(RushDuel.SelectEffectTarget(eff1con, eff2con))
	e:SetOperation(RushDuel.SelectEffectOperation(eff1hint, eff1con, eff1op, eff2hint, eff2con, eff2op))
	return e
end
function RushDuel.SelectEffectCondition(eff1con, eff2con)
	return function(e, tp, eg, ep, ev, re, r, rp)
		return eff1con(e, tp, eg, ep, ev, re, r, rp, false) or eff2con(e, tp, eg, ep, ev, re, r, rp, false)
	end
end
function RushDuel.SelectEffectTarget(eff1con, eff2con)
	return function(e, tp, eg, ep, ev, re, r, rp, chk)
		if chk == 0 then
			return eff1con(e, tp, eg, ep, ev, re, r, rp, false) or eff2con(e, tp, eg, ep, ev, re, r, rp, false)
		end
	end
end
function RushDuel.SelectEffectOperation(eff1hint, eff1con, eff1op, eff2hint, eff2con, eff2op)
	return function(e, tp, eg, ep, ev, re, r, rp)
		local eff1 = eff1con(e, tp, eg, ep, ev, re, r, rp, true)
		local eff2 = eff2con(e, tp, eg, ep, ev, re, r, rp, true)
		local select = 0
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_EFFECT)
		if eff1 and eff2 then
			select = Duel.SelectOption(tp, eff1hint, eff2hint) + 1
		elseif eff1 then
			Duel.SelectOption(tp, eff1hint)
			select = 1
		elseif eff2 then
			Duel.SelectOption(tp, eff2hint)
			select = 2
		end
		if select == 1 then
			eff1op(e, tp, eg, ep, ev, re, r, rp)
		elseif select == 2 then
			eff2op(e, tp, eg, ep, ev, re, r, rp)
		end
	end
end
-- Special Summon Count
function RushDuel.GetSPCount(tp, max)
	if Duel.IsPlayerAffectedByEffect(tp, 59822133) then
		max = 1
	end
	local ct = Duel.GetMZoneCount(tp)
	if ct < 1 then
		return 0
	elseif ct > max then
		return max
	else
		return ct
	end
end
TYPE_MAXIMUM = 0x400 -- 极大怪兽
SUMMON_TYPE_MAXIMUM = 0x45000000 -- 极大模式
-- 初始化极大怪兽规则
function RushDuel.InitMaximum()
	-- 不可改变表示形式
	local e1 = Effect.GlobalEffect()
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SET_POSITION)
	e1:SetTargetRange(LOCATION_MZONE, LOCATION_MZONE)
	e1:SetTarget(RushDuel.MaximumMonster)
	e1:SetValue(POS_FACEUP_ATTACK)
	Duel.RegisterEffect(e1, 0)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_CANNOT_CHANGE_POSITION)
	e2:SetValue(0)
	Duel.RegisterEffect(e2, 0)
	local e3 = e2:Clone()
	e3:SetCode(EFFECT_CANNOT_CHANGE_POS_E)
	e3:SetTarget(RushDuel.MaximumMonsterAtk)
	Duel.RegisterEffect(e3, 0)
	local e4 = e2:Clone()
	e4:SetCode(EFFECT_CANNOT_TURN_SET)
	Duel.RegisterEffect(e4, 0)
end
function RushDuel.MaximumMonster(e, c)
	return c:IsSummonType(SUMMON_TYPE_MAXIMUM) and c:GetOverlayCount() > 0
end
function RushDuel.MaximumMonsterAtk(e, c)
	return c:IsPosition(POS_FACEUP_ATTACK) and RushDuel.MaximumMonster(e, c)
end
-- 添加极大召唤手续
function RushDuel.AddMaximumProcedure(c, max_atk, left_code, right_code)
	if c:IsStatus(STATUS_COPYING_EFFECT) then
		return
	end
	-- 记录状态
	if c.maximum_attack == nil then
		local mt = getmetatable(c)
		mt.maximum_attack = max_atk
	end
	local e0 = Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_SINGLE)
	e0:SetCode(EFFECT_SET_BASE_ATTACK)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
	e0:SetCondition(aux.NOT(RushDuel.MaximumMode))
	e0:SetValue(c:GetTextAttack() & 0xffff)
	c:RegisterEffect(e0)
	-- 极大召唤 手续
	RushDuel.AddHandSpecialSummonProcedure(c, aux.Stringid(120000000, 0), RushDuel.MaximumSummonCondition(left_code, right_code), RushDuel.MaximumSummonTarget(left_code, right_code),
		RushDuel.MaximumSummonOperation(left_code, right_code), RushDuel.MaximumSummonValue, POS_FACEUP_ATTACK)
	-- 极大攻击力
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_SET_BASE_ATTACK)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCondition(RushDuel.MaximumMode)
	e1:SetValue(max_atk)
	c:RegisterEffect(e1)
	-- 占用3个主要怪兽区域
	local e2 = Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_MAX_MZONE)
	e2:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
	e2:SetRange(LOCATION_MZONE)
	e2:SetTargetRange(1, 0)
	e2:SetCondition(RushDuel.MaximumMode)
	e2:SetValue(1)
	c:RegisterEffect(e2)
	-- 离开场上时, 所有部件一同离开
	local e3 = Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_LEAVE_FIELD_P)
	e3:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
	e3:SetCondition(RushDuel.MaximumMode)
	e3:SetOperation(RushDuel.MaximumLeaveOperation)
	c:RegisterEffect(e3)
end
function RushDuel.MaximumSummonFilter(c, e, tp, left_code, right_code)
	return c:IsCode(left_code, right_code) and c:IsCanBeSpecialSummoned(e, 0, tp, false, false, POS_FACEUP)
end
function RushDuel.MaximumSummonCheck(g)
	return g:GetClassCount(Card.GetCode) == g:GetCount()
end
function RushDuel.MaximumSummonCondition(left_code, right_code)
	return function(e, c, og, min, max)
		if c == nil then
			return true
		end
		local tp = c:GetControler()
		local mg = Duel.GetMatchingGroup(RushDuel.MaximumSummonFilter, tp, LOCATION_HAND, 0, nil, e, tp, left_code, right_code)
		local fg = Duel.GetFieldGroup(tp, LOCATION_MZONE, 0)
		return Duel.GetMZoneCount(tp, fg) > 0 and mg:CheckSubGroup(RushDuel.MaximumSummonCheck, 2, 2)
	end
end
function RushDuel.MaximumSummonTarget(left_code, right_code)
	return function(e, tp, eg, ep, ev, re, r, rp, chk, c, og, min, max)
		local mg = Duel.GetMatchingGroup(RushDuel.MaximumSummonFilter, tp, LOCATION_HAND, 0, nil, e, tp, left_code, right_code)
		Duel.Hint(HINT_SELECTMSG, tp, aux.Stringid(120000000, 1))
		local cancel = Duel.GetCurrentChain() == 0
		local g = mg:SelectSubGroup(tp, RushDuel.MaximumSummonCheck, cancel, 2, 2)
		if g then
			g:KeepAlive()
			e:SetLabelObject(g)
			return true
		else
			return false
		end
	end
end
function RushDuel.MaximumSummonOperation(left_code, right_code)
	return function(e, tp, eg, ep, ev, re, r, rp, c, og, min, max)
		local fg = Duel.GetFieldGroup(tp, LOCATION_MZONE, 0)
		Duel.SendtoGrave(fg, REASON_RULE)
		local mg = e:GetLabelObject()
		local left = mg:GetFirst()
		local right = mg:GetNext()
		if left:IsCode(right_code) then
			left, right = right, left
		end
		Duel.MoveToField(left, tp, tp, LOCATION_MZONE, POS_FACEUP_ATTACK, false, 0x2)
		Duel.MoveToField(right, tp, tp, LOCATION_MZONE, POS_FACEUP_ATTACK, false, 0x8)
		c:SetMaterial(mg)
		Duel.Overlay(c, mg)
		mg:DeleteGroup()
	end
end
function RushDuel.MaximumSummonValue(e, c)
	return SUMMON_TYPE_MAXIMUM, 0x4
end
function RushDuel.MaximumMode(e)
	local c=e:GetHandler()
	return c:IsSummonType(SUMMON_TYPE_MAXIMUM) and c:GetOverlayCount() > 0
end
function RushDuel.MaximumLeaveOperation(e, tp, eg, ep, ev, re, r, rp)
	local c = e:GetHandler()
	local loc = c:GetDestination()
	local g = c:GetOverlayGroup()
	if loc == LOCATION_HAND then
		Duel.SendtoHand(g, nil, REASON_RULE)
	elseif loc == LOCATION_DECK then
		Duel.SendtoDeck(g, nil, 2, REASON_RULE)
	elseif loc == LOCATION_REMOVED then
		Duel.Remove(g, POS_FACEUP, REASON_RULE)
	end
end
-- Expand
-- Is Monster has Def (Maximum)
function RushDuel.IsHasDefense(c)
	return c:IsDefenseAbove(0) and not (c:IsSummonType(SUMMON_TYPE_MAXIMUM) and c:GetOverlayCount() > 0)
end
-- 设置Cost标签, 无视Cost发动效果需做额外判断时使用
function RushDuel.SetCostLabel(e)
	e:SetLabel(1)
end
-- 重置Cost标签, 无视Cost发动效果需做额外判断时使用
function RushDuel.ResetCostLabel(e)
	e:SetLabel(0)
end
-- 是否设置了Cost标签
function RushDuel.IsCostLabel(e)
	return e:GetLabel()==1
end
-- 为效果设置标签
function RushDuel.SetLabelAndObject(effect, target, set_label, set_object)
	if effect ~= nil and target ~= nil then
		if set_label ~= nil then
			effect:SetLabel(set_label(target))
		end
		if set_object ~= nil then
			effect:SetLabelObject(set_object(target))
		end
	end
end
-- 带有额外参数的过滤器
function RushDuel.Filter(filter, ...)
	local args = {...}
	return function(c)
		return filter(c, table.unpack(args))
	end
end
-- 带有额外参数的检测器
function RushDuel.Check(check, ...)
	local args = {...}
	return function(g)
		return check(g, table.unpack(args))
	end
end
-- 对卡片组里的全部卡片作位或运算
function RushDuel.GroupBor(g, func, ...)
	local result = 0
	local args = {...}
	g:ForEach(function(tc)
		result = result | func(tc, table.unpack(args))
	end)
	return result
end
-- 显示选择动画, 或者展示卡片组
function RushDuel.HintOrConfirm(group, hint_selection, confirm, target_player)
	if hint_selection then
		Duel.HintSelection(group)
	elseif confirm then
		Duel.ConfirmCards(target_player, group)
	end
end
-- 将 卡片组/卡片/效果 转化为卡片组, 对于极大怪兽, 其素材也包含其中
function RushDuel.ToMaximunGroup(target)
	local type = aux.GetValueType(target)
	local g = Group.CreateGroup()
	if type == "Group" then
		g:Merge(target)
	elseif type == "Card" then
		g:AddCard(target)
	elseif type == "Effect" then
		g:AddCard(target:GetHandler())
	end
	local overlay = Group.CreateGroup()
	g:ForEach(function(tc)
		if RushDuel.IsMaximumMode(tc) then
			overlay:Merge(tc:GetOverlayGroup())
		end
	end)
	g:Merge(overlay)
	return g
end
-- 获取可用的主要怪兽区域数量
function RushDuel.GetMZoneCount(player, max)
	local ct = Duel.GetLocationCount(player, LOCATION_MZONE)
	if Duel.IsPlayerAffectedByEffect(player, 59822133) then
		ct = math.min(ct, 1)
	end
	return math.min(ct, max)
end
-- 获取可用的魔法与陷阱区域数量
function RushDuel.GetSZoneCount(player, max)
	local ct = Duel.GetLocationCount(player, LOCATION_SZONE)
	return math.min(ct, max)
end
-- 将玩家卡组最上面的N张卡移到卡组最下面
function RushDuel.SendDeckTopToBottom(player, count)
	for i = 1, count do
		Duel.MoveSequence(Duel.GetDecktopGroup(player, 1):GetFirst(), 1)
	end
end
-- 将玩家卡组最下面的N张卡移到卡组最上面
function RushDuel.SendDeckBottomToTop(player, count)
	local g = Duel.GetFieldGroup(player, LOCATION_DECK, 0)
	for i = 1, count do
		Duel.MoveSequence(g:GetMinGroup(Card.GetSequence):GetFirst(), 0)
	end
end
-- 获取卡组底的N张卡
function RushDuel.GetDeckBottomGroup(player, count)
	local dg = Duel.GetFieldGroup(player, LOCATION_DECK, 0)
	local ct = dg:GetCount()
	if (count < ct) then
		local top = Duel.GetDecktopGroup(player, ct - count)
		dg:Sub(top)
	end
	return dg
end
-- 返回卡组并排序
function RushDuel.SendToDeckSort(target, sequence, reason, sort_player, target_player)
	local g = RushDuel.ToMaximunGroup(target)
	if g:GetCount() > 1 and sequence == SEQ_DECKBOTTOM then
		Duel.SendtoDeck(g, nil, SEQ_DECKTOP, reason)
	else
		Duel.SendtoDeck(g, nil, sequence, reason)
	end
	local og = Duel.GetOperatedGroup()
	local ct = og:FilterCount(Card.IsLocation, nil, LOCATION_DECK)
	if sequence == SEQ_DECKTOP and ct > 1 then
		Duel.SortDecktop(sort_player, target_player, ct)
	elseif sequence == SEQ_DECKBOTTOM then
		if ct > 1 then
			Duel.SortDecktop(sort_player, target_player, ct)
		end
		RushDuel.SendDeckTopToBottom(target_player, ct)
	end
	return og, ct
end
-- 获取被上级召唤解放时的基础攻击力
function RushDuel.GetBaseAttackOnTribute(c)
	local atk
	if RushDuel.IsMaximumMode(c) then
		atk = c.maximum_attack
	else
		atk = c:GetTextAttack()
	end
	return math.max(0, atk)
end
-- 条件: 是否为同名卡
function RushDuel.IsSameCode(card1, card2)
	if (card1:IsCode(120000000) or card2:IsCode(120000000)) then
		return card1:GetOriginalCode() == card2:GetOriginalCode()
	else
		return card1:IsCode(card2:GetCode())
	end
end
-- 内部方法: 检测效果范围是否覆盖
function RushDuel._private_check_effect_values(value, values, decode)
	local attach, attachs = {decode(value)}, {}
	local start = 2
	local count = #attach
	for i = start, count do
		attach[i] = attach[i] or 0
		attachs[i] = 0
	end
	for _, val in ipairs(values) do
		local _attach = {decode(val)}
		if _attach[1] then
			-- 忽略判定
		else
			for i = start, count do
				attachs[i] = attachs[i] | (_attach[i] or 0)
			end
		end
	end
	for i = start, count do
		if (attachs[i] | attach[i]) ~= attachs[i] then
			return true
		end
	end
	return false
end
-- 内部方法：检测效果破坏抗性的控制权
function RushDuel._private_swap_effect_indes(value, swap)
	if swap then
		local check = value(nil)
		return function()
			return check[1], check[3], check[2]
		end
	else
		return value
	end
end
-- 条件: 卡片是否处于"极大模式"
function RushDuel.IsMaximumMode(card)
	return card:IsLocation(LOCATION_MZONE) and card:IsSummonType(SUMMON_TYPE_MAXIMUM) and card:GetOverlayCount() > 0
end
-- 条件: 这张卡召唤的回合
function RushDuel.IsSummonTurn(card)
	return card:IsReason(REASON_SUMMON) and card:IsStatus(STATUS_SUMMON_TURN)
end
-- 条件: 这张卡特殊召唤的回合
function RushDuel.IsSpecialSummonTurn(card)
	return card:IsReason(REASON_SPSUMMON) and card:IsStatus(STATUS_SPSUMMON_TURN)
end
-- 条件: 玩家的LP在 lp 以上
function RushDuel.IsLPAbove(player, lp)
	return Duel.GetLP(player) >= lp
end
-- 条件: 玩家的LP在 lp 以下
function RushDuel.IsLPBelow(player, lp)
	return Duel.GetLP(player) <= lp
end
-- 条件: 玩家的LP比对方少 lp 以上
function RushDuel.IsLPBelowOpponent(player, lp)
	return Duel.GetLP(player) <= Duel.GetLP(1 - player) - (lp or 0)
end
-- 条件: 守备力为 def
function RushDuel.IsDefense(card, def)
	return card:IsDefense(def) and not RushDuel.IsMaximumMode(card)
end
-- 条件: 守备力在 def 以上
function RushDuel.IsDefenseAbove(card, def)
	return card:IsDefenseAbove(def) and not RushDuel.IsMaximumMode(card)
end
-- 条件: 守备力在 def 以下
function RushDuel.IsDefenseBelow(card, def)
	return card:IsDefenseBelow(def) and not RushDuel.IsMaximumMode(card)
end
-- 条件: 可否改变守备力
function RushDuel.IsCanChangeDef(card)
	return card:IsDefenseAbove(0) and not RushDuel.IsMaximumMode(card)
end
-- 条件: 可否改变表示形式
function RushDuel.IsCanChangePosition(card)
	return card:IsCanChangePosition() and not RushDuel.IsMaximumMode(card)
end
-- 条件: 可否特殊召唤
function RushDuel.IsCanBeSpecialSummoned(card, effect, player, position)
	return card:IsCanBeSpecialSummoned(effect, 0, player, false, false, position)
end
-- 条件: 位置变化前的控制者
function RushDuel.IsPreviousControler(card, player)
	return card:GetPreviousControler() == player
end
-- 条件: 位置变化前的类型
function RushDuel.IsPreviousType(card, type)
	return (card:GetPreviousTypeOnField() & type) ~= 0
end
-- 条件: 位置变化前的属性
function RushDuel.IsPreviousAttribute(card, attr)
	return (card:GetPreviousAttributeOnField() & attr) ~= 0
end
-- 条件: 位置变化前的种族
function RushDuel.IsPreviousRace(card, race)
	return (card:GetPreviousRaceOnField() & race) ~= 0
end
-- 条件：可以攻击
function RushDuel.IsCanAttack(card)
	return not card:IsHasEffect(EFFECT_CANNOT_ATTACK)
end
-- 条件: 可否赋予效果 - 直接攻击
function RushDuel.IsCanAttachDirectAttack(card)
	return RushDuel.IsCanAttack(card) and not card:IsHasEffect(EFFECT_DIRECT_ATTACK) and not card:IsHasEffect(EFFECT_CANNOT_DIRECT_ATTACK)
end
-- 条件: 可否赋予效果 - 贯通
function RushDuel.IsCanAttachPierce(card)
	return RushDuel.IsCanAttack(card) and not card:IsHasEffect(EFFECT_PIERCE)
end
-- 条件: 可否赋予效果 - 多次攻击
function RushDuel.IsCanAttachExtraAttack(card, value)
	if not RushDuel.IsCanAttack(card) then
		return false
	end
	local values = RushDuel.GetEffectValues(card, EFFECT_EXTRA_ATTACK)
	for _, val in ipairs(values) do
		if val >= value then
			return false
		end
	end
	return true
end
-- 条件: 可否赋予效果 - 多次攻击 (怪兽限定)
function RushDuel.IsCanAttachExtraAttackMonster(card, value)
	if not RushDuel.IsCanAttack(card) then
		return false
	end
	local values = RushDuel.GetEffectValues(card, EFFECT_EXTRA_ATTACK_MONSTER)
	for _, val in ipairs(values) do
		if val >= value then
			return false
		end
	end
	return true
end
-- 条件: 可否赋予效果 - 全体攻击
function RushDuel.IsCanAttachAttackAll(card, value)
	return RushDuel.IsCanAttack(card)
end
-- 条件: 可否赋予效果 - 双重解放
function RushDuel.IsCanAttachDoubleTribute(card, value)
	if Duel.IsPlayerAffectedByEffect(card:GetControler(), EFFECT_CANNOT_DOUBLE_TRIBUTE) then
		return false
	end
	if card:IsHasEffect(EFFECT_UNRELEASABLE_SUM) then
		return false
	end
	local values = RushDuel.GetEffectValues(card, EFFECT_DOUBLE_TRIBUTE)
	return RushDuel.CheckValueDoubleTribute(values, value)
end
-- 条件: 可否赋予效果 - 使用对方的怪兽解放
function RushDuel.IsCanAttachOpponentTribute(card, flag)
	return card:GetFlagEffect(flag) == 0
end
-- 条件: 可否赋予效果 - 战斗破坏抗性
function RushDuel.IsCanAttachBattleIndes(card, value)
	local values = RushDuel.GetEffectValues(card, EFFECT_INDESTRUCTABLE_BATTLE)
	for _, val in ipairs(values) do
		if val == 1 then
			return false
		end
	end
	return true
end
-- 条件: 可否赋予效果 - 效果破坏抗性
function RushDuel.IsCanAttachEffectIndes(card, player, value)
	local swap = card:GetOwner() ~= player
	local values = RushDuel.GetEffectValues(card, EFFECT_INDESTRUCTABLE_EFFECT)
	return RushDuel.CheckValueEffectIndesType(swap, values, value)
end
-- 条件: 玩家是否能够使用特定种族进行攻击
function RushDuel.IsPlayerCanUseRaceAttack(player, race)
	if Duel.IsPlayerAffectedByEffect(player, EFFECT_PLAYER_CANNOT_ATTACK) then
		return false
	end
	local effects = {Duel.IsPlayerAffectedByEffect(player, EFFECT_PLAYER_RACE_CANNOT_ATTACK)}
	for i, effect in ipairs(effects) do
		if (race & effect:GetValue()) == race then
			return false
		end
	end
	return true
end
-- 额外条件: 最后的操作是否包含某种卡
function RushDuel.IsOperatedGroupExists(filter, count, expect)
	return filter == nil or Duel.GetOperatedGroup():IsExists(filter, count, expect)
end
-- 内部方法: 选择匹配卡片, 执行操作
function RushDuel._private_cost_select_match(hint, filter, s_range, o_range, min, max, except_self, action)
	return function(e, tp, eg, ep, ev, re, r, rp, chk)
		local expect = nil
		if except_self then
			expect = e:GetHandler()
		end
		if chk == 0 then
			return Duel.IsExistingMatchingCard(filter, tp, s_range, o_range, min, expect, e, tp, eg, ep, ev, re, r, rp)
		end
		Duel.Hint(HINT_SELECTMSG, tp, hint)
		local g = Duel.SelectMatchingCard(tp, filter, tp, s_range, o_range, min, max, expect, e, tp, eg, ep, ev, re, r, rp)
		action(g, e, tp, eg, ep, ev, re, r, rp)
	end
end
-- 内部方法: 选择子卡片组, 执行操作
function RushDuel._private_cost_select_group(hint, filter, check, s_range, o_range, min, max, except_self, action)
	return function(e, tp, eg, ep, ev, re, r, rp, chk)
		local expect = nil
		if except_self then
			expect = e:GetHandler()
		end
		local g = Duel.GetMatchingGroup(filter, tp, s_range, o_range, expect, e, tp, eg, ep, ev, re, r, rp)
		if chk == 0 then
			return g:CheckSubGroup(check, min, max, e, tp, eg, ep, ev, re, r, rp)
		end
		Duel.Hint(HINT_SELECTMSG, tp, hint)
		local sg = g:SelectSubGroup(tp, check, false, min, max, e, tp, eg, ep, ev, re, r, rp)
		action(sg, e, tp, eg, ep, ev, re, r, rp)
	end
end
-- 内部方法: 送去墓地动作
function RushDuel._private_action_send_grave(reason, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return function(g, e, tp, eg, ep, ev, re, r, rp)
		RushDuel.HintOrConfirm(g, hint_selection, confirm, 1 - tp)
		RushDuel.SetLabelAndObject(e, g, set_label_before, set_object_before)
		if Duel.SendtoGrave(g, reason) ~= 0 and (set_label_after ~= nil or set_object_after ~= nil) then
			local og = Duel.GetOperatedGroup()
			RushDuel.SetLabelAndObject(e, og, set_label_after, set_object_after)
		end
	end
end
-- 内部方法: 返回卡组动作
function RushDuel._private_action_send_deck_sort(sequence, reason, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return function(g, e, tp, eg, ep, ev, re, r, rp)
		RushDuel.HintOrConfirm(g, hint_selection, confirm, 1 - tp)
		RushDuel.SetLabelAndObject(e, g, set_label_before, set_object_before)
		local og, ct = RushDuel.SendToDeckSort(g, sequence, reason, tp, tp)
		RushDuel.SetLabelAndObject(e, og, set_label_after, set_object_after)
	end
end
-- 内部方法: 返回卡组上面或下面动作
function RushDuel._private_action_send_deck_top_or_bottom(top_desc, bottom_desc, reason, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return function(g, e, tp, eg, ep, ev, re, r, rp)
		RushDuel.HintOrConfirm(g, hint_selection, confirm, 1 - tp)
		RushDuel.SetLabelAndObject(e, g, set_label_before, set_object_before)
		local sequence = Duel.SelectOption(tp, top_desc, bottom_desc)
		local og, ct = RushDuel.SendToDeckSort(g, sequence, reason, tp, tp)
		RushDuel.SetLabelAndObject(e, og, set_label_after, set_object_after)
	end
end
-- 内部方法: 返回手卡动作
function RushDuel._private_action_send_hand(reason, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return function(g, e, tp, eg, ep, ev, re, r, rp)
		if hint_selection then
			Duel.HintSelection(g)
		end
		RushDuel.SetLabelAndObject(e, g, set_label_before, set_object_before)
		if Duel.SendtoHand(g, nil, reason) ~= 0 then
			local og = Duel.GetOperatedGroup()
			if confirm then
				Duel.ConfirmCards(1 - tp, og)
			end
			RushDuel.SetLabelAndObject(e, og, set_label_after, set_object_after)
		end
	end
end
-- 代价: 选择匹配卡片, 送去墓地
function RushDuel.CostSendMatchToGrave(filter, field, min, max, except_self, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	local action = RushDuel._private_action_send_grave(REASON_COST, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel._private_cost_select_match(HINTMSG_TOGRAVE, filter, field, 0, min, max, except_self, action)
end
-- 代价: 选择子卡片组, 送去墓地
function RushDuel.CostSendGroupToGrave(filter, check, field, min, max, except_self, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	local action = RushDuel._private_action_send_grave(REASON_COST, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel._private_cost_select_group(HINTMSG_TOGRAVE, filter, check, field, 0, min, max, except_self, action)
end
-- 代价: 选择匹配卡片, 返回卡组 (排序)
function RushDuel.CostSendMatchToDeckSort(filter, field, min, max, except_self, sequence, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	local action = RushDuel._private_action_send_deck_sort(sequence, REASON_COST, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel._private_cost_select_match(HINTMSG_TODECK, filter, field, 0, min, max, except_self, action)
end
-- 代价: 选择子卡片组, 返回卡组 (排序)
function RushDuel.CostSendGroupToDeckSort(filter, check, field, min, max, except_self, sequence, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	local action = RushDuel._private_action_send_deck_sort(sequence, REASON_COST, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel._private_cost_select_group(HINTMSG_TODECK, filter, check, field, 0, min, max, except_self, action)
end
-- 代价: 选择匹配卡片, 返回卡组上面或下面
function RushDuel.CostSendMatchToDeckTopOrBottom(filter, field, min, max, except_self, top_desc, bottom_desc, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	local action = RushDuel._private_action_send_deck_top_or_bottom(top_desc, bottom_desc, REASON_COST, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel._private_cost_select_match(HINTMSG_TODECK, filter, field, 0, min, max, except_self, action)
end
-- 代价: 选择子卡片组, 返回卡组上面或下面
function RushDuel.CostSendGroupToDeckTopOrBottom(filter, check, field, min, max, except_self, top_desc, bottom_desc, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	local action = RushDuel._private_action_send_deck_top_or_bottom(top_desc, bottom_desc, REASON_COST, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel._private_cost_select_group(HINTMSG_TODECK, filter, check, field, 0, min, max, except_self, action)
end
-- 代价: 选择匹配卡片, 返回手卡
function RushDuel.CostSendMatchToHand(filter, field, min, max, except_self, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	local action = RushDuel._private_action_send_hand(REASON_COST, hint_selection, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel._private_cost_select_match(HINTMSG_TOGRAVE, filter, field, 0, min, max, except_self, action)
end
-- 代价: 支付LP
function RushDuel.CostPayLP(lp)
	return function(e, tp, eg, ep, ev, re, r, rp, chk)
		if chk == 0 then
			return Duel.CheckLPCost(tp, lp)
		end
		Duel.PayLPCost(tp, lp)
	end
end
-- 代价: 把手卡给对方观看
function RushDuel.CostShowHand(filter, min, max, set_label, set_object)
	return RushDuel._private_cost_select_match(HINTMSG_CONFIRM, filter, LOCATION_HAND, 0, min, max, true, function(g, e, tp, eg, ep, ev, re, r, rp)
		RushDuel.SetLabelAndObject(e, g, set_label, set_object)
		Duel.ConfirmCards(1 - tp, g)
		Duel.ShuffleHand(tp)
	end)
end
-- 代价: 把手卡给对方观看 (子卡片组)
function RushDuel.CostShowGroupHand(filter, check, min, max, set_label, set_object)
	return RushDuel._private_cost_select_group(HINTMSG_CONFIRM, filter, check, LOCATION_HAND, 0, min, max, true, function(g, e, tp, eg, ep, ev, re, r, rp)
		RushDuel.SetLabelAndObject(e, g, set_label, set_object)
		Duel.ConfirmCards(1 - tp, g)
		Duel.ShuffleHand(tp)
	end)
end
-- 代价: 把额外卡组给对方观看 (子卡片组)
function RushDuel.CostShowGroupExtra(filter, check, min, max, set_label, set_object)
	return RushDuel._private_cost_select_group(HINTMSG_CONFIRM, filter, check, LOCATION_EXTRA, 0, min, max, false, function(g, e, tp, eg, ep, ev, re, r, rp)
		RushDuel.SetLabelAndObject(e, g, set_label, set_object)
		Duel.ConfirmCards(1 - tp, g)
	end)
end
-- 代价: 从卡组上面把卡送去墓地
function RushDuel.CostSendDeckTopToGrave(count, set_label, set_object)
	return function(e, tp, eg, ep, ev, re, r, rp, chk)
		if chk == 0 then
			return Duel.IsPlayerCanDiscardDeckAsCost(tp, count)
		end
		if Duel.DiscardDeck(tp, count, REASON_COST) ~= 0 and (set_label ~= nil or set_object ~= nil) then
			local og = Duel.GetOperatedGroup()
			RushDuel.SetLabelAndObject(e, og, set_label, set_object)
		end
	end
end
-- 代价: 从卡组下面把卡送去墓地
function RushDuel.CostSendDeckBottomToGrave(count, set_label, set_object)
	return function(e, tp, eg, ep, ev, re, r, rp, chk)
		if chk == 0 then
			return Duel.IsPlayerCanDiscardDeckAsCost(tp, count)
		end
		local dg = RushDuel.GetDeckBottomGroup(tp, count)
		Duel.DisableShuffleCheck()
		if Duel.SendtoGrave(dg, REASON_COST) ~= 0 and (set_label ~= nil or set_object ~= nil) then
			local og = Duel.GetOperatedGroup()
			RushDuel.SetLabelAndObject(e, og, set_label, set_object)
		end
	end
end
-- 代价: 把自己场上表侧表示的这张卡送去墓地
function RushDuel.CostSendSelfToGrave()
	return function(e, tp, eg, ep, ev, re, r, rp, chk)
		if chk == 0 then
			return e:GetHandler():IsAbleToGraveAsCost()
		end
		Duel.SendtoGrave(RushDuel.ToMaximunGroup(e:GetHandler()), REASON_COST)
	end
end
-- 代价: 把手卡送去墓地
function RushDuel.CostSendHandToGrave(filter, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToGrave(filter, LOCATION_HAND, min, max, true, false, false, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 把手卡送去墓地 (子卡片组)
function RushDuel.CostSendHandSubToGrave(filter, check, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendGroupToGrave(filter, check, LOCATION_HAND, min, max, true, false, false, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 把怪兽送去墓地
function RushDuel.CostSendMZoneToGrave(filter, min, max, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToGrave(filter, LOCATION_MZONE, min, max, except_self, false, false, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 把场上的卡送去墓地
function RushDuel.CostSendOnFieldToGrave(filter, min, max, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToGrave(filter, LOCATION_ONFIELD, min, max, except_self, false, false, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 让怪兽返回卡组
function RushDuel.CostSendMZoneToDeck(filter, min, max, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToDeckSort(filter, LOCATION_MZONE, min, max, except_self, SEQ_DECKSHUFFLE, true, false, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 让怪兽返回卡组下面
function RushDuel.CostSendMZoneToDeckBottom(filter, min, max, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToDeckSort(filter, LOCATION_MZONE, min, max, except_self, SEQ_DECKBOTTOM, true, false, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 让怪兽返回卡组上面或下面
function RushDuel.CostSendMZoneToDeckTopOrBottom(filter, min, max, top_desc, bottom_desc, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToDeckTopOrBottom(filter, LOCATION_MZONE, min, max, except_self, top_desc, bottom_desc, true, false, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 让场上的卡返回卡组
function RushDuel.CostSendOnFieldToDeck(filter, min, max, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToDeckSort(filter, LOCATION_ONFIELD, min, max, except_self, SEQ_DECKSHUFFLE, true, false, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 让场上的卡返回卡组下面
function RushDuel.CostSendOnFieldToDeckBottom(filter, min, max, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToDeckSort(filter, LOCATION_ONFIELD, min, max, except_self, SEQ_DECKBOTTOM, true, false, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 把场上的卡送去墓地 (子卡片组)
function RushDuel.CostSendOnFieldSubToGrave(filter, check, min, max, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendGroupToGrave(filter, check, LOCATION_ONFIELD, min, max, except_self, false, false, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 把手卡返回卡组上面
function RushDuel.CostSendHandToDeckTop(filter, min, max, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToDeckSort(filter, LOCATION_HAND, min, max, true, SEQ_DECKTOP, false, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 把手卡返回卡组
function RushDuel.CostSendHandToDeck(filter, min, max, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToDeckSort(filter, LOCATION_HAND, min, max, true, SEQ_DECKSHUFFLE, false, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 把手卡返回卡组下面
function RushDuel.CostSendHandToDeckBottom(filter, min, max, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToDeckSort(filter, LOCATION_HAND, min, max, true, SEQ_DECKBOTTOM, false, confirm, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 让墓地的卡返回卡组
function RushDuel.CostSendGraveToDeck(filter, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToDeckSort(filter, LOCATION_GRAVE, min, max, false, SEQ_DECKSHUFFLE, false, true, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 让墓地的卡返回卡组 (子卡片组)
function RushDuel.CostSendGraveSubToDeck(filter, check, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendGroupToDeckSort(filter, check, LOCATION_GRAVE, min, max, false, SEQ_DECKSHUFFLE, false, true, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 让墓地的卡返回卡组上面
function RushDuel.CostSendGraveToDeckTop(filter, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToDeckSort(filter, LOCATION_GRAVE, min, max, false, SEQ_DECKTOP, false, true, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 让墓地的卡返回卡组下面
function RushDuel.CostSendGraveToDeckBottom(filter, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToDeckSort(filter, LOCATION_GRAVE, min, max, false, SEQ_DECKBOTTOM, false, true, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 让墓地的卡返回卡组下面 (子卡片组)
function RushDuel.CostSendGraveSubToDeckBottom(filter, check, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendGroupToDeckSort(filter, check, LOCATION_GRAVE, min, max, false, SEQ_DECKBOTTOM, false, true, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 让墓地的卡返回卡组上面或下面
function RushDuel.CostSendGraveToDeckTopOrBottom(filter, min, max, top_desc, bottom_desc, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToDeckTopOrBottom(filter, LOCATION_GRAVE, min, max, false, top_desc, bottom_desc, false, true, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 让墓地的卡返回卡组上面或下面 (子卡片组)
function RushDuel.CostSendGraveSubToDeckTopOrBottom(filter, check, min, max, top_desc, bottom_desc, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendGroupToDeckTopOrBottom(filter, check, LOCATION_GRAVE, min, max, false, top_desc, bottom_desc, false, true, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 代价: 把怪兽返回手卡
function RushDuel.CostSendMZoneToHand(filter, min, max, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
	return RushDuel.CostSendMatchToHand(filter, LOCATION_MZONE, min, max, except_self, false, true, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- 可选操作: 盲堆
function RushDuel.CanDiscardDeck(desc, player, count, break_effect)
	if Duel.IsPlayerCanDiscardDeck(player, count) and Duel.SelectYesNo(player, desc) then
		if break_effect then
			Duel.BreakEffect()
		end
		Duel.DiscardDeck(player, count, REASON_EFFECT)
	end
end
-- 对玩家效果: 抽卡 - 对象
function RushDuel.TargetDraw(player, count)
	Duel.SetTargetPlayer(player)
	Duel.SetTargetParam(count)
	Duel.SetOperationInfo(0, CATEGORY_DRAW, nil, 0, player, count)
end
-- 对玩家效果: 恢复 - 对象
function RushDuel.TargetRecover(player, recover)
	Duel.SetTargetPlayer(player)
	Duel.SetTargetParam(recover)
	Duel.SetOperationInfo(0, CATEGORY_RECOVER, nil, 0, player, recover)
end
-- 对玩家效果: 伤害 - 对象
function RushDuel.TargetDamage(player, damage)
	Duel.SetTargetPlayer(player)
	Duel.SetTargetParam(damage)
	Duel.SetOperationInfo(0, CATEGORY_DAMAGE, nil, 0, player, damage)
end
-- 对玩家效果: 盲堆 - 对象
function RushDuel.TargetDiscardDeck(player, count)
	Duel.SetTargetPlayer(player)
	Duel.SetTargetParam(count)
	Duel.SetOperationInfo(0, CATEGORY_DECKDES, nil, 0, player, count)
end
-- 对玩家效果: 抽卡 - 结算
function RushDuel.Draw(player, count)
	local p, d = Duel.GetChainInfo(0, CHAININFO_TARGET_PLAYER, CHAININFO_TARGET_PARAM)
	return Duel.Draw(player or p, count or d, REASON_EFFECT)
end
-- 对玩家效果: 恢复 - 结算
function RushDuel.Recover(player, recover)
	local p, d = Duel.GetChainInfo(0, CHAININFO_TARGET_PLAYER, CHAININFO_TARGET_PARAM)
	return Duel.Recover(player or p, recover or d, REASON_EFFECT)
end
-- 对玩家效果: 伤害 - 结算
function RushDuel.Damage(player, damage)
	local p, d = Duel.GetChainInfo(0, CHAININFO_TARGET_PLAYER, CHAININFO_TARGET_PARAM)
	return Duel.Damage(player or p, damage or d, REASON_EFFECT)
end
-- 对玩家效果: 盲堆 - 结算
function RushDuel.DiscardDeck(player, count)
	local p, d = Duel.GetChainInfo(0, CHAININFO_TARGET_PLAYER, CHAININFO_TARGET_PARAM)
	return Duel.DiscardDeck(player or p, count or d, REASON_EFFECT)
end
-- 内部方法: 选择匹配卡片, 执行操作
function RushDuel._private_action_select_match(hint, filter, tp, s_range, o_range, min, max, expect, hint_selection, confirm, action, ...)
	if min < 2 or Duel.IsExistingMatchingCard(filter, tp, s_range, o_range, min, expect, ...) then
		Duel.Hint(HINT_SELECTMSG, tp, hint)
		local g = Duel.SelectMatchingCard(tp, filter, tp, s_range, o_range, min, max, expect, ...)
		if g:GetCount() > 0 then
			RushDuel.HintOrConfirm(g, hint_selection, confirm, 1 - tp)
			return action(g, ...)
		end
	end
	return 0
end
-- 内部方法: 可以选择匹配卡片, 执行操作
function RushDuel._private_action_can_select_match(desc, hint, filter, tp, s_range, o_range, min, max, expect, hint_selection, confirm, action, ...)
	if Duel.IsExistingMatchingCard(filter, tp, s_range, o_range, min, expect, ...) and Duel.SelectYesNo(tp, desc) then
		Duel.Hint(HINT_SELECTMSG, tp, hint)
		local g = Duel.SelectMatchingCard(tp, filter, tp, s_range, o_range, min, max, expect, ...)
		if g:GetCount() > 0 then
			RushDuel.HintOrConfirm(g, hint_selection, confirm, 1 - tp)
			return action(g, ...)
		end
	end
	return 0
end
-- 内部方法: 选择子卡片组, 执行操作
function RushDuel._private_action_select_group(hint, filter, check, tp, s_range, o_range, min, max, expect, hint_selection, confirm, action, ...)
	local g = Duel.GetMatchingGroup(filter, tp, s_range, o_range, expect, ...)
	if g:CheckSubGroup(check, min, max, ...) then
		Duel.Hint(HINT_SELECTMSG, tp, hint)
		local sg = g:SelectSubGroup(tp, check, false, min, max, ...)
		if sg:GetCount() > 0 then
			RushDuel.HintOrConfirm(sg, hint_selection, confirm, 1 - tp)
			return action(sg, ...)
		end
	end
	return 0
end
-- 内部方法: 可以选择子卡片组, 执行操作
function RushDuel._private_action_can_select_group(desc, hint, filter, check, tp, s_range, o_range, min, max, expect, hint_selection, confirm, action, ...)
	local g = Duel.GetMatchingGroup(filter, tp, s_range, o_range, expect, ...)
	if g:CheckSubGroup(check, min, max, ...) and Duel.SelectYesNo(tp, desc) then
		Duel.Hint(HINT_SELECTMSG, tp, hint)
		local sg = g:SelectSubGroup(tp, check, false, min, max, ...)
		if sg:GetCount() > 0 then
			RushDuel.HintOrConfirm(sg, hint_selection, confirm, 1 - tp)
			return action(sg, ...)
		end
	end
	return 0
end
-- 内部方法: 是否包含公开区域
function RushDuel._private_is_include_public(s_range, o_range)
	return (s_range | o_range) & (LOCATION_ONFIELD | LOCATION_GRAVE | LOCATION_REMOVED) ~= 0
end
-- 内部方法: 特殊召唤
function RushDuel._special_summon(target, effect, player, position, break_effect, target_player)
	if break_effect then
		Duel.BreakEffect()
	end
	local ct = Duel.SpecialSummon(target, 0, player, target_player or player, false, false, position)
	if (position & POS_FACEDOWN) ~= 0 then
		Duel.ConfirmCards(1 - player, target)
		local og = Duel.GetOperatedGroup():Filter(Card.IsFacedown, nil)
		if og:GetCount() > 1 then
			Duel.ShuffleSetCard(og)
		end
	end
	return ct
end
-- 内部方法: 盖放魔法陷阱
function RushDuel._set_spell_trap(target, effect, player, break_effect)
	if break_effect then
		Duel.BreakEffect()
	end
	return Duel.SSet(player, target)
end
-- 操作: 选择匹配卡片
function RushDuel.SelectAndDoAction(hint, filter, tp, s_range, o_range, min, max, expect, action)
	local hint_selection = RushDuel._private_is_include_public(s_range, o_range)
	return RushDuel._private_action_select_match(hint, filter, tp, s_range, o_range, min, max, expect, hint_selection, false, action)
end
-- 可选操作: 选择匹配卡片
function RushDuel.CanSelectAndDoAction(desc, hint, filter, tp, s_range, o_range, min, max, expect, action)
	local hint_selection = RushDuel._private_is_include_public(s_range, o_range)
	return RushDuel._private_action_can_select_match(desc, hint, filter, tp, s_range, o_range, min, max, expect, hint_selection, false, action)
end
-- 操作: 选择子卡片组
function RushDuel.SelectGroupAndDoAction(hint, filter, check, tp, s_range, o_range, min, max, expect, action)
	local hint_selection = RushDuel._private_is_include_public(s_range, o_range)
	return RushDuel._private_action_select_group(hint, filter, check, tp, s_range, o_range, min, max, expect, hint_selection, false, action)
end
-- 可选操作: 选择子卡片组
function RushDuel.CanSelectGroupAndDoAction(desc, hint, filter, check, tp, s_range, o_range, min, max, expect, action)
	local hint_selection = RushDuel._private_is_include_public(s_range, o_range)
	return RushDuel._private_action_can_select_group(desc, hint, filter, check, tp, s_range, o_range, min, max, expect, hint_selection, false, action)
end
-- 操作: 选择怪兽特殊召唤
function RushDuel.SelectAndSpecialSummon(filter, tp, s_range, o_range, min, max, expect, e, pos, break_effect, target_player)
	local ct = RushDuel.GetMZoneCount(target_player or tp, max)
	if ct >= min then
		return RushDuel._private_action_select_match(HINTMSG_SPSUMMON, filter, tp, s_range, o_range, min, ct, expect, false, false, RushDuel._special_summon, e, tp, pos, break_effect, target_player)
	end
	return 0
end
-- 可选操作: 选择怪兽特殊召唤
function RushDuel.CanSelectAndSpecialSummon(desc, filter, tp, s_range, o_range, min, max, expect, e, pos, break_effect, target_player)
	local ct = RushDuel.GetMZoneCount(target_player or tp, max)
	if ct >= min then
		return RushDuel._private_action_can_select_match(desc, HINTMSG_SPSUMMON, filter, tp, s_range, o_range, min, ct, expect, false, false, RushDuel._special_summon, e, tp, pos, break_effect,
			target_player)
	end
	return 0
end
-- 操作: 选择怪兽特殊召唤 (子卡片组)
function RushDuel.SelectGroupAndSpecialSummon(filter, check, tp, s_range, o_range, min, max, expect, e, pos, break_effect, target_player)
	local ct = RushDuel.GetMZoneCount(target_player or tp, max)
	if ct >= min then
		return RushDuel._private_action_select_group(HINTMSG_SPSUMMON, filter, check, tp, s_range, o_range, min, ct, expect, false, false, RushDuel._special_summon, e, tp, pos, break_effect,
			target_player)
	end
	return 0
end
-- 可选操作: 选择怪兽特殊召唤 (子卡片组)
function RushDuel.CanSelectGroupAndSpecialSummon(desc, filter, check, tp, s_range, o_range, min, max, expect, e, pos, break_effect, target_player)
	local ct = RushDuel.GetMZoneCount(target_player or tp, max)
	if ct >= min then
		return RushDuel._private_action_can_select_group(desc, HINTMSG_SPSUMMON, filter, check, tp, s_range, o_range, min, ct, expect, false, false, RushDuel._special_summon, e, tp, pos, break_effect,
			target_player)
	end
	return 0
end
-- 操作: 选择魔法，陷阱卡盖放
function RushDuel.SelectAndSet(filter, tp, s_range, o_range, min, max, expect, e, break_effect)
	local ct = RushDuel.GetSZoneCount(tp, max)
	if ct >= min then
		return RushDuel._private_action_select_match(HINTMSG_SET, filter, tp, s_range, o_range, min, ct, expect, false, false, RushDuel._set_spell_trap, e, tp, break_effect)
	end
	return 0
end
-- 可选操作: 选择魔法，陷阱卡盖放
function RushDuel.CanSelectAndSet(desc, filter, tp, s_range, o_range, min, max, expect, e, break_effect)
	local ct = RushDuel.GetSZoneCount(tp, max)
	if ct >= min then
		return RushDuel._private_action_can_select_match(desc, HINTMSG_SET, filter, tp, s_range, o_range, min, ct, expect, false, false, RushDuel._set_spell_trap, e, tp, break_effect)
	end
	return 0
end
-- 操作: 选择魔法，陷阱卡盖放 (子卡片组)
function RushDuel.SelectGroupAndSet(filter, check, tp, s_range, o_range, min, max, expect, e, break_effect)
	local ct = RushDuel.GetSZoneCount(tp, max)
	if ct >= min then
		return RushDuel._private_action_select_group(HINTMSG_SET, filter, check, tp, s_range, o_range, min, ct, expect, false, false, RushDuel._set_spell_trap, e, tp, break_effect)
	end
	return 0
end
-- 可选操作: 选择魔法，陷阱卡盖放 (子卡片组)
function RushDuel.CanSelectGroupAndSet(desc, filter, check, tp, s_range, o_range, min, max, expect, e, break_effect)
	local ct = RushDuel.GetSZoneCount(tp, max)
	if ct >= min then
		return RushDuel._private_action_can_select_group(desc, HINTMSG_SET, filter, check, tp, s_range, o_range, min, ct, expect, false, false, RushDuel._set_spell_trap, e, tp, break_effect)
	end
	return 0
end
-- 操作: 改变表示形式
function RushDuel.ChangePosition(target, pos)
	if pos == nil then
		return Duel.ChangePosition(target, POS_FACEUP_DEFENSE, POS_FACEUP_DEFENSE, POS_FACEUP_ATTACK, POS_FACEUP_ATTACK)
	else
		return Duel.ChangePosition(target, pos)
	end
end
-- 操作: 加入/返回手卡, 并给对方确认
function RushDuel.SendToHandAndExists(target, confirm_player, filter, count, expect)
	local g = RushDuel.ToMaximunGroup(target)
	if Duel.SendtoHand(g, nil, REASON_EFFECT) == 0 then
		return false
	end
	if confirm_player ~= nil then
		Duel.ConfirmCards(confirm_player, g)
	end
	return RushDuel.IsOperatedGroupExists(filter, count, expect)
end
-- 操作: 返回对方手卡, 不能确认那些卡
function RushDuel.SendToOpponentHand(target)
	local g = RushDuel.ToMaximunGroup(target)
	return Duel.SendtoHand(g, nil, REASON_EFFECT)
end
-- 操作: 送去墓地
function RushDuel.SendToGraveAndExists(target, filter, count, expect)
	local g = RushDuel.ToMaximunGroup(target)
	return Duel.SendtoGrave(g, REASON_EFFECT) ~= 0 and RushDuel.IsOperatedGroupExists(filter, count, expect)
end
-- 操作: 从卡组上面把卡送去墓地
function RushDuel.SendDeckTopToGraveAndExists(player, card_count, filter, count, expect)
	return Duel.DiscardDeck(player, card_count, REASON_EFFECT) ~= 0 and RushDuel.IsOperatedGroupExists(filter, count, expect)
end
-- 操作: 从卡组下面把卡送去墓地
function RushDuel.SendDeckBottomToGraveAndExists(player, card_count, filter, count, expect)
	local dg = RushDuel.GetDeckBottomGroup(player, card_count)
	if dg:GetCount() == 0 then
		return false
	end
	Duel.DisableShuffleCheck()
	return Duel.SendtoGrave(dg, REASON_EFFECT) ~= 0 and RushDuel.IsOperatedGroupExists(filter, count, expect)
end
-- 操作: 随机选对方的手卡送去墓地
function RushDuel.SendOpponentHandToGrave(tp, desc, min, max)
	local g = Duel.GetFieldGroup(tp, 0, LOCATION_HAND)
	local ct = g:GetCount()
	if ct < min then
		return 0
	end
	local ops = {}
	for i = min, math.min(max, ct), min do
		table.insert(ops, i)
	end
	local ac = 0
	if #ops == 1 then
		ac = table.remove(ops)
	elseif #ops > 1 then
		Duel.Hint(HINT_SELECTMSG, tp, desc)
		ac = Duel.AnnounceNumber(tp, table.unpack(ops))
	end
	if ac > 0 then
		local sg = g:RandomSelect(tp, ac)
		return Duel.SendtoGrave(sg, REASON_EFFECT)
	end
	return 0
end
-- 操作: 返回卡组
function RushDuel.SendToDeckAndExists(target, filter, count, expect)
	local g = RushDuel.ToMaximunGroup(target)
	return Duel.SendtoDeck(g, nil, SEQ_DECKSHUFFLE, REASON_EFFECT) ~= 0 and RushDuel.IsOperatedGroupExists(filter, count, expect)
end
-- 操作: 返回卡组上面 (排序)
function RushDuel.SendToDeckTop(target, sort_player, target_player, sort)
	local g = RushDuel.ToMaximunGroup(target)
	if sort then
		local og, ct = RushDuel.SendToDeckSort(g, SEQ_DECKTOP, REASON_EFFECT, sort_player, target_player)
		return ct
	else
		return Duel.SendtoDeck(g, nil, SEQ_DECKTOP, REASON_EFFECT)
	end
end
-- 操作: 返回卡组下面 (排序)
function RushDuel.SendToDeckBottom(target, sort_player, target_player, sort)
	local g = RushDuel.ToMaximunGroup(target)
	if sort then
		local og, ct = RushDuel.SendToDeckSort(g, SEQ_DECKBOTTOM, REASON_EFFECT, sort_player, target_player)
		return ct
	else
		return Duel.SendtoDeck(g, nil, SEQ_DECKBOTTOM, REASON_EFFECT)
	end
end
-- 操作: 返回对方卡组上面 (排序)
function RushDuel.SendToOpponentDeckTop(target, player)
	local g = RushDuel.ToMaximunGroup(target)
	if g:GetCount() == 1 then
		return Duel.SendtoDeck(g, nil, SEQ_DECKTOP, REASON_EFFECT)
	else
		local og, ct = RushDuel.SendToDeckSort(g, SEQ_DECKTOP, REASON_EFFECT, player, 1 - player)
		return ct
	end
end
-- 操作: 返回对方卡组下面 (排序)
function RushDuel.SendToOpponentDeckBottom(target, player)
	local g = RushDuel.ToMaximunGroup(target)
	if g:GetCount() == 1 then
		return Duel.SendtoDeck(g, nil, SEQ_DECKBOTTOM, REASON_EFFECT)
	else
		local og, ct = RushDuel.SendToDeckSort(g, SEQ_DECKBOTTOM, REASON_EFFECT, player, 1 - player)
		return ct
	end
end
-- 操作: 翻开卡组并选择卡
function RushDuel.RevealDeckTopAndSelect(player, count, hint, filter, min, max)
	Duel.ConfirmDecktop(player, count)
	local g = Duel.GetDecktopGroup(player, count)
	if g:GetCount() > 0 then
		Duel.Hint(HINT_SELECTMSG, player, hint)
		local sg = g:FilterSelect(player, filter, min, max, nil)
		g:Sub(sg)
		return sg, g
	else
		return g, g
	end
end
-- 操作: 翻开卡组并可以选择卡
function RushDuel.RevealDeckTopAndCanSelect(player, count, desc, hint, filter, min, max, ...)
	Duel.ConfirmDecktop(player, count)
	local g = Duel.GetDecktopGroup(player, count)
	if g:GetCount() > 0 then
		if g:IsExists(filter, min, nil, ...) and Duel.SelectYesNo(player, desc) then
			Duel.Hint(HINT_SELECTMSG, player, hint)
			local sg = g:FilterSelect(player, filter, min, max, nil, ...)
			g:Sub(sg)
			return sg, g
		else
			return Group.CreateGroup(), g
		end
	else
		return g, g
	end
end
-- 可选操作: 抽卡
function RushDuel.CanDraw(desc, player, count, break_effect)
	if Duel.IsPlayerCanDraw(player, count) and Duel.SelectYesNo(player, desc) then
		if break_effect then
			Duel.BreakEffect()
		end
		return Duel.Draw(player, count, REASON_EFFECT)
	end
	return 0
end
-- 可选操作: 盲堆
function RushDuel.CanDiscardDeck(desc, player, count, break_effect)
	if Duel.IsPlayerCanDiscardDeck(player, count) and Duel.SelectYesNo(player, desc) then
		if break_effect then
			Duel.BreakEffect()
		end
		return Duel.DiscardDeck(player, count, REASON_EFFECT)
	end
	return 0
end
-- 赋予: 改变卡名
function RushDuel.AttachCardCode(e, card, code, reset, forced)
	return RushDuel.CreateSingleEffect(e, nil, card, EFFECT_CHANGE_CODE, code, reset, forced)
end
-- 赋予: 攻守升降
function RushDuel.AttachAtkDef(e, card, atk, def, reset, forced)
	if atk ~= nil and atk ~= 0 then
		RushDuel.CreateSingleEffect(e, nil, card, EFFECT_UPDATE_ATTACK, atk, reset, forced)
	end
	if def ~= nil and def ~= 0 and RushDuel.IsCanChangeDef(card) then
		RushDuel.CreateSingleEffect(e, nil, card, EFFECT_UPDATE_DEFENSE, def, reset, forced)
	end
end
-- 赋予: 等级升降
function RushDuel.AttachLevel(e, card, level, reset, forced)
	return RushDuel.CreateSingleEffect(e, nil, card, EFFECT_UPDATE_LEVEL, level, reset, forced)
end
-- 赋予: 直接攻击
function RushDuel.AttachDirectAttack(e, card, desc, reset, forced)
	return RushDuel.CreateSingleEffect(e, desc, card, EFFECT_DIRECT_ATTACK, nil, reset, forced)
end
-- 赋予: 贯通
function RushDuel.AttachPierce(e, card, desc, reset, forced)
	return RushDuel.CreateSingleEffect(e, desc, card, EFFECT_PIERCE, nil, reset, forced)
end
-- 赋予: 多次攻击
function RushDuel.AttachExtraAttack(e, card, value, desc, reset, forced)
	return RushDuel.CreateSingleEffect(e, desc, card, EFFECT_EXTRA_ATTACK, value, reset, forced)
end
-- 赋予: 多次攻击 (怪兽限定)
function RushDuel.AttachExtraAttackMonster(e, card, value, desc, reset, forced)
	return RushDuel.CreateSingleEffect(e, desc, card, EFFECT_EXTRA_ATTACK_MONSTER, value, reset, forced)
end
-- 赋予: 全体攻击
function RushDuel.AttachAttackAll(e, card, value, desc, reset, forced)
	return RushDuel.CreateSingleEffect(e, desc, card, EFFECT_ATTACK_ALL, value, reset, forced)
end
-- 赋予: 双重解放
function RushDuel.AttachDoubleTribute(e, card, value, desc, reset, forced)
	return RushDuel.CreateSingleEffect(e, desc, card, EFFECT_DOUBLE_TRIBUTE, value, reset, forced)
end
-- 赋予: 使用对方的怪兽解放
function RushDuel.AttachOpponentTribute(e, card, flag, desc, reset, reset_player)
	local tp = e:GetHandlerPlayer()
	if Duel.IsPlayerAffectedByEffect(tp, EFFECT_CANNOT_SINGLE_TRIBUTE) and Duel.IsPlayerAffectedByEffect(tp, EFFECT_CANNOT_DOUBLE_TRIBUTE) then
		return nil
	else
		card:RegisterFlagEffect(flag, reset, EFFECT_FLAG_CLIENT_HINT, 1, 0, desc)
		local e1 = Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_ADD_EXTRA_TRIBUTE)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE)
		e1:SetTargetRange(0, LOCATION_MZONE)
		e1:SetLabelObject(card)
		e1:SetTarget(function(e, c)
			return c == e:GetLabelObject() and card:GetFlagEffect(flag) ~= 0
		end)
		e1:SetValue(POS_FACEUP_ATTACK + POS_FACEDOWN_DEFENSE)
		local e2 = Effect.CreateEffect(e:GetHandler())
		e2:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_GRANT)
		e2:SetTarget(aux.TargetBoolFunction(Card.IsType, TYPE_MONSTER))
		e2:SetTargetRange(LOCATION_HAND, 0)
		e2:SetLabelObject(e1)
		e2:SetReset(reset_player or reset)
		Duel.RegisterEffect(e2, e:GetHandlerPlayer())
		return e1, e2
	end
end
-- 赋予: 效果战斗抗性
function RushDuel.AttachBattleIndes(e, card, value, desc, reset, forced)
	return RushDuel.CreateSingleEffect(e, desc, card, EFFECT_INDESTRUCTABLE_BATTLE, value, reset, forced)
end
-- 赋予: 效果破坏抗性
function RushDuel.AttachEffectIndes(e, card, value, desc, reset, forced)
	local attach = RushDuel.CreateSingleEffect(e, desc, card, EFFECT_INDESTRUCTABLE_EFFECT, value, reset, forced)
	attach:SetLabel(e:GetHandlerPlayer() + 1)
	return attach
end
-- 赋予: 战斗, 效果破坏抗性 (有次数限制)
function RushDuel.AttachIndesCount(e, card, count, value, desc, reset, forced)
	local e1 = RushDuel.CreateSingleEffect(e, desc, card, EFFECT_INDESTRUCTABLE_COUNT, value, reset, forced)
	e1:SetCountLimit(count)
	return e1
end
-- 赋予: 攻击宣言时特效
function RushDuel.AttachAttackAnnounce(e, card, operation, desc, reset, forced)
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_ATTACK_ANNOUNCE)
	e1:SetOperation(operation)
	if desc ~= nil then
		e1:SetDescription(desc)
		e1:SetProperty(EFFECT_FLAG_CLIENT_HINT)
	end
	e1:SetReset(reset)
	card:RegisterEffect(e1, forced)
	return e1
end
-- 赋予: 回合结束时特效
function RushDuel.AttachEndPhase(e, card, player, code, operation, desc)
	card:RegisterFlagEffect(0, RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END, EFFECT_FLAG_CLIENT_HINT, 1, code, desc)
	local e1 = Effect.CreateEffect(e:GetHandler())
	e1:SetType(EFFECT_TYPE_FIELD + EFFECT_TYPE_CONTINUOUS)
	e1:SetCode(EVENT_PHASE + PHASE_END)
	e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
	e1:SetCountLimit(1)
	e1:SetLabelObject(card)
	e1:SetCondition(function(e, tp, eg, ep, ev, re, r, rp)
		local tc = e:GetLabelObject()
		local fids = {tc:GetFlagEffectLabel(0)}
		for i = 1, #fids do
			if fids[i] == code then
				return true
			end
		end
		e:Reset()
		return false
	end)
	e1:SetOperation(operation)
	e1:SetReset(RESET_PHASE + PHASE_END)
	Duel.RegisterEffect(e1, player)
	return e1
end
-- 赋予: 不能攻击
function RushDuel.AttachCannotAttack(e, card, desc, reset, forced)
	return RushDuel.CreateSingleEffect(e, desc, card, EFFECT_CANNOT_ATTACK, nil, reset, forced)
end
-- 赋予: 不能直接攻击
function RushDuel.AttachCannotDirectAttack(e, card, desc, reset, forced)
	return RushDuel.CreateSingleEffect(e, desc, card, EFFECT_CANNOT_DIRECT_ATTACK, nil, reset, forced)
end
-- 赋予: 不能选择攻击目标
function RushDuel.AttachCannotSelectBattleTarget(e, card, value, desc, reset, forced)
	return RushDuel.CreateSingleEffect(e, desc, card, EFFECT_CANNOT_SELECT_BATTLE_TARGET, value, reset, forced)
end
-- 赋予: 不能用于上级召唤而解放
function RushDuel.AttachCannotTribute(e, card, value, desc, reset, forced)
	return RushDuel.CreateSingleEffect(e, desc, card, EFFECT_UNRELEASABLE_SUM, value, reset, forced)
end
-- 赋予: 变更原本攻守
function RushDuel.SetBaseAtkDef(e, card, atk, def, reset, forced)
	if atk ~= nil then
		RushDuel.CreateSingleEffect(e, nil, card, EFFECT_SET_BASE_ATTACK, atk, reset, forced)
	end
	if def ~= nil and RushDuel.IsCanChangeDef(card) then
		RushDuel.CreateSingleEffect(e, nil, card, EFFECT_SET_BASE_DEFENSE, def, reset, forced)
	end
end
-- 赋予: 交换原本攻守
function RushDuel.SwapBaseAtkDef(e, card, reset, forced)
	if RushDuel.IsCanChangeDef(card) then
		RushDuel.CreateSingleEffect(e, nil, card, EFFECT_SWAP_BASE_AD, nil, reset, forced)
	end
end
-- 赋予: 交换当前攻守
function RushDuel.SwapAtkDef(e, card, reset, forced)
	if RushDuel.IsCanChangeDef(card) then
		local atk = card:GetAttack()
		local def = card:GetDefense()
		RushDuel.CreateSingleEffect(e, nil, card, EFFECT_SET_ATTACK_FINAL, def, reset, forced)
		RushDuel.CreateSingleEffect(e, nil, card, EFFECT_SET_DEFENSE_FINAL, atk, reset, forced)
	end
end
-- 赋予: 改变属性
function RushDuel.ChangeAttribute(e, card, attribute, reset, forced)
	return RushDuel.CreateSingleEffect(e, nil, card, EFFECT_CHANGE_ATTRIBUTE, attribute, reset, forced)
end
-- 赋予: 改变种族
function RushDuel.ChangeRace(e, card, race, reset, forced)
	return RushDuel.CreateSingleEffect(e, nil, card, EFFECT_CHANGE_RACE, race, reset, forced)
end
-- 赋予: 改变卡名
function RushDuel.ChangeCode(e, card, code, reset, forced)
	-- 使用 LinkCode 来判断传说卡
	RushDuel.CreateSingleEffect(e, nil, card, EFFECT_ADD_LINK_CODE, code, reset, forced)
	RushDuel.CreateSingleEffect(e, nil, card, EFFECT_ADD_FUSION_CODE, code, reset, forced)
	return RushDuel.CreateSingleEffect(e, nil, card, EFFECT_CHANGE_CODE, code, reset, forced)
end
-- 效果值: 双重解放 属性/种族
function RushDuel.ValueDoubleTribute(attribute, race, ignore)
	return function(e, c)
		if e == nil then
			return ignore or false, attribute or 0, race or 0
		end
		return (attribute == nil or c:IsAttribute(attribute)) and (race == nil or c:IsRace(race))
	end
end
-- 效果值: 效果破坏抗性 抵抗类型
function RushDuel.ValueEffectIndesType(self_type, opponent_type, ignore)
	local s_type = self_type or 0
	local o_type = opponent_type or 0
	return function(e, re, rp)
		if e == nil then
			return ignore or false, s_type or 0, o_type or 0
		end
		local tp = e:GetHandlerPlayer()
		if e:GetLabel() ~= 0 then
			tp = e:GetLabel() - 1
		end
		if rp == tp then
			return s_type ~= 0 and re:IsActiveType(s_type)
			else
			return o_type ~= 0 and re:IsActiveType(o_type)
		end
	end
end
-- 注册效果: 装备魔法的装备效果
function RushDuel.RegisterEquipEffect(card, condition, cost, target)
	-- Activate
	local e1 = Effect.CreateEffect(card)
	e1:SetCategory(CATEGORY_EQUIP)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	if condition ~= nil then
		e1:SetCondition(condition)
	end
	if cost ~= nil then
		e1:SetCost(cost)
	end
	e1:SetTarget(RushDuel.EquipTarget(target))
	e1:SetOperation(RushDuel.EquipOperation(target))
	card:RegisterEffect(e1)
	-- Equip Limit
	local e2 = Effect.CreateEffect(card)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_EQUIP_LIMIT)
	e2:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e2:SetValue(RushDuel.EquipFilter(target))
	card:RegisterEffect(e2)
	return e1
end
function RushDuel.EquipTarget(target)
	return function(e, tp, eg, ep, ev, re, r, rp, chk)
		if chk == 0 then
			return Duel.IsExistingMatchingCard(target, tp, LOCATION_MZONE, LOCATION_MZONE, 1, nil, e, tp, true)
		end
		Duel.SetOperationInfo(0, CATEGORY_EQUIP, e:GetHandler(), 1, 0, 0)
	end
end
function RushDuel.EquipOperation(target)
	return function(e, tp, eg, ep, ev, re, r, rp)
		local c = e:GetHandler()
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_EQUIP)
		local g = Duel.SelectMatchingCard(tp, target, tp, LOCATION_MZONE, LOCATION_MZONE, 1, 1, nil, e, tp, true)
		local tc = g:GetFirst()
		if tc and c:IsRelateToEffect(e) then
			Duel.HintSelection(g)
			Duel.Equip(tp, c, tc)
		end
	end
end
function RushDuel.EquipFilter(target)
	return function(e, c)
		return e:GetHandler():GetEquipTarget() == c or target(c, e, e:GetHandlerPlayer(), false)
	end
end
LEGEND_MONSTER = 120000000
LEGEND_SPELL = 120000001
LEGEND_TRAP = 120000002
RushDuel.LegendCodes = {
	-- 青眼白龙
	{120120000, 120198001, 120231001},
	-- 真红眼黑龙
	{120125001, 120203016, 120229101},
	-- 黑魔术师
	{120130000, 120203015},
	-- 死者苏生
	{120194004, 120195004},
	-- 天使的施舍
	{120196049, 120195005},
	-- 海龙-泰达路斯
	{120199000, 120239060}
}
-- 初始化传说卡
function RushDuel.InitLegend()
	local g = Duel.GetMatchingGroup(Card.IsCode, 0, 0xff, 0xff, nil, LEGEND_MONSTER, LEGEND_SPELL, LEGEND_TRAP)
	g:ForEach(function(c)
		local code = RushDuel.GetLegendCode(c:GetOriginalCode())
		local e1 = Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_LEGEND_CARD)
		e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE + EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_SET_AVAILABLE)
		e1:SetRange(0xff)
		e1:SetValue(code)
		c:RegisterEffect(e1, true)
		-- 修改卡牌数据 (删除同名卡：传说卡)
		c:SetEntityCode(code, true)
	end)
end
-- 获取传说卡原卡名
function RushDuel.GetLegendCode(code)
	for _, codes in ipairs(RushDuel.LegendCodes) do
		for _, legend_code in ipairs(codes) do
			if (code == legend_code) then
				return codes[1]
			end
		end
	end
	return code
end
-- 条件: 是否为传说卡
function RushDuel.IsLegendCard(card)
	return card:IsHasEffect(EFFECT_LEGEND_CARD)
end
-- 条件: 是否为同名卡
function RushDuel.IsSameCode(card1, card2)
	return card1:IsLinkCode(card2:GetLinkCode())
end
-- 永续改变卡名
function RushDuel.EnableChangeCode(c, code, location, condition)
	Auxiliary.AddCodeList(c, code)
	local loc = c:GetOriginalType() & TYPE_MONSTER ~= 0 and LOCATION_MZONE or LOCATION_SZONE
	local e1 = Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_CHANGE_CODE)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(location or loc)
	if condition ~= nil then
		e1:SetCondition(condition)
	end
	e1:SetValue(code)
	c:RegisterEffect(e1)
	local e2 = e1:Clone()
	e2:SetCode(EFFECT_ADD_LINK_CODE)
	c:RegisterEffect(e2)
	local e3 = e1:Clone()
	e3:SetCode(EFFECT_ADD_FUSION_CODE)
	c:RegisterEffect(e3)
	return e1, e2, e3
end
-- 条件: 是否为同名卡
function RushDuel.IsSameCode(card1, card2)
	return RushDuel.GetCardCode(card1) == RushDuel.GetCardCode(card2)
-- 条件: 是否卡名不同
end
function RushDuel.IsDifferentCode(card1, card2)
	return RushDuel.GetCardCode(card1) ~= RushDuel.GetCardCode(card2)
end
LEGEND_MONSTER = 120000000
LEGEND_SPELL = 120000001
LEGEND_TRAP = 120000002
LEGEND_DARK_MAGICIAN = {120130000, 120203015}
LEGEND_BLUE_EYES_WHITE_DRAGON = {120120000, 120198001, 120230001}
LEGEND_RED_EYES_BLACK_DRAGON = {120125001, 120203016, 120229101}
-- 添加记述卡牌列表
function RushDuel.AddCodeList(card, ...)
	for _, list in ipairs {...} do
		local type = aux.GetValueType(list)
		if type == "number" then
			aux.AddCodeList(card, list)
		elseif type == "table" then
			aux.AddCodeList(card, table.unpack(list))
		end
	end
end
-- 条件: 当前卡名是否为传说卡
function RushDuel.IsLegend(card)
	return card:IsCode(LEGEND_MONSTER, LEGEND_SPELL, LEGEND_TRAP)
end
-- 条件: 是否为传说卡
function RushDuel.IsLegendCard(card)
	return card:IsHasEffect(EFFECT_LEGEND_CARD)
end
-- 条件: 是否为传说卡的卡名
function RushDuel.IsLegendCode(card, ...)
	local code = RushDuel.GetCardCode(card)
	return RushDuel.FlatCheck(function(item)
		return code == item
	end, ...)
end
-- 获取卡牌的密码
function RushDuel.GetCardCode(card)
	local code = card:GetCode()
	if RushDuel.IsLegendCard(card) then
		code = card:GetOriginalCode()
	end
	local codes = RushDuel.GetEffectValues(card, EFFECT_CHANGE_CODE)
	for _, val in ipairs(codes) do
		code = val
	end
	return code
end
-- 条件: 是否为同名卡
function RushDuel.IsSameCode(card1, card2)
	return RushDuel.GetCardCode(card1) == RushDuel.GetCardCode(card2)
end
-- 条件: 是否卡名不同
function RushDuel.IsDifferentCode(card1, card2)
	return RushDuel.GetCardCode(card1) ~= RushDuel.GetCardCode(card2)
end
-- 添加 融合术/结合 素材
function RushDuel.AddFusionProcedure(card, ...)
	Auxiliary.AddFusionProcMix(card, true, true, table.unpack({...}))
end
-- 创建效果: 融合术/结合 召唤
function RushDuel.CreateFusionEffect(card, matfilter, spfilter, exfilter, s_range, o_range, mat_check, mat_move, target_action, operation_action)
	local self_range = s_range or 0
	local opponent_range = o_range or 0
	local move = mat_move or RushDuel.FusionToGrave
	local e = Effect.CreateEffect(card)
	e:SetTarget(RushDuel.FusionTarget(matfilter, spfilter, exfilter, self_range, opponent_range, mat_check, target_action))
	e:SetOperation(RushDuel.FusionOperation(matfilter, spfilter, exfilter, self_range, opponent_range, mat_check, move, operation_action))
	return e
end
function RushDuel.FusionMaterialFilter(c, filter, e)
	return (not filter or filter(c)) and (not e or not c:IsImmuneToEffect(e))
end
function RushDuel.FusionSpecialSummonFilter(c, e, tp, m, f, chkf, filter)
	return c:IsType(TYPE_FUSION) and (not filter or filter(c, e, tp, m, f, chkf)) and (not f or f(c)) and c:IsCanBeSpecialSummoned(e, SUMMON_TYPE_FUSION, tp, false, false) and c:CheckFusionMaterial(m, nil, chkf)
end
function RushDuel.ConfirmCardFilter(c)
	return c:IsLocation(LOCATION_HAND) or (c:IsLocation(LOCATION_MZONE) and c:IsFacedown())
end
function RushDuel.FusionTarget(matfilter, spfilter, exfilter, s_range, o_range, mat_check, action)
	return function(e, tp, eg, ep, ev, re, r, rp, chk)
		if chk == 0 then
			local chkf = tp
			local mg1 = Duel.GetFusionMaterial(tp):Filter(RushDuel.FusionMaterialFilter, nil, matfilter)
			if s_range ~= 0 or o_range ~= 0 then
				local mg2 = Duel.GetMatchingGroup(exfilter, tp, s_range, o_range, nil)
				mg1:Merge(mg2)
			end
			aux.FGoalCheckAdditional = mat_check
			local res = Duel.IsExistingMatchingCard(RushDuel.FusionSpecialSummonFilter, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, mg1, nil, chkf, spfilter)
			if not res then
				local ce = Duel.GetChainMaterial(tp)
				if ce ~= nil then
					local fgroup = ce:GetTarget()
					local mg3 = fgroup(ce, e, tp)
					local mf = ce:GetValue()
					res = Duel.IsExistingMatchingCard(RushDuel.FusionSpecialSummonFilter, tp, LOCATION_EXTRA, 0, 1, nil, e, tp, mg3, mf, chkf, spfilter)
				end
			end
			aux.FGoalCheckAdditional = nil
			return res
		end
		if action ~= nil then
			action(e, tp, eg, ep, ev, re, r, rp)
		end
		Duel.SetOperationInfo(0, CATEGORY_SPECIAL_SUMMON, nil, 1, tp, LOCATION_EXTRA)
	end
end
function RushDuel.FusionOperation(matfilter, spfilter, exfilter, s_range, o_range, mat_check, mat_move, action)
	return function(e, tp, eg, ep, ev, re, r, rp)
		local chkf = tp
		local mg1 = Duel.GetFusionMaterial(tp):Filter(RushDuel.FusionMaterialFilter, nil, matfilter, e)
		if s_range ~= 0 or o_range ~= 0 then
			local mg2 = Duel.GetMatchingGroup(aux.NecroValleyFilter(exfilter), tp, s_range, o_range, nil, e)
			mg1:Merge(mg2)
		end
		aux.FGoalCheckAdditional = mat_check
		local sg1 = Duel.GetMatchingGroup(RushDuel.FusionSpecialSummonFilter, tp, LOCATION_EXTRA, 0, nil, e, tp, mg1, nil, chkf, spfilter)
		local mg3 = nil
		local sg2 = nil
		local ce = Duel.GetChainMaterial(tp)
		if ce ~= nil then
			local fgroup = ce:GetTarget()
			mg3 = fgroup(ce, e, tp)
			local mf = ce:GetValue()
			sg2 = Duel.GetMatchingGroup(RushDuel.FusionSpecialSummonFilter, tp, LOCATION_EXTRA, 0, nil, e, tp, mg3, mf, chkf, spfilter)
		end
		local mat = nil
		local fc = nil
		if sg1:GetCount() > 0 or (sg2 ~= nil and sg2:GetCount() > 0) then
			local sg = sg1:Clone()
			if sg2 then
				sg:Merge(sg2)
			end
			::cancel::
			Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
			local tg = sg:Select(tp, 1, 1, nil)
			fc = tg:GetFirst()
			if sg1:IsContains(fc) and (sg2 == nil or not sg2:IsContains(fc) or not Duel.SelectYesNo(tp, ce:GetDescription())) then
				mat = Duel.SelectFusionMaterial(tp, fc, mg1, nil, chkf)
				if #mat<2 then goto cancel end
				fc:SetMaterial(mat)
				local cg = mat:Filter(RushDuel.ConfirmCardFilter, nil)
				if cg:GetCount() > 0 then
					Duel.ConfirmCards(1 - tp, cg)
				end
				mat_move(mat)
				Duel.BreakEffect()
				Duel.SpecialSummon(fc, SUMMON_TYPE_FUSION, tp, tp, false, false, POS_FACEUP)
			else
				mat = Duel.SelectFusionMaterial(tp, fc, mg3, nil, chkf)
				if #mat<2 then goto cancel end
				local fop = ce:GetOperation()
				fop(ce, e, tp, fc, mat)
			end
			fc:CompleteProcedure()
		end
		if action ~= nil then
			action(e, tp, eg, ep, ev, re, r, rp, mat, fc)
		end
		aux.FGoalCheckAdditional = nil
	end
end
-- 素材去向: 墓地
function RushDuel.FusionToGrave(mat)
	Duel.SendtoGrave(mat, REASON_EFFECT + REASON_MATERIAL + REASON_FUSION)
end
-- 素材去向: 卡组
function RushDuel.FusionToDeck(mat)
	Duel.SendtoDeck(mat, nil, SEQ_DECKSHUFFLE, REASON_EFFECT + REASON_MATERIAL + REASON_FUSION)
end
-- 可以进行融合术召唤
function RushDuel.CanFusionSummon(desc, matfilter, spfilter, exfilter, s_range, o_range, mat_check, mat_move, e, tp, break_effect)
	local chkf = tp
	local mg1 = Duel.GetFusionMaterial(tp):Filter(RushDuel.FusionMaterialFilter, nil, matfilter, e)
	if s_range ~= 0 or o_range ~= 0 then
		local mg2 = Duel.GetMatchingGroup(aux.NecroValleyFilter(exfilter), tp, s_range, o_range, nil, e)
		mg1:Merge(mg2)
	end
	aux.FGoalCheckAdditional = mat_check
	local sg1 = Duel.GetMatchingGroup(RushDuel.FusionSpecialSummonFilter, tp, LOCATION_EXTRA, 0, nil, e, tp, mg1, nil, chkf, spfilter)
	local mg3 = nil
	local sg2 = nil
	local ce = Duel.GetChainMaterial(tp)
	if ce ~= nil then
		local fgroup = ce:GetTarget()
		mg3 = fgroup(ce, e, tp)
		local mf = ce:GetValue()
		sg2 = Duel.GetMatchingGroup(RushDuel.FusionSpecialSummonFilter, tp, LOCATION_EXTRA, 0, nil, e, tp, mg3, mf, chkf, spfilter)
	end
	local mat = nil
	local fc = nil
	if (sg1:GetCount() > 0 or (sg2 ~= nil and sg2:GetCount() > 0)) and Duel.SelectYesNo(tp, desc) then
		if break_effect then
			Duel.BreakEffect()
		end
		local sg = sg1:Clone()
		if sg2 then
			sg:Merge(sg2)
		end
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_SPSUMMON)
		local tg = sg:Select(tp, 1, 1, nil)
		fc = tg:GetFirst()
		if sg1:IsContains(fc) and (sg2 == nil or not sg2:IsContains(fc) or not Duel.SelectYesNo(tp, ce:GetDescription())) then
			mat = Duel.SelectFusionMaterial(tp, fc, mg1, nil, chkf)
			fc:SetMaterial(mat)
			local cg = mat:Filter(RushDuel.ConfirmCardFilter, nil)
			if cg:GetCount() > 0 then
				Duel.ConfirmCards(1 - tp, cg)
			end
			mat_move(mat)
			Duel.BreakEffect()
			Duel.SpecialSummon(fc, SUMMON_TYPE_FUSION, tp, tp, false, false, POS_FACEUP)
		else
			mat = Duel.SelectFusionMaterial(tp, fc, mg3, nil, chkf)
			local fop = ce:GetOperation()
			fop(ce, e, tp, fc, mat)
		end
		fc:CompleteProcedure()
	end
	aux.FGoalCheckAdditional = nil
	return fc
end
-- 内部方法: 双重解放的对象怪兽信息
function RushDuel._private_double_tribute_info(code, attribute, level, race, attack, defense)
	local info = {}
	info.code = code
	info.attribute = attribute
	info.level = level
	info.race = race
	info.attack = attack
	info.defense = defense
	return info
end
-- 内部方法: 判断双重解放的对象怪兽是否重合
function RushDuel._private_is_double_tribute_info_different(info1, info2)
	if info1.code ~= info2.code then
		return true
	elseif info1.attribute ~= info2.attribute then
		return true
	elseif info1.level ~= info2.level then
		return true
	elseif info1.race ~= info2.race then
		return true
	elseif info1.attack ~= info2.attack then
		return true
	elseif info1.defense ~= info2.defense then
		return true
	end
	return false
end
-- 效果值: 双重解放
function RushDuel.ValueDoubleTributeMix(ignore, code, attribute, level, race, attack, defense)
	return function(e, c)
		if e == nil then
			return ignore, RushDuel._private_double_tribute_info(code, attribute, level, race, attack, defense)
		end
		return (code == nil or c:IsCode(code)) and (attribute == nil or c:IsAttribute(attribute)) and (level == nil or c:IsLevel(level)) and (race == nil or c:IsRace(race)) and
				   (attack == nil or c:IsAttack(attack)) and (defense == nil or c:IsDefense(defense))
	end
end
-- 效果值: 双重解放 全范围
function RushDuel.ValueDoubleTributeAll(ignore)
	return function(e, c)
		if e == nil then
			return ignore, 1
		end
		return true
	end
end
-- 效果值: 双重解放 卡名
function RushDuel.ValueDoubleTributeCode(code, ignore)
	return RushDuel.ValueDoubleTributeMix(ignore, code, nil, nil, nil, nil, nil)
end
-- 效果值: 双重解放 属性/种族
function RushDuel.ValueDoubleTributeAttrRace(attribute, race, ignore)
	return RushDuel.ValueDoubleTributeMix(ignore, nil, attribute, nil, race, nil, nil)
end
-- 效果值: 双重解放 等级/属性/种族
function RushDuel.ValueDoubleTributeLvAttrRace(level, attribute, race, ignore)
	return RushDuel.ValueDoubleTributeMix(ignore, nil, attribute, level, race, nil, nil)
end
-- 效果值: 双重解放 攻击力/守备力
function RushDuel.ValueDoubleTributeAtkDef(attack, defense, ignore)
	return RushDuel.ValueDoubleTributeMix(ignore, nil, nil, nil, nil, attack, defense)
end
-- 判断： 是否可以赋予双重解放
function RushDuel.CheckValueDoubleTribute(values, value)
	local _, info = value(nil)
	for _, val in ipairs(values) do
		if val == 1 then
			-- 全范围双重解放, 无法再赋予双重解放
			return false
		else
			-- 已有抗性全部叠加
			local ignore, attach_info = val(nil)
			if not ignore then
				if attach_info == 1 then
					-- 全范围双重解放, 无法再赋予双重解放
					return false
				elseif info == 1 then
					-- 全范围双重解放, 无需判断
				elseif not RushDuel._private_is_double_tribute_info_different(info, attach_info) then
					-- 已存在相同的双重解放效果
					return false
				end
			end
		end
	end
	return true
end
-- 效果值: 效果破坏抗性 抵抗类型
function RushDuel.ValueEffectIndesType(self_type, opponent_type, ignore)
	local s_type = self_type or 0
	local o_type = opponent_type or 0
	return function(e, re, rp)
		if e == nil then
			return ignore or false, s_type, o_type
		end
		local tp = e:GetHandlerPlayer()
		if e:GetLabel() ~= 0 then
			tp = e:GetLabel() - 1
		end
		if rp == tp then
			return s_type ~= 0 and re:IsActiveType(s_type)
		else
			return o_type ~= 0 and re:IsActiveType(o_type)
		end
	end
end
-- 判断： 是否可以赋予效果破坏抗性
function RushDuel.CheckValueEffectIndesType(swap, values, value)
	local attachs_s, attachs_o = 0, 0
	for _, val in ipairs(values) do
		if val == 1 then
			-- 全破坏抗性, 无法再赋予其他抗性
			return false
		else
			-- 已有抗性全部叠加
			local ignore, s_type, o_type = val(nil)
			if not ignore then
				attachs_s = attachs_s | s_type
				attachs_o = attachs_o | o_type
			end
		end
	end
	-- 判断抗性是否有变化
	local _, s_type, o_type = value(nil)
	if swap then
		-- 控制权交换中
		s_type, o_type = o_type, s_type
	end
	return (attachs_s | s_type) ~= attachs_s or (attachs_o | o_type) ~= attachs_o
end
function RushDuel.Init()
	-- 初始化
	RushDuel.InitRule()
	Duel.BreakEffect = function()
		-- "那之后" 不打断时点
	end
	-- 决斗开始
	RushDuel.CreateFieldGlobalEffect(true, EVENT_PHASE_START + PHASE_DRAW, function(e)
		-- 先攻抽卡
		Duel.Draw(Duel.GetTurnPlayer(), 1, REASON_RULE)
		-- 传说卡
		RushDuel.InitLegend()
		e:Reset()
	end)
end
